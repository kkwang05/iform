package com.allen_sauer.gwt.dnd.client;

/**
 * Contain widgets implementing this behavior will not have their Draggable restored to their original location when 
 * there is no place to drop. 
 *
 * @author kw
 */
public interface Palette {
}


<html>
    <body>
       <p>Dear $userName$,</p>
       
       <p>
        Thank you for your interest in using iForm! You are one click away from creating your first form. Please <a href="$activationUrl$">use this secure URL</a> to activate your account within $period$ days. 
       </p>

	<p>
	If you cannot access the link above, you can paste the following address into your browser:
	$activationUrl$.
       </p>

       <p>
       - The iForm Team
       </p>
       
   </body>
</html>
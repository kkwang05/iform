<html>
    <body>
       <p>Dear $userName$,</p>
       
       <p>
       Your iForm password is: $password$. <br/>
       We have sent this information on your request; but for maximum security you are advised to change it at your next logon.
       </p>
       
       <p>
       Thank you for using iForm!
       </p>
       
       <p>
       - The iForm Team
       </p>
       
   </body>
</html>
package fx.presentment;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.*;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

import fx.model.FormEntry;
import fx.model.operation.*;
import fx.service.RPC;

/**
 * Access point for second-tier users.
 * 
 * @author kw
 */
public class FormConnApp implements EntryPoint {
  
  private static final String PAGE_HOOK_ENTRY="entry";
  
  interface GlobalResources extends ClientBundle {
    @NotStrict
    @Source("iForm.css")
    CssResource css();
  }

  public void onModuleLoad() {
    GWT.<GlobalResources>create(GlobalResources.class).css().ensureInjected();
    
    String url=Document.get().getURL();
    int queryIdx=url.lastIndexOf("?");
    String secureToken=url.substring(++queryIdx);
    queryIdx=url.lastIndexOf("&");
    if(queryIdx>0)
    	secureToken=url.substring(++queryIdx);
    	
    AuthorizeEntry req = new AuthorizeEntry(secureToken);
    RPC.Operation<AuthorizeEntryResponse> op = new RPC.Operation<AuthorizeEntryResponse>(req) {
      public void onSuccess(AuthorizeEntryResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null) {
          displayMessage(resp.errorMessage);
          return;
        }
        FormEntry newEntry=resp.formEntry;
        if(resp.authToken!=null)
          requestAuthToken(newEntry, resp.authToken);
        else
          presentFormEntry(newEntry);
      }
    };
    RPC.Instance.exec(op);
  }

  private void requestAuthToken(FormEntry entry, String authToken){
    displayMessage("User authorization required.");
    AuthTokenDialog dialog=new AuthTokenDialog(entry, authToken);
    dialog.center();
  }
  
  private void presentFormEntry(FormEntry entry){
    FormEntryEditor fee = new FormEntryEditor();
    ScrollPanel scroll=new ScrollPanel(fee);
    fee.addStyleName("box1");

    RootPanel.get(PAGE_HOOK_ENTRY).clear();
    RootPanel.get(PAGE_HOOK_ENTRY).add(scroll);

    fee.loadEntry(entry);
  }
  
  private void displayMessage(String msg){
    RootPanel.get(PAGE_HOOK_ENTRY).add(new HTML(msg));
  }
  
  //
  // A simple internal popup dialog to accept user input. 
  // It stays up until correct answer is provided.
  //
  private class AuthTokenDialog extends PopupPanel implements ValueChangeHandler<String>{
    FormEntry entry;
    String authToken;
    boolean isPhrase;
    TextBox input=new TextBox();
    
    AuthTokenDialog(FormEntry entry, String authToken){
      super(false);
      this.entry=entry;
      this.authToken=authToken;
      isPhrase=authToken.contains(" ");
      
      FlowPanel p=new FlowPanel();
      String prompt=isPhrase? "Enter passphrase: " : "Enter password: ";
      p.add(new Label(prompt));
      p.add(input);
      String inputWidth=isPhrase? "30px":"100px";
      input.setWidth(inputWidth);
      input.addValueChangeHandler(this);
      
      setWidget(p);
    }
    
    public void onValueChange(ValueChangeEvent<String> e){
      String input=e.getValue().trim();
      boolean correct;
      if(isPhrase)
        correct=authToken.equalsIgnoreCase(input);
      else
        correct=authToken.equals(input);
      
      if(correct){
      	presentFormEntry(entry);
      	hide();
      }
      else
        displayMessage("Wrong answer. Try again");
    }
  }
}
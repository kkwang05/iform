package fx.presentment.renderer;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.FormEntryEvent;
import fx.presentment.widget.Paginator;

/**
 * @author kw
 */
public class PageBrowser extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Paginator paginator=new Paginator(false);
    if(node.text!=null && !node.text.trim().isEmpty()){
      String[] components=node.text.split(",");
      paginator.changeTextAppearance(components);
    }
    applyStyles(node, paginator);
    applyBorder(node, paginator);
    applyMargin(node, paginator);
    applyPadding(node, paginator);
    applyColor(node, paginator, 1);
    applyWidth(node, paginator);
    applyHeight(node, paginator);
    applyHorzAlign(node, paginator);
    applyTextStyles(node, paginator);

    if(entryContext!=null){
      paginator.reloadForm(entryContext.entry.theForm, false);
      MetaForm currentPage=(MetaForm)entryContext.retrievePrivateData(this);
      if(currentPage!=null)
        paginator.focusOnPage(currentPage);
      
      paginator.addSelectionHandler(new SelectionHandler<MetaForm>(){
        public void onSelection(SelectionEvent<MetaForm> event){
          MetaForm page=event.getSelectedItem();
          if(!page.isPage())
            return;
          FormEntryEvent evt=new FormEntryEvent.Navigation(entryContext.entry, node, page);
          entryContext.putPrivateData(PageBrowser.this, page);
          FormEntryEvent.fire(evt);
        }
      });
    }
    else{
      paginator.reloadFromNode(node);
    }
    
    stampFormNodeToWidget(paginator, node);
    return paginator;
  }
}
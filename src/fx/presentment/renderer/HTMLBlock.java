package fx.presentment.renderer;

import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

public class HTMLBlock extends Renderer{
  public Widget render(MetaForm node, final FormEntrySessionContext entryContext){
    Widget t=node.typeCode==MetaForm.TC_HTML_BLOCK? new HTML(node.text) : new Label(node.text);    
    applyStyles(node, t);
    applyBorder(node, t);
    applyMargin(node,t);
    applyPadding(node,t);
    applyColor(node,t,1);
    applyWidth(node,t);
    applyHeight(node,t);
    applyHorzAlign(node,t);
    applyTextStyles(node,t);
    stampFormNodeToWidget(t, node);
    return t;
  }
}
package fx.presentment.renderer;

import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 * A delegating Renderer that utilizes another Renderer, one which is designated for the node type
 * being observed by this node.
 */
public class Observer extends DomainObject {
  
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    if(entryContext==null)
      return super.render(node, entryContext);

    FormData dynaNode=entryContext.nodeData(node);
    if(dynaNode.bo==null)
      return null;
   
    FormData observedNode=dynaNode.bo.attachedNode();
    Renderer suitableRenderer = Renderer.forNode(observedNode.node);
    return suitableRenderer.render(node, entryContext);
  }
}
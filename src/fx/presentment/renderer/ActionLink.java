package fx.presentment.renderer;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.FormEntryEvent;

public class ActionLink extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Anchor a=new Anchor(node.text);
    
    applyStyles(node, a);
    applyBorder(node, a);
    applyMargin(node, a);
    applyPadding(node, a);
    applyColor(node, a, 1);
    applyWidth(node, a);
    applyHeight(node, a);
    applyHorzAlign(node, a);
    applyTextStyles(node, a);
    
    if(entryContext!=null){
      a.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event) {
          FormEntryEvent evt=new FormEntryEvent.DataInput(entryContext.entry, node);
          FormEntryEvent.fire(evt);
        }
      });
    }
    stampFormNodeToWidget(a, node);
    return a;
  }
}
package fx.presentment.renderer;

import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

public class ImageBlock extends Renderer {
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    String uri=node.text.split("=")[0];
    final Image img=new Image();
    img.setUrl(uri);

    if(img.getHeight()>0 || img.getWidth()>0){
      inflateImage(node, img);
    }
    else {
      img.addLoadHandler(new com.google.gwt.event.dom.client.LoadHandler(){
        public void onLoad(LoadEvent event){
          inflateImage(node, img);
        }
      });
    }
    
    applyStyles(node, img);
    applyBorder(node, img);
    applyMargin(node, img);
    applyPadding(node, img);
    applyWidth(node, img);
    applyHeight(node, img);
    applyHorzAlign(node, img);
    stampFormNodeToWidget(img, node);
    return img;
  }
  private transient boolean adjustedHeight=false;
  protected void applyWidth(MetaForm node, Widget img){
    String width=getHint(node.renderingHints, NODE_ATTRIBUTE_WIDTH);
    if(width!=null && !width.isEmpty()){
      DOM.setStyleAttribute(img.getElement(), "width", width);
      int imgW=((Image)img).getWidth();
      int imgH=((Image)img).getHeight();
      if(imgW>0 && imgH >0){
        float w=Float.parseFloat(width.substring(0,width.length()-2));
        float h=imgH*w/imgW;
        String height=h+width.substring(width.length()-2);
        DOM.setStyleAttribute(img.getElement(), "height", height);
        adjustedHeight=true;
      }
    }
    else{
      img.getElement().getStyle().clearWidth();
      img.getElement().getStyle().clearHeight();
    }
  }
  protected void applyHeight(MetaForm node, Widget img){
    String height=getHint(node.renderingHints, NODE_ATTRIBUTE_HEIGHT);
    if(height!=null && !height.isEmpty())
      DOM.setStyleAttribute(img.getElement(), "height", height);
    else if(!adjustedHeight)
      img.getElement().getStyle().clearHeight();
  }
  
  private void inflateImage(MetaForm node, Image img){
    String scaleString=getHint(node.renderingHints,"scale");
    float scale=1.0f;
    if(scaleString!=null)
      scale=Integer.parseInt(scaleString)/100f;
    int h=(int)(img.getHeight()*scale);
    int w=(int)(img.getWidth()*scale);
    img.setPixelSize(w, h);
  }
  
  class PrefetchCallback implements RequestCallback {
    MetaForm node;
    Image img;
    PrefetchCallback(MetaForm node, Image img){
      this.node=node;
      this.img=img;
    }
    public void onResponseReceived(Request request, Response response){
      inflateImage(node, img);
    }
    public void onError(Request request, Throwable exception){
      System.err.println("Image Prefetching request failed: "+exception.getMessage());
    }
  }
}
package fx.presentment.renderer;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.FillStrokeStyle;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.*;

public class Html5Canvas extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
	Canvas can=Canvas.createIfSupported();
	if(can==null)
		return new Label("Canvas not Supported");
	
	applyStyles(node, can);
    applyBorder(node, can);
    applyMargin(node, can);
    applyPadding(node, can);
    applyColor(node, can, 1);
    applyWidth(node, can);
    applyHeight(node, can);
    applyHorzAlign(node, can);
    applyTextStyles(node, can);

	Context2d g2d=can.getContext2d();
	g2d.fillText("Canvas", 10, 10);

    stampFormNodeToWidget(can, node);
    return can;
  }
  
  protected void applyWidth(MetaForm node, Widget renderedWidget){
	    String width=getHint(node.renderingHints, NODE_ATTRIBUTE_WIDTH);
	    if(width!=null && !width.isEmpty()){
	    	((Canvas)renderedWidget).setWidth(width);
	    	int w=(int)Float.parseFloat(width.substring(0, width.length()-2));
	    	((Canvas)renderedWidget).setCoordinateSpaceWidth(w);
	    }
  }
  
  protected void applyHeight(MetaForm node, Widget renderedWidget){
	    String height=getHint(node.renderingHints, NODE_ATTRIBUTE_HEIGHT);
	    if(height!=null && !height.isEmpty()){
	    	((Canvas)renderedWidget).setHeight(height);
	    	int h=(int)Float.parseFloat(height.substring(0, height.length()-2));
	    	((Canvas)renderedWidget).setCoordinateSpaceHeight(h);
	    }
  }
}
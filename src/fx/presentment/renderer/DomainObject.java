package fx.presentment.renderer;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.resources.client.ImageResource;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 * Base class for all DomainObject renderers.
 */
public class DomainObject extends Renderer{
  
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Panel panelContainer=new FlowPanel();
    
    String iconName=null;
    String label=null;
    if(entryContext==null){
      iconName=getHint(node.renderingHints, "icon");
      label=node.text;
    }
    else {
      FormData dynaNode=entryContext.nodeData(node);
      iconName=getHint(dynaNode.renderingHints, "icon");
      label=dynaNode.text;
    }
    if(iconName!=null){
      ImageResource ir=(ImageResource)NodeImages.Instance.getResource(iconName);
      Image img=new Image(ir);
      DOM.setStyleAttribute(img.getElement(), NODE_ATTRIBUTE_MARGIN, "4px 4px 0px 0px");
      panelContainer.add(img);
    }
    if(label!=null){
      HTML staticInfo=new InlineHTML(label);
      applyTextStyles(node, staticInfo);
      panelContainer.add(staticInfo);
    }
    applyStyles(node, panelContainer);
    applyBorder(node, panelContainer);
    applyMargin(node, panelContainer);
    applyPadding(node, panelContainer);
    applyColor(node, panelContainer, 1);
    applyWidth(node, panelContainer);
    applyHeight(node, panelContainer);
    applyHorzAlign(node, panelContainer);
    stampFormNodeToWidget(panelContainer, node);
    return panelContainer;
 }
}
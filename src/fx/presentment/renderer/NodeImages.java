package fx.presentment.renderer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.*;

public interface NodeImages extends ClientBundleWithLookup {
  public static NodeImages Instance=GWT.create(NodeImages.class);
    
  @Source("./images/open.png")
  public ImageResource open();
  @Source("./images/close.png")
  public ImageResource close();

  @Source("./images/smallCart.png")
  public ImageResource shoppingCart();

  @Source("./images/grader.png")
  public ImageResource grader();

  @Source("./images/eye.png")
  public ImageResource observer();

  @Source("./images/infoSheet.png")
  public ImageResource infoSheet();

}

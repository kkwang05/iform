package fx.presentment.renderer;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.model.bo.ShoppingCart;
import fx.model.bo.Grader;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.FormEntryEvent;

/**
 * Summary view of a Grading Robot, Shopping Cart, etc., with links to point to item details rendered by <code>DetailDisclosure</code>.
 */
public class Itemizer extends DomainObject {
  
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Panel panelContainer=(Panel)super.render(node, entryContext);
    if(entryContext==null)
      return panelContainer;
    FormData dynaNode=entryContext.nodeData(node);
    if(dynaNode.bo instanceof fx.model.bo.Itemizer==false)
      return panelContainer;
    
    Anchor link=null;
    Widget dynamicInfo=null;
    fx.model.bo.Itemizer bo=(fx.model.bo.Itemizer)dynaNode.bo;

    if("D".equalsIgnoreCase(getHint(node.settings, "content"))){
      dynamicInfo = detailView(bo);
      if(dynamicInfo!=null)
        panelContainer.add(dynamicInfo);
    }
    else{
      String[] links=linkText(bo);
      if(links!=null && links.length>0){
        link=new Anchor(links[0]);
        link.addClickHandler(new ClickHandler(){
          public void onClick(ClickEvent event) {
            FormEntryEvent evt=new FormEntryEvent.DataInput(entryContext.entry, node);
            FormEntryEvent.fire(evt);
          }
        });
        DOM.setStyleAttribute(link.getElement(), "display", "block");
        panelContainer.add(link);
      }
      String iTxt=infoText(bo, entryContext);
      if(iTxt!=null){
        dynamicInfo=new HTML(iTxt);
        panelContainer.add(dynamicInfo);
      }
    }
    return panelContainer;
  }
  
  private Widget detailView(fx.model.bo.Itemizer bo){
    if(bo instanceof ShoppingCart){
      if(bo.items().isEmpty())
        return new HTML("Your cart is empty");
      int i=0;
      float total=0;
      Grid g = new Grid(bo.items().size()+1, 2);
      for(; i<bo.items().size(); i++){
        MetaForm node=bo.items().get(i);
        float sub=bo.weights().get(node).floatValue();
        total+=sub;
        g.setWidget(i,0, Renderer.Render(node, null));
        g.setText(i,1, "$"+sub);
      }
      g.setWidget(i, 0, new HTML("<b>Total Price:</b>"));
      g.setText(i, 1, "$"+total);
      return g;
    }
    else{
      return null;
    }
  }

  private String[] linkText(fx.model.bo.Itemizer bo){
    if(bo instanceof ShoppingCart){
      if(bo.items().size()>0)
        return new String[]{bo.items().size()+" item(s)"};
    }
    return null;
  }

  private String infoText(fx.model.bo.Itemizer bo, FormEntrySessionContext context){
    if(bo instanceof ShoppingCart){
      float total=0;
      for(MetaForm node:bo.items()){
        float sub=bo.weights().get(node).floatValue();
        total+=sub;
      }
      String v="Total Price: $" + total;
      return v;
    }
    else {
      int pts=0, total=0;
      for(MetaForm node:bo.items()){
        String answer=((Grader)bo).answers().get(node);
        String resp=context.nodeData(node).value;
        int weight=1;
        if(bo.weights().get(node)!=null)
          weight=bo.weights().get(node).intValue();
        total+=weight;
        if(answer!=null && answer.equals(resp))
          pts+=weight;
      }
      String v="<br/>Total Points: " + pts+"/"+total;
      return v;
    }
  }

}
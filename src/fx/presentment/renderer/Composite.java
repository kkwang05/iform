package fx.presentment.renderer;

import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.Widget;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.FormPanel;
import fx.presentment.widget.CustomDPHeader;

/**
 * Renderer for TC_FORMLET type of MetaForm nodes, or subforms. A subform is rendered in a similar fashion
 * as a form on the same type of canvas Panel, but is unconcerned about child-level editing via DnD and 
 * other means. Instead, it is edited as a whole. 
 *
 * @author kw
 */
public class Composite extends Renderer{
  public Widget render(MetaForm node, final FormEntrySessionContext entryContext){
    FormPanel canvas=new FormPanel();
    canvas.layout(node, entryContext);
    if(node.isComposite() && node.text!=null){
      CustomDPHeader hdr=new CustomDPHeader(NodeImages.Instance.open(), NodeImages.Instance.close(), node.text);
      DisclosurePanel dp=new DisclosurePanel();
      hdr.bind(dp);
      dp.setContent(canvas);
      dp.setOpen(true);
      applyStyles(node, dp);
      applyBorder(node,dp);
      applyMargin(node,dp);
      applyPadding(node,dp);
      applyColor(node,dp, 1);
      applyWidth(node,dp);
      applyHeight(node,dp);
      applyHorzAlign(node,dp);
      applyTextStyles(node,dp.getHeader());
      stampFormNodeToWidget(dp, node);
      return dp;
    }
    applyStyles(node, canvas);
    applyBorder(node,canvas);
    applyMargin(node,canvas);
    applyPadding(node,canvas);
    applyColor(node,canvas, 1);
    applyWidth(node,canvas);
    applyHeight(node,canvas);
    applyHorzAlign(node,canvas);

    stampFormNodeToWidget(canvas, node);
    return canvas;
  }
}
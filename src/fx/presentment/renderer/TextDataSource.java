package fx.presentment.renderer;

import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.*;

public class TextDataSource extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    TextBoxBase ans;
    if(node.typeCode==MetaForm.TC_TEXT_FIELD){
      ans=new TextBox();
    }
    else if(node.typeCode==MetaForm.TC_TEXT_BOX){
      ans=new TextArea();
    }
    else
      throw new RuntimeException("Form node of type "+node.typeCode+" was routed to wrong Renderer class");
    
    applyStyles(node, ans);
    applyBorder(node, ans);
    applyMargin(node, ans);
    applyPadding(node, ans);
    applyColor(node, ans, 1);
    applyWidth(node, ans);
    applyHeight(node, ans);
    applyHorzAlign(node, ans);
    applyTextStyles(node, ans);
    
    FormData initialData=(entryContext==null)? null : entryContext.nodeData(node);
    if(initialData!=null && initialData.value!=null )
      ans.setText(initialData.value.trim());

    if(entryContext!=null){
      ans.setEnabled(true);
      if(entryContext.isReadOnly()){
        ans.setReadOnly(true);
        ensureVisible(ans);
      }
      else{
        ans.setReadOnly(false);
        ans.addValueChangeHandler(new ValueChangeHandler<String>(){
          public void onValueChange(ValueChangeEvent<String> event){
            FormEntryEvent evt=new FormEntryEvent.DataInput(entryContext.entry, node,event.getValue());
            FormEntryEvent.fire(evt);
          }
        });
      }
    }
    
    stampFormNodeToWidget(ans, node);
    return ans;
  }
  private void ensureVisible(TextBoxBase tb){
    String val=tb.getText().trim();
    if(val.equals(""))
      return;
    if(tb instanceof TextBox){
      int lineWidth=val.length();
      ((TextBox) tb).setVisibleLength(lineWidth>80? 80:lineWidth);
    }
    if(tb instanceof TextArea){
      String[] lines=val.split("\r?\n");
      int lineWidth=-1;
      for(String l:lines){
        int lw=l.trim().length();
        if(lw>lineWidth)
          lineWidth=lw;
      }
      if(lines.length>0)
        ((TextArea) tb).setVisibleLines(lines.length);
      if(lineWidth>0)
        ((TextArea) tb).setCharacterWidth(lineWidth>80? 80:lineWidth);
    }
  }
}
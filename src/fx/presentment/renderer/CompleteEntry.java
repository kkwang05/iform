package fx.presentment.renderer;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.FormEntryEvent;

public class CompleteEntry extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Button b=new Button(node.text);
    applyStyles(node, b);
    applyBorder(node, b);
    applyMargin(node, b);
    applyPadding(node, b);
    applyColor(node, b, 1);
    applyWidth(node, b);
    applyHeight(node, b);
    applyHorzAlign(node, b);
    applyTextStyles(node, b);
    
    if(entryContext!=null){
      if(entryContext.isReadOnly())
        b.setEnabled(false);
      else
        b.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event) {
          entryContext.entry.submit();
          FormEntryEvent evt=new FormEntryEvent.LifeCycle(entryContext.entry, node, FormEntryEvent.LifeCycle.SUBMITTED);
          FormEntryEvent.fire(evt);
        }
      });
    }
    stampFormNodeToWidget(b, node);
    return b;
  }

  protected String defaultStyleName(MetaForm node){
    return super.defaultStyleName(node)+" roundedCorner";
  }
}
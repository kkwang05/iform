package fx.presentment.renderer;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.event.FormEntryEvent;

/**
 * @author kw
 */
public class PageTurner extends Renderer {
  interface MyBinder extends UiBinder<Widget, PageTurner> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Anchor previous, next;
  @UiField Label pno;
  private int totalPages, pageIndex;
  
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Widget widget=binder.createAndBindUi(this);
    if(node.text!=null && !node.text.trim().isEmpty()){
      String[] components=node.text.split(",");
      if(components.length>0)
        previous.setText(components[0]);
      if(components.length>1)
        next.setText(components[1]);
    }
    
    applyStyles(node, widget);
    applyBorder(node, widget);
    applyMargin(node, widget);
    applyPadding(node, widget);
    applyColor(node, widget, 1);
    applyWidth(node, widget);
    applyHeight(node, widget);
    applyHorzAlign(node, widget);
    applyTextStyles(node, widget);
    
    MetaForm page=node.myPage();
    List<MetaForm> undockedPages=null;
    if(page!=null && page.getParent()!=null && (undockedPages=page.getParent().undockedPages())!=null){
      totalPages=undockedPages.size();
      pageIndex=undockedPages.indexOf(page);
      pno.setText((pageIndex+1)+"/"+totalPages);
    }
    if(entryContext==null || page==null){
      next.setEnabled(false);
      previous.setEnabled(false);
      stampFormNodeToWidget(widget, node);
      return widget;
    }

    final MetaForm nextPage= (pageIndex+1)<undockedPages.size()? undockedPages.get(pageIndex+1) : null;
    final MetaForm prevPage= pageIndex>0? undockedPages.get(pageIndex-1) : null;
    if(nextPage==null){
      next.setEnabled(false);
      next.getElement().getStyle().setColor("#999");
    }
    else
      next.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event) {
          FormEntryEvent evt=new FormEntryEvent.Navigation(entryContext.entry, node, nextPage);
          FormEntryEvent.fire(evt);
        }
      });
    
    if(prevPage==null){
      previous.setEnabled(false);
      previous.getElement().getStyle().setColor("#999");
    }
    else
      previous.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event) {
          FormEntryEvent evt=new FormEntryEvent.Navigation(entryContext.entry, node, prevPage);
          FormEntryEvent.fire(evt);
        }
      });
    
    stampFormNodeToWidget(widget, node);
    return widget;
  }
}
package fx.presentment.renderer;

import java.util.*;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

public abstract class Renderer {
  public static final String NODE_ATTRIBUTE_STYLE="style";
  public static final String NODE_ATTRIBUTE_BORDER="border";
  public static final String NODE_ATTRIBUTE_MARGIN="margin";
  public static final String NODE_ATTRIBUTE_PADDING="padding";
  public static final String NODE_ATTRIBUTE_WIDTH="width";
  public static final String NODE_ATTRIBUTE_HEIGHT="height";
  public static final String NODE_ATTRIBUTE_COLOR="color";
  public static final String NODE_ATTRIBUTE_HORZ_ALIGN="horzAlign";
  public static final String NODE_ATTRIBUTE_TD_ALIGN="verticalAlign";
  public static final String NODE_ATTRIBUTE_TD_WIDTH="tdWidth";

  public static final String NODE_ATTRIBUTE_FONT_NAME="font-family";
  public static final String NODE_ATTRIBUTE_FONT_SIZE="font-size";
  public static final String NODE_ATTRIBUTE_FONT_COLOR="font-Color";
  public static final String NODE_ATTRIBUTE_FONT_WEIGHT="font-weight";
  public static final String NODE_ATTRIBUTE_FONT_STYLE="font-style";
  public static final String NODE_ATTRIBUTE_TEXT_DECO="text-decoration";
  public static final String NODE_ATTRIBUTE_TEXT_VALIGN="text-valign";
  public static final String NODE_ATTRIBUTE_TEXT_ALIGN="text-align";
  
  private static HashMap<Short, Renderer> SupportedRenderers=new HashMap<Short, Renderer>();
  static{
    SupportedRenderers.put(MetaForm.TC_ROOT, new Root());
    SupportedRenderers.put(MetaForm.TC_PAGE, new Page());
    SupportedRenderers.put(MetaForm.TC_DOCKED_PAGE, new DockedPage());
    SupportedRenderers.put(MetaForm.TC_COMPOSITE, new Composite());

    SupportedRenderers.put(MetaForm.TC_HTML_BLOCK, new HTMLBlock());
    SupportedRenderers.put(MetaForm.TC_TEXT_BLOCK, new HTMLBlock());
    SupportedRenderers.put(MetaForm.TC_IMG_BLOCK, new ImageBlock());
    SupportedRenderers.put(MetaForm.TC_TEXT_FIELD, new TextDataSource());
    SupportedRenderers.put(MetaForm.TC_TEXT_BOX, new TextDataSource());
    SupportedRenderers.put(MetaForm.TC_LIST_BOX, new ChoiceDataSource());
    SupportedRenderers.put(MetaForm.TC_H_RADIO_BUTTON, new ChoiceDataSource());
    SupportedRenderers.put(MetaForm.TC_V_RADIO_BUTTON, new ChoiceDataSource());
    SupportedRenderers.put(MetaForm.TC_H_CHECK_BOX, new ChoiceDataSource());
    SupportedRenderers.put(MetaForm.TC_V_CHECK_BOX, new ChoiceDataSource());
    
    SupportedRenderers.put(MetaForm.TC_ACTION_LINK, new ActionLink());
    SupportedRenderers.put(MetaForm.TC_ACTION_BUTTON, new CompleteEntry());
    SupportedRenderers.put(MetaForm.TC_PAGE_TURNER, new PageTurner());
    SupportedRenderers.put(MetaForm.TC_PAGE_BROWSER, new PageBrowser());

    SupportedRenderers.put(MetaForm.TC_HTML5_CANVAS, new Html5Canvas());

    SupportedRenderers.put(MetaForm.TC_GRADER, new Itemizer());
    SupportedRenderers.put(MetaForm.TC_SHOPPING_CART, new Itemizer());
    SupportedRenderers.put(MetaForm.TC_OBSERVER, new Observer());
    
    SupportedRenderers.put(MetaForm.TC_CONTACT, new InfoSheet());
  }
  
  public static Renderer forNode(MetaForm node){
    Renderer r=SupportedRenderers.get(node.typeCode);
    if(r==null)
      throw new RuntimeException("Unsupported typeCode("+node.typeCode+")");
    return r;
  }
  public static Widget Render(MetaForm node, FormEntrySessionContext entryContext){
    Widget w=forNode(node).render(node,entryContext);
    if(entryContext!=null && w!=null){
      entryContext.putNodeWidget(node, w);
      FormData d=entryContext.nodeData(node);
      if(d!=null && d.hidden)
        w.setVisible(false);
    }
    return w;
  }
  
  public static String getHint(String hints, String key){
    if(hints==null)
      return null;
    int idx=hints.indexOf(key+'=');
    if(idx<0)
      return null;
    hints=hints.substring(idx+key.length()+1);
    idx=hints.indexOf(";");
    if(idx>=0) // we actually enforce this
      hints=hints.substring(0,idx);
    return hints;
  }
  public static String[] getHintComponents(String hints, String key){
    String h=getHint(hints, key);
    if(h==null)
      return null;
    return h.split(",");
  }
  
  public static String encode(String renderingHints, String key, String value){
    if(value==null)
      return renderingHints;
    String singleSetting=value.startsWith(key+"=")? value: key+"="+value;
    singleSetting+=";";
    if(singleSetting.endsWith("=;"))
      singleSetting="";
    
    if(renderingHints==null)
      return singleSetting;
    else if(renderingHints.contains(key+"="))
      return renderingHints.replaceAll(key+"=[^;]+;", singleSetting);
    else
      return renderingHints+singleSetting;
  }
  
  protected static void stampFormNodeToWidget(Widget w, MetaForm node){
    w.getElement().setPropertyObject("_metaform_", node);
  }
  public static MetaForm extractNodeFromWidget(Widget w){
    return (MetaForm)w.getElement().getPropertyObject("_metaform_");
  }
  
  public static void applyTDStyles(MetaForm c, Widget w){
    if(w==null)
      return;
    
    com.google.gwt.user.client.Element e=w.getElement();
    Element td=DOM.getParent(e);
    try{
      if(td!=null && "td".equalsIgnoreCase(td.getTagName())==false)
        return;
    }catch(Throwable t) {
      //t.printStackTrace();
      return;
    }

    String va=getHint(c.renderingHints, NODE_ATTRIBUTE_TD_ALIGN);
    if(va!=null)
      DOM.setStyleAttribute(td, "verticalAlign", va);
    String width=getHint(c.renderingHints, NODE_ATTRIBUTE_TD_WIDTH);
    if(width!=null)
      DOM.setStyleAttribute(td, "width", width);
  }
  
  public abstract Widget render(MetaForm node, final FormEntrySessionContext entryContext);

  public void applyStyles(MetaForm node, Widget renderedWidget){
    String defaultStyleNames=defaultStyleName(node);
    if(defaultStyleNames!=null)
      renderedWidget.addStyleName(defaultStyleNames);
    
    String styleNames=getHint(node.renderingHints, NODE_ATTRIBUTE_STYLE);
    if(styleNames!=null && !styleNames.equals(""))
      renderedWidget.addStyleName(styleNames);
  }
  
  protected String defaultStyleName(MetaForm node){
    String rendererClsName=getClass().getName();
    rendererClsName=rendererClsName.substring(rendererClsName.lastIndexOf(".")+1);
    return "fx-"+rendererClsName;
  }

  protected void applyBorder(MetaForm node, Widget renderedWidget){
    String bg=getHint(node.renderingHints, NODE_ATTRIBUTE_BORDER);
    if(bg!=null && !bg.isEmpty()){
      DOM.setStyleAttribute(renderedWidget.getElement(), "border", bg);
    }
  }
  protected void applyMargin(MetaForm node, Widget renderedWidget){
    String margin=getHint(node.renderingHints, NODE_ATTRIBUTE_MARGIN);
    if(margin!=null && !margin.isEmpty()){
      DOM.setStyleAttribute(renderedWidget.getElement(), "margin", margin);
    }
  }
  protected void applyPadding(MetaForm node, Widget renderedWidget){
    String padding=getHint(node.renderingHints, NODE_ATTRIBUTE_PADDING);
    if(padding!=null && !padding.isEmpty())
      DOM.setStyleAttribute(renderedWidget.getElement(), "padding", padding);
  }
  protected void applyHorzAlign(MetaForm node, Widget renderedWidget){
    String ha=getHint(node.renderingHints, NODE_ATTRIBUTE_HORZ_ALIGN);
    if(ha==null || ha.isEmpty()){
      ;
    }
    else if(ha.equalsIgnoreCase("left")){
      if(DOM.getStyleAttribute(renderedWidget.getElement(), "marginLeft").equalsIgnoreCase("auto"))
        renderedWidget.getElement().getStyle().clearMarginLeft();
      DOM.setStyleAttribute(renderedWidget.getElement(), "marginRight", "auto");
    }
    else if(ha.equalsIgnoreCase("center")){
      DOM.setStyleAttribute(renderedWidget.getElement(), "marginLeft", "auto");
      DOM.setStyleAttribute(renderedWidget.getElement(), "marginRight", "auto");
    }
    else if (ha.equalsIgnoreCase("right")){
      if(DOM.getStyleAttribute(renderedWidget.getElement(), "marginRight").equalsIgnoreCase("auto"))
        renderedWidget.getElement().getStyle().clearMarginRight();
      DOM.setStyleAttribute(renderedWidget.getElement(), "marginLeft", "auto");
    }
  }
  protected void applyColor(MetaForm node, Widget renderedWidget, int target){
    String bg=getHint(node.renderingHints, NODE_ATTRIBUTE_COLOR);
    if(bg!=null && !bg.isEmpty()){
      if(target==0)
        renderedWidget.getElement().getStyle().setColor(bg);
      else if(target==1){
        DOM.setStyleAttribute(renderedWidget.getElement(), "background", bg);
      }
    }
  }
  protected void applyWidth(MetaForm node, Widget renderedWidget){
    String width=getHint(node.renderingHints, NODE_ATTRIBUTE_WIDTH);
    if(width!=null && !width.isEmpty())
      DOM.setStyleAttribute(renderedWidget.getElement(), "width", width);
  }
  protected void applyHeight(MetaForm node, Widget renderedWidget){
    String height=getHint(node.renderingHints, NODE_ATTRIBUTE_HEIGHT);
    if(height!=null && !height.isEmpty())
      DOM.setStyleAttribute(renderedWidget.getElement(), "height", height);
  }

  protected void applyTextStyles(MetaForm node, Widget renderedWidget){
    String family=getHint(node.renderingHints, NODE_ATTRIBUTE_FONT_NAME);
    if(family!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "fontFamily", family);
    
    String size=getHint(node.renderingHints, NODE_ATTRIBUTE_FONT_SIZE);
    if(size!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "fontSize", size);

    String style=getHint(node.renderingHints, NODE_ATTRIBUTE_FONT_STYLE);
    if(style!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "fontStyle", style);
    
    String weight=getHint(node.renderingHints, NODE_ATTRIBUTE_FONT_WEIGHT);
    if(weight!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "fontWeight", weight);

    String color=getHint(node.renderingHints, NODE_ATTRIBUTE_FONT_COLOR);
    if(color!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "color", color);
    
    String deco=getHint(node.renderingHints, NODE_ATTRIBUTE_TEXT_DECO);
    if(deco!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "textDecoration", deco);

    String align=getHint(node.renderingHints, NODE_ATTRIBUTE_TEXT_ALIGN);
    if(align!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "textAlign", align);

    String valign=getHint(node.renderingHints, NODE_ATTRIBUTE_TEXT_VALIGN);
    if(valign!=null)
      DOM.setStyleAttribute(renderedWidget.getElement(), "verticalAlign", valign);
  }
}
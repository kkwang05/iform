package fx.presentment.renderer;

import fx.model.MetaForm;

/**
 * @author kw
 */
public class DockedPage extends Page {
  protected String defaultStyleName(MetaForm node){
    return "fx-Page fx-DockedPage";
  }
}

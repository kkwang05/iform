package fx.presentment.renderer;

import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;
import fx.presentment.widget.*;
import fx.presentment.event.*;

public class ChoiceDataSource extends Renderer{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    ValueSetValues vsv=answerWidget(node);
    if(vsv==null){
      return new Label("<<ERROR>>");
    }
    applyStyles(node, vsv);
    applyBorder(node, vsv);
    applyMargin(node, vsv);
    applyPadding(node, vsv);
    applyColor(node, vsv, 1);
    applyWidth(node, vsv);
    applyHeight(node, vsv);
    applyHorzAlign(node, vsv);

    if(vsv.getChildComponents()==null)
      applyTextStyles(node, vsv);
    else{
      for(CheckBox cb: vsv.getChildComponents())
        applyTextStyles(node, cb);
    }
    
    FormData data=(entryContext==null)? null : entryContext.nodeData(node);
    if(data!=null && data.value!=null )
      vsv.setValue(data.value);
    
    if(entryContext!=null){
      if(entryContext.isReadOnly())
        vsv.setEnabled(false);
      else
        vsv.addValueChangeHandler(new ValueChangeHandler<String>() {
          public void onValueChange(ValueChangeEvent<String> event) {
            FormEntryEvent evt= new FormEntryEvent.DataInput(entryContext.entry, node, event.getValue());
            FormEntryEvent.fire(evt);
          }
        });
    }

    stampFormNodeToWidget(vsv, node);
    return vsv;
  }
  
  protected ValueSetValues answerWidget(MetaForm node){
    if(node.valueSet==null)
      return null;
    ValueSetValues vsv=null;
    if(node.typeCode==MetaForm.TC_LIST_BOX)
      vsv=new ValueSetValues.AsDropdownList(node.valueSet); 
    else{
      ComplexPanel grpPanel;
      if(node.typeCode==MetaForm.TC_H_RADIO_BUTTON){
        grpPanel=new HorizontalPanel();
        vsv=new ValueSetValues.AsRadioButtonGroup(node.valueSet, grpPanel);
      }
      else if(node.typeCode==MetaForm.TC_V_RADIO_BUTTON){
        grpPanel=new VerticalPanel();
        vsv=new ValueSetValues.AsRadioButtonGroup(node.valueSet, grpPanel);
      }
      else if(node.typeCode==MetaForm.TC_H_CHECK_BOX){
        grpPanel=new HorizontalPanel();
        vsv=new ValueSetValues.AsCheckBoxGroup(node.valueSet, grpPanel);
      }
      else if(node.typeCode==MetaForm.TC_V_CHECK_BOX){
        grpPanel=new VerticalPanel();
        vsv=new ValueSetValues.AsCheckBoxGroup(node.valueSet, grpPanel);
      }
      else
        throw new RuntimeException("Form node of type "+node.typeCode+" was routed to wrong Renderer class");
    }
    return vsv;
  }
}
package fx.presentment.renderer;

import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 */
public class InfoSheet extends DomainObject{
  public Widget render(final MetaForm node, final FormEntrySessionContext entryContext){
    Panel panelContainer=(Panel)super.render(node, entryContext);
    if(entryContext==null)
      return panelContainer;
    FormData dynaNode=entryContext.nodeData(node);
    if(dynaNode.bo instanceof fx.model.bo.InfoSheet==false)
      return panelContainer;

    fx.model.bo.InfoSheet bo=(fx.model.bo.InfoSheet)dynaNode.bo;
    Grid g = new Grid(bo.propertyList.length, 2);
    for(int i=0; i<bo.propertyList.length; i++){
      String propName=bo.propertyList[i];
      Object val=bo.getProperty(propName);
      g.setText(i,0, propName);
      g.setText(i,1, ""+val);
    }
    panelContainer.add(g);
    
    return panelContainer;
  }
}
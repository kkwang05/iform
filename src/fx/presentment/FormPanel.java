package fx.presentment;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.ui.Widget;

import fx.model.Form;
import fx.model.MetaForm;
import fx.presentment.renderer.*;
import fx.presentment.widget.TabularFlowPanel;

/**
 * Container for FormEntry OR Formlet nodes.
 * @author kw
 */
public class FormPanel extends TabularFlowPanel {

  public void layout(Form.DockView dv, MetaForm node, FormEntrySessionContext entryContext){
    // Render the whole root on load only if the form is not paged
    if(dv==null){
      layout(node, entryContext);
      return;
    }
    
    clear();
    
    // Render dock and one (and only one - even if there is no dock) page
    
    for(MetaForm c:dv.s0)
      layoutSingleNode(c, null, entryContext);

    if(node.isRoot())
      layoutSingleNode(dv.p, null, entryContext);
    else
      layoutSingleNode(node, dv.p, entryContext);
    
    for(MetaForm c:dv.s1)
      layoutSingleNode(c, null, entryContext);
  }

  public void layout(MetaForm root, FormEntrySessionContext entryContext){
    clear();
    for(MetaForm c:root.getChildren())
      layoutSingleNode(c, null, entryContext);
  }
  
  Map<MetaForm, Widget> row=new HashMap<MetaForm, Widget>();
  private Widget layoutSingleNode(MetaForm c, MetaForm centralPage, FormEntrySessionContext entryContext){
    Widget w=Renderer.Render(c, entryContext);
    if(w==null)
      return w;
    
    int layoutMarker=centralPage!=null? centralPage.rowMarker : c.rowMarker;
    if(layoutMarker==1 || inRow)
      row.put(c, w);
    add(w);
    if(layoutMarker==1)
      rowStarted();
    else if(layoutMarker==2){
      rowEnded();
      applyRowStyling(row);
      row.clear();
    }
    
    return w;
  }
}
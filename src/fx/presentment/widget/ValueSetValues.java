package fx.presentment.widget;

import java.util.*;

import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;

import fx.model.ValueSet;

/**
 * @author kw
 */
public abstract class ValueSetValues extends Composite implements HasValue<String> {
  
  protected final ValueSet valueSet;
  protected ValueSetValues(ValueSet vs){
    this.valueSet=vs;
  }

  public abstract List<CheckBox> getChildComponents();
  
  public String getValue(){
    StringBuffer sb=new StringBuffer(valueSet.id.toString()).append("[");
    for(int i: getSelectedItems())
      sb.append(i+",");
    if(sb.charAt(sb.length()-1)==',')
      sb.deleteCharAt(sb.length()-1);
    sb.append("]");
    return sb.toString(); 
  }

  public void setValue(String newValue){
    int demarc=newValue.indexOf("[");
    int endIdx=newValue.indexOf(",]");
    if(endIdx<0)
      endIdx=newValue.indexOf("]");
    String vsId=newValue.substring(0, demarc);
    String indices=newValue.substring(demarc+1, endIdx);
    assert(valueSet.id.equals(Long.valueOf(vsId)));
    String[] selected =indices.split(",");
    int[] selectedIndices=new int[selected.length];
    for(int i=0; i<selected.length; i++)
      selectedIndices[i]=Integer.parseInt(selected[i]);
    setSelectedItems(selectedIndices);
  }

  public void setValue(String newValue, boolean fireEvent){
    setValue(newValue);
    if(fireEvent)
      ValueChangeEvent.fire(this, getValue());
  }
  
  private transient boolean valueChangeHandlerInitialized = false;
  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
    if (!valueChangeHandlerInitialized) {
      ensureDomEventHandlers();
      valueChangeHandlerInitialized = true;
    }
    return addHandler(handler, ValueChangeEvent.getType());
  }
  
  // Implementation class responsibilities
  protected abstract int[] getSelectedItems();
  protected abstract void setSelectedItems(int[] selected);
  protected abstract void ensureDomEventHandlers();
  public abstract void setEnabled(boolean enabled);

  //
  // Single Choice - Drop-down List rendition
  //
  public static class AsDropdownList extends ValueSetValues {
    private ListBox listBox;
    public AsDropdownList(ValueSet vs){
      super(vs);
      listBox = new ListBox();
      listBox.addItem("");
      for (String v:valueSet.values)
        listBox.addItem(v);
      listBox.setVisibleItemCount(1);
      listBox.setSelectedIndex(0);
      initWidget(listBox);
    }
    
    public List<CheckBox> getChildComponents(){
      return null;
    }
  
    protected void ensureDomEventHandlers() {
      listBox.addChangeHandler(new ChangeHandler(){
        public void onChange(ChangeEvent event) {
          ValueChangeEvent.fire(AsDropdownList.this, getValue());
        }
      });
    }
    
    protected int[] getSelectedItems(){
      int selectedIdx=listBox.getSelectedIndex();
      if(selectedIdx==0)
        return new int[0];
      else
        return new int[]{selectedIdx-1};
    }
    
    protected void setSelectedItems(int[] selected){
      listBox.setSelectedIndex(selected[0]+1);
    }

    public void setEnabled(boolean enabled){
      listBox.setEnabled(enabled);
    }
  }

  //
  // Single Choice - RadioButton group rendition
  //
  public static class AsRadioButtonGroup extends AsCheckBoxGroup {
    public AsRadioButtonGroup(ValueSet vs, ComplexPanel grpPanel){
      super(vs);
      rbs=new ArrayList<CheckBox>();
      String grpId="RBG-"+this.hashCode()+"-"+Random.nextInt();
      for (String v:valueSet.values){
        RadioButton rb = new RadioButton(grpId, v, true);
        grpPanel.add(rb);
        rbs.add(rb);
      }
      initWidget(grpPanel);
    }
  }

  //
  // Multiple Choice - Checkbox group rendition
  //
  public static class AsCheckBoxGroup extends ValueSetValues {
    protected List<CheckBox> rbs;
    
    public AsCheckBoxGroup(ValueSet vs){
      super(vs);
    }
    public AsCheckBoxGroup(ValueSet vs, ComplexPanel grpPanel){
      super(vs);
      rbs=new ArrayList<CheckBox>();
      for (String v:valueSet.values){
        CheckBox rb=new CheckBox(v, true);
        grpPanel.add(rb);
        rbs.add(rb);
      }
      initWidget(grpPanel);
    }
    
    public List<CheckBox> getChildComponents(){
      return rbs;
    }
  
    protected void ensureDomEventHandlers() {
      for(CheckBox rb:rbs){
        rb.addValueChangeHandler(new ValueChangeHandler<Boolean>(){
          public void onValueChange(ValueChangeEvent<Boolean> event) {
            ValueChangeEvent.fire(AsCheckBoxGroup.this, getValue());
          }
        });
      }
    }
    protected int[] getSelectedItems(){
      List<Integer> checked=new ArrayList<Integer>();
      for(int i=0;i<rbs.size();i++)
        if(rbs.get(i).getValue())
          checked.add(i);
      int[] result=new int[checked.size()];
      for(int i=0; i<checked.size(); i++)
        result[i]=checked.get(i).intValue();
      return result;
    }
    protected void setSelectedItems(int[] selected){
      for(int i=0; i<selected.length; i++)
        rbs.get(selected[i]).setValue(Boolean.TRUE);
    }
    public void setEnabled(boolean enabled){
      for(CheckBox rb:rbs)
        rb.setEnabled(enabled);
    }
  }
}
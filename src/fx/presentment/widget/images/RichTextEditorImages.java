package fx.presentment.widget.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface RichTextEditorImages extends ClientBundle {

  @Source("fx/presentment/widget/images/bold.gif")
  ImageResource bold();

  @Source("fx/presentment/widget/images/createLink.gif")
  ImageResource createLink();

  @Source("fx/presentment/widget/images/hr.gif")
  ImageResource hr();

  @Source("fx/presentment/widget/images/indent.gif")
  ImageResource indent();

  @Source("fx/presentment/widget/images/insertImage.gif")
  ImageResource insertImage();

  @Source("fx/presentment/widget/images/italic.gif")
  ImageResource italic();

  @Source("fx/presentment/widget/images/justifyCenter.gif")
  ImageResource justifyCenter();

  @Source("fx/presentment/widget/images/justifyLeft.gif")
  ImageResource justifyLeft();

  @Source("fx/presentment/widget/images/justifyRight.gif")
  ImageResource justifyRight();

  @Source("fx/presentment/widget/images/ol.gif")
  ImageResource ol();

  @Source("fx/presentment/widget/images/outdent.gif")
  ImageResource outdent();

  @Source("fx/presentment/widget/images/removeFormat.gif")
  ImageResource removeFormat();

  @Source("fx/presentment/widget/images/removeLink.gif")
  ImageResource removeLink();

  @Source("fx/presentment/widget/images/strikeThrough.gif")
  ImageResource strikeThrough();

  @Source("fx/presentment/widget/images/subscript.gif")
  ImageResource subscript();

  @Source("fx/presentment/widget/images/superscript.gif")
  ImageResource superscript();

  @Source("fx/presentment/widget/images/ul.gif")
  ImageResource ul();

  @Source("fx/presentment/widget/images/underline.gif")
  ImageResource underline();
  
  @Source("fx/presentment/widget/images/text_size.png")
  ImageResource textSize();
  @Source("fx/presentment/widget/images/text_color.png")
  ImageResource textColor();
  @Source("fx/presentment/widget/images/text_bold.png")
  ImageResource textBold();
  @Source("fx/presentment/widget/images/text_italic.png")
  ImageResource textItalic();
  @Source("fx/presentment/widget/images/text_underline.png")
  ImageResource textUnderline();
  @Source("fx/presentment/widget/images/text_strikethrough.png")
  ImageResource textStrikethrough();
  @Source("fx/presentment/widget/images/text_superscript.png")
  ImageResource textSuperscript();
  @Source("fx/presentment/widget/images/text_subscript.png")
  ImageResource textSubscript();
  @Source("fx/presentment/widget/images/text_align_left.png")
  ImageResource textAlignLeft();
  @Source("fx/presentment/widget/images/text_align_center.png")
  ImageResource textAlignCenter();
  @Source("fx/presentment/widget/images/text_align_right.png")
  ImageResource textAlignRight();
  @Source("fx/presentment/widget/images/text_align_justify.png")
  ImageResource textAlignJustify();
}

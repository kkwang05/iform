package fx.presentment.widget;

import java.util.Map;

import com.google.gwt.user.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.MetaForm;
import fx.presentment.renderer.Renderer;

/**
 * A FlowPanel with tabular rows mixed with regular child widgets. Each row contains >1 child widgets.
 * With a two-level hierarchy in the DOM tree, the logical index and the physical index of a widget are no
 * longer the same. The insertion semantics becomes two-fold (intra-row and inter-row).
 * @author kw
 */
public class TabularFlowPanel extends FlowPanel {
  protected transient boolean inRow;
  public void rowStarted(){
    inRow=true;
  }
  public void rowEnded(){
    inRow=false;
  }
  
  @Override
  public void clear(){
    super.clear();
    inRow=false;
  }
  
  @Override
  public void add(Widget w) {
    if(inRow)
      append(w, getWidgetCount()-1);
    else
      add(w, getElement());
  }

  @Override
  public void insert(Widget w, int beforeIndex) {
    if(beforeIndex==getWidgetCount())
      insert(w, getElement(), beforeIndex, true);
    else
      insertAbove(w,beforeIndex);
  }

  public void insertAbove(Widget w, int index){
    Widget sibling=getWidget(index);
    if(isInRow(sibling))
      index-=positionInRow(sibling,false);
    insert(w, index, getWidgetPhysicalIndex(sibling));
  }
  
  public void insertBelow(Widget w, int index){
    Widget sibling=getWidget(index);
    if(isInRow(sibling))
      index+=positionInRow(sibling,true);
    insert(w, index+1, getWidgetPhysicalIndex(sibling)+1);
  }

  public int getWidgetPhysicalIndex(Widget w){
    int idx=DOM.getChildIndex(getElement(), w.getElement());
    if(idx>=0)
      return idx;
    Element td=DOM.getParent(w.getElement());
    Element tr=DOM.getParent(td);
    Element tb=DOM.getParent(tr);
    Element t=DOM.getParent(tb);
    return DOM.getChildIndex(getElement(), t);
  }

  // Modified from ComplexPanel.insert()
  protected void insert(Widget child, int beforeIndex, int beforePhysicalIndex) {
    beforeIndex = adjustIndex(child, beforeIndex);
    // Adjust physical index.
    int curIdx=DOM.getChildIndex(getElement(), child.getElement());
    if(curIdx>=0 && curIdx<beforePhysicalIndex)
      beforePhysicalIndex--;
    child.removeFromParent();
    getChildren().insert(child, beforeIndex);
    DOM.insertChild(getElement(), child.getElement(), beforePhysicalIndex);
    adopt(child);
  }

  /**
   * Annex a child Widget to another in a row, on the right.
   * @param w
   * @param sibling
   */
  public void prepend(Widget w, int beforeIndex) {
    Widget sibling=getWidget(beforeIndex);
    annex(w,sibling,false,true);
  }
  
  /**
   * Annex a child Widget to another in a row, on the left.
   * @param w
   * @param sibling
   */
  public void append(Widget w, int afterIndex) {
    Widget sibling=getWidget(afterIndex);
    annex(w,sibling,true,true);
  }
  
  /**
   * Annex a child Widget to another Widget on the same row.
   * 
   * @param w
   * @param sibling
   */
  protected void annex(Widget w, Widget sibling, boolean append, boolean reDistributeWidth) {
    // Logical attach.
    int siblingIndex=getWidgetIndex(sibling);
    siblingIndex = adjustIndex(w, siblingIndex);
    w.removeFromParent();
    getChildren().insert(w, append? siblingIndex+1 : siblingIndex);
  
    // Physical attach.
    int siblingPosition=-1;
    Element tr=null;
    if(!isInRow(sibling)){
      tr=putInRow(sibling);
      siblingPosition=0;
    }
    else{
      tr=DOM.getParent(DOM.getParent(sibling.getElement()));
      siblingPosition=positionInRow(sibling,false);
    }
    Element td = putInCell(w.getElement());
    DOM.insertChild(tr, td, append? siblingPosition+1 : siblingPosition);
    if(reDistributeWidth)
      reDistributeRow(tr);
  
    adopt(w);
  }

  public void replace(Widget w1, Widget w2){
    if(!isInRow(w2)){
      int idx=getWidgetIndex(w2);
      insert(w1, idx);
      remove(w2);
      return;
    }
    annex(w1,w2,false,false);
    remove(w2);
  }

  @Override
  public boolean remove(Widget w) {
    if(!isInRow(w))
      return super.remove(w);
    Element td = DOM.getParent(w.getElement());
    boolean removed = super.remove(w);
    if (!removed) 
      return false;
    
    Element tr = DOM.getParent(td);
    Element tb = DOM.getParent(tr);
    Element t = DOM.getParent(tb);
    DOM.removeChild(tr, td);
    if(DOM.getChildCount(tr)==0){
      DOM.removeChild(getElement(), t);
    }
    else if (DOM.getChildCount(tr)==1){
      td=DOM.getChild(tr,0);
      Element e=DOM.getChild(td,0);
      int idx=DOM.getChildIndex(getElement(), t);
      DOM.removeChild(getElement(), t);
      DOM.insertChild(getElement(), e, idx);
    }
    else 
      reDistributeRow(tr);

    return true;
  }

  private void reDistributeRow(Element tr){
    int count=DOM.getChildCount(tr);
    String width=(100/count)+"%";
    for(int i=0; i<count; i++){
      Element td=DOM.getChild(tr, i);
      DOM.setStyleAttribute(td, "width", width);
    }
  }
  
  private Element putInRow(Widget w){
    Element e=w.getElement();
    int idx=DOM.getChildIndex(getElement(), e);
    DOM.removeChild(getElement(), e);
    
    Element t=new HorizontalPanel().getElement(); 
    Element tr=DOM.getChild(DOM.getChild(t,0),0);
    Element td = putInCell(e);
    DOM.appendChild(tr, td);
    DOM.insertChild(getElement(), t, idx);
    DOM.setStyleAttribute(t, "width", "100%");
    return tr;
  }
  
  private Element putInCell(Element e) {
    Element td = DOM.createTD();
    DOM.appendChild(td, e);
  
    //Default verticalAlign is center.
    //DOM.setStyleAttribute(td, "verticalAlign", "top");
    //DOM.setStyleAttribute(td, "align", "left");
    return td;
  }
  
  public boolean isInRow(Widget w){
    Element p=DOM.getParent(w.getElement());
    return (p!=getElement());
  }
  
  public int positionInRow(Widget w, boolean fromRowEnd){
    Element td=DOM.getParent(w.getElement());
    Element tr=DOM.getParent(td);
    if(fromRowEnd)
      return DOM.getChildCount(tr) - DOM.getChildIndex(tr, td) -1;
    else
      return DOM.getChildIndex(tr, td);
  }
  
  public boolean isLastInRow(Widget w){
    Element td=DOM.getParent(w.getElement());
    return DOM.getNextSibling(td)==null;
  }

  public boolean isInMiddleOfRow(Widget w){
    Element td=DOM.getParent(w.getElement());
    Element tr=DOM.getParent(td);
    return DOM.getChildIndex(tr,td)>0 && DOM.getNextSibling(td)!=null;
  }

  public boolean isFirstInRow(Widget w){
    return positionInRow(w,false)==0;
  }

  public int getFirstWidgetInRowIndex(Widget w){
    int idx=getWidgetIndex(w);
    if(isInRow(w))
      idx-=positionInRow(w,false);
    return idx;
  }
  
  public boolean isInSameRow(Widget w, Widget w2){
    Element tr=DOM.getParent(DOM.getParent(w.getElement()));
    Element p2=DOM.getParent(DOM.getParent(w2.getElement()));
    return (tr==p2);
  }
  
  public int getRowWidth(Widget w){
    Element td=DOM.getParent(w.getElement());
    Element tr=DOM.getParent(td);
    return DOM.getChildCount(tr);
  }
  
  protected void applyRowStyling(Map<MetaForm, Widget> row){
    for(Map.Entry<MetaForm, Widget> entry: row.entrySet())
      Renderer.applyTDStyles(entry.getKey(), entry.getValue());
  }
}
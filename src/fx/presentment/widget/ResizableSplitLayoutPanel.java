package fx.presentment.widget;

import com.google.gwt.user.client.ui.SplitLayoutPanel;
import com.google.gwt.user.client.ui.Widget;

public class ResizableSplitLayoutPanel extends SplitLayoutPanel{

  private boolean animate=false;
  
  public void setSplitPosition(Widget widgetBeforeTheSplitter, 
      double size) {
    LayoutData layout = (LayoutData) widgetBeforeTheSplitter.getLayoutData();
    layout.oldSize = layout.size;
    layout.size = size;
    if (animate)
      animate(100);
    else
      forceLayout();
  }
  
  public void restoreToPreviousSplitPosition(Widget widgetBeforeTheSplitter) {
    LayoutData layout = (LayoutData) widgetBeforeTheSplitter.getLayoutData();
    double prevSize= layout.oldSize;
    layout.oldSize = layout.size;
    layout.size = prevSize;
    if (animate)
      animate(100);
    else
      forceLayout();
  }
} 
package fx.presentment.widget;

import java.util.*;

import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

/**
 * @author kw
 */
public class ItemizedSelector extends FormatControl implements ValueChangeHandler<Boolean>{
	
  private Map<RadioButton, String> itemMap=new HashMap<RadioButton, String>();
  private transient String[] initializedItems;
  
  public ItemizedSelector(ButtonBase trigger){
    super(trigger);
  }

  public void onValueChange(ValueChangeEvent<Boolean> evt){
    String v=itemMap.get(evt.getSource());
    ValueChangeEvent.<String>fire(this, v);
  }
  
  public void setSelectionItems(String...items){
    if(initializedItems!=null && initializedItems.equals(items))
      return;
    
    itemMap.clear();
    clear();
    String grpId="RBG-"+this.hashCode()+"-"+Random.nextInt();
    FlowPanel grpPanel=new FlowPanel();
    for (String v:items){
      RadioButton rb;
      String[] kv=v.split(":");
      if(kv.length>1){
        rb = new RadioButton(grpId, kv[0], true);
        v=kv[1];
      }
      else{
        if(v.endsWith(":")){
          v=v.substring(0,v.length()-1);
          rb = new RadioButton(grpId, v, true);
          v="";
        }
        else
          rb = new RadioButton(grpId, v, true);
      }
      
      grpPanel.add(rb);
      itemMap.put(rb, v);
      rb.addValueChangeHandler(this);
      rb.addStyleName("InternalLabel");
    }
    add(grpPanel);
    initializedItems=items;
  }
  
  public void setSelectedValue(String value){
    if(value==null)
      value="";
    for(Map.Entry<RadioButton,String> e: itemMap.entrySet()){
      if(e.getValue().equalsIgnoreCase(value))
        e.getKey().setValue(true);
      else
        e.getKey().setValue(false);
    }
  }
  
}
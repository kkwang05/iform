package fx.presentment.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.resources.client.*;

/**
 * A widget that allows the user to select a value within a range of possible
 * values using a sliding bar that responds to mouse events.
 * 
 * <h3>Keyboard Events</h3>
 * <p>
 * SliderBar listens for the following key events. Holding down a key will
 * repeat the action until the key is released. <ul class='css'>
 * <li>left arrow - shift left one step</li>
 * <li>right arrow - shift right one step</li>
 * <li>ctrl+left arrow - jump left 10% of the distance</li>
 * <li>ctrl+right arrow - jump right 10% of the distance</li>
 * <li>home - jump to min value</li>
 * <li>end - jump to max value</li>
 * <li>space - jump to middle value</li>
 * </ul>
 * </p>
 * 
 * <h3>CSS Style Rules</h3> <ul class='css'> <li>.gwt-SliderBar-shell { primary
 * style }</li> <li>.gwt-SliderBar-shell-focused { primary style when focused }</li>
 * <li>.gwt-SliderBar-shell gwt-SliderBar-line { the line that the knob moves
 * along }</li> <li>.gwt-SliderBar-shell gwt-SliderBar-line-sliding { the line
 * that the knob moves along when sliding }</li> <li>.gwt-SliderBar-shell
 * .gwt-SliderBar-knob { the sliding knob }</li> <li>.gwt-SliderBar-shell
 * .gwt-SliderBar-knob-sliding { the sliding knob when sliding }</li> <li>
 * .gwt-SliderBar-shell .gwt-SliderBar-tick { the ticks along the line }</li>
 * <li>.gwt-SliderBar-shell .gwt-SliderBar-label { the text labels along the
 * line }</li> </ul>
 */
public class SliderBar extends FocusPanel implements HasValueChangeHandlers<Double> {

  static interface SliderBarImageResources extends ClientBundle {
    static SliderBarImageResources Instance=GWT.create(SliderBarImageResources.class);
    @Source("./images/slider.gif")
    ImageResource slider();
    @Source("./images/sliderDisabled.gif")
    ImageResource sliderDisabled();
    @Source("./images/sliderSliding.gif")
    ImageResource sliderSliding();
  }
  
  private Image knobImage = new Image();
  private KeyTimer keyTimer = new KeyTimer();

  private List<Element> tickElements = new ArrayList<Element>();
  private List<Element> labelElements = new ArrayList<Element>();
  private Element lineElement;

  private int lineLeftOffset = 0;

  private double minValue;
  private double maxValue;
  private double stepSize;
  private double curValue;

  private int numLabels = 0;
  private int numTicks = 0;

  private boolean slidingKeyboard = false;
  private boolean slidingMouse = false;
  private boolean enabled = true;

  public SliderBar(double minValue, double maxValue, int width) {
    this.minValue = minValue;
    this.maxValue = maxValue;

    // Create the outer shell
    DOM.setStyleAttribute(getElement(), "position", "relative");
    setStyleName("gwt-SliderBar-shell");
    DOM.setStyleAttribute(getElement(), "width", width+"px");

    // Create the line
    lineElement = DOM.createDiv();
    DOM.appendChild(getElement(), lineElement);
    DOM.setStyleAttribute(lineElement, "position", "absolute");
    DOM.setElementProperty(lineElement, "className", "gwt-SliderBar-line");

    // Create the knob
    AbstractImagePrototype.create(SliderBarImageResources.Instance.slider()).applyTo(knobImage);
    Element knobElement = knobImage.getElement();
    DOM.appendChild(getElement(), knobElement);
    DOM.setStyleAttribute(knobElement, "position", "absolute");
    DOM.setElementProperty(knobElement, "className", "gwt-SliderBar-knob");

    sinkEvents(Event.MOUSEEVENTS | Event.KEYEVENTS | Event.FOCUSEVENTS);
  }
  
  @Override
  protected void onLoad() {
    redraw();
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
    if (enabled) {
      AbstractImagePrototype.create(SliderBarImageResources.Instance.slider()).applyTo(knobImage);
      DOM.setElementProperty(lineElement, "className", "gwt-SliderBar-line");
    } else {
      AbstractImagePrototype.create(SliderBarImageResources.Instance.sliderDisabled()).applyTo(knobImage);
      DOM.setElementProperty(lineElement, "className", "gwt-SliderBar-line gwt-SliderBar-line-disabled");
    }
    redraw();
  }

  public void redraw() {
    if (!isAttached())
      return;
    
    int width = DOM.getElementPropertyInt(getElement(), "clientWidth");
    int lineWidth = DOM.getElementPropertyInt(lineElement, "offsetWidth");
    lineLeftOffset = (width / 2) - (lineWidth / 2);
    DOM.setStyleAttribute(lineElement, "left", lineLeftOffset + "px");
  
    drawLabels();
    drawTicks();
    drawKnob();
  }

  @Override
  public void onBrowserEvent(Event event) {
    super.onBrowserEvent(event);
    if (enabled) {
      switch (DOM.eventGetType(event)) {
        // Unhighlight and cancel keyboard events
        case Event.ONBLUR:
          keyTimer.cancel();
          if (slidingMouse) {
            DOM.releaseCapture(getElement());
            slidingMouse = false;
            slideKnob(event);
            stopSliding(true, true);
          } else if (slidingKeyboard) {
            slidingKeyboard = false;
            stopSliding(true, true);
          }
          break;
  
        // Highlight on focus
        case Event.ONFOCUS:
          break;
  
        // Mousewheel events
        case Event.ONMOUSEWHEEL:
          int velocityY = DOM.eventGetMouseWheelVelocityY(event);
          DOM.eventPreventDefault(event);
          if (velocityY > 0) {
            shiftRight(1);
          } else {
            shiftLeft(1);
          }
          break;
  
        // Shift left or right on key press
        case Event.ONKEYDOWN:
          if (!slidingKeyboard) {
            int multiplier = 1;
            if (DOM.eventGetCtrlKey(event)) {
              multiplier = (int) (getTotalRange() / stepSize / 10);
            }
  
            switch (DOM.eventGetKeyCode(event)) {
              case KeyCodes.KEY_HOME:
                DOM.eventPreventDefault(event);
                setCurrentValue(minValue);
                break;
              case KeyCodes.KEY_END:
                DOM.eventPreventDefault(event);
                setCurrentValue(maxValue);
                break;
              case KeyCodes.KEY_LEFT:
                DOM.eventPreventDefault(event);
                slidingKeyboard = true;
                startSliding(false, true);
                shiftLeft(multiplier);
                keyTimer.schedule(400, false, multiplier);
                break;
              case KeyCodes.KEY_RIGHT:
                DOM.eventPreventDefault(event);
                slidingKeyboard = true;
                startSliding(false, true);
                shiftRight(multiplier);
                keyTimer.schedule(400, true, multiplier);
                break;
              case 32:
                DOM.eventPreventDefault(event);
                setCurrentValue(minValue + getTotalRange() / 2);
                break;
            }
          }
          break;
        // Stop shifting on key up
        case Event.ONKEYUP:
          keyTimer.cancel();
          if (slidingKeyboard) {
            slidingKeyboard = false;
            stopSliding(true, true);
          }
          break;
  
        // Mouse Events
        case Event.ONMOUSEDOWN:
          setFocus(true);
          slidingMouse = true;
          DOM.setCapture(getElement());
          startSliding(true, true);
          DOM.eventPreventDefault(event);
          slideKnob(event);
          break;
        case Event.ONMOUSEUP:
          if (slidingMouse) {
            DOM.releaseCapture(getElement());
            slidingMouse = false;
            slideKnob(event);
            stopSliding(true, true);
          }
          break;
        case Event.ONMOUSEMOVE:
          if (slidingMouse) {
            slideKnob(event);
          }
          break;
      }
    }
  }

  public void setCurrentValue(double curValue) {
    setCurrentValue(curValue, true);
  }

  public void setCurrentValue(double curValue, boolean fireEvent) {
    // Confine the value to the range
    this.curValue = Math.max(minValue, Math.min(maxValue, curValue));
  
    // Go to next step if more than halfway there
    double remainder = (this.curValue - minValue) % stepSize;
    this.curValue -= remainder;
    if ((remainder > (stepSize / 2)) && ((this.curValue + stepSize) <= maxValue))
      this.curValue += stepSize;
  
    // Redraw the knob
    drawKnob();
    
    if(fireEvent)
      ValueChangeEvent.<Double>fire(this, this.curValue);
  }

  public void unset(){
    setCurrentValue(minValue, false);
  }
  
  public void setMaxValue(double maxValue) {
    this.maxValue = maxValue;
    drawLabels();
    resetCurrentValue();
  }

  public void setMinValue(double minValue) {
    this.minValue = minValue;
    drawLabels();
    resetCurrentValue();
  }

  /**
   * Set the number of labels to show on the line. Labels indicate the value of
   * the slider at that point. Use this method to enable labels.
   * 
   * If you set the number of labels equal to the total range divided by the
   * step size, you will get a properly aligned "jumping" effect where the knob
   * jumps between labels.
   * 
   * Note that the number of labels displayed will be one more than the number
   * you specify, so specify 1 labels to show labels on either end of the line.
   * In other words, numLabels is really the number of slots between the labels.
   * 
   * setNumLabels(0) will disable labels.
   * 
   * @param numLabels the number of labels to show
   */
  public void setNumLabels(int numLabels) {
    this.numLabels = numLabels;
    drawLabels();
  }

  /**
   * Set the number of ticks to show on the line. A tick is a vertical line that
   * represents a division of the overall line. Use this method to enable ticks.
   * 
   * If you set the number of ticks equal to the total range divided by the step
   * size, you will get a properly aligned "jumping" effect where the knob jumps
   * between ticks.
   * 
   * Note that the number of ticks displayed will be one more than the number
   * you specify, so specify 1 tick to show ticks on either end of the line. In
   * other words, numTicks is really the number of slots between the ticks.
   * 
   * setNumTicks(0) will disable ticks.
   * 
   * @param numTicks the number of ticks to show
   */
  public void setNumTicks(int numTicks) {
    this.numTicks = numTicks;
    drawTicks();
  }

  public void setStepSize(double stepSize) {
    this.stepSize = stepSize;
    resetCurrentValue();
  }

  public void shiftLeft(int numSteps) {
    setCurrentValue(getCurrentValue() - numSteps * stepSize);
  }

  public void shiftRight(int numSteps) {
    setCurrentValue(getCurrentValue() + numSteps * stepSize);
  }

  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Double> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  public double getCurrentValue() {
    return curValue;
  }

  public double getMaxValue() {
    return maxValue;
  }

  public double getMinValue() {
    return minValue;
  }

  public int getNumLabels() {
    return numLabels;
  }

  public int getNumTicks() {
    return numTicks;
  }

  public double getStepSize() {
    return stepSize;
  }

  public double getTotalRange() {
    if (minValue > maxValue) {
      return 0;
    } else {
      return maxValue - minValue;
    }
  }

  /**
   * @return Gets whether this widget is enabled
   */
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * Format the label to display above the ticks
   * 
   * Override this method in a subclass to customize the format. By default,
   * this method returns the integer portion of the value.
   * 
   * @param value the value at the label
   * @return the text to put in the label
   */
  protected String formatLabel(double value) {
    return (int)value+"";
  }

  /**
   * Get the percentage of the knob's position relative to the size of the line.
   * The return value will be between 0.0 and 1.0.
   * 
   * @return the current percent complete
   */
  protected double getKnobPercent() {
    // If we have no range
    if (maxValue <= minValue) {
      return 0;
    }

    // Calculate the relative progress
    double percent = (curValue - minValue) / (maxValue - minValue);
    return Math.max(0.0, Math.min(1.0, percent));
  }

  private void drawKnob() {
    if (!isAttached())
      return;
    Element knobElement = knobImage.getElement();
    int lineWidth = DOM.getElementPropertyInt(lineElement, "offsetWidth");
    int knobWidth = DOM.getElementPropertyInt(knobElement, "offsetWidth");
    int knobLeftOffset = (int) (lineLeftOffset + (getKnobPercent() * lineWidth) - (knobWidth / 2));
    knobLeftOffset = Math.min(knobLeftOffset, lineLeftOffset + lineWidth - (knobWidth / 2) - 1);
    DOM.setStyleAttribute(knobElement, "left", knobLeftOffset + "px");
  }

  /**
   * Draw the labels along the line.
   */
  private void drawLabels() {
    // Abort if not attached
    if (!isAttached()) {
      return;
    }

    // Draw the labels
    int lineWidth = DOM.getElementPropertyInt(lineElement, "offsetWidth");
    if (numLabels > 0) {
      // Create the labels or make them visible
      for (int i = 0; i <= numLabels; i++) {
        Element label = null;
        if (i < labelElements.size()) {
          label = labelElements.get(i);
        } else { // Create the new label
          label = DOM.createDiv();
          DOM.setStyleAttribute(label, "position", "absolute");
          DOM.setStyleAttribute(label, "display", "none");
          if (enabled) {
            DOM.setElementProperty(label, "className", "gwt-SliderBar-label");
          } else {
            DOM.setElementProperty(label, "className", "gwt-SliderBar-label-disabled");
          }
          DOM.appendChild(getElement(), label);
          labelElements.add(label);
        }

        // Set the label text
        double value = minValue + (getTotalRange() * i / numLabels);
        DOM.setStyleAttribute(label, "visibility", "hidden");
        DOM.setStyleAttribute(label, "display", "");
        DOM.setElementProperty(label, "innerHTML", formatLabel(value));

        // Move to the left so the label width is not clipped by the shell
        DOM.setStyleAttribute(label, "left", "0px");

        // Position the label and make it visible
        int labelWidth = DOM.getElementPropertyInt(label, "offsetWidth");
        int labelLeftOffset = lineLeftOffset + (lineWidth * i / numLabels)
            - (labelWidth / 2);
        labelLeftOffset = Math.min(labelLeftOffset, lineLeftOffset + lineWidth
            - labelWidth);
        labelLeftOffset = Math.max(labelLeftOffset, lineLeftOffset);
        DOM.setStyleAttribute(label, "left", labelLeftOffset + "px");
        DOM.setStyleAttribute(label, "visibility", "visible");
      }

      // Hide unused labels
      for (int i = (numLabels + 1); i < labelElements.size(); i++) {
        DOM.setStyleAttribute(labelElements.get(i), "display", "none");
      }
    } else { // Hide all labels
      for (Element elem : labelElements) {
        DOM.setStyleAttribute(elem, "display", "none");
      }
    }
  }

  /**
   * Draw the tick along the line.
   */
  private void drawTicks() {
    // Abort if not attached
    if (!isAttached()) {
      return;
    }

    // Draw the ticks
    int lineWidth = DOM.getElementPropertyInt(lineElement, "offsetWidth");
    if (numTicks > 0) {
      // Create the ticks or make them visible
      for (int i = 0; i <= numTicks; i++) {
        Element tick = null;
        if (i < tickElements.size()) {
          tick = tickElements.get(i);
        } else { // Create the new tick
          tick = DOM.createDiv();
          DOM.setStyleAttribute(tick, "position", "absolute");
          DOM.setStyleAttribute(tick, "display", "none");
          DOM.appendChild(getElement(), tick);
          tickElements.add(tick);
        }
        if (enabled) {
          DOM.setElementProperty(tick, "className", "gwt-SliderBar-tick");
        } else {
          DOM.setElementProperty(tick, "className",
              "gwt-SliderBar-tick gwt-SliderBar-tick-disabled");
        }
        // Position the tick and make it visible
        DOM.setStyleAttribute(tick, "visibility", "hidden");
        DOM.setStyleAttribute(tick, "display", "");
        int tickWidth = DOM.getElementPropertyInt(tick, "offsetWidth");
        int tickLeftOffset = lineLeftOffset + (lineWidth * i / numTicks)
            - (tickWidth / 2);
        tickLeftOffset = Math.min(tickLeftOffset, lineLeftOffset + lineWidth
            - tickWidth);
        DOM.setStyleAttribute(tick, "left", tickLeftOffset + "px");
        DOM.setStyleAttribute(tick, "visibility", "visible");
      }

      // Hide unused ticks
      for (int i = (numTicks + 1); i < tickElements.size(); i++) {
        DOM.setStyleAttribute(tickElements.get(i), "display", "none");
      }
    } else { // Hide all ticks
      for (Element elem : tickElements) {
        DOM.setStyleAttribute(elem, "display", "none");
      }
    }
  }

  /**
   * Reset the progress to constrain the progress to the current range and
   * redraw the knob as needed.
   */
  private void resetCurrentValue() {
    setCurrentValue(getCurrentValue());
  }

  /**
   * Slide the knob to a new location.
   * 
   * @param event the mouse event
   */
  private void slideKnob(Event event) {
    int x = DOM.eventGetClientX(event);
    if (x > 0) {
      int lineWidth = DOM.getElementPropertyInt(lineElement, "offsetWidth");
      int lineLeft = DOM.getAbsoluteLeft(lineElement);
      double percent = (double) (x - lineLeft) / lineWidth * 1.0;
      setCurrentValue(getTotalRange() * percent + minValue, true);
    }
  }

  /**
   * Start sliding the knob.
   * 
   * @param highlight true to change the style
   * @param fireEvent true to fire the event
   */
  private void startSliding(boolean highlight, boolean fireEvent) {
    if (highlight) {
      DOM.setElementProperty(lineElement, "className", "gwt-SliderBar-line gwt-SliderBar-line-sliding");
      DOM.setElementProperty(knobImage.getElement(), "className", "gwt-SliderBar-knob gwt-SliderBar-knob-sliding");
      AbstractImagePrototype.create(SliderBarImageResources.Instance.sliderSliding()).applyTo(knobImage);
    }
  }

  /**
   * Stop sliding the knob.
   * 
   * @param unhighlight true to change the style
   * @param fireEvent true to fire the event
   */
  private void stopSliding(boolean unhighlight, boolean fireEvent) {
    if (unhighlight) {
      DOM.setElementProperty(lineElement, "className", "gwt-SliderBar-line");
      DOM.setElementProperty(knobImage.getElement(), "className","gwt-SliderBar-knob");
      AbstractImagePrototype.create(SliderBarImageResources.Instance.sliderSliding()).applyTo(knobImage);
    }
  }

  /**
   * The timer used to continue to shift the knob as the user holds down one of
   * the left/right arrow keys. Only IE auto-repeats, so we just keep catching
   * the events.
   */
  private class KeyTimer extends Timer {
    private boolean firstRun = true;
    private int repeatDelay = 30;
    private boolean shiftRight = false;
    private int multiplier = 1;

    @Override
    public void run() {
      if (firstRun) {
        firstRun = false;
        startSliding(true, false);
      }
      // Slide the slider bar
      if (shiftRight) {
        setCurrentValue(curValue + multiplier * stepSize);
      } else {
        setCurrentValue(curValue - multiplier * stepSize);
      }
      // Repeat this timer until cancelled by keyup event
      schedule(repeatDelay);
    }

    public void schedule(int delayMillis, boolean shiftRight, int multiplier) {
      firstRun = true;
      this.shiftRight = shiftRight;
      this.multiplier = multiplier;
      super.schedule(delayMillis);
    }
  }
  
}
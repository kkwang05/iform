package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.dom.client.*;

/**
 * @author kw
 */
public abstract class FormatControl extends PopupPanel implements ClickHandler, HasValueChangeHandlers<String> {
  
  protected ButtonBase trigger;
  
  public FormatControl(ButtonBase trigger){
    super(true, true);
    addStyleName("FormatControl");
    this.trigger=trigger;
    this.trigger.addClickHandler(this);
  }

  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  public void onClick(ClickEvent event){
    popup();
  }

  public void popup(){
    int x=trigger.getAbsoluteLeft();
    int y=trigger.getAbsoluteTop();
    int h=trigger.getOffsetHeight();
    setPopupPosition(x,y+h);
    show();
  }
}
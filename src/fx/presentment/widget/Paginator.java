package fx.presentment.widget;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.model.*;

/**
 * A page turner that fires SelectionEvent<MetaForm>. Has a fixed viewport with a minimal set of 
 * navigation controls. 
 *
 * @author kw
 */
public class Paginator extends Composite implements HasSelectionHandlers<MetaForm>{
  interface MyBinder extends UiBinder<Widget, Paginator> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField InlineHTML label;
  @UiField Anchor first, previous, one, two, three, four, five, next, last, all;

  private Anchor[] viewport=new Anchor[5];
  private transient List<MetaForm> pages;
  private transient int currentPageIndex;
  private transient int currentWindowIndex;
  
  public Paginator(){
    initWidget(binder.createAndBindUi(this));
    viewport[0]=one;
    viewport[1]=two;
    viewport[2]=three;
    viewport[3]=four;
    viewport[4]=five;
  }

  // New Constructor
  // Only used for the PageBrowser control at this time of new introduction (1/30/2011).
  // The "ALL" link is not applicable to the consumer user, so it needs be hidden away.
  // Future enhancements may involve customization of the view port etc.
  public Paginator(boolean showAll){
    this();
    all.setVisible(showAll);
  }

  public void changeTextAppearance(String[] components){
    if(components.length>0)
      label.setHTML(components[0]);
    if(components.length>1)
      first.setHTML(components[1]);
    if(components.length>2)
      last.setHTML(components[2]);
  }
  
  public HandlerRegistration addSelectionHandler(SelectionHandler<MetaForm> handler){
    return addHandler(handler, SelectionEvent.getType());
  }

  public void reloadForm(Form theForm){
    reloadForm(theForm, true);
  }
  
  public void reloadForm(Form theForm, boolean designMode){
    if(theForm==null){
      currentPageIndex=-1;
      return;
    }
    
    // In design mode, show all pages
    // Otherwise, only show not-docked pages
    if(designMode)
      pages=theForm.pages();
    else{
      Form.DockView dv=theForm.dockView();
      if(dv!=null && !dv.isDockingEmpty())
        pages=theForm.getBody().undockedPages();
      else
        pages=theForm.pages();
    }
    if(pages!=null && pages.size()>0)
      currentPageIndex=0;
    else
      currentPageIndex=-1;
    
    // Update the viewport to show current selection
    if(!designMode)
      slide(false);
  }

  // Especially created for the PageBrowser node renderer. Used either on the palette or the
  // form editing canvas when rendering the EditableNode.
  public void reloadFromNode(MetaForm node){
    // When the node is orphaned (as after being cut), it has no root ancestor.
    while(node!=null && !node.isRoot())
      node=node.getParent();
    
    // Node is the form root now.
    pages=node==null? null: node.undockedPages();
    if(pages!=null && pages.size()>0)
      currentPageIndex=0;
    else
      currentPageIndex=-1;
    
    slide(false);
  }
  
  public void focusOnPage(MetaForm node){
    while(!node.isPage())
      node=node.getParent();
    MetaForm pageInFocus=node;
    if(!pages.contains(pageInFocus))
      pages.add(pageInFocus);
    currentPageIndex=pages.indexOf(pageInFocus);
    slide(false);
  }
  
  private void slide(boolean fireEvents){
    for(Anchor a: viewport)
      a.setVisible(false);

    // This condition can be created in the reloadFromNode() method.
    // If true, there is no point in going further.
    if(pages==null || pages.isEmpty())
      return;

    int i=currentPageIndex-2;
    if(i>0 && i+5>pages.size())
      i-=i+5-pages.size();
    if(i<0)
      i=0;
    currentWindowIndex=i;
    for(int j=0;i<pages.size() && j<5; i++,j++){
      String pageNumber=""+(i+1);
      if(i==currentPageIndex)
        pageNumber="<b>"+pageNumber+"</b>";
      viewport[j].setHTML(pageNumber);
      viewport[j].setVisible(true);
    }

    MetaForm inFocus;
    if(currentPageIndex<0){
      inFocus=pages.get(0).getParent();
      all.getElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
    }
    else{
      inFocus=pages.get(currentPageIndex);
      all.getElement().getStyle().setFontWeight(Style.FontWeight.NORMAL);
    }
    if(fireEvents){
      SelectionEvent.fire(this, inFocus);
    }
  }

  @UiHandler("previous")
  void clickedPrevious(ClickEvent event){
    if(currentPageIndex>0){
      currentPageIndex--;
      slide(true);
    }
  }
  @UiHandler("next")
  void clickedNext(ClickEvent event){
    if(currentPageIndex<pages.size()-1){
      currentPageIndex++;
      slide(true);
    }
  }
  @UiHandler("first")
  void clickedFirst(ClickEvent event){
    if(currentPageIndex!=0){
      currentPageIndex=0;
      slide(true);
    }
  }
  @UiHandler("last")
  void clickedLast(ClickEvent event){
    if(currentPageIndex!=pages.size()-1){
      currentPageIndex=pages.size()-1;
      slide(true);
    }
  }
  @UiHandler("one")
  void clickedOne(ClickEvent event){
    currentPageIndex=currentWindowIndex;
    slide(true);
  }
  @UiHandler("two")
  void clickedTwo(ClickEvent event){
    currentPageIndex=currentWindowIndex+1;
    slide(true);
  }
  @UiHandler("three")
  void clickedThree(ClickEvent event){
    currentPageIndex=currentWindowIndex+2;
    slide(true);
  }
  @UiHandler("four")
  void clickedFour(ClickEvent event){
    currentPageIndex=currentWindowIndex+3;
    slide(true);
  }
  @UiHandler("five")
  void clickedFive(ClickEvent event){
    currentPageIndex=currentWindowIndex+4;
    slide(true);
  }
  @UiHandler("all")
  void clickedAll(ClickEvent event){
    currentPageIndex=-1;
    slide(true);
  }
}
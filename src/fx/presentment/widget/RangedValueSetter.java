package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;

/**
 *
 * @author kw
 */
public class RangedValueSetter extends FormatControl implements ValueChangeHandler<Double> {
  
  private SliderBar slider;
  private CheckBox unsetCheckBox;
  
  private String key="margin";
  private String unit="px";
  
  public RangedValueSetter(ButtonBase trigger){
    super(trigger);
    
    FlowPanel fp=new FlowPanel();
    add(fp);
    fp.setStyleName("InternalPanel");
    
    Label lbl=new Label("Unset");
    lbl.addStyleName("InternalLabel");
    unsetCheckBox=new CheckBox();
    unsetCheckBox.addStyleName("InternalCheckBox");

    slider = new SliderBar(0.0, 50.0, 240);
    slider.setStepSize(1.0);
    slider.setCurrentValue(0.0);
    slider.setNumTicks(10);
    slider.setNumLabels(5);
    fp.add(slider);
    
    fp.add(lbl);
    fp.add(unsetCheckBox);
    
    slider.addValueChangeHandler(this);
    unsetCheckBox.addValueChangeHandler(new ValueChangeHandler<Boolean>(){
      public void onValueChange(ValueChangeEvent<Boolean> event){
        if(event.getValue()){
          slider.unset();
          ValueChangeEvent.<String>fire(RangedValueSetter.this, key+"=");
        }
      }
    });
  }

  public void setKeyAndUnit(String...kau){
    int i=0;
    if(i<kau.length)
      key=kau[i++];
    if(i<kau.length)
      unit=kau[i];
  }
  
  public void setMaxValue(double max){
    slider.setMaxValue(max);
  }
  
  public void onValueChange(ValueChangeEvent<Double> event){
    double v=event.getValue();
    String margin=key+"="+v+unit;
    ValueChangeEvent.<String>fire(this, margin);
  }

  public void setValue(String margin){
    if(margin==null || margin.trim().isEmpty()){
      slider.unset();
      unsetCheckBox.setValue(true, false);
      unsetCheckBox.setEnabled(false);
      return;
    }
    unsetCheckBox.setValue(false, false);
    unsetCheckBox.setEnabled(true);
    margin=margin.replaceAll(unit, "");
    double v=Double.parseDouble(margin);
    slider.setCurrentValue(v, false);
  }
}
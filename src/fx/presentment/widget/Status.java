package fx.presentment.widget;

import com.google.gwt.user.client.ui.HTML;

/**
 * A starting HTML-based status widget.
 * @author kw
 */
public class Status extends HTML {
  public void clear(){
    setHTML("");
  }

  public void info(String msg){
    setHTML("<i>"+msg+"</i>");
  }
  
  public void error(String msg){
    setHTML(msg);
  }
}


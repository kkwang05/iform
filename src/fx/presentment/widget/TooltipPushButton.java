package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;

/**
 * Tooltip-enabled, wrapped PushButton with UiBinder support.
 * 
 * @author kwang
 */
public class TooltipPushButton extends TooltipButton<PushButton> {
  protected PushButton wrappedButton(){
    return new PushButton();
  }
}
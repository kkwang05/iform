package fx.presentment.widget;

import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 * Tooltip-enabled button wrapper with UiBinder support.
 * 
 * @author kwang
 */
public abstract class TooltipButton<T extends CustomButton> extends Composite implements HasClickHandlers {
  private FocusPanel base;
  protected T button;
  
  private Tooltip tt;

  public TooltipButton(){
    base=new FocusPanel();
    initWidget(base);
    button=wrappedButton();
    button.addStyleName("TooltipButton");
    base.add(button);
  }
  
  public T getButton(){
    if(button==null)
      button=wrappedButton();
    return button;
  }
  
  protected abstract T wrappedButton();
  
  public HandlerRegistration addClickHandler(ClickHandler handler){
    return button.addClickHandler(handler);
  }

  public void setEnabled(boolean enable){
    button.setEnabled(enable);
  }
  public boolean isEnabled(){
    return button.isEnabled();
  }
  
  public void setUpFaceImage(ImageResource img){
    button.getUpFace().setImage(AbstractImagePrototype.create(img).createImage());
  }
  
  public void setDownFaceImage(ImageResource img){
    button.getDownFace().setImage(AbstractImagePrototype.create(img).createImage());
  }

  public void setTooltipText(String tip){
    tt=new Tooltip(base,tip);
  }
  
  public void setTooltipOffsetX(int ox){
    tt.setOffsetX(ox);
  }
  public void setTooltipOffsetY(int oy){
    tt.setOffsetY(oy);
  }
}

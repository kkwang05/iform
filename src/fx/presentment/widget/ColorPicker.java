package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.dom.client.*;

import fx.model.ColorList;
import fx.presentment.design.*;
import fx.presentment.design.repository.ColorBlock;
import fx.presentment.design.repository.ColorRepository;
import fx.presentment.event.GlobalEvent;

/**
 * A popup color picker tied to the system ColorRepository. This is used to replace the now deprecated
 * ColorRepositoryPalette, which was modeled after ImageRepositoryPalette.
 *
 * @author kw
 */
public class ColorPicker extends FormatControl implements DoubleClickHandler, ChangeHandler, SelectionHandler<EditableNode> {
  
  private FormCanvas colorPane;
  private Anchor moreLink;
  private SelectionOnlyModel selectionModel;
  
  private transient String selected;
  
  public ColorPicker(ButtonBase trigger){
    this(trigger, false);
  }
  
  public ColorPicker(ButtonBase trigger, boolean hideLink){
    super(trigger);
    
    FlowPanel fp=new FlowPanel();
    add(fp);
    fp.setStyleName("InternalPanel");
    
    colorPane=new FormCanvas();
    selectionModel=new SelectionOnlyModel(colorPane);
    fp.add(colorPane);
    moreLink=new Anchor("<<More/Change");
    moreLink.addStyleName("InternalLabel");
    fp.add(moreLink);
    moreLink.addClickHandler(new ClickHandler(){
      public void onClick(ClickEvent event){
        hide();
        GlobalEvent.fire(GlobalEvent.Kind.SHOW_COLOR_REPOSITORY, null);
      }
    });
    moreLink.setVisible(!hideLink);
  }

  public void popup(){
    int x=trigger.getAbsoluteLeft();
    int y=trigger.getAbsoluteTop();
    int h=trigger.getOffsetHeight();
    setPopupPosition(x,y+h);
    reload();
  }

  public void onDoubleClick(DoubleClickEvent event){
  }

  private HandlerRegistration changeRegistration;
  public void onChange(ChangeEvent event){
    reload();
    changeRegistration.removeHandler();
  }

  public void onSelection(SelectionEvent<EditableNode> event){
    hide();
    ColorBlock cb=(ColorBlock)event.getSelectedItem();
    String colorDef=cb.getColorDefinition();
    if(ColorBlock.NO_COLOR.equals(colorDef))
      colorDef="";
    ValueChangeEvent.<String>fire(this, colorDef);
  }

  private void reload(){
    colorPane.clear();
    selectionModel.clearSelection();
    ColorList currentColorList = ColorRepository.Instance.currentlyShowing();
    if(currentColorList==null){
      changeRegistration=ColorRepository.Instance.addChangeHandler(this);
      ColorRepository.Instance.refresh();
      return;
    }

    String defs=currentColorList.colorDefinitions;
    if(!defs.contains(ColorBlock.NO_COLOR))
      defs=ColorBlock.NO_COLOR+","+defs;
    if(selected!=null && !defs.contains(selected))
      defs=selected+","+defs;
    String[] colorDefs=defs.split("\\s*,\\s*");
    for(String color:colorDefs){
      ColorBlock node=new ColorBlock(color, true);
      colorPane.add(node);
      node.addDoubleClickHandler(this);
      selectionModel.come(node);
      node.addSelectionHandler(this);

      if(color.equals(selected))
        node.setSelected(true);
    }
    show();
  }
  
  public void setColor(String selection){
    if(selection==null || selection.isEmpty())
      selection=ColorBlock.NO_COLOR;
    selected=selection;
  }
}
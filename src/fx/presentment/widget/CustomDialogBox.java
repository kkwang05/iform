package fx.presentment.widget;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.user.client.ui.DialogBox;

/**
 * Escapable, resizable, etc.
 * @author kw
 */
public class CustomDialogBox extends DialogBox {
  private boolean escapable=true;
  private boolean hasShown;
  
  @Override
  public boolean onKeyDownPreview(char key, int modifiers) {
    if (escapable && key == KeyCodes.KEY_ESCAPE)
      CloseEvent.fire(this, null, false);
    return true;
  }
  public void setEscapable(boolean escapable){
    this.escapable=escapable;
  }
  
  @Override
  public void center(){
    if(hasShown){
      super.center();
      return;
    }
    // Workaround for the "first center() not centered" problem
    Scheduler.get().scheduleDeferred(new ScheduledCommand(){
      public void execute(){
        CustomDialogBox.super.center();
        hasShown=true;
      }
    });
  }
}

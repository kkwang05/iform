package fx.presentment.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

/**
 * @author kw
 */
public class RichTextEditor extends Composite implements HasValueChangeHandlers<String>{
  interface MyBinder extends UiBinder<Widget, RichTextEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  private static final RichTextArea.FontSize[] fontSizesConstants = new RichTextArea.FontSize[] {
    RichTextArea.FontSize.XX_SMALL,
    RichTextArea.FontSize.X_SMALL,
    RichTextArea.FontSize.SMALL,
    RichTextArea.FontSize.MEDIUM,
    RichTextArea.FontSize.LARGE,
    RichTextArea.FontSize.X_LARGE,
    RichTextArea.FontSize.XX_LARGE};

  @UiField TooltipToggleButton bold,italic,underline,strikethrough, subscript,superscript;
  @UiField TooltipPushButton indent,outdent,justifyLeft,justifyCenter, justifyRight, hr, ol, ul;
  @UiField TooltipPushButton insertImage;
  @UiField ListBox colors, fonts, fontSizes;
  
  @UiField ComplexPanel toolbar, modePanel;
  @UiField RadioButton richMode, rawMode;
  
  @UiField RichTextArea rta;

  private RichTextArea.Formatter formatter;
  private Prompt imagePrompt=new Prompt ();
  private boolean richEditingMode=true;
  
  public RichTextEditor(){
    initWidget(binder.createAndBindUi(this));
    formatter=rta.getFormatter();
    richMode.setValue(richEditingMode);
    rawMode.setValue(!richEditingMode);
    toolbar.setVisible(richEditingMode);
  }

  public void setPlainTextOnly(boolean plainTextOnly){
    if(plainTextOnly && richEditingMode){
      richEditingMode=false;
      toolbar.setVisible(false);
      modePanel.setVisible(false);
      rta.getElement().getStyle().setMarginBottom(1.5, Unit.EM);
    }
  }
  
  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler){
    return addHandler(handler, ValueChangeEvent.getType());
  }

  public void setEnabled(boolean enabled){
    rta.setEnabled(enabled);
  }
  public void setReadOnly(boolean ro){
    rta.setEnabled(!ro);
  }
  public void setFocus(boolean focused){
    rta.setFocus(focused);
  }
  
  private transient String currentContent;
  public void setValue(String value){
    if(richEditingMode)
      rta.setHTML(value);
    else
      rta.setText(value);
    
    currentContent=value;
    richMode.setValue(richEditingMode);
    rawMode.setValue(!richEditingMode);
    toolbar.setVisible(richEditingMode);
  }
  
  private int toolbarHeight=0, rtaWidth, rtaHeight;
  public void toggleMode(){
    if(toolbarHeight==0){
      toolbarHeight=toolbar.getOffsetHeight();
      rtaWidth=rta.getOffsetWidth();
      rtaHeight=rta.getOffsetHeight();
    }
    richEditingMode=!richEditingMode;
    toolbar.setVisible(richEditingMode);
    if(richEditingMode){
      rta.setHTML(rta.getText());
      rta.setPixelSize(rtaWidth, rtaHeight);
    }
    else
    {
      rta.setText(rta.getHTML());
      rta.setPixelSize(rtaWidth, rtaHeight+toolbarHeight+4);
    }
  }
  
  @UiHandler("colors")
  void changeColor(ChangeEvent event){
    formatter.setForeColor(colors.getValue(colors.getSelectedIndex()));
    colors.setSelectedIndex(0);
  }
  @UiHandler("fonts")
  void changeFont(ChangeEvent event){
    formatter.setFontName(fonts.getValue(fonts.getSelectedIndex()));
    fonts.setSelectedIndex(0);
  }
  @UiHandler("fontSizes")
  void changeFontSize(ChangeEvent event){
    formatter.setFontSize(fontSizesConstants[fontSizes.getSelectedIndex() - 1]);
    fontSizes.setSelectedIndex(0);
  }

  @UiHandler("bold")
  void toBold(ClickEvent event) {
    formatter.toggleBold();
  }
  @UiHandler("italic")
  void toItalic(ClickEvent event) {
    formatter.toggleItalic();
  }
  @UiHandler("underline")
  void toUnderline(ClickEvent event) {
    formatter.toggleUnderline();
  }
  @UiHandler("strikethrough")
  void toStrikethrough(ClickEvent event) {
    formatter.toggleStrikethrough();
  }
  @UiHandler("subscript")
  void toSubscript(ClickEvent event) {
    formatter.toggleSubscript();
  }
  @UiHandler("superscript")
  void toSuperscript(ClickEvent event) {
    formatter.toggleSuperscript();
  }

  @UiHandler("indent")
  void indent(ClickEvent event) {
    formatter.rightIndent();
  }
  @UiHandler("outdent")
  void outdent(ClickEvent event) {
    formatter.leftIndent();
  }
  @UiHandler("justifyLeft")
  void justifyLeft(ClickEvent event) {
    formatter.setJustification(RichTextArea.Justification.LEFT);
  }
  @UiHandler("justifyCenter")
  void justifyCenter(ClickEvent event) {
    formatter.setJustification(RichTextArea.Justification.CENTER);
  }
  @UiHandler("justifyRight")
  void justifyRight(ClickEvent event) {
    formatter.setJustification(RichTextArea.Justification.RIGHT);
  }
  @UiHandler("hr")
  void hr(ClickEvent event) {
    formatter.insertHorizontalRule();
  }
  @UiHandler("ol")
  void ol(ClickEvent event) {
    formatter.insertOrderedList();
  }
  @UiHandler("ul")
  void ul(ClickEvent event) {
    formatter.insertUnorderedList();
  }
  @UiHandler("insertImage")
  void insertImage(ClickEvent event) {
    imagePrompt.prompt();
  }
  @UiHandler("removeFormat")
  void removeFormat(ClickEvent event) {
    //formatter.removeFormat();
    rta.setHTML(rta.getText());
  }

  @UiHandler("rta")
  void rtaClicked(ClickEvent event) {
    updateStatus();
  }
  @UiHandler("rta")
  void rtaKeyed(KeyUpEvent event) {
    updateStatus();
  }
  
  private void updateStatus() {
    bold.setDown(formatter.isBold());
    italic.setDown(formatter.isItalic());
    underline.setDown(formatter.isUnderlined());
    subscript.setDown(formatter.isSubscript());
    superscript.setDown(formatter.isSuperscript());
    strikethrough.setDown(formatter.isStrikethrough());
  }

  @UiHandler("richMode")
  void toggle1(ValueChangeEvent<Boolean> evt){
    toggleMode();
  }
  @UiHandler("rawMode")
  void toggle2(ValueChangeEvent<Boolean> evt){
    toggleMode();
  }
  
  @UiHandler("rta")
  void rtaBlurred(BlurEvent event) {
    String html=richEditingMode? rta.getHTML(): rta.getText();
    if(!html.equals(currentContent)){
      ValueChangeEvent.<String>fire(this, html);
      currentContent=html;
    }
  }

  class Prompt extends PopupPanel implements ValueChangeHandler<String>, KeyUpHandler{
    TextBox input;
    Prompt(){
      super(true);
      add(input=new TextBox());
      input.setVisibleLength(48);
      input.setText("http://");
      input.addValueChangeHandler(this);
      input.addKeyUpHandler(this);
    }
    public void prompt(){
      super.showRelativeTo(insertImage);
      input.selectAll();
      input.setFocus(true);
    }
    
    public void onKeyUp(KeyUpEvent event){
      if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
        insert();
      else if(event.getNativeKeyCode()==KeyCodes.KEY_ESCAPE)
        hide();
    }
    public void onValueChange(ValueChangeEvent<String> event){
      insert();
    }
    
    private void insert(){
      this.hide();
      String url=input.getText();
      // validation???
      formatter.insertImage(url);
    }
  }
}
package fx.presentment.widget;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Tooltip upport for widgets that listen to mouse events. 
 *
 * @author kwang
 */
public class Tooltip extends PopupPanel implements MouseOverHandler, MouseOutHandler {
  
  /**
   * A Widget implements this interface to supply dynamic tooltip texts.
   */
  public interface Supplier{
    String tooltipText();
    Widget tooltipSource();
  }
  
  private static final int DEFAULT_DELAY = 500;
  private static final int DEFAULT_OFFSET_X = 10;
  private static final int DEFAULT_OFFSET_Y = 34;

  private int offsetX=DEFAULT_OFFSET_X;
  private int offsetY=DEFAULT_OFFSET_Y;
  
  private Widget source;
  private Label content;
  private Timer start,end;

  public Tooltip(HasAllMouseHandlers source, String text) {
    super(true);
    this.source = (Widget) source;
    this.content = new Label(text);
    add(content);
    
    source.addMouseOverHandler(this);
    source.addMouseOutHandler(this);
    
    start = new Timer() {
      public void run() {
        showUp();
      }
    };
    end = new Timer() {
      public void run() {
        hide();
      }
    };
    setStyleName("TooltipPopup");
  }
  private void showUp() {
    int left,top;
    if(source instanceof Supplier){
      content.setText(((Supplier)source).tooltipText());
      Widget s=((Supplier)source).tooltipSource();
      left = s.getAbsoluteLeft() + offsetX;
      top = s.getAbsoluteTop() + offsetY;
    }
    else{
      left = source.getAbsoluteLeft() + offsetX;
      top = source.getAbsoluteTop() + offsetY;
    }
    setPopupPosition(left, top);
    super.show();
    end.schedule(10*DEFAULT_DELAY);	 
  }

  public void onMouseOver(MouseOverEvent evt) {
    end.cancel();	 
    start.schedule(DEFAULT_DELAY);
  }
  public void onMouseOut(MouseOutEvent evt) {
    start.cancel();
    hide();
  }

  public void setText(String tip){
    content.setText(tip);
  }
  public void setOffsetX(int ox){
    offsetX=ox;
  }
  public void setOffsetY(int oy){
    offsetY=oy;
  }
  
}
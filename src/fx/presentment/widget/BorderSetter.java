package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.dom.client.*;

/**
 *
 * @author kw
 */
public class BorderSetter extends FormatControl implements ChangeHandler, ValueChangeHandler<String> {
  
  private Button colorButton;
  private ColorPicker colorCtrl;
  private ListBox widthCtrl;
  
  public BorderSetter(ButtonBase trigger){
    super(trigger);
    
    FlowPanel fp=new FlowPanel();
    add(fp);
    fp.setStyleName("InternalPanel");
    
    colorButton = new Button("Border Color");
    fp.add(colorButton);
    colorButton.setStyleName("InternalLabel");
    colorCtrl=new ColorPicker(colorButton,true);
    colorCtrl.addValueChangeHandler(this);
    
    widthCtrl=new ListBox();
    Label lbl=new InlineHTML("Width:");
    lbl.setStyleName("InternalLabel");
    widthCtrl.setStyleName("InternalLabel");
    fp.add(lbl);
    fp.add(widthCtrl);
    
    widthCtrl.addItem("None");
    for(int i=1; i<8; i++)
      widthCtrl.addItem(i+"px");
    widthCtrl.addChangeHandler(this);
  }

  public void onChange(ChangeEvent evt){
    ValueChangeEvent.<String>fire(this, borderValue());
  }
  private transient String colorDef="black";
  public void onValueChange(ValueChangeEvent<String> event){
    colorDef=event.getValue();
    if("".equals(colorDef))
        colorDef="black";
    ValueChangeEvent.<String>fire(this, borderValue());
  }
  protected String borderValue(){
    int size=widthCtrl.getSelectedIndex();
    if(size==0)
      return "";
    return widthCtrl.getValue(size)+" solid "+colorDef;
  }
  
  public void setBorderValue(String border){
    if(border==null){
      widthCtrl.setSelectedIndex(0);
      return;
    }
    int borderWidth=0;
    int idx=border.indexOf("px");
    if(idx>0)
      borderWidth=Integer.parseInt(border.substring(0, idx));
    widthCtrl.setSelectedIndex(borderWidth);
    
    idx=border.lastIndexOf(" ");
    if(idx>0){
      colorCtrl.setColor(border.substring(idx).trim());
    }
  }
}
package fx.presentment.widget;

import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.OpenEvent;
import com.google.gwt.event.logical.shared.OpenHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.InlineHTML;

public class CustomDPHeader extends FlowPanel implements HasText,
    OpenHandler<DisclosurePanel>, CloseHandler<DisclosurePanel> {
  private final Image iconImage;
  private final HTML headingText;
  private final ImageResource openImage, closedImage;

  public CustomDPHeader(ImageResource openImage, ImageResource closedImage, String text) {
    iconImage = new Image(openImage);
    headingText = new InlineHTML(text);
    add(iconImage);
    add(headingText);
    this.openImage = openImage;
    this.closedImage = closedImage;

    DOM.setStyleAttribute(getElement(), "margin", "0px");
    DOM.setStyleAttribute(iconImage.getElement(), "position", "relative");
    DOM.setStyleAttribute(iconImage.getElement(), "bottom", "-3px");
    DOM.setStyleAttribute(headingText.getElement(), "marginLeft", "8px");
  }

  public void bind(DisclosurePanel host) {
    host.setHeader(this);
    host.addOpenHandler(this);
    host.addCloseHandler(this);
  }

  public final void onClose(CloseEvent<DisclosurePanel> event) {
    iconImage.setResource(closedImage);
  }

  public final void onOpen(OpenEvent<DisclosurePanel> event) {
    iconImage.setResource(openImage);
  }

  public final String getText() {
    return headingText.getText();
  }

  public final void setText(String text) {
    headingText.setText(text);
  }
}
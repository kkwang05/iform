package fx.presentment.widget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class ComboBox extends Composite implements HasValueChangeHandlers<String> {
  interface MyBinder extends UiBinder<Widget, ComboBox > { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField TextBox input;
  @UiField ListBox list;

  private int maxItems=Integer.MAX_VALUE; 
  
  public ComboBox(){
    initWidget(binder.createAndBindUi(this));
  }
  
  public void setItems(String[] items){
    for(String s:items)
      list.addItem(s);
  }
  
  public String getItems(){
    StringBuffer sb=new StringBuffer();
    for(int i=0;i<list.getItemCount();i++)
      sb.append(list.getItemText(i)).append(",");
    return sb.toString();
  }
  
  @Override
  public void setWidth(String w){
    input.setWidth(w);
    list.setWidth(w);
  }
  
  public void setMaxItemCount(int maxItems) {
    list.setVisibleItemCount(maxItems);
    this.maxItems=maxItems;
  }
  
  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }

  @UiHandler("input")
  void enteringInput(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(input,null);
  }
  
  @UiHandler("input")
  void enteredInput(ValueChangeEvent<String> e){
    int idx=list.getSelectedIndex();
    String entered=input.getValue().trim();
    if(!entered.isEmpty()){
      if(idx<0 && list.getItemCount()<maxItems){
        list.addItem(input.getValue());
        ValueChangeEvent.<String>fire(this, getItems());
      }
      else if(idx>=0){
        list.setItemText(idx, entered);
        ValueChangeEvent.<String>fire(this, getItems());
      }
      input.setValue("");
    }
    else if(idx>=0){
      list.removeItem(idx);
      ValueChangeEvent.<String>fire(this, getItems());
    }
  }

  @UiHandler("list")
  void selected(ChangeEvent evt){
    int idx=list.getSelectedIndex();
    if(idx>=0){
      input.setValue(list.getItemText(idx));
      input.setFocus(true);
    }
  }
}
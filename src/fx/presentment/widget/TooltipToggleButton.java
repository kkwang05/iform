package fx.presentment.widget;

import com.google.gwt.user.client.ui.*;

/**
 * Tooltip-enabled, wrapped ToggleButton with UiBinder support.
 * 
 * @author kwang
 */
public class TooltipToggleButton extends TooltipButton<ToggleButton> {
  
  protected ToggleButton wrappedButton(){
    return new ToggleButton();
  }
  
  public boolean isDown(){
    return button.isDown();
  }
  
  public void setDown(boolean down){
    button.setDown(down);
  }
}
package fx.presentment.script;

import java.util.*;

import com.google.gwt.user.client.ui.Widget;

import fx.model.FormEntry;
import fx.model.MetaForm;
import fx.model.FormData;
import fx.model.ValueSet;
import fx.presentment.FormEntryPresenter;
import fx.presentment.event.FormEntryEvent;
import fx.presentment.renderer.Renderer;
import fx.presentment.widget.TabularFlowPanel;

/**
 * Basic implementation of the FormScript interpreter. Supports a JavaScript-like syntax with the following features:
 * 
 * Expressions:<br/>
 *  <li> Dynamic typing with the following primitive types: BOOL, ENUM/CODED(null), STR, and NUM 
 *      (See <code>SyntaxParser.Literal</code>);
 *  <li> Form-wide declared node variables, and referential access to the following objects: current FormEntryEvent, 
 *      current node, current entry, and current user (see <code>SyntaxParser.Singular</code>);
 *  <li> Unary and binary expressions with arithmetic, comparison, and logic operators 
 *      (see <code>SyntaxParser.Negation</code>, <code>SyntaxParser.Binary</code>);
 *  <li> Parenthesized expressions for precedence and constructing parameter list(<code>SyntaxParser.Enclosure</code>).
 *  
 * Statements:<br/>
 *  <li> Block-scoped variable declaration (<code>SyntaxParser.Declaration</code>);
 *  <li> Assignment (<code>SyntaxParser.Assignment</code>);
 *  <li> Built-in functions (<code>SyntaxParser.Function</code>);
 *  <li> Methods on selected Object types (<code>SyntaxParser.Method</code>);
 *  <li> Compound and conditional statements.
 *  
 *  The design goal is to keep the language feature set limited for ease of use; and instead encapsulate rich business 
 *  logic and functional behaviors in specialized node types. 
 *  
 * @author kw
 */
public class Interpreter {

  protected static class Variable {
    String name;
    Object value;
    Variable(String name, Object value){
      this.name=name;
      this.value=value;
    }
  }
  
  public static Interpreter getInstance(FormEntryPresenter presenter){
    return new Interpreter(presenter);
  }
  
  protected FormEntryPresenter presenter;
  
  private transient FormEntryEvent thisEvent;
  private transient MetaForm thisNode;
  private transient FormEntry thisEntry;
  
  protected Interpreter(FormEntryPresenter presenter){
    this.presenter=presenter;
  }
  
  public void contextSwitched(){
    thisEvent=null;
    thisNode=null;
    thisEntry=presenter.context().entry;
    MetaForm root=thisEntry.theForm.getBody();
    if(root.script!=null && !root.script.trim().isEmpty()){
      System.out.println(">>> onLoad invoked...");
      presenter.context().pushScope();
      execScript(root.script);
    }
  }
  
  public void eventReceived(FormEntryEvent evt){
    thisEvent=evt;
    thisNode=evt.source;
    if(thisNode!=null && thisNode.script!=null && !thisNode.script.trim().isEmpty()){
      System.out.println(">>> onEvent invoked...");
      presenter.context().pushScope();
      execScript(thisNode.script);
      presenter.context().popScope();
    }
  }
  
  protected void execScript(String script){
    System.out.println("(Parsing......)");
    Vector<SyntaxParser.Statement> ast = SyntaxParser.parse(script);
    
    System.out.println("(Executing......)");
    int i=0;
    for(SyntaxParser.Statement s : ast){
      System.out.println(i++ +") "+s.toString(""));
      Object v=exec(s);
      if(!interpAsBool(v))
        break;
    }
  }

  //
  // Leaf level interpretations
  //
  protected boolean interpAsBool(Object v){
    if(v==null)
      return false;
    if(v instanceof String)
      return !"false".equalsIgnoreCase((String)v) && !"no".equalsIgnoreCase((String)v) && !"null".equalsIgnoreCase((String)v);
    if(v instanceof Boolean)
      return ((Boolean) v).booleanValue();
    if(v instanceof Number)
      return !Double.isNaN(((Number)v).doubleValue()) 
          && ((Number)v).intValue()>0;
    return true;
  }
  protected Number interpAsNumber(Object v){
    if(v instanceof Number)
      return (Number)v;
    if(v instanceof String){
      String n=(String)v;
      if(n.matches("\\d+"))
        return Long.parseLong(n);
      else if(n.matches("\\d+\\.\\d*"))
        return Double.parseDouble(n);
    }
    return Double.NaN;
  }

  //
  // Scaffolding
  //
  public Object eval(SyntaxParser.Expression expr){
    if(expr instanceof SyntaxParser.Literal){
      SyntaxParser.Literal v=(SyntaxParser.Literal)expr;
      if(v.number!=null)
        return interpAsNumber(v.number);
      else if("null".equals(v.coded))
        return null;
      else if(v.coded!=null)
        return interpAsBool(v.coded);
      return v.string;
    }
    else if(expr instanceof SyntaxParser.Singular){
      return evalSingular((SyntaxParser.Singular)expr);
    }
    else if(expr instanceof SyntaxParser.Binary){
      return evalBinary((SyntaxParser.Binary)expr);
    }
    else if(expr instanceof SyntaxParser.Negation){
      SyntaxParser.Negation neg=(SyntaxParser.Negation)expr;
      Object v=eval(neg.innerExpression);
      boolean result=!interpAsBool(v);
      return Boolean.valueOf(result);
    }
    else if(expr instanceof SyntaxParser.Enclosure){
      SyntaxParser.Enclosure en=(SyntaxParser.Enclosure)expr;
      return eval(en.innerExpressions.get(0));
    }
    throw new RuntimeException("Interpreter.eval(): Not implemented: "+expr);
  }  
  public List<Object> eval(List<SyntaxParser.Expression> exprs){
    List<Object> result=new ArrayList<Object>(exprs.size());
    for(SyntaxParser.Expression e: exprs)
      result.add(eval(e));
    return result;
  }
  
  public Object exec(SyntaxParser.Statement stmt){
    System.out.println("Exec'ing "+stmt);
    if(stmt instanceof SyntaxParser.Declaration)
      return execDeclaration((SyntaxParser.Declaration)stmt);
    else if(stmt instanceof SyntaxParser.Assignment)
      return execAssignment((SyntaxParser.Assignment)stmt);
    else if(stmt instanceof SyntaxParser.Method)
      return execMethod((SyntaxParser.Method)stmt);
    else if(stmt instanceof SyntaxParser.Function)
      return execFunction((SyntaxParser.Function)stmt);
    else if(stmt instanceof SyntaxParser.CompoundStatement){
      SyntaxParser.CompoundStatement bs=(SyntaxParser.CompoundStatement)stmt;
      Object result=Boolean.TRUE;
      presenter.context().pushScope();
      for(SyntaxParser.Statement s: bs.getSubstatements()){
        result=exec(s);
        if(!interpAsBool(result))
          break;
      }
      presenter.context().popScope();
      return result;
    }
    else if(stmt instanceof SyntaxParser.ConditionalStatement){
      SyntaxParser.ConditionalStatement cs=(SyntaxParser.ConditionalStatement)stmt;
      boolean conditionResult=interpAsBool(eval(cs.condition));
      if(conditionResult){
        return exec(cs.s1);
      }
      else if(cs.s2!=null){
        return exec(cs.s2);
      }
      else {
        return "skipped";
      }
    }
    throw new RuntimeException("Interpreter.exec(): Not implemented: "+stmt);
  }

  //
  // The implementation
  //
  
  /**
   * Resolving an object reference
   */
  protected Object bind(SyntaxParser.Singular expr){
    if(expr.objectReference.equalsIgnoreCase("this.event"))
      return thisEvent;
    else if(expr.objectReference.equalsIgnoreCase("this.node"))
      return thisNode;
    else if(expr.objectReference.equalsIgnoreCase("this.entry"))
      return thisEntry;
    else if(expr.objectReference.equalsIgnoreCase("this.user"))
      return null;
    // Then it must be referring to a node or a variable
    else if(presenter.context().node(expr.objectReference)!=null)
      return presenter.context().node(expr.objectReference);
    else
      return presenter.context().fetchVariable(expr.objectReference);
  }

  /**
   * Recursively resolving an attribute on an object; the result can be another object. 
   * The attribute string can be dot-separated or indexed (array brackets).
   */
  protected Object recursiveEval(Object objRef, String attr){
    int secondPeriodIdx=attr.indexOf('.',1);
    // I. Degenerate case
    if(secondPeriodIdx<0){  
      int componentIdx=attr.indexOf('[',2);
      String component=null;
      Object value=null;
      if(componentIdx>0){
        component=attr.substring(componentIdx);
        attr=attr.substring(0, componentIdx);
      }
      if(objRef instanceof MetaForm){
        MetaForm node=(MetaForm)objRef;
        if(attr.equalsIgnoreCase(".vs") || attr.equalsIgnoreCase(".choices") || attr.equalsIgnoreCase(".choice"))
          value=node.valueSet;
        if(value==null || component==null)
          return value;
        if(value instanceof ValueSet){
          return ((ValueSet)value).id+component;
        }
      }
      throw new RuntimeException("Not implemented: "+objRef.getClass().getName()+attr);
    }
    // II. Needs Recursion
    else {  
      String intermediateAttr=attr.substring(0, secondPeriodIdx);
      Object intermediateObjRef=recursiveEval(objRef, intermediateAttr);
      if(intermediateObjRef!=null)
        return recursiveEval(intermediateObjRef, attr.substring(secondPeriodIdx));
      else
        return null;
    }
  }
  
  /**
   * Evaluating a SyntaxParser.Singular expression. Only a limited set of object types and corresponding attributes
   * are supported. 
   */
  protected Object evalSingular(SyntaxParser.Singular expr){
    Object objRef=bind(expr);
    if(objRef instanceof Variable)
      objRef=((Variable)objRef).value;
    if(objRef==null)
      throw new RuntimeException("Object not found: "+expr);
    if(expr.attribute==null)
      return objRef;
    
    if(objRef instanceof FormEntryEvent){
      FormEntryEvent evt=(FormEntryEvent)objRef;
      if(expr.attribute.equalsIgnoreCase(".value"))
        return evt.data;
      if(expr.attribute.equalsIgnoreCase(".expired"))
        return evt.expired;
    }
    
    else if(objRef instanceof MetaForm){
      MetaForm node=(MetaForm)objRef;
      if(expr.attribute.equalsIgnoreCase(".type"))
        return node.typeCode;
      FormData dynaNode=presenter.context().nodeData(node);
      if(expr.attribute.equalsIgnoreCase(".text") || expr.attribute.equalsIgnoreCase(".display"))
        return dynaNode.text;
      if(expr.attribute.equalsIgnoreCase(".settings"))
        return dynaNode.settings;
      if(expr.attribute.equalsIgnoreCase(".style"))
        return dynaNode.renderingHints;
      if(expr.attribute.equalsIgnoreCase(".hidden"))
        return dynaNode.hidden;
      if(expr.attribute.equalsIgnoreCase(".value")|| expr.attribute.equalsIgnoreCase(".answer"))
        return dynaNode.value;
      // Recursion candidate
      return recursiveEval(objRef, expr.attribute);
    }
    
    else if(objRef instanceof FormEntry){
      FormEntry fe=(FormEntry)objRef;
      if(expr.attribute.equalsIgnoreCase(".status"))
        return fe.status;
      else if(expr.attribute.equalsIgnoreCase(".readOnly"))
        return fe.isReadOnly();
    }
    
    else if(objRef instanceof fx.model.User){
    }
    throw new RuntimeException("Not implemented: "+expr);
  }

  /**
   * Arithmetic, comparison, and logic computation.
   */
  protected Object evalBinary(SyntaxParser.Binary expr){
    Object lv=eval(expr.lhs);
    Object rv=eval(expr.rhs);

    // Get logical formulas off the table
    if(expr.operator==LexParser.TT.AND)
      return interpAsBool(lv) && interpAsBool(rv);
    if(expr.operator==LexParser.TT.OR)
      return interpAsBool(lv) || interpAsBool(rv);
    
    // Arithmetic and concatenations
    Number l=interpAsNumber(lv);
    Number r=interpAsNumber(rv);
    boolean bothAreNumbers=!(Double.isNaN(l.doubleValue()) || Double.isNaN(r.doubleValue()));
    boolean bothAreIntegers=(l instanceof Long || l instanceof Short || l instanceof Integer) && 
                            (r instanceof Long || r instanceof Short || r instanceof Integer);
    if(bothAreNumbers && expr.operator==LexParser.TT.MINUS)
      return bothAreIntegers? (long)(l.longValue()-r.longValue()) : l.doubleValue()-r.doubleValue();
    if(bothAreNumbers && expr.operator==LexParser.TT.TIMES)
      return bothAreIntegers? (long)(l.longValue()*r.longValue()) : l.doubleValue()*r.doubleValue();
    if(bothAreNumbers && expr.operator==LexParser.TT.DVDB)
      return bothAreIntegers? l.longValue()/r.longValue() : l.doubleValue()/r.doubleValue();

    if(expr.operator==LexParser.TT.PLUS){
      if(bothAreNumbers)
        return bothAreIntegers? (long)(l.longValue()+r.longValue()) : l.doubleValue()+r.doubleValue();
      else if(lv==null)
        return rv;
      else if(rv==null)
        return lv;
      else 
        return lv.toString()+rv.toString();
    }
    
    // Encountered bad grammar 
    if(expr.isArithmetic())
      return Double.NaN;
      
    // Comparison and (n)equality
    if(bothAreNumbers && expr.operator==LexParser.TT.GTEQ)
      return bothAreIntegers? l.longValue()>=r.longValue() : l.doubleValue()>=r.doubleValue();
    if (bothAreNumbers && expr.operator == LexParser.TT.GT)
      return bothAreIntegers ? l.longValue() > r.longValue() : l.doubleValue() > r.doubleValue();
    if(bothAreNumbers && expr.operator==LexParser.TT.LTEQ)
      return bothAreIntegers? l.longValue()<=r.longValue() : l.doubleValue()<=r.doubleValue();
    if (bothAreNumbers && expr.operator == LexParser.TT.LT)
      return bothAreIntegers ? l.longValue() < r.longValue() : l.doubleValue() < r.doubleValue();

    if(expr.operator==LexParser.TT.EQ){
      if(lv==rv)
        return true;
      else if(lv==null || rv==null)
        return false;
      else if(bothAreNumbers)
        return bothAreIntegers? l.longValue()==r.longValue() : l.doubleValue()==r.doubleValue();
      else if(lv instanceof Boolean)
        return interpAsBool(lv)==interpAsBool(rv);
      else
        return lv.toString().equals(rv.toString());
    }
    if(expr.operator==LexParser.TT.NEQ){
      SyntaxParser.Binary neg=new SyntaxParser.Binary(expr.lhs, LexParser.TT.EQ, expr.rhs);
      return evalBinary(neg)==Boolean.FALSE;
    }
    return false;
  }

  /**
   * Create new variables (Declaration=a variable name + optional value expression)
   */
  protected Object execDeclaration(SyntaxParser.Declaration stmt){
    Object value=null;
    if(stmt.value!=null)
      value=eval(stmt.value);
    presenter.context().insertVariable(stmt.var, new Variable(stmt.var,value));
    return Boolean.TRUE;
  }
  
  /**
   * Process assignment instructions, see also <code>evalSingular()</code>.
   * (Assignment=Singular+value expression)
   */
  protected Object execAssignment(SyntaxParser.Assignment stmt){
    Object objRef=bind(stmt.subject);
    if(objRef==null)
      throw new RuntimeException("Assignment: Subject not found: "+stmt.subject);
    
    Variable var=null;
    if(objRef instanceof Variable){
      var=(Variable)objRef;
      objRef=var.value;
    }
    
    Object current=evalSingular(stmt.subject);
    Object tobe=eval(stmt.value);
    if(current==tobe || current!=null && current.equals(tobe))
      return Boolean.TRUE;
    
    String attr=stmt.subject.attribute;
    if(objRef instanceof FormEntryEvent){
      FormEntryEvent evt=(FormEntryEvent)objRef;
      if(".expired".equalsIgnoreCase(attr)){
        evt.expired=interpAsBool(tobe);
        return Boolean.TRUE;
      }
    }
    else if(objRef instanceof MetaForm){
      FormData dynaNode=presenter.context().nodeData((MetaForm)objRef);
      if(".text".equalsIgnoreCase(attr) || ".display".equalsIgnoreCase(attr)){
        if(tobe==null)
          dynaNode.text=null;
        else
          dynaNode.text=tobe.toString();
        return Boolean.TRUE;
      }
      if(".settings".equalsIgnoreCase(attr)){
        if(tobe==null)
          dynaNode.settings=null;
        else
          dynaNode.settings=tobe.toString();
        return Boolean.TRUE;
      }
      if(".style".equalsIgnoreCase(attr)){
        if(tobe==null)
          dynaNode.renderingHints=null;
        else
          dynaNode.renderingHints=tobe.toString();
        return Boolean.TRUE;
      }
      if(".hidden".equalsIgnoreCase(attr)){
        dynaNode.hidden=interpAsBool(tobe);
        return Boolean.TRUE;
      }
      if(".value".equalsIgnoreCase(attr) || ".answer".equalsIgnoreCase(attr)){
        if(tobe==null)
          dynaNode.value=null;
        else
          dynaNode.value=tobe.toString();
        return Boolean.TRUE;
      }
    }
    else if(objRef instanceof FormEntry){
      FormEntry fe=(FormEntry)objRef;
      if(".status".equalsIgnoreCase(attr)){
        fe.status=interpAsNumber(tobe).shortValue();
        return Boolean.TRUE;
      }
    }
    else if(objRef instanceof fx.model.User){
    }
    else if(var!=null && attr==null){
      var.value=tobe;
      return Boolean.TRUE;
    }
    
    throw new RuntimeException("Assignment: Subject Readonly or mismatch: "+stmt.subject+", val="+tobe);
  }

  /**
   * Carry out method invocations on selected objects (entry or node). The extension mechanism is via the BusinessObject
   * attached to the node.
   * (Method=Singular object + Function)
   */
  protected Object execMethod(SyntaxParser.Method stmt){
    Object obj=eval(stmt.obj);
    
    if(obj instanceof FormEntry){
      if(stmt.funcName.equalsIgnoreCase(".save")){
        presenter.saveEntry();
        return Boolean.TRUE;
      }
    }
    
    else if(obj instanceof MetaForm){
      final MetaForm node=(MetaForm)obj;
      final FormData dynaNode=presenter.context().nodeData(node);
      if(stmt.funcName.equalsIgnoreCase(".save")){
        presenter.saveNode(dynaNode);
        return Boolean.TRUE;
      }

      else if(stmt.funcName.equalsIgnoreCase(".refresh")){
        Widget currentWidget=presenter.context().nodeWidget(node);
        TabularFlowPanel parentWidget=(TabularFlowPanel)currentWidget.getParent();
        int idx=parentWidget.getWidgetIndex(currentWidget);
        Widget newWidget= Renderer.Render(node, presenter.context());
        parentWidget.insert(newWidget, idx);
        parentWidget.remove(currentWidget);
        return Boolean.TRUE;
      }
      
      else if(dynaNode.bo!=null && dynaNode.bo.supports(stmt.funcName)){
        dynaNode.bo.actuate(stmt.funcName, eval(stmt.params));
        return Boolean.TRUE;
      }
      else if(node.typeCode==MetaForm.TC_OBSERVER && stmt.funcName.equalsIgnoreCase(".observe")){
        MetaForm observedNode=(MetaForm)eval(stmt.params.get(0));
        FormData observedDynaNode=presenter.context().nodeData(observedNode);
        dynaNode.bo=observedDynaNode.bo;
        return Boolean.TRUE;
      }
    }
    throw new RuntimeException("The method "+stmt.funcName+" cannot be invoked on "+stmt.obj);
  }

  /*
   * General purpose system functions.
   */
  protected Object execFunction(SyntaxParser.Function stmt){
    if(stmt.funcName.equalsIgnoreCase("return"))
      return Boolean.FALSE;
    
    if(stmt.funcName.equalsIgnoreCase("goto")){
      Object param=eval(stmt.params.get(0));
      if(!(param instanceof MetaForm))
        throw new RuntimeException("goto(page): param mismatch: "+param);
      MetaForm page=((MetaForm)param).myPage();
      if(page!=null)
        presenter.gotoPage(page);
      else
        presenter.viewNode(thisEntry.theForm.getBody());
      return Boolean.TRUE;
    }

    if(stmt.funcName.equalsIgnoreCase("alert")){
      Object param=eval(stmt.params.get(0));
      if(!(param instanceof String))
        throw new RuntimeException("alert(message): param mismatch: "+param);
      com.google.gwt.user.client.ui.PopupPanel pp=new com.google.gwt.user.client.ui.PopupPanel(true, true);
      com.google.gwt.user.client.ui.HTML content=new com.google.gwt.user.client.ui.HTML();
      pp.add(content);
      //pp.setGlassEnabled(true);
      //pp.setAnimationEnabled(true);
      content.setText((String)param);
      if(stmt.params.size()>1){
        Object target=eval(stmt.params.get(1));
        if(!(target instanceof MetaForm))
          throw new RuntimeException("alert(message, node): target mismatch: "+target);
        Widget s=presenter.context().nodeWidget((MetaForm)target);
        int left = s.getAbsoluteLeft();
        int top = s.getAbsoluteTop()+s.getOffsetHeight();
        pp.setPopupPosition(left, top);
        pp.show();
      }
      else
        pp.center();
      System.out.println("<<<ran alert!");
      return Boolean.TRUE;
    }

    throw new RuntimeException("Interpreter.eval(): "+stmt+" not implemented or incorrect parameter.");
  }
}
package fx.presentment.script;

import java.util.*;

/**
 * Classes representing the FormScript grammar and parsing logic.
 *
 * @author kw
 */
public class SyntaxParser {
  
  interface Expression {
  }
  
  static class Literal implements Expression{
    String number;
    String string;
    String coded;
    Literal(String number, String string, String code){
      this.number=number;
      this.string=string;
      this.coded=code;
    }
    public String toString(){
      return "L";
    }
  }
  
  static class Singular implements Expression{
    String objectReference;
    String attribute;
    Singular(String obj){
      objectReference=obj;
    }
    public String toString(){
      return "S["+objectReference+(attribute!=null? attribute:"")+"]";
    }
  }
  
  static class Binary implements Expression{
    public static Binary aneal(Expression expr, LexParser.TT operator){
      Binary x=new Binary(expr, operator);
      if(expr instanceof Binary){
        Binary b=(Binary)expr;
        LexParser.TT left=b.operator;
        boolean isComparison= 
            operator==LexParser.TT.GTEQ ||
            operator==LexParser.TT.LTEQ ||
            operator==LexParser.TT.GT ||
            operator==LexParser.TT.LT ||
            operator==LexParser.TT.EQ ||
            operator==LexParser.TT.NEQ;
        
        if(operator==LexParser.TT.AND && left==LexParser.TT.OR ||
           isComparison && (left==LexParser.TT.OR || left==LexParser.TT.AND))
          x=new Binary(b.lhs, b.operator, new Binary(b.rhs, operator));
      }
      if(DEBUGGING)
        System.out.println("Binary.aneal: "+expr+", "+operator+"=>"+x);
      return x;
    }
    
    Expression lhs;
    LexParser.TT operator;
    Expression rhs;
    Binary(Expression leftExpr, LexParser.TT op){
      lhs=leftExpr;
      operator=op;
      assert(lhs!=null):"Invalid param: lhs expr missing!";
    }
    Binary(Expression leftExpr, LexParser.TT op, Expression rightExpr){
      lhs=leftExpr;
      operator=op;
      rhs=rightExpr;
      assert(lhs!=null):"Invalid param: lhs expr missing!";
      assert(rhs!=null):"Invalid param: rhs expr missing!";
    }
    void setRhs(Expression expr){
      Binary b=this;
      while(b!=null && b.rhs!=null){
        if(b.rhs instanceof Binary)
          b=(Binary)b.rhs;
        else
          b=null;
      }
      if(b!=null)
        b.rhs=expr;
      else
        throw new RuntimeException("Binary.addRhs: Wrong state");
    }
    boolean isArithmetic(){
      return operator==LexParser.TT.PLUS || operator==LexParser.TT.MINUS || 
        operator==LexParser.TT.TIMES || operator==LexParser.TT.DVDB;
    }
    public String toString(){
      return operator+":("+lhs+","+rhs+")";
    }
  }
  
  static class Negation implements Expression{
    Expression innerExpression;
    void setInnerExpression(Expression expr){
      if(innerExpression!=null)
        throw new RuntimeException("Negation.addInnerExpression: Wrong state");
      innerExpression=expr;
    }
    public String toString(){
      return "!"+innerExpression;
    }
  }
  static class Enclosure implements Expression{
    List<Expression> innerExpressions=new ArrayList<Expression>();
    boolean isClosed;
    void addInnerExpression(Expression expr){
      innerExpressions.add(expr);
    }
    public String toString(){
      return "("+innerExpressions+")";
    }
  }
  
  interface Statement {
    String toString(String indent);
  }
  
  static class Declaration implements Statement {
    String var;
    Expression value;
    Declaration(String var, Expression value){
      this.var=var;
      this.value=value;
    }
    public String toString(String indent){
      return indent+"Declaration: var "+var+"<-"+value;
    }
  }
  static class Assignment implements Statement {
    Singular subject;
    Expression value;
    Assignment(Singular subject){
      this.subject=subject;
    }
    void addValue(Expression expr){
      if(value==null)
        value=expr;
      else
        throw new RuntimeException("Assignment.addExpression: Wrong state:"+value);
    }
    public String toString(String indent){
      return indent+"Assignment: "+subject+"<-"+value;
    }
  }
  static class Function implements Statement {
    String funcName;
    List<Expression> params=new ArrayList<Expression>();
    Function(String name){
      funcName=name;
    }
    void addParam(Expression expr){
      if(expr instanceof Enclosure)
        params.addAll(((Enclosure)expr).innerExpressions);
      else
        params.add(expr);
    }
    public String toString(String indent){
      return indent+"Function: "+funcName+((params==null)? "" : " "+params);
    }
  }
  static class Method extends Function {
    Singular obj;
    Method(Singular objRef, String name){
      super(name);
      obj=objRef;
    }
    public String toString(String indent){
      return indent+"Method: "+obj+funcName+((params==null)? "" : " "+params);
    }
  }
  static class CompoundStatement implements Statement {
    private Vector<Statement> subs=new Vector<Statement>();
    public void add(Statement s){
      subs.add(s);
    }
    public Vector<Statement> getSubstatements(){
      return subs;
    }
    public String toString(String indent){
      StringBuffer sb=new StringBuffer();
      sb.append(indent+"CompoundStatement:");
      indent="\t"+indent;
      for(Statement s: subs)
        sb.append("\n").append(s.toString(indent));
      return sb.toString();
    }
  }
  static class ConditionalStatement implements Statement {
    Expression condition;
    Statement s1, s2;
    private boolean goneElse;
    void goElse(){
      if(!goneElse)
        goneElse=true;
      else
        throw new RuntimeException("ConditionalStatement.goElse: Wrong state");
    }
    boolean isExpectingStatement(){
      return condition!=null && s1==null || goneElse && s2==null;
    }
    boolean canClose(){
      return goneElse && s2!=null || !goneElse && s1!=null;
    }
    void setCondition(Expression expr){
      if(condition!=null)
        throw new RuntimeException("ConditionalStatement.addCondition: Wrong state");
      condition=expr;
    }
    void setStatement(Statement s){
      if(s1==null)
        s1=s;
      else if(s2==null)
        s2=s;
      else
        throw new RuntimeException("ConditionalStatement.addStatement: Wrong state");
    }
    public String toString(String indent){
      StringBuffer sb=new StringBuffer();
      sb.append(indent+"ConditionalStatement:"+condition);
      indent="\t"+indent;
      sb.append("\n").append(s1.toString(indent));
      if(s2!=null)
        sb.append("\n").append(s2.toString(indent));
      return sb.toString();
    }
  }
  
  //
  // The implementation
  //
  
  @SuppressWarnings("unchecked")
  static Vector<Statement> parse(String source){
    return (Vector<Statement>)parse(source, false);
  }
  private static Object parse(String source, boolean isExpression){
    Vector<Statement> ast=new Vector<Statement>();
    Stack<Object> stack=new Stack<Object>();
    LexParser l=new LexParser(source);
    while(true){
      LexParser.Token token=l.emit();
      if(token==null)
        break;
      if(DEBUGGING)
        System.out.println(token);
      switch(token.tt) {
        case COMMT0:
        case COMMT1:
          break;

        case DCLR:
          ast.addAll(parseDeclarations(token.str));
          break;

        case ASSIGN:
          Singular subject=(Singular)stack.pop();
          startNewStatement(ast, stack, new Assignment(subject));
          break;
  
        case BLTIN:
          startNewStatement(ast, stack, new Function(token.str));
          break;
  
        case SEMICO:
          if(stack.peek() instanceof Statement)
            addPoppedStatement(ast, stack, (Statement)stack.pop());
          else if(stack.peek() instanceof Expression){
            compactExprContext(stack);
            addPoppedStatement(ast, stack, (Statement)stack.pop());
          }
          break;

        case IF:
          startNewStatement(ast, stack, new ConditionalStatement());
          break;
        case ELSE:
          compactExprContext(stack);
          ConditionalStatement cs=(ConditionalStatement)stack.peek();
          cs.goElse();
          break;

        case LC:
          startNewStatement(ast, stack, new CompoundStatement());
          break;
        case RC:
          while(true){
            Statement s=(Statement)stack.pop();
            addPoppedStatement(ast, stack, s);
            if(s instanceof CompoundStatement)
              break;
          }
          break;
        
        case LP:
          startNewExpression(stack, new Enclosure());
          break;
        case RP:
          compactExprContext(stack);
          if(stack.peek() instanceof Enclosure){
            Enclosure en=(Enclosure)stack.peek();
            en.isClosed=true;
            compactExprContext(stack);
          }
          break;

        case COMMA:
          compactExprContext(stack);
          break;
          
        case Q:
          break;
        case COLON:
          break;
          
        case NOT:
          startNewExpression(stack, new Negation());
          break;
          
        case AND:
        case OR:
        case PLUS:
        case MINUS:
        case TIMES:
        case DVDB:
        case GTEQ:
        case LTEQ:
        case GT:
        case LT:
        case EQ:
        case NEQ:
          Expression expr=(Expression)stack.pop();
          stack.push(Binary.aneal(expr, token.tt));
          break;

        case BOOL:
        case NULL:
          startNewExpression(stack, new Literal(null, null, token.str));
          break;
        case NUM:
          startNewExpression(stack, new Literal(token.str, null, null));
          break;
        case STR:
          startNewExpression(stack, new Literal(null, token.str, null));
          break;
          
        case OBJ:
          startNewExpression(stack, new Singular(token.str));
          break;
        case ATTR:
          Singular se=(Singular)stack.peek();
          if(se.attribute==null)
            se.attribute=token.str;
          else
            se.attribute+=token.str;
          break;
        case CMPNT:
          se=(Singular)stack.peek();
          if(se.attribute!=null)
            se.attribute+=token.str;
          break;
        case MTHD:
          se=(Singular)stack.pop();
          startNewStatement(ast, stack, new Method(se, token.str));
          break;

        default:
          break;
      }
    }

    if(isExpression){
      compactExprContext(stack);
      Object x=stack.pop();
      if(x instanceof Expression && stack.isEmpty())
        return x;
      else
        throw new RuntimeException("Expression Parsing failed:x="+x);
    }
    while(!stack.isEmpty()){
      Object x=stack.pop();
      if(x instanceof ConditionalStatement && ((ConditionalStatement)x).canClose())
        addPoppedStatement(ast, stack, (ConditionalStatement)x);
      else
        throw new RuntimeException("Parsing terminated with unclosed stack:"+x);
    }
    return ast;
  }
  private static List<Declaration> parseDeclarations(String stmt){
    List<Declaration> result=new ArrayList<Declaration>();
    stmt=stmt.trim();
    stmt=stmt.substring(3);
    String[] parts=stmt.split("[,;]");
    for(int i=0; i<parts.length; i++){
      String[] decl=parts[i].split("=");
      Expression e = decl.length>1? (Expression)parse(decl[1], true) : null;
      Declaration d=new Declaration(decl[0].trim(), e);
      result.add(d);
    }
    return result;
  }
  
  private static void startNewExpression(Stack<Object> stack, Expression expr){
    stack.push(expr);
  }
  private static void compactExprContext(Stack<Object> stack){
    if(stack.isEmpty() || stack.peek() instanceof Statement || stack.size()==1)
      return;
    Expression expr=(Expression)stack.pop();
    if(stack.peek() instanceof Assignment)
      ((Assignment)stack.peek()).addValue(expr);
    else if(stack.peek() instanceof Function)
      ((Function)stack.peek()).addParam(expr);
    else if(stack.peek() instanceof ConditionalStatement)
      ((ConditionalStatement)stack.peek()).setCondition(expr);
    
    else if(stack.peek() instanceof Negation){
      ((Negation)stack.peek()).setInnerExpression(expr);
      compactExprContext(stack);
    }
    else if(stack.peek() instanceof Enclosure){
      Enclosure en=(Enclosure)stack.peek();
      en.addInnerExpression(expr);
    }
    else if(stack.peek() instanceof Binary){
      ((Binary)stack.peek()).setRhs(expr);
      compactExprContext(stack);
    }
  }
  
  private static void startNewStatement(Vector<Statement> ast, Stack<Object> stack, Statement stmt){
    boolean canStart=false;
    if(stack.isEmpty() || stack.peek() instanceof CompoundStatement)
      canStart=true;
    else if(stack.peek() instanceof ConditionalStatement){
      ConditionalStatement cs=(ConditionalStatement)stack.peek();
      if(cs.isExpectingStatement())
        canStart=true;
      else if(cs.canClose()){
        stack.pop();
        addPoppedStatement(ast, stack, cs);
        startNewStatement(ast, stack, stmt);
        return;
      }
    }
    if(!canStart)
      throw new RuntimeException("Not ready to start new statement! stack="+stack.peek());
    stack.push(stmt);
    
    if(DEBUGGING)
      System.out.println("Pushed new stmt to stack: "+stmt);
  }
  private static void addPoppedStatement(Vector<Statement> ast, Stack<Object> stack, Statement stmt){
    if(DEBUGGING)
      System.out.println("Popped finished stmt from stack: "+stmt);
    if(stack.isEmpty()){
      ast.add(stmt);
      return;
    }
    Object outer=stack.peek();
    if(outer instanceof CompoundStatement){
      CompoundStatement stmtBlk=(CompoundStatement)outer;
      stmtBlk.add(stmt);
    }
    else if(outer instanceof ConditionalStatement){
      ConditionalStatement cs=(ConditionalStatement)stack.peek();
      if(cs.isExpectingStatement()){
        cs.setStatement(stmt);
        if(cs.goneElse){
          stack.pop();
          addPoppedStatement(ast, stack, cs);
        }
      }
    }
    else
      throw new RuntimeException("AddCompletedStatement: Wrong context: "+outer);
  }
  
  static final boolean DEBUGGING=true;
}

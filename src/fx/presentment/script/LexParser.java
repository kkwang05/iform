package fx.presentment.script;

import com.google.gwt.regexp.shared.*;

/**
 * Lexical parser for the FormScript Language.
 */
class LexParser {
  /** 
   * Token and Token Types
   */
  static class Token {
    String str;
    TT tt;
    Token(String s, TT tt){
      this.str=s; this.tt=tt;
    }
    public String toString(){
      return tt+"\t"+str;
    }
  }

  enum TT {FILLER, COMMT0, COMMT1, LC, RC, RC1, LP, RP, Q, COLON, SEMICO, SEMICO1, COMMA, PLUS,MINUS,TIMES,DVDB, GTEQ,LTEQ,GT,LT,EQ,NEQ,
    NOT,AND,OR, ASSIGN, BOOL, NULL, NUM, STR, DCLR,DCLR1,DCLR2,DCLR3,DCLR4, IF, ELSE, MTHD, BLTIN, OBJ, ATTR, CMPNT,CMPNT1};
  
  // Comments
  static final String COMMT0="//[^\n]*\\n";
  static final String COMMT1="/\\*[\\s\\S]*\\*/";
  
  // Special characters
  static final String LC="{";
  static final String RC="}(\\s*;)*";
  static final String LP="\\(";
  static final String RP="\\)";
  static final String Q="\\?";
  static final String COLON=":";
  static final String SEMICO=";(\\s*;)*";
  static final String COMMA=",";
  
  // Operators - Arithmetic
  static final String PLUS="\\+";
  static final String MINUS="\\-";
  static final String TIMES="\\*";
  static final String DVDB="/";
  // Comparison
  static final String GTEQ=">=";
  static final String LTEQ="<=";
  static final String GT=">";
  static final String LT="<";
  static final String EQ="==";
  static final String NEQ="!=";
  // Logical
  static final String NOT="!";
  static final String AND="&&";
  static final String OR="\\|\\|";
  // Assignment
  static final String ASSIGN="={1}";
  
  // Primitive data type values
  static final String BOOL="true|false";
  static final String NULL="null";
  static final String NUM="\\d+\\.?\\d*";
  static final String STR="'[^']*'|\"[^\"]*\"";

  // Reserved words
  static final String VAR="\\w+\\s*(=[^,;]+)?";
  static final String DCLR="var\\s+(" + VAR + ",\\s*)*" + VAR + SEMICO;
  static final String IF="if";
  static final String ELSE="else";
  static final String MTHD="\\.save|\\.refresh|\\.observe|\\.add|\\.remove|\\.set";
  static final String BLTIN="return|goto|save|alert";
  static final String OBJ="this\\.event|this\\.node|this\\.entry|this\\.user|\\w+";
  static final String ATTR="\\.type|\\.vs|\\.choices|\\.choice|\\.text|\\.display|\\.settings|\\.style|\\.hidden|\\.value|\\.answer|\\.status|\\.readOnly";
  static final String CMPNT="\\[(\\d,)*\\d?\\]";
  
  private static final String[] ALL={COMMT0, COMMT1, LC, RC, LP, RP, Q, COLON, SEMICO, COMMA, PLUS,MINUS,TIMES,DVDB, GTEQ,LTEQ,GT,LT,EQ,NEQ,
    NOT,AND,OR, ASSIGN, BOOL, NULL, NUM, STR, DCLR, IF, ELSE, MTHD, BLTIN, OBJ, ATTR, CMPNT};
  
  private static final RegExp RE;
  static {
    String pattern="("+ALL[0]+")";
    for(int i=1; i<ALL.length; i++)
      pattern+="|("+ALL[i]+")";
    RE=RegExp.compile(pattern);
  }

  private static final String ESC_SQ="_esc~ed00~sq_";
  private static final String ESC_DQ="_esc~ed11~dq_";
  
  private transient String input;
  private transient int startingIndex;
  LexParser(String src){
    src=src.replaceAll("\\\\'", ESC_SQ);
    src=src.replaceAll("\\\\\"", ESC_DQ);
    if(!src.endsWith("\n"))
      src+="\n";
    input=src;
    startingIndex=0;
  }
  
  Token emit(){
    input=input.substring(startingIndex);
    MatchResult result=RE.exec(input);
    if(result==null)
      return null;
    String ts=result.getGroup(0);
    for(int i=1;i<result.getGroupCount();i++){
      String gs=result.getGroup(i);
      if(ts.equals(gs)){
        startingIndex=result.getIndex()+ts.length();
        ts=ts.trim();
        if(TT.values()[i]==TT.STR){
          if(ts.startsWith("'")){
            ts=ts.replaceAll(ESC_SQ, "'");
            ts=ts.replaceAll(ESC_DQ, "\\\\\"");
          }
          else{
            ts=ts.replaceAll(ESC_SQ, "\\\\'");
            ts=ts.replaceAll(ESC_DQ, "\"");
          }
          ts=ts.substring(1, ts.length()-1);
        }
        return new Token(ts, TT.values()[i]);
      }
    }
    throw new RuntimeException("The unlikely happened!");
  }
}
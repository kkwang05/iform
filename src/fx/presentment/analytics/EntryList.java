package fx.presentment.analytics;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.HTMLTable.Cell;

import fx.model.*;
import fx.presentment.FxConstants;

/**
 * @author kw
 */
public class EntryList extends ResizeComposite implements HasSelectionHandlers<FormEntry> {
  interface SelectionStyle extends CssResource {
    String selectedRow();
  }
  
  interface MyBinder extends UiBinder<Widget, EntryList> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  private static final DateTimeFormat Dtf=DateTimeFormat.getFormat("MM/dd/yyyy HH:mm:ss");
  private static final HashMap<Short,String> StatusDisplayMap=new HashMap<Short,String>();
  static{
    String[] statusTexts=FxConstants.Instance.entryStatusText().split(",");
    for(String s:statusTexts){
      String[] codeAndText=s.split("=");
      short c=Short.parseShort(codeAndText[0]);
      StatusDisplayMap.put(c, codeAndText[1]);
    }
  }
  
  @UiField protected FlexTable header;
  @UiField protected ScrollPanel scroll;
  @UiField protected FlexTable table;
  @UiField protected SelectionStyle selectionStyle;

  public EntryList() {
    initWidget(binder.createAndBindUi(this));
    
    String[] hdrs=FxConstants.Instance.entryListTableHeaders().split(",");
    String[] widths=FxConstants.Instance.entryListTableHeaderWidths().split(",");
    for(int i=0; i<5; i++)
      header.setText(0, i, hdrs[i]);
    for(int i=0; i<4; i++){
      header.getColumnFormatter().setWidth(i, widths[i]);
      table.getColumnFormatter().setWidth(i, widths[i]);
    }
  }
  
  public HandlerRegistration addSelectionHandler(SelectionHandler<FormEntry> handler){
    return addHandler(handler, SelectionEvent.getType());
  }

  transient List<FormEntry> entries;
  transient int selectedRow = -1;
  public void load(List<FormEntry> entryList){
    entries=entryList;
    table.removeAllRows();
    for (int i = 0; entryList!=null && i < entries.size(); ++i) {
      FormEntry item = entries.get(i);

      for(int j=0;j<5;j++)
	table.getCellFormatter().setHorizontalAlignment(i, j, HasHorizontalAlignment.ALIGN_LEFT);
      table.setText(i, 0, item.id.toString());
      table.setWidget(i, 1, new Label(StatusDisplayMap.get(item.status)));
      table.setText(i, 2, Dtf.format(item.startDate));
      if(item.lastChangeDate!=null)
	table.setText(i, 3, Dtf.format(item.lastChangeDate));
      table.setText(i, 4, item.tracker);
      //if(item.voucherId!=null)
	//table.setWidget(i, 5, new Anchor("..."));
      if(item.voucher!=null)
	table.setText(i, 5, item.voucher.recipientId);
    }
    selectedRow=-1;
  }

  @UiHandler("table")
  void onTableClick(ClickEvent event) {
    Cell cell = table.getCellForEvent(event);
    if (cell != null) {
      int row = cell.getRowIndex();
      selectRow(row);
    }
  }
  
  /*
  @UiHandler("table")
  void onTableClick1(ClickEvent event) {
    Cell cell = table.getCellForEvent(event);
    if (cell != null) {
      int row = cell.getRowIndex();
      int col= cell.getCellIndex();
      Widget w=table.getWidget(row, col);
      if(w instanceof Anchor)
	table.setText(row, col, "voucher");
    }
  }
  */

  void selectRow(int row) {
    if(row==selectedRow)
      return;
    FormEntry item = entries.get(row);
    if (item == null)
      return;

    String style = selectionStyle.selectedRow();
    table.getRowFormatter().addStyleName(row, style);
    if(selectedRow>=0)
      table.getRowFormatter().removeStyleName(selectedRow, style);
    selectedRow = row;

    Widget w=table.getWidget(selectedRow, 1);
    scroll.ensureVisible(w);
    SelectionEvent.fire(this, item);
  }

  void navigate(int inc){
    if(entries==null || entries.size()==0)
      return;
    
    if(selectedRow<0){
      selectRow(inc>0? 0 : entries.size()-1);
      return;
    }
    
    int newSel=selectedRow+inc;
    if(newSel<0) 
      newSel=0;
    if(newSel>entries.size()-1) 
      newSel=entries.size()-1;

    selectRow(newSel);
  }
}
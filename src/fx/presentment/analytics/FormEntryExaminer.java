package fx.presentment.analytics;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.model.operation.*;
import fx.presentment.FxMessages;
import fx.presentment.design.repository.ContextualPublicationDirectory;
import fx.presentment.event.FxEventHandler;
import fx.presentment.event.PublicationEvent;
import fx.presentment.widget.*;

import fx.service.RPC;

/**
 * 
 * @author kw
 */
public class FormEntryExaminer extends Composite implements FxEventHandler<PublicationEvent>{
  interface MyBinder extends UiBinder<Widget, FormEntryExaminer> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField public ContextualPublicationDirectory explorer;
  @UiField EntryList entryList;
  @UiField FormEntryReviewer entryReviewer;
  @UiField Label queryResultMessage;
  @UiField TooltipPushButton refreshB, nxtB, prevB;

  @UiField TooltipToggleButton vButton;
  @UiField ResizableSplitLayoutPanel topContainer, rhs;
  @UiField Widget lhs;
  
  public FormEntryExaminer(){
    initWidget(binder.createAndBindUi(this));
    PublicationEvent.addHandler(this);
    explorer.setChangeNotifyButton(refreshB.getButton());
    nxtB.setEnabled(false);
    prevB.setEnabled(false);
  }
  
  public void showEntries(Publication pub){
    explorer.searchAndSelect(pub, false);
  }
  
  @UiHandler("explorer")
  void publicationSelected(SelectionEvent<Publication> item) {
    retrieveEntries();
  }

  @UiHandler("entryList")
  void entrySelected(SelectionEvent<FormEntry> item) {
    loadEntry(item.getSelectedItem());
  }

  @UiHandler("refreshB")
  void refreshClicked(ClickEvent event) {
    retrieveEntries();
  }

  @UiHandler("nxtB")
  void nxtClicked(ClickEvent event) {
    entryList.navigate(+1);
  }
  @UiHandler("prevB")
  void prevClicked(ClickEvent event) {
    entryList.navigate(-1);
  }
  
  @UiHandler("vButton") 
  void clickedV(ClickEvent event) {
    if(vButton.isDown()){
      rhs.setSplitPosition(entryList, 0);
      topContainer.setSplitPosition(lhs, 0);
    }
    else{
      topContainer.restoreToPreviousSplitPosition(lhs);
      rhs.restoreToPreviousSplitPosition(entryList);
    }
  }

  private void retrieveEntries(){
    final Publication selectedPublication=explorer.getSelectedSubject();
    if(selectedPublication==null){
      entryList.load(null);

      queryResultMessage.setText("");
      nxtB.setEnabled(false);
      prevB.setEnabled(false);
      return;
    }
    entryReviewer.closeEntry();
    GetEntryList req=new GetEntryList();
    req.publicationId=selectedPublication.id;
    RPC.Operation<GetEntryListResponse> op=new RPC.Operation<GetEntryListResponse>(req){
      public void onSuccess(GetEntryListResponse result){
        super.onSuccess(result);
        entryList.load(result.entryList);
        
        int count=result.entryList.size();
        if(count!=selectedPublication.entryCount){
          selectedPublication.entryCount=count;
        }
        PublicationEvent.fire(PublicationEvent.Kind.UPDATED, selectedPublication, FormEntryExaminer.this);
        queryResultMessage.setText(FxMessages.Instance.entryQueryResultMessage(count));
        nxtB.setEnabled(count>0);
        prevB.setEnabled(count>0);
      }
    };
    RPC.Instance.exec(op);
  }
  
  private void loadEntry(final FormEntry fe){
    if(fe.theForm!=null){
      entryReviewer.loadEntry(fe, true);
      return;
    }
    GetForm req=new GetForm(fe.formId);
    RPC.Operation<GetFormResponse> op=new RPC.Operation<GetFormResponse>(req){
      public void onSuccess(GetFormResponse result){
        super.onSuccess(result);
        fe.theForm=result.retrievedForm;
        loadEntry(fe);
      }
    };
    RPC.Instance.exec(op);
  }

  public boolean process(PublicationEvent evt){
    if(evt.nature==PublicationEvent.Kind.CREATED){
      explorer.add(explorer.personal, (Publication)evt.data, false);
    }
    else if(evt.nature==PublicationEvent.Kind.DELETED){
      explorer.remove((Publication)evt.data);
    }
    else if(evt.nature==PublicationEvent.Kind.UPDATED){
      if(evt.eventSource!=this)
        explorer.update((Publication)evt.data);
    }
    return true;
  }
  
}
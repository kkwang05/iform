package fx.presentment.analytics;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Widget;

import fx.model.*;
import fx.presentment.FormEntryPresenter;
import fx.presentment.FormPanel;
import fx.presentment.widget.Paginator;

/**
 * @author kw
 */
public class FormEntryReviewer extends FormEntryPresenter {
  interface MyBinder extends UiBinder<Widget, FormEntryReviewer> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField Paginator paginator;
  @UiField FormPanel canvas;
  
  @Override
  protected void initWidgets(){
    initWidget(binder.createAndBindUi(this));
    paginator.setVisible(false);
  }

  @Override
  protected FormPanel getViewingPanel(){
    return canvas;
  }
  
  @UiHandler("paginator") 
  void turnPage(SelectionEvent<MetaForm> event) {
    MetaForm focused=event.getSelectedItem();
    if(focused.isRoot())
      viewNode(focused);
    else if(focused.isDockedPage())
      // This will not happen anymore after the introduction of "design mode"
      ; 
    else
      getViewingPanel().layout(dv, focused, entryContext);
  }

  @Override
  public void loadEntry(FormEntry entry, boolean ro){
    super.loadEntry(entry, ro);
    if(entry.theForm.pages()!=null){
      paginator.setVisible(true);
      // Only load the real (not docked) pages
      paginator.reloadForm(entry.theForm, false);
    }
    else{
      paginator.setVisible(false);
    }
  }  

  @Override
  public void closeEntry(){
    super.closeEntry();
    paginator.setVisible(false);
  }
}
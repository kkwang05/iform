package fx.presentment.pub;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.datepicker.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.i18n.client.DateTimeFormat;

import fx.model.*;
import fx.model.operation.design.AcquireVouchers;
import fx.model.operation.design.AcquireVouchersResponse;
import fx.model.operation.design.CUDPublication;
import fx.model.operation.design.CUDPublicationResponse;
import fx.presentment.ClientAppContext;
import fx.presentment.design.repository.ContextualPublicationDirectory;
import fx.presentment.event.*;
import fx.presentment.widget.TooltipPushButton;
import fx.service.RPC;
import fx.util.FxCipher;

public class PublicationManager extends Composite implements FxEventHandler<PublicationEvent>{
  interface MyBinder extends UiBinder<Widget, PublicationManager> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  private static final DateTimeFormat Dtf0=DateTimeFormat.getFormat("yyyyMMdd");
  private static final DateTimeFormat Dtf=DateTimeFormat.getFormat("MM/dd/yyyy HH:mm:ss");

  @UiField public ContextualPublicationDirectory explorer;
  
  @UiField TooltipPushButton newB, deleteB;
  
  @UiField ComplexPanel publicationContent;
  @UiField Anchor formLink;
  @UiField Anchor entriesLink;
  @UiField TextBox name;
  @UiField TextArea desc;
  @UiField Label date;
  @UiField ToggleButton publicationState;
  
  @UiField ToggleButton timeLimitToggle;
  @UiField HTML timeLimitAnnotation;
  @UiField ComplexPanel timeLimitContent;
  @UiField DateBox fromDate, toDate;
  
  @UiField ToggleButton entryLimitToggle;
  @UiField HTML entryLimitAnnotation;
  @UiField ComplexPanel entryLimitContent;
  @UiField TextBox entryLimit;
  
  @UiField ToggleButton passwordToggle;
  @UiField HTML passwordAnnotation;
  @UiField ComplexPanel passwordContent;
  @UiField TextBox password;

  @UiField ToggleButton voucherToggle;
  @UiField HTML voucherAnnotation;
  @UiField ComplexPanel voucherContent;
  @UiField RadioButton voucherType0, voucherType1;
  @UiField TextBox voucherExpiration;
  
  @UiField TooltipPushButton testButton;
  @UiField HTMLPanel furtherActionsPanel;
  @UiField ToggleButton accordionButton;
  @UiField Anchor postLink, withdrawLink;
  @UiField Anchor sendHistoryLink, emailListLink, templateLink;
  @UiField ButtonBase sendButton;
  @UiField TextBoxBase formUrl, formFrame;
  
  private transient Publication thePublication;
  
  public PublicationManager(){
    initWidget(binder.createAndBindUi(this));
    PublicationEvent.addHandler(this);
    loadPublication(null);
  }

  @UiHandler("explorer")
  void publicationSelected(SelectionEvent<Publication> item){
    loadPublication(item.getSelectedItem());
  }

  public void loadPublication(Publication publication){
    thePublication=publication;
    boolean shown=(thePublication!=null);
    for(java.util.Iterator<Widget> it=publicationContent.iterator(); it.hasNext();)
      it.next().setVisible(shown);
    if(!shown){
      TreeItem ti=explorer.treeList.getSelectedItem();
      newB.setEnabled(ti!=null && (ti.getUserObject() instanceof Form));
      if(ti!=null && ti.getUserObject() instanceof Form)
	System.out.println("selected form ="+ti.getUserObject()+", st="+((Form)ti.getUserObject()).status);
      deleteB.setEnabled(false);
      testButton.setEnabled(false);
      return;
    }
    updateVisualStates();
    updateActionStates();
    accordionButton.setDown(false);
  }

  private void updateVisualStates(){
    if(thePublication.getForm()!=null)
      formLink.setText("<<"+thePublication.getForm().name);
    name.setText(thePublication.name);
    desc.setText(thePublication.desc);
    if(thePublication.publishingDate!=null)
      date.setText(Dtf.format(thePublication.publishingDate));
    if(thePublication.id!=null && thePublication.entryCount>=0){
      entriesLink.setVisible(true);
      entriesLink.setText(thePublication.entryCount+" Entri(es)>>");
    }
    else
      entriesLink.setVisible(false);
    
    boolean timeLimitSet=(thePublication.startDate!=null || thePublication.endDate!=null);
    timeLimitToggle.setDown(timeLimitSet);
    timeLimitAnnotation.setVisible(!timeLimitSet);
    timeLimitContent.setVisible(timeLimitSet);
    fromDate.setValue(thePublication.startDate);
    toDate.setValue(thePublication.endDate);
    
    boolean entryLimitSet=(thePublication.entryLimit>0);
    entryLimitToggle.setDown(entryLimitSet);
    entryLimitAnnotation.setVisible(!entryLimitSet);
    entryLimitContent.setVisible(entryLimitSet);
    if(thePublication.entryLimit>0)
      entryLimit.setValue(Integer.toString(thePublication.entryLimit));
    
    boolean passwordSet=(thePublication.authToken!=null);
    passwordToggle.setDown(passwordSet);
    passwordAnnotation.setVisible(!passwordSet);
    passwordContent.setVisible(passwordSet);
    password.setValue(thePublication.authToken);
  
    boolean voucherSet=(thePublication.requiresVoucher);
    voucherToggle.setDown(voucherSet);
    voucherAnnotation.setVisible(!voucherSet);
    voucherContent.setVisible(voucherSet);
    boolean multi=(thePublication.voucherType!=null && thePublication.voucherType.equals("MULTI_VISIT"));
    voucherType0.setValue(!multi);
    voucherType1.setValue(multi);
    if(thePublication.voucherExpiration>0)
      voucherExpiration.setValue(Integer.toString(thePublication.voucherExpiration));

    boolean isActive=!thePublication.inactive;
    furtherActionsPanel.setVisible(isActive);
    if(isActive){
      updatePostActionVisual();
      sendButton.addStyleName("buttonActive");
      String url=buildPublicationAccessUrl(thePublication, null);
      String iframe="<iframe align=\"left\" scrolling=\"no\" frameborder=\"0\"\n    src=\""+url+"\"/>";
      formUrl.setText(url);
      formFrame.setText(iframe);
    }
  }
  private void updatePostActionVisual(){
    com.google.gwt.user.client.Element listed=furtherActionsPanel.getElementById("listed");
    com.google.gwt.user.client.Element unlisted=furtherActionsPanel.getElementById("unlisted");
    if(thePublication.isListed){
      listed.getStyle().setDisplay(Display.INLINE);
      unlisted.getStyle().setDisplay(Display.NONE);
    }
    else{
      listed.getStyle().setDisplay(Display.NONE);
      unlisted.getStyle().setDisplay(Display.INLINE);
    }
  }
  private void updateActionStates(){
    boolean inactive=thePublication.inactive;
    
    publicationState.setDown(!inactive);
    if(!inactive)
      publicationState.addStyleName("buttonActive");
    else
      publicationState.removeStyleName("buttonActive");
  
    name.setReadOnly(!inactive);
    desc.setReadOnly(!inactive);
    
    timeLimitToggle.setEnabled(inactive);
    fromDate.setEnabled(inactive);
    toDate.setEnabled(inactive);
    
    entryLimitToggle.setEnabled(inactive);
    entryLimit.setEnabled(inactive);
    
    passwordToggle.setEnabled(inactive);
    password.setEnabled(inactive);
    
    voucherToggle.setEnabled(inactive);
    voucherType0.setEnabled(inactive);
    voucherType1.setEnabled(inactive);
    voucherExpiration.setEnabled(inactive);
  
    newB.setEnabled(false);
    deleteB.setEnabled(thePublication.inactive && thePublication.entryCount==0);
    testButton.setEnabled(!inactive);
  }

  @UiHandler("newB")
  void clickedCreate(ClickEvent evt){
    TreeItem ti=explorer.treeList.getSelectedItem();
    if (!(ti.getUserObject() instanceof Form))
      return;
    Form f=(Form)ti.getUserObject();
    createPublication(f);
  }
  
  @UiHandler("deleteB")
  void clickedDelete(ClickEvent evt){
    persist(thePublication, true, true);
  }
  
  @UiHandler("testButton")
  void clickedTest(ClickEvent evt){
    if(thePublication.requiresVoucher){
      AcquireVouchers req=new AcquireVouchers(thePublication.id, 1, -1);
      req.count=1;
      req.recipients=new String[]{ClientAppContext.CurrentUser.email};
      RPC.Operation<AcquireVouchersResponse> op=new RPC.Operation<AcquireVouchersResponse>(req){
        public void processSuccess(AcquireVouchersResponse response) {
          Voucher vc=response.vouchers[0];
          String url=buildPublicationAccessUrl(thePublication, vc);
          Window.open(url, "PublicationAccessTest", "resizable=yes");
        }
      };
      RPC.Instance.exec(op);
    }
    else{
      String url=buildPublicationAccessUrl(thePublication, null);
      Window.open(url, "PublicationAccessTest", "resizable=yes");
    }
  }
  private String buildPublicationAccessUrl(Publication aPub, Voucher voucher){
    String urlBase=Window.Location.getProtocol()+"//"+Window.Location.getHost()+"/entry.html?";
    String url=urlBase+FxCipher.encrypt("publicationid="+aPub.id+"|voucherid=" + (aPub.requiresVoucher&&voucher!=null? voucher.id : ""));
    return url;
  }

  @UiHandler("formLink")
  void clickedFormLink(ClickEvent evt){
    GlobalEvent.fire(GlobalEvent.Kind.SHOW_FORM, thePublication.getForm());
  }

  @UiHandler("entriesLink")
  void clickedEntriesLink(ClickEvent evt){
    GlobalEvent.fire(GlobalEvent.Kind.SHOW_ENTRIES, thePublication);
  }
  
  public void createPublication(Form form){
    Publication pub=new Publication();
    pub.setForm(form);
    pub.formId=form.id;
    pub.name="Pub-"+Dtf0.format(pub.publishingDate);
    pub.createdByUserId=ClientAppContext.CurrentUser.id;
    loadPublication(pub);
    explorer.clearSelection();
  }
  
  /**
   * Programmatically search and select/focus on the last publication of the given form. 
   * Requery if necessary.
   */
  public void showLastPublication(Form form){
    loadPublication(null);
    explorer.searchAndSelect(form,true);
  }

  @UiHandler("name")
  void nameFocused(FocusEvent fe){
    if(!name.isReadOnly())
      name.selectAll();
  }
  @UiHandler("desc")
  void descFocused(FocusEvent fe){
    if(!desc.isReadOnly())
      desc.selectAll();
  }
  @UiHandler("name")
  void NameChanged(ValueChangeEvent<String> e){
    name.setFocus(false);
    thePublication.name=e.getValue().trim();
    persist(thePublication, false, true);
  }
  @UiHandler("desc")
  void descChanged(ValueChangeEvent<String> e){
    desc.setFocus(false);
    thePublication.desc=e.getValue().trim();
    persist(thePublication, false, true);
  }
  
  @UiHandler("publicationState")
  void publicationStateChanged(ClickEvent evt){
    thePublication.inactive=!publicationState.isDown();
    updateVisualStates();
    updateActionStates();
    persist(thePublication, false, true);
  }

  @UiHandler("timeLimitToggle")
  void timeLimitToggled(ClickEvent evt){
    timeLimitAnnotation.setVisible(!timeLimitToggle.isDown());
    timeLimitContent.setVisible(timeLimitToggle.isDown());
    boolean timeLimitSet=(thePublication.startDate!=null || thePublication.endDate!=null);
    if(!timeLimitToggle.isDown() && timeLimitSet){
      thePublication.startDate=null;
      thePublication.endDate=null;
      persist(thePublication, false, true);
    }
  }
  @UiHandler("fromDate")
  void fromDateChanged(ValueChangeEvent<Date> evt){
    Date sd=evt.getValue();
    if(thePublication.endDate!=null && sd.after(thePublication.endDate)){
      fromDate.setValue(null);
      return;
    }
    thePublication.startDate=sd;
    persist(thePublication, false, true);
  }
  @UiHandler("toDate")
  void toDateChanged(ValueChangeEvent<Date> evt){
    Date ed=evt.getValue();
    if(thePublication.startDate!=null && ed.before(thePublication.startDate)){
      toDate.setValue(null);
      return;
    }
    thePublication.endDate=ed;
    persist(thePublication, false, true);
  }

  @UiHandler("entryLimitToggle")
  void entryLimitToggled(ClickEvent evt){
    entryLimitAnnotation.setVisible(!entryLimitToggle.isDown());
    entryLimitContent.setVisible(entryLimitToggle.isDown());
    if(!entryLimitToggle.isDown() && thePublication.entryLimit>0){
      thePublication.entryLimit=-1;
      entryLimit.setText(null);
      persist(thePublication, false, false);
    }
  }
  @UiHandler("entryLimit")
  void entryLimitChanged(ValueChangeEvent<String> evt){
    int limit=0;
    try{
      limit=Integer.parseInt(evt.getValue());
    }
    catch(Exception ex){
      entryLimit.setValue("0");
      return;
    }
    thePublication.entryLimit=limit;
    persist(thePublication, false, false);
  }
  
  @UiHandler("passwordToggle")
  void passwordToggled(ClickEvent evt){
    passwordAnnotation.setVisible(!passwordToggle.isDown());
    passwordContent.setVisible(passwordToggle.isDown());
    if(!passwordToggle.isDown() && thePublication.authToken!=null){
      thePublication.authToken=null;
      persist(thePublication, false, false);
    }
  }
  @UiHandler("password")
  void passwordChanged(ValueChangeEvent<String> evt){
    String pw=evt.getValue().trim();
    if("".equals(pw))
      pw=null;
    thePublication.authToken=pw;
    persist(thePublication, false, false);
  }

  @UiHandler("voucherToggle")
  void voucherToggled(ClickEvent evt){
    voucherAnnotation.setVisible(!voucherToggle.isDown());
    voucherContent.setVisible(voucherToggle.isDown());
    thePublication.requiresVoucher=voucherToggle.isDown();
    persist(thePublication, false, false);
  }
  @UiHandler("voucherType0")
  void voucherType0Clicked(ValueChangeEvent<Boolean> evt){
    thePublication.voucherType="SINGLE_VISIT";
    persist(thePublication, false, false);
  }
  @UiHandler("voucherType1")
  void voucherType1Clicked(ValueChangeEvent<Boolean> evt){
    thePublication.voucherType="MULTI_VISIT";
    persist(thePublication, false, false);
  }
  @UiHandler("voucherExpiration")
  void voucherExpirationChanged(ValueChangeEvent<String> evt){
    int limit=0;
    try{
      limit=Integer.parseInt(evt.getValue());
    }
    catch(Exception ex){
      voucherExpiration.setValue("0");
    }
    thePublication.voucherExpiration=limit;
    persist(thePublication, false, false);
  }

  @UiHandler("accordionButton")
  void clickedAccordionButton(ClickEvent event){
    boolean showContent=(!accordionButton.isDown());
    accordionButton.setFocus(false);
    java.util.Iterator<Widget> it=publicationContent.iterator();
    it.next();
    for(; it.hasNext();){
      Widget w=it.next();
      if(w!=furtherActionsPanel)
	w.setVisible(showContent);
    }
  }

  @UiHandler("postLink")
  void clickedPostLink(ClickEvent event){
    thePublication.isListed=true;
    persist(thePublication, false, false);
    updatePostActionVisual();
  }
  @UiHandler("withdrawLink")
  void clickedWithdrawLink(ClickEvent event){
    thePublication.isListed=false;
    persist(thePublication, false, false);
    updatePostActionVisual();
  }
  
  private void persist(final Publication thePublication, final boolean isDelete, final boolean fireEvent){
    CUDPublication req;
    final PublicationEvent.Kind changeType;
    final boolean isCreate=!isDelete && (thePublication.id==null);
    if(isDelete){
      req=CUDPublication.delete(thePublication);
      changeType=PublicationEvent.Kind.DELETED;
    }
    else if(isCreate){
      req=CUDPublication.create(thePublication);
      changeType=PublicationEvent.Kind.CREATED;
    }
    else{
      req=CUDPublication.update(thePublication);
      changeType=PublicationEvent.Kind.UPDATED;
    }
    RPC.Operation<CUDPublicationResponse> op=new RPC.Operation<CUDPublicationResponse>(req){
      public void processSuccess(CUDPublicationResponse response) {
	if(isCreate){
	  short newStatus=response.persistedRecord.getForm().status;
	  if(thePublication.getForm().status!=newStatus){
	    thePublication.getForm().status=newStatus;
	    // Sync up the current form with the newly changed and persisted record.
	    FormEvent.fire(FormEvent.FormLevel.UPDATED, thePublication.getForm());
	  }
	  // Fixing a bug - if id remains unset, each subsequent CUD request will generate an unindended copy
	  thePublication.id=response.persistedRecord.id;
	}
	else if(isDelete){
	  if(response.associatedForm!=null)
	    FormEvent.fire(FormEvent.FormLevel.UPDATED, response.associatedForm);
	}
	if(fireEvent)
	  PublicationEvent.fire(changeType, thePublication);
      }
    };
    RPC.Instance.exec(op);
  }
  
  public boolean process(PublicationEvent evt){
    if(evt.nature==PublicationEvent.Kind.CREATED){
      explorer.add(explorer.personal, (Publication)evt.data, true);
    }
    else if(evt.nature==PublicationEvent.Kind.DELETED){
      explorer.remove((Publication)evt.data);
    }
    else if(evt.nature==PublicationEvent.Kind.UPDATED){
      explorer.update((Publication)evt.data);
    }
    return true;
  }
  
}
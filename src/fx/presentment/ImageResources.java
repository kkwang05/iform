package fx.presentment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.*;

/**
 * UI images collection (icons, graphics, whatnot) 
 * @author kw
 */
public interface ImageResources extends ClientBundle {
  public static ImageResources Instance=GWT.create(ImageResources.class);
    
  @Source("fx/presentment/images/home.png")
  public ImageResource homeIcon();
  @Source("fx/presentment/images/barChart.png")
  public ImageResource barChartIcon();
  
  @Source("fx/presentment/images/color.png")
  public ImageResource colorIcon();
  @Source("fx/presentment/images/image.png")
  public ImageResource imageIcon();
  @Source("fx/presentment/images/valueSet.png")
  public ImageResource valueSetIcon();
  @Source("fx/presentment/images/contactList.png")
  public ImageResource contactListIcon();
  @Source("fx/presentment/images/template.png")
  public ImageResource templateIcon();
  
  @Source("fx/presentment/images/personal.png")
  public ImageResource personalIcon();
  @Source("fx/presentment/images/domain.png")
  public ImageResource domainIcon();
  @Source("fx/presentment/images/public.png")
  public ImageResource publicIcon();
  @Source("fx/presentment/images/publication.png")
  public ImageResource publicationIcon();

  @Source("fx/presentment/images/upload.png")
  public ImageResource imageUploadIcon();
  @Source("fx/presentment/images/action_delete.png")
  public ImageResource imageDeleteIcon();
  @Source("fx/presentment/images/group_share.png")
  public ImageResource imageShareIcon();

  @Source("fx/presentment/images/form.png")
  public ImageResource formIcon();

  @Source("fx/presentment/images/add.png")
  public ImageResource addIcon();

  @Source("fx/presentment/images/copy.png")
  public ImageResource copyIcon();

  @Source("fx/presentment/images/delete.png")
  public ImageResource deleteIcon();

  @Source("fx/presentment/images/generic_add.png")
  public ImageResource genericAddIcon();

  @Source("fx/presentment/images/generic_delete.png")
  public ImageResource genericDeleteIcon();

  @Source("fx/presentment/images/lock.png")
  public ImageResource lockIcon();

  @Source("fx/presentment/images/unlock.png")
  public ImageResource unlockIcon();
  
  @Source("fx/presentment/images/cut.png")
  public ImageResource cutIcon();
  
  @Source("fx/presentment/images/node_copy.png")
  public ImageResource nodeCopyIcon();
  
  @Source("fx/presentment/images/paste.png")
  public ImageResource pasteIcon();

  @Source("fx/presentment/images/merge.png")
  public ImageResource mergeIcon();

  @Source("fx/presentment/images/unmerge.png")
  public ImageResource unmergeIcon();

  @Source("fx/presentment/images/zoom_in.png")
  public ImageResource zoomInIcon();

  @Source("fx/presentment/images/zoom_out.png")
  public ImageResource zoomOutIcon();

  @Source("fx/presentment/images/info.png")
  public ImageResource infoIcon();
  
  @Source("fx/presentment/images/events.png")
  public ImageResource eventsIcon();

  @Source("fx/presentment/images/insert_page.png")
  public ImageResource insertPageIcon();

  @Source("fx/presentment/images/pin.png")
  public ImageResource pinIcon();
  
  @Source("fx/presentment/images/flag_green.png")
  public ImageResource greenFlagIcon();

  @Source("fx/presentment/images/refresh.png")
  public ImageResource refreshIcon();

  @Source("fx/presentment/images/tool.png")
  public ImageResource toolIcon();
  @Source("fx/presentment/images/directory.png")
  public ImageResource directoryIcon();

  @Source("fx/presentment/images/design.png")
  public ImageResource designIcon();
  @Source("fx/presentment/images/preview.png")
  public ImageResource previewIcon();
  
  @Source("fx/presentment/images/close_window.png")
  public ImageResource closeWindowIcon();
  @Source("fx/presentment/images/restore_window.png")
  public ImageResource restoreWindowIcon();
  @Source("fx/presentment/images/maximize_window.png")
  public ImageResource maximizeWindowIcon();
  @Source("fx/presentment/images/accordionExpand.png")
  public ImageResource accordionExpandIcon();
  @Source("fx/presentment/images/accordionCollapse.png")
  public ImageResource accordionCollapseIcon();

  @Source("fx/presentment/images/search.png")
  public ImageResource searchIcon();
  @Source("fx/presentment/images/compose.png")
  public ImageResource composeIcon();
  @Source("fx/presentment/images/cancel.png")
  public ImageResource cancelIcon();

  @Source("fx/presentment/images/resultset_next.png")
  public ImageResource nextIcon();
  @Source("fx/presentment/images/resultset_previous.png")
  public ImageResource previousIcon();
  @Source("fx/presentment/images/resultset_first.png")
  public ImageResource firstIcon();
  @Source("fx/presentment/images/resultset_last.png")
  public ImageResource lastIcon();

  @Source("fx/presentment/images/recycleBin.png")
  public ImageResource recycleBinImage();
  
}

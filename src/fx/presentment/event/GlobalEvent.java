package fx.presentment.event;

import com.google.gwt.event.shared.*;

/**
 * Global(Cross-tab) interactions requested by the tab components and handled by FormApp, the 
 * overall controller-cooridnator.
 * 
 * @author kw
 * @param <T>
 */
public class GlobalEvent extends GwtEvent<FxEventHandler<GlobalEvent>> implements Centralized {
  public static final Type<FxEventHandler<GlobalEvent>> TYPE=new Type<FxEventHandler<GlobalEvent>>();
  
  public static HandlerRegistration addHandler(FxEventHandler<GlobalEvent> handler){
    return TheEventBus.addHandler(TYPE, handler);
  }
  
  public static void fire(Kind kind, Object data){
    GlobalEvent event=new GlobalEvent(kind, data);
    TheEventBus.fireEvent(event);
  }
  
  public enum Kind {CREATE_PUBLICATION, SHOW_PUBLICATION, SHOW_FORM, SHOW_ENTRIES, SHOW_IMAGE_REPOSITORY, SHOW_COLOR_REPOSITORY, SHOW_PROFILE };
  
  public Type<FxEventHandler<GlobalEvent>> getAssociatedType(){
    return TYPE;
  }
  protected void dispatch(FxEventHandler<GlobalEvent> handler){
    handler.process(this);
  }
 
  public Kind kind;
  public Object data;
  
  private GlobalEvent(Kind kind, Object data){
    this.kind=kind;
    this.data=data;
  }
}
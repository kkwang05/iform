package fx.presentment.event;

import com.google.gwt.event.shared.*;

/**
 * All events around the form; handled by FormEditor.
 * @author kw
 */
public class FormEvent extends GwtEvent<FxEventHandler<FormEvent>> implements Centralized {
  public static final Type<FxEventHandler<FormEvent>> TYPE=new Type<FxEventHandler<FormEvent>>();

  public static HandlerRegistration addHandler(FxEventHandler<FormEvent> handler){
    return TheEventBus.addHandler(TYPE, handler);
  }
  
  public static void fire(Object nature, Object data){
    FormEvent event=new FormEvent(nature, data);
    TheEventBus.fireEvent(event);
  }
  
  public enum FormLevel {
    CREATING, OPENED_RO, OPENED_RW, UPDATED, REQUESTED_OPEN, JOGGLED_VIEW,
  };
  public enum NodeLevel {
    ADDING_CANCLED,ADDED,REMOVED,MOVED,CHANGED,
  };

  public Type<FxEventHandler<FormEvent>> getAssociatedType(){
    return TYPE;
  }
  protected void dispatch(FxEventHandler<FormEvent> handler){
    handler.process(this);
  }
 
  public Object nature;
  public Object data;
  public FormEvent(Object nature,Object data){
    this.nature=nature;
    this.data=data;
  }
}
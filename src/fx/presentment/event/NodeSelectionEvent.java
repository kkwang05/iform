package fx.presentment.event;

import com.google.gwt.event.logical.shared.SelectionEvent;

import fx.presentment.design.EditableNode;

public class NodeSelectionEvent extends SelectionEvent<EditableNode> {
  
  public static void fire(EditableNode node){
    node.fireEvent(new NodeSelectionEvent(node, false,false));
  }
  
  public static void fire(EditableNode node, boolean ctrl, boolean shft){
    node.fireEvent(new NodeSelectionEvent(node, ctrl,shft));
  }
  
  private boolean toggling, sweeping, exclusive;
  private NodeSelectionEvent(EditableNode node, boolean ctrl, boolean shft){
    super(node);
    toggling=ctrl;
    exclusive=!ctrl && !shft;
    sweeping=!(toggling || exclusive);
  }
  
  public boolean isTogglingSelect(){
    return toggling;
  }
  
  public boolean isSweepingSelect(){
    return sweeping;
  }
  
  public boolean isExclusiveSelect(){
    return exclusive;
  }
}

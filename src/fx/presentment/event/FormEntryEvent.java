package fx.presentment.event;

import com.google.gwt.event.shared.*;

import fx.model.*;

public class FormEntryEvent extends GwtEvent<FxEventHandler<FormEntryEvent>> implements Centralized {
  public static final Type<FxEventHandler<FormEntryEvent>> TYPE=new Type<FxEventHandler<FormEntryEvent>>();

  public static HandlerRegistration addHandler(FxEventHandler<FormEntryEvent> handler){
    return TheEventBus.addHandler(TYPE, handler);
  }
  
  public static class Navigation extends FormEntryEvent {
    public Navigation(FormEntry entry, MetaForm source, MetaForm target){
      super(entry,source,target);
    }
  }

  public static class LifeCycle extends FormEntryEvent {
    public static final String STARTED="STARTED";
    public static final String SUBMITTED="SUBMITTED";
    public LifeCycle(FormEntry entry, MetaForm source, String transition){
      super(entry,source,transition);
    }
  }

  public static class DataInput extends FormEntryEvent {
    public DataInput(FormEntry entry, MetaForm source){
      super(entry,source,null);
      persistable=false;
    }
    public DataInput(FormEntry entry, MetaForm source, String data){
      super(entry,source,data);
      persistable=true;
    }
  }
  
  public static void fire(FormEntryEvent event){
     TheEventBus.fireEvent(event);
  }
  
  public Type<FxEventHandler<FormEntryEvent>> getAssociatedType(){
    return TYPE;
  }
  protected void dispatch(FxEventHandler<FormEntryEvent> handler){
    handler.process(this);
  }
 
  public FormEntry entry;
  public MetaForm source;
  public Object data;
  public boolean persistable;
  public boolean expired;
  
  public FormEntryEvent(FormEntry entry, MetaForm source, Object data){
    this.entry=entry;
    this.source=source;
    this.data=data;
  }
}
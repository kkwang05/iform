package fx.presentment.event;

import com.google.gwt.event.shared.EventHandler;

public interface FxEventHandler<E> extends EventHandler {
  boolean process(E evt);
}

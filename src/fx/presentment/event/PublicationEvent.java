package fx.presentment.event;

import com.google.gwt.event.shared.*;

/**
 * @author kw
 */
public class PublicationEvent extends GwtEvent<FxEventHandler<PublicationEvent>> implements Centralized {
  public static final Type<FxEventHandler<PublicationEvent>> TYPE=new Type<FxEventHandler<PublicationEvent>>();

  public static HandlerRegistration addHandler(FxEventHandler<PublicationEvent> handler){
    return TheEventBus.addHandler(TYPE, handler);
  }
  
  public static void fire(Kind nature, Object data){
    fire(nature, data, null);
  }
  public static void fire(Kind nature, Object data, Object eventSource){
    PublicationEvent event=new PublicationEvent(nature, data);
    event.eventSource=eventSource;
    TheEventBus.fireEvent(event);
  }
  
  public enum Kind {
    CREATED, UPDATED, DELETED
  };

  public Type<FxEventHandler<PublicationEvent>> getAssociatedType(){
    return TYPE;
  }
  protected void dispatch(FxEventHandler<PublicationEvent> handler){
    handler.process(this);
  }
 
  public Kind nature;
  public Object data;
  public Object eventSource;
  public PublicationEvent(Kind nature,Object data){
    this.nature=nature;
    this.data=data;
  }
}
package fx.presentment;

import java.util.*;

import com.google.gwt.user.client.ui.Widget;

import fx.model.MetaForm;
import fx.model.FormData;
import fx.model.FormEntry;
import fx.model.bo.*;

/**
 * The data and session state store associated with a FormEntry. Maintains the following information:
 *  1. Static symbol table of nodes;
 *  2. Lookup table of dynamic node data and business objects;
 *  3. Lookup table of transient rendered widgets;
 *  4. Stack of Scopes containing declared variables.
 *
 * @author kw
 */
public class FormEntrySessionContext extends HashMap<Long, FormData> {
  public static final long serialVersionUID=1L;

  public FormEntry entry;
  private boolean readOnly;

  private HashMap<String, MetaForm> symbolTab=new HashMap<String, MetaForm>();
  private HashMap<MetaForm, Widget> widgetMap=new HashMap<MetaForm, Widget>();
  private Stack<Map<String,Object>> scopes=new Stack<Map<String,Object>>();
  
  public void load(FormEntry entry, boolean ro){
    clear();
    this.entry=entry;
    setReadOnly(ro);
    initSymbolTable(entry.theForm.getBody());
    
    if(entry.data==null)
      return;
    for (FormData d : entry.data)
      put(d.formNodeId, d);
  }
  
  private void initSymbolTable(MetaForm n){
    symbolTab.put(n.getNodeId(), n);
    if(n.getChildren()!=null)
      for(MetaForm c: n.getChildren())
        initSymbolTable(c);
  }
  
  public void setReadOnly(boolean ro){
    readOnly=ro;
  }
  
  public boolean isReadOnly(){
    return readOnly || entry.isReadOnly();
  }
  
  public MetaForm node(String nodeId){
    return symbolTab.get(nodeId);
  }
  
  public FormData nodeData(MetaForm node){
    if(node==null)
      return null;
    
    FormData data=super.get(node.id);
    if(data==null){
      data=new FormData(entry.id, node);
      put(node.id, data);
    }
    if(!data.bound)
      bind(node, data);
    
    return data;
  }
  
  private void bind(MetaForm node, FormData dynaNode){
    dynaNode.node=node;
    dynaNode.text=node.text;
    dynaNode.renderingHints=node.renderingHints;
    dynaNode.settings=node.settings;
    
    // Instantiate BOs if applicable
    if(dynaNode.bo==null)
      if(node.typeCode==MetaForm.TC_GRADER)
        dynaNode.bo=new Grader(this,dynaNode);
      else if(node.typeCode==MetaForm.TC_SHOPPING_CART)
        dynaNode.bo=new ShoppingCart(this,dynaNode);
      else if(node.typeCode==MetaForm.TC_CONTACT)
        dynaNode.bo=new InfoSheet(this,dynaNode);

    dynaNode.bound=true;
  }
  
  public Widget nodeWidget(MetaForm node){
    return widgetMap.get(node);
  }

  public void putNodeWidget(MetaForm node, Widget w){
    widgetMap.put(node, w);
  }
  
  public void pushScope(){
    scopes.push(new HashMap<String,Object>());
  }
  public void popScope(){
    if(!scopes.isEmpty())
      scopes.pop();
  }
  
  public void insertVariable(String var, Object val){
    scopes.peek().put(var, val);
  }
  
  public Object fetchVariable(String var){
    int lvl=scopes.size()-1;
    while(lvl >= 0){
      Map<String,Object> m= scopes.get(lvl--);
      Object v=m.get(var);
      if(v!=null)
        return v;
    }
    return null;
  }
  
  private transient HashMap<Object, Object> privateDataStore; 
  public void putPrivateData(Object ownerKey, Object data){
    if(privateDataStore==null)
      privateDataStore=new HashMap<Object, Object>();
    privateDataStore.put(ownerKey, data);
  }
  
  public Object retrievePrivateData(Object ownerKey){
    if(privateDataStore==null)
      return null;
    return privateDataStore.get(ownerKey);
  }
  
  @Override
  public FormData get(Object k){
    throw new RuntimeException("API verboten");
  }
  
  @Override
  public void clear(){
    super.clear();
    symbolTab.clear();
    widgetMap.clear();
    scopes.clear();
    if(privateDataStore!=null){
      privateDataStore.clear();
      privateDataStore=null;
    }
  }
}
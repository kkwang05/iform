package fx.presentment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.*;

/**
 * Main App UI Messages
 * @author kw
 */
public interface FxMessages extends Messages {
  public static final FxMessages Instance=GWT.create(FxMessages.class);
  
  @DefaultMessage("{0} entries retrieved.")
  String entryQueryResultMessage(int count);
  
  String toolkitHeaderFormat(String templateName);
  String greeting0();
  String greeting1(String name);
  
  @DefaultMessage("Created at {0}")
  String createdLine(String dt);
  @DefaultMessage("Last changed at {0}")
  String lastChangedLine(String dt);

  @DefaultMessage("Published at {0}")
  String publishedLine(String dt);
  @DefaultMessage("Last accessed at {0}")
  String lastAccessedLine(String dt);
  
  @DefaultMessage("{0} Total visits")
  String totalVisitsLine(int ct);
}


package fx.presentment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.*;

/**
 * Main App UI Resources
 * @author kw
 */
public interface FxConstants extends Constants {
  public static final FxConstants Instance=GWT.create(FxConstants.class);

  @DefaultStringValue("Home")
  String homeTabText();

  @DefaultStringValue("Forms")
  String formEditorTabText();

  @DefaultStringValue("Publications")
  String pubManagerTabText();

  @DefaultStringValue("Analytics")
  String entryExaminerTabText();
  
  @DefaultStringValue("(Drag nodes from palette and drop on this panel)")
  String dragAndDropHint();
  @DefaultStringValue("New Form")
  String newFormName();
  @DefaultStringValue("A new form created from scratch")
  String newFormDescription();

  @DefaultStringValue("<b>Myself</b>,<b>My Domain</b>,<b>Public</b>")
  String topLevelNodeTexts();

  @DefaultStringValue("Entry Id,Status,Started,Last Modified,Tracking")
  String entryListTableHeaders();
  @DefaultStringValue("64px,128px,150px,150px,150px")
  String entryListTableHeaderWidths();
  @DefaultStringValue("0=NEW,1=EDITING,2=SUSPENDED,3=COMPLETED")
  String entryStatusText();
}


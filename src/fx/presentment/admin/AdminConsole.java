package fx.presentment.admin;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.presentment.FxConstants;
import fx.presentment.FxMessages;
import fx.service.*;
import fx.model.operation.admin.*;

/**
 * Main entry point for the entire application.
 * 
 * @author kw
 */
public class AdminConsole extends Composite implements EntryPoint{
  interface GlobalResources extends ClientBundle {
    @NotStrict
    @Source("../iForm.css")
    CssResource css();
  }
  interface MyBinder extends UiBinder<Widget, AdminConsole> { }
  
  public static final FxConstants Res=GWT.create(FxConstants.class);
  public static final FxMessages Msgs=GWT.create(FxMessages.class);
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Button submit;
  @UiField TextArea data;
  @UiField HTML log;

  @UiField com.google.gwt.user.client.ui.FormPanel formPanel;
  @UiField FileUpload uploader;
  @UiField Button submitForm;

  public void onModuleLoad() {
    GWT.<GlobalResources>create(GlobalResources.class).css().ensureInjected();
    RootPanel.get("admin").add(binder.createAndBindUi(this)); 
    
    formPanel.setEncoding(com.google.gwt.user.client.ui.FormPanel.ENCODING_MULTIPART);
    formPanel.setMethod(com.google.gwt.user.client.ui.FormPanel.METHOD_POST);
    formPanel.setAction("/image/store?uid=1000");
  }

  @UiHandler("submitForm")
  void upload(ClickEvent event){
    formPanel.submit();
  }
  
  @UiHandler("submit")
  void entered(ClickEvent event){
    final String statement=data.getValue().trim();
    
    Request req=new Instruction(statement);
    RPC.Operation<?> op = new RPC.Operation<InstructionResponse>(req) {
      protected void processSuccess(InstructionResponse response){
        String logentry="<h5>"+statement+"</h5>";
        String result=response.errorMessage==null? response.outcome : response.errorMessage;
        logentry+="<i>"+result+"</i>";
        log.setHTML(log.getHTML()+logentry);
      }

      protected void processFailure(Throwable oops){
        String logentry="<h5>"+statement+"</h5>";
        logentry+="<em>"+oops.getMessage()+"</em>";
        log.setHTML(log.getHTML()+logentry);
      }
    };
    RPC.Instance.exec(op);
  }
}
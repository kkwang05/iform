package fx.presentment;

import java.util.*;

import fx.model.Domain;
import fx.model.Form;
import fx.model.User;

public class ClientAppContext {
  public static boolean reverseSelectionMode=true;
  
  public static User CurrentUser;
  public static Domain CurrentDomain;
  public static List<Form> Templates;
  
  public static void init(User user){
    CurrentUser=user;
    CurrentDomain=user.defaultDomain;
    Templates=user.templates;
  }
}
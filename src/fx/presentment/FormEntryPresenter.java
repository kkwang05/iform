package fx.presentment;

import java.util.Date;

import com.google.gwt.user.client.ui.Composite;

import fx.model.*;
import fx.model.operation.CUDFormData;
import fx.model.operation.CUDFormDataResponse;
import fx.model.operation.CUDFormEntry;
import fx.model.operation.CUDFormEntryResponse;
import fx.presentment.event.*;
import fx.presentment.script.Interpreter;
import fx.service.RPC;

/**
 * @author kw
 */
public class FormEntryPresenter extends Composite implements FxEventHandler<FormEntryEvent> {
  
  protected FormEntrySessionContext entryContext;
  protected Interpreter interpreter;
  protected transient FormEntry entry; 
  protected transient Form.DockView dv;
  
  private FormPanel panel;

  public FormEntryPresenter(){
    initWidgets();
    FormEntryEvent.addHandler(this);
    entryContext=new FormEntrySessionContext();
    interpreter=Interpreter.getInstance(this);
  }

  protected void initWidgets(){
    panel=new FormPanel();
    initWidget(panel);
  }
  
  protected FormPanel getViewingPanel(){
    return panel;
  }

  public FormEntrySessionContext context(){
    return entryContext;
  }
  
  public void loadEntry(FormEntry entry, boolean ro){
    this.entry=entry;
    entryContext.load(entry, ro);
    interpreter.contextSwitched();
    dv=null;
    renderEntry();
  }

  public boolean process(FormEntryEvent evt){
    if(evt.entry!=entry)
      return false;

    FormData dynaNode=null;
    if(evt instanceof FormEntryEvent.DataInput)
      dynaNode=updateNode((FormEntryEvent.DataInput)evt);
    
    interpreter.eventReceived(evt);
    if(evt.expired)
      return false;

    if(evt instanceof FormEntryEvent.DataInput)
      saveNode(dynaNode);

    else if(evt instanceof FormEntryEvent.LifeCycle){
      saveEntry();
      if(FormEntryEvent.LifeCycle.SUBMITTED.equals(evt.data))
        renderEntry();
    }

    else if(evt instanceof FormEntryEvent.Navigation)
      gotoPage((MetaForm)evt.data);

    else
      throw new RuntimeException("Unsupported FormEntryEvent: "+evt.getClass().getName());

    return true;
  }

  protected FormData updateNode(FormEntryEvent.DataInput evt){
    if(entryContext.isReadOnly() || !evt.persistable)
      return null;
    FormData currentData=entryContext.nodeData(evt.source);
    if(currentData.value==evt.data)
      return null;
    if(currentData.value==null || !currentData.value.equals(evt.data)){
      currentData.value=(String)evt.data;
      return currentData;
    }
    return null;
  }

  public void saveNode(final FormData dynaNode){
    if(entryContext.isReadOnly() || entryContext.entry.isSimulated() || dynaNode==null)
      return;
    entry.lastChangeDate=new Date(); // persist?
    CUDFormData req = CUDFormData.createOrUpdate(dynaNode);
    RPC.Operation<?> op = new RPC.Operation<CUDFormDataResponse>(req){
      public void onSuccess(CUDFormDataResponse resp){
        super.onSuccess(resp);
        if (resp.errorMessage == null && dynaNode.id==null)
          dynaNode.id=resp.persistedRecord.id;
      };
    };
    RPC.Instance.exec(op);
  }

  public void saveEntry(){
    if(entryContext.isReadOnly() || entryContext.entry.isSimulated())
      return;
    CUDFormEntry req=CUDFormEntry.createOrUpdate(entryContext.entry);
    RPC.Operation<?> op = new RPC.Operation<CUDFormEntryResponse>(req){
      public void onSuccess(CUDFormEntryResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage == null && entry.id==null)
          entry.id=resp.persistedRecord.id;
      }
    };
    RPC.Instance.exec(op);
  }

  public void renderEntry(){
    if(dv==null)
      dv=entry.theForm.dockView();
    getViewingPanel().layout(dv, entry.theForm.getBody(), entryContext);
  }

  public void viewNode(MetaForm root){
    getViewingPanel().layout(root, entryContext);
  }

  public void gotoPage(MetaForm page){
    if(dv==null)
      dv=entry.theForm.dockView();
    getViewingPanel().layout(dv, page, entryContext);
  }

  public void closeEntry(){
    entryContext.clear();
    getViewingPanel().clear();
    entry=null;
    dv=null;
  }
}
package fx.presentment;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.logical.shared.BeforeSelectionEvent;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.model.operation.GainedAccess;
import fx.model.operation.user.AuthenticateUser;
import fx.model.operation.user.AuthenticateUserResponse;
import fx.presentment.pub.PublicationManager;
import fx.presentment.user.UserConstants;
import fx.presentment.analytics.FormEntryExaminer;
import fx.presentment.design.FormEditor;
import fx.presentment.design.Home;
import fx.presentment.design.repository.ContextualDirectory;
import fx.presentment.event.*;
import fx.service.RPC;

/**
 * Main entry point for the entire application.
 * 
 * @author kw
 */
public class Designer implements EntryPoint, FxEventHandler<GlobalEvent>, ResizeHandler {

  interface GlobalResources extends ClientBundle {
    @NotStrict
    @Source("./iForm.css")
    CssResource css();
  }
  
  interface MyBinder extends UiBinder<Widget, Designer> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField HTML userInfo;
  @UiField Anchor signOutLink;
  @UiField TabLayoutPanel tabLayoutPanel;
  @UiField Home home;
  @UiField FormEditor fe;
  @UiField PublicationManager pubMan;
  @UiField FormEntryExaminer entryExaminer;
  @UiField HTML homeHdr, feHdr, pubManHdr, entryExaminerHdr;

  public Designer(){
    GlobalEvent.addHandler(this);
    GWT.<GlobalResources>create(GlobalResources.class).css().ensureInjected();
  }

  public void onModuleLoad() {
    RootPanel.get().add(new HTML("<h2>Loading....Please wait</h2>"));
    AuthenticateUser req = null;
    String v=Cookies.getCookie("voucher");
    if(v==null || v.trim().isEmpty()){
      return;
    }
    else {
      req = new AuthenticateUser(v,null,null);
      Cookies.removeCookie("voucher");
    }
    
    RPC.Operation<AuthenticateUserResponse> op = new RPC.Operation<AuthenticateUserResponse>(req) {
      public void onSuccess(AuthenticateUserResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage == null){
          ClientAppContext.init(((GainedAccess)resp).user);
          load();
        }
      }          
    };
    RPC.Instance.exec(op);
  }
  
  private void load(){
    RootPanel.get().clear();
    RootPanel.get().add(binder.createAndBindUi(this));
    userInfo.setHTML(ClientAppContext.CurrentUser.email+"&nbsp;|&nbsp;");
    signOutLink.setHTML("Sign Out");
    homeHdr.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.homeIcon(), FxConstants.Instance.homeTabText()));
    feHdr.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.designIcon(), FxConstants.Instance.formEditorTabText()));
    pubManHdr.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.publicationIcon(), FxConstants.Instance.pubManagerTabText()));
    entryExaminerHdr.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.barChartIcon(), FxConstants.Instance.entryExaminerTabText()));

    Window.addResizeHandler(this);
  }

  @UiHandler("signOutLink")
  void onSignOutClicked(ClickEvent event) {
    Window.open(UserConstants.Instance.portalUrl(), "_self", "");
  };

  @UiHandler("tabLayoutPanel")
  void tabSelected(BeforeSelectionEvent<Integer> evt){
    int t=evt.getItem();
    if(t==1)
      fe.formDirectory.open();
    else if (t==2)
      pubMan.explorer.open();
    else if (t==3)
      entryExaminer.explorer.open();
  }
  
  /**
   * GlobalEventHandler interface
   */
  public boolean process(GlobalEvent evt){
    if(evt.kind==GlobalEvent.Kind.CREATE_PUBLICATION){
      pubMan.createPublication((Form)evt.data);
      tabLayoutPanel.selectTab(2);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_PUBLICATION){
      pubMan.showLastPublication((Form)evt.data);
      tabLayoutPanel.selectTab(2);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_FORM){
      fe.showForm((Form)evt.data);
      tabLayoutPanel.selectTab(1);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_ENTRIES){
      entryExaminer.showEntries((Publication)evt.data);
      tabLayoutPanel.selectTab(3);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_IMAGE_REPOSITORY){
      home.showImages(null);
      tabLayoutPanel.selectTab(0);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_COLOR_REPOSITORY){
      home.showColors(null);
      tabLayoutPanel.selectTab(0);
    }
    else if(evt.kind==GlobalEvent.Kind.SHOW_PROFILE){
      home.showProfile(null);
      tabLayoutPanel.selectTab(0);
    }
    return true;
  }
  
  public void onResize(ResizeEvent event){
    int h=Window.getClientHeight()-20;
    tabLayoutPanel.setHeight(h+"px");
  }
}
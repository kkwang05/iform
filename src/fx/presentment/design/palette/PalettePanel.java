package fx.presentment.design.palette;

import com.allen_sauer.gwt.dnd.client.Palette;
import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.user.client.ui.*;

import fx.presentment.design.EditableNode;


public class PalettePanel extends FlowPanel implements Palette {
  PickupDragController dragController;
  public boolean remove(Widget w) {
    int index = getWidgetIndex(w);
    if (index != -1 && w instanceof EditableNode) {
      EditableNode en = (EditableNode) w;
      EditableNode clone = new EditableNode(en.metaForm);
      dragController.makeDraggable(clone);
      en.metaForm=en.metaForm.clone();
      insert(clone, index);
      // Bug fix - this is critical for the image palette display after dragging 
      if(clone.metaForm.isImage())
        clone.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
    }
    return super.remove(w);
  }
}
package fx.presentment.design.palette;

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.allen_sauer.gwt.dnd.client.*;

import fx.model.*;
import fx.presentment.ClientAppContext;
import fx.presentment.FxMessages;
import fx.presentment.design.EditableNode;
import fx.presentment.widget.TabularFlowPanel;

/**
 * Stacked palettes with roll-up enhancement
 * 
 * @author kw
 */
public class Toolkit extends Composite {
  private StackLayoutPanel stacks;
  private HandlerRegistration rollupHandlerRegistration;
  public Toolkit() {
    stacks=new StackLayoutPanel(Unit.EM);
    initWidget(stacks);
  }

  public void setRollupBehaviorEnabled(boolean rollup){
    // Add rollupBehaviorEnabled='true' to enable
    if(rollup && rollupHandlerRegistration==null)
      rollupHandlerRegistration=stacks.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
	    public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
	      if (event.getItem().intValue() == 0)
	        return;
	      rollup(event.getItem());
	      event.cancel();
	    }
	  });
    else if(!rollup && rollupHandlerRegistration!=null){
      rollupHandlerRegistration.removeHandler();
      rollupHandlerRegistration=null;
    }
  }
  
  private void rollup(Integer item) {
    for(int i=0; i<item.intValue();i++){
      NodePalette w=(NodePalette)stacks.getWidget(0);
      Widget h=stacks.getHeaderWidget(0);
      stacks.remove(h);
      stacks.remove(w);
      stacks.add(w,w.getHeading(true), true, 2.5);
    }
  }
  
  public void initiate(PickupDragController dragController){
    for (Form f : ClientAppContext.Templates) {
      NodePalette p = new NodePalette(dragController, f);
      stacks.add(p, p.getHeading(true), true, 2.5);
    }
    // Load the dynamic palette(s)
    final ImageRepositoryPalette imagesPalette=new ImageRepositoryPalette();
    imagesPalette.setDragController(dragController);
    stacks.add(imagesPalette, imagesPalette.getHeading(true), true, 2.5);
    stacks.addBeforeSelectionHandler(new BeforeSelectionHandler<Integer>() {
      public void onBeforeSelection(BeforeSelectionEvent<Integer> event) {
        if (event.getItem().intValue() == stacks.getWidgetIndex(imagesPalette))
          imagesPalette.refresh();
      }
    });
  }
}

//
// Palette panel containing node prototypes implemented with form templates.
//
class NodePalette extends TabularFlowPanel implements Palette {

  protected PickupDragController dragController;
  private Form template;

  public NodePalette() {}

  public NodePalette(PickupDragController dragController, Form template) {
    this.dragController = dragController;
    this.template = template;

    boolean isFormlet=false;
    for (MetaForm n : template.getBody().getChildren()) {
      EditableNode en = new EditableNode(n);
      dragController.makeDraggable(en);
      add(en);
      isFormlet=n.isComposite();
      if (n.rowMarker == 1)
        rowStarted();
      else if (n.rowMarker == 2)
        rowEnded();
    }
    if(isFormlet)
      addStyleName("zoom");
    getElement().getStyle().setOverflow(Style.Overflow.AUTO);
    getElement().getStyle().setPadding(0.5, Unit.EM);
    getElement().getStyle().setBackgroundColor("#555555");
  }
  
  public String getHeading(boolean asHtml){
    return asHtml? FxMessages.Instance.toolkitHeaderFormat(template.name) : template.name;
  }
  
  public boolean remove(Widget w) {
    int index = getWidgetIndex(w);
    if (index != -1 && w instanceof EditableNode) {
      EditableNode en = (EditableNode) w;
      EditableNode clone = new EditableNode(en.metaForm);
      dragController.makeDraggable(clone);
      en.metaForm=en.metaForm.clone();
      en.metaForm.rowMarker=0;
      if(isInRow(en))
        prepend(clone, index);
      else
        insert(clone, index);
    }
    return super.remove(w);
  }
}
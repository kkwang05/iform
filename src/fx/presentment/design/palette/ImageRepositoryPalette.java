package fx.presentment.design.palette;

import java.util.List;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.FxMessages;
import fx.presentment.design.EditableNode;
import fx.presentment.design.repository.ImageRepository;
import fx.presentment.event.GlobalEvent;

class ImageRepositoryPalette extends Composite implements ChangeHandler {
  interface MyBinder extends UiBinder<Widget, ImageRepositoryPalette> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField PalettePanel container;
  @UiField Anchor moreLink;

  public ImageRepositoryPalette(){
    initWidget(binder.createAndBindUi(this));
    ImageRepository.Instance.addChangeHandler(this);
  }

  public String getHeading(boolean asHtml){
    return asHtml? FxMessages.Instance.toolkitHeaderFormat("Images") : "Images";
  }
  
  public void setDragController(PickupDragController dragController) {
    container.dragController=dragController;
  }
  
  public void onChange(ChangeEvent event){
    refresh();
  }

  @UiHandler("moreLink")
  void showRepository(ClickEvent event){
    GlobalEvent.fire(GlobalEvent.Kind.SHOW_IMAGE_REPOSITORY, null);
  }
  
  public void refresh(){
    if(ImageRepository.Instance.list()==null){
      ImageRepository.Instance.refresh();
      return;
    }
    container.clear();
    List<MetaForm> lst=ImageRepository.Instance.list();
    if(lst!=null && !lst.isEmpty()){
      for(MetaForm mf:lst){
        EditableNode node=new EditableNode(mf);
        node.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
        container.dragController.makeDraggable(node);
        container.add(node);
      }
    }
  }
}

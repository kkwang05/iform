package fx.presentment.design;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;

import fx.model.User;
import fx.model.operation.UpdateUser;
import fx.presentment.user.RegisterPanel;
import fx.presentment.widget.Status;
import fx.presentment.ClientAppContext;
import fx.service.RPC;
import fx.service.Response;
import fx.util.FxCipher;

public class AccountProfile extends Composite {
  interface MyBinder extends UiBinder<Widget, AccountProfile> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Grid personalSection;
  @UiField TextBox firstName, lastName;
  @UiField Label email;
  @UiField Button save, cancel;
  
  @UiField Status lastLogin;
  @UiField CheckBox remember;
  @UiField Anchor changePassword;
  @UiField Grid changePwSection;
  @UiField PasswordTextBox oldPasswordField;
  @UiField PasswordTextBox passwordField;
  @UiField PasswordTextBox passwordField2;
  @UiField Button chgPwOk;
  
  @UiField Anchor addPassphrase;
  @UiField FlowPanel ppSection;
  @UiField TextBox passphrase;
  
  private User currentUser, backup;
  public AccountProfile(){
    initWidget(binder.createAndBindUi(this));
    populateData(ClientAppContext.CurrentUser);
    personalSection.getColumnFormatter().setWidth(0, "200");
  }

  private void populateData(User thisUser){
    currentUser=thisUser;
    backup=currentUser.dto();
    firstName.setText(currentUser.firstName);
    lastName.setText(currentUser.lastName);
    email.setText(currentUser.email);
    
    String dt="";
    if(thisUser.lastLoggedinDate!=null)
      dt=DateTimeFormat.getFormat(PredefinedFormat.DATE_TIME_MEDIUM).format(thisUser.lastLoggedinDate);
    lastLogin.info("Your last login was at: "+dt);
    
    changePwSection.setVisible(false);
    ppSection.setVisible(false);
    String savedLogin=Cookies.getCookie("loggedInState");
    remember.setValue(savedLogin!=null);
  }

  @UiHandler("firstName")
  void enteringFirstName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(firstName,null);
  }
  @UiHandler("lastName")
  void enteringLastName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(lastName,null);
  }
  @UiHandler("firstName")
  void enteredFirstName(ValueChangeEvent<String> e){
    lastName.setFocus(true);        
  }
  @UiHandler("lastName")
  void enteredLastName(ValueChangeEvent<String> e){
    save.setFocus(true);        
  }
  
  @UiHandler("save")
  void save(ClickEvent event){
    boolean changed=!firstName.getValue().trim().equals(currentUser.firstName) || 
        !lastName.getValue().trim().equals(currentUser.lastName);
    if(changed){
      currentUser.firstName=firstName.getValue().trim();
      currentUser.lastName=lastName.getValue().trim();
      updateUser();
    }
  }
  @UiHandler("cancel")
  void cancel(ClickEvent event){
    populateData(currentUser);
  }

  //
  // Security Settings
  //
  
  @UiHandler("remember")
  void toggleAutoLogin(ClickEvent event){
    if(remember.getValue())
      Cookies.setCookie("loggedInState", currentUser.email+"|"+currentUser.password);
    else
      Cookies.removeCookie("loggedInState");
  }
  
  @UiHandler("changePassword")
  void changePassword(ClickEvent event){
    boolean vis=changePwSection.isVisible();
    changePwSection.setVisible(!vis);
    toggleSymbol(changePassword, vis);
    if(!vis){
      // Initiate with blanks
      oldPasswordField.setValue("");
      oldPasswordField.setFocus(true);
      passwordField.setValue("");
      passwordField2.setValue("");
      passwordField.setEnabled(false);
      passwordField2.setEnabled(false);
    }
  }
  @UiHandler("addPassphrase")
  void recoverPassword(ClickEvent event){
    boolean vis=ppSection.isVisible();
    ppSection.setVisible(!vis);
    toggleSymbol(addPassphrase, vis);
    if(!vis){
      // Initiate with current value
      passphrase.setValue(currentUser.passPhrase);
    }
  }
  private void toggleSymbol(Anchor lnk, boolean wasExploded){
    String linkText=lnk.getText();
    if(wasExploded)
      lnk.setHTML("&#9658;"+linkText.substring(1));
    else
      lnk.setHTML("&#9660;"+linkText.substring(1));
  }

  @UiHandler("oldPasswordField")
  void enteringPassword(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(oldPasswordField,null);
  }
  @UiHandler("oldPasswordField")
  void enteredOldPassword(ValueChangeEvent<String> e){
    String opw=oldPasswordField.getValue();
    if(currentUser.password.equals(FxCipher.encrypt(opw))){
      passwordField.setEnabled(true);
      passwordField2.setEnabled(true);
      passwordField.setFocus(true);
    }
    else {
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          oldPasswordField.setValue("");
          oldPasswordField.setFocus(true);        
        }
      });
    }
  }
  @UiHandler("chgPwOk")
  void changePasswordOkay(ClickEvent event){
    String pw=passwordField.getText().trim();
    String pw2=passwordField2.getText().trim();
    if(pw.isEmpty() || pw2.isEmpty())
      return;
    
    String opw=oldPasswordField.getValue();
    boolean valid=pw2.equals(pw) && !pw.equals(opw) &&
      currentUser.password.equals(FxCipher.encrypt(opw)) &&
      RegisterPanel.ValidatePassword(pw);
    if(valid){
      currentUser.password=FxCipher.encrypt(pw);
      remember.setValue(false);
      toggleAutoLogin(null);
      updateUser();
      changePwSection.setVisible(false);
    }
    else {
      oldPasswordField.setValue("");
      passwordField.setValue("");
      passwordField2.setValue("");
      oldPasswordField.setFocus(true);
    }
  }
  
  @UiHandler("passphrase")
  void enteringPassphrase(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(passphrase,null);
  }
  @UiHandler("passphrase")
  void passphraseChanged(ValueChangeEvent<String> evt){
    String nv=passphrase.getValue().trim();
    if(!nv.equals(currentUser.passPhrase)){
      currentUser.passPhrase=nv;
      updateUser();
      ppSection.setVisible(false);
    }
  }
  
  private void updateUser(){
    UpdateUser req=new UpdateUser(currentUser);
    RPC.Operation<Response> op=new RPC.Operation<Response>(req){
      public void onSuccess(Response resp){
        super.onSuccess(resp);
        if(resp.errorMessage!=null){
          processResponse(resp.errorMessage);
        }
        else{
          // Successful
          // Sync up the backup copy of currentUser
          backup=currentUser.dto();
        }
      }
    };
    RPC.Instance.exec(op);
  }
  
  private void processResponse(String msg){
    // Display an error status message
    // Restore to original values
    populateData(backup);
  }
}
package fx.presentment.design;

import java.util.Map;

import com.google.gwt.i18n.client.*;
import com.google.gwt.core.client.GWT;


/**
 * @author kw
 */
public interface NodeTypeProperties extends Constants {
  public static final NodeTypeProperties Instance = GWT.create(NodeTypeProperties.class);
  
  String defaultStyleLabel();
  Map<String, String> descMap();
  Map<String, String> stylesMap();
  Map<String, String> morphsMap();
  String[] scriptableList();
  String[] editableList();
  String[] changeOnDropList();
}


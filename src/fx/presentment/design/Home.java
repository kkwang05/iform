package fx.presentment.design;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.presentment.design.repository.ColorRepository;
import fx.presentment.design.repository.ImageRepository;
import fx.presentment.design.repository.ValueSetRepository;

public class Home extends Composite {
  interface MyBinder extends UiBinder<Widget, Home> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Anchor dashboardLink, profileLink, colorsLink, imagesLink, valueSetsLink;
  @UiField DeckPanel joggler;

  @UiField Dashboard dashboard;
  @UiField ColorRepository colorRepos;
  @UiField ImageRepository imageRepos;
  @UiField ValueSetRepository valueSetRepos;

  public Home(){
    initWidget(binder.createAndBindUi(this));
    showDashboard(null);
  }

  @UiHandler("dashboardLink")
  public void showDashboard(ClickEvent event){
    joggler.showWidget(0);
    dashboard.update();
  }
  
  @UiHandler("colorsLink")
  public void showColors(ClickEvent event){
    joggler.showWidget(1);
    colorRepos.refresh();
  }
  @UiHandler("imagesLink")
  public void showImages(ClickEvent event){
    joggler.showWidget(2);
    imageRepos.refresh();
  }
  @UiHandler("valueSetsLink")
  public void showValueSets(ClickEvent event){
    joggler.showWidget(3);
    valueSetRepos.refresh();
  }

  @UiHandler("profileLink")
  public void showProfile(ClickEvent event){
    joggler.showWidget(4);
  }
}
package fx.presentment.design;

import java.util.Arrays;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.util.CoordinateLocation;
import com.allen_sauer.gwt.dnd.client.util.WidgetArea;
import com.google.gwt.user.client.ui.Widget;

import fx.model.Form;
import fx.presentment.design.NodeTypeProperties;
import fx.presentment.widget.*;
import fx.presentment.design.dnd.TabularFlowPanelDropController;
import fx.presentment.event.FormEvent;
import fx.presentment.design.editing.*;

class FormEditingDropController extends TabularFlowPanelDropController {
  
  private FormEditor editor;
  private transient Form.DockView dv;
  FormEditingDropController(FormEditor editor, TabularFlowPanel dropTarget){
    super(dropTarget);
    this.editor=editor;
  }
  
  @Override
  public void onEnter(DragContext context) {
    EditableNode node=(EditableNode)context.selectedWidgets.get(0);
    boolean lookingAtPages=editor.theForm.pages()!=null && editor.zoomLevel==editor.theForm.getBody();
    if(node.isOffPalette() && lookingAtPages){
      if(positioner != null){
        positioner.removeFromParent();
        positioner = null;
      }
      return;
    }
    super.onEnter(context);
    
    if(lookingAtPages)
      dv=editor.theForm.dockView();
    else 
      dv=null;
    
    if(editor.zoomLevel==null || editor.zoomLevel.id==null)
      editor.formCanvas.accept();
  }
  
  @Override
  public void onDrop(DragContext context) {
    if(positioner==null)
      return;
    super.onDrop(context);
    EditableNode node=(EditableNode)context.selectedWidgets.get(0);
    if(node.isOffPalette()){
      node.addDoubleClickHandler(editor);
      editor.selectionModel.come(node);
      node.addSelectionHandler(editor);
      if(node.metaForm.isImage()){
        node.metaForm.enlargeImage();
        //node.refresh();
      }
      boolean showEditingDialog=Arrays.asList(NodeTypeProperties.Instance.changeOnDropList()).contains(node.metaForm.typeCode+"");
      if(showEditingDialog)
        NodeEditor.Show(node, false, true);
      else
        FormEvent.fire(FormEvent.NodeLevel.ADDED, node);
    }
    else{
      FormEvent.fire(FormEvent.NodeLevel.MOVED, node);
    }
    node.refresh();
    editor.selectionModel.clearSelection();
    editor.selectionModel.select(node);
    // Should we save one call here?
    //updateMenuStates();
  }
  
  @Override
  protected boolean offLimit(Widget hitTarget){
    if(super.offLimit(hitTarget))
      return true;
    if(hitTarget instanceof EditableNode){
      EditableNode p=(EditableNode)hitTarget;
      if(p.metaForm.isDockedPage())
        return true;
      Form.DockView dv=editor.theForm.dockView();
      if(dv!=null && dv.isCenterPage(p.getInnerNode()) && dv.needShiftWhenPaste())
        return true;
    }
    return false;
  }
  
  @Override
  protected int findPosture(CoordinateLocation location, WidgetArea wa){
    if(!(wa.getWidget() instanceof EditableNode) || 
       !((EditableNode)wa.getWidget()).getInnerNode().isPage())
      return super.findPosture(location, wa);
    if (location.getTop() - wa.getTop() < wa.getHeight() / 2)
      return 0;
    else
      return 2;
  }
  
  @Override
  protected void updateInsertionPoint(CoordinateLocation location){
    super.updateInsertionPoint(location);
    // For some reason, there still is chance a page can be moved above the footer.
    // This checking measure fixes that.
    if(dv!=null && dv.needShiftWhenPaste() && dropIndex<=dv.s0.size()+dv.s1.size())
      dropIndex=-1;
  }
}

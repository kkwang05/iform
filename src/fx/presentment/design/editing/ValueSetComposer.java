package fx.presentment.design.editing;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.design.repository.ValueSetRepository;

/**
 * @author kw
 */
public class ValueSetComposer extends Composite implements HasValueChangeHandlers<ValueSet> {
  interface MyBinder extends UiBinder<Widget, ValueSetComposer> {}
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField DeckPanel viewJoggler;
  @UiField RadioButton reviewMode, lookupMode;

  @UiField ValueSetEditor vse;
  @UiField ValueSetRepository directory;

  public ValueSetComposer(){
    initWidget(binder.createAndBindUi(this));
    // Somehow this superfluous request cause 6 identical queries on the server side.
    //directory.refresh();
  }

  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<ValueSet> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }
  
  private transient ValueSet initialValue;
  private transient boolean readOnly;
  
  public void load(ValueSet initialValue, boolean readOnly){
    this.readOnly=readOnly;
    reviewMode.setValue(true);
    lookupMode.setValue(false);
    viewJoggler.showWidget(0);
    reviewMode.setEnabled(!readOnly);
    lookupMode.setEnabled(!readOnly);
    directory.refresh();
    load(initialValue);
  }
  private void load(ValueSet initialValue){
    this.initialValue=initialValue;
    vse.load(initialValue, readOnly);
  }
  
  @UiHandler("vse")
  void onValueSetChange(ValueChangeEvent<ValueSet> event) {
    this.fireEvent(event);
  }

  @UiHandler("reviewMode")
  void toggle1(ValueChangeEvent<Boolean> evt){
    viewJoggler.showWidget(0);
    ValueSet reSelected=directory.selected();
    if(reSelected!=null && !reSelected.id.equals(initialValue.id)){
      load(reSelected);
      ValueChangeEvent.<ValueSet>fire(this, vse.getValueSet());
    }
  }
  
  @UiHandler("lookupMode")
  void toggle2(ValueChangeEvent<Boolean> evt){
    viewJoggler.showWidget(1);
  }
}
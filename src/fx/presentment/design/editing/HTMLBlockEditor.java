package fx.presentment.design.editing;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.MetaForm;
import fx.presentment.widget.*;

/**
 * @author kw
 */
public class HTMLBlockEditor extends NodeEditor {
  interface MyBinder extends UiBinder<PopupPanel, HTMLBlockEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField RichTextEditor nodeText;
  HTMLBlockEditor(boolean plainTextOnly){
    dialogBox=(CustomDialogBox)binder.createAndBindUi(this);
    nodeText.setPlainTextOnly(plainTextOnly);
  }
  
  protected void bind(MetaForm mf){
    nodeText.setValue(mf.text);
    nodeText.setReadOnly(readOnly);
    nodeText.setFocus(!readOnly);
  }
  
  @UiHandler("nodeText")
  void onTextChange(ValueChangeEvent<String> event) {
    String nv = event.getValue().trim();
    target.getInnerNode().text=nv;
    target.refresh();
    apply.setEnabled(hasChanged(target.getInnerNode()));
  }
}
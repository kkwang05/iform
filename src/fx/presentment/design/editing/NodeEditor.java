package fx.presentment.design.editing;

import java.util.HashMap;

import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.MetaForm;
import fx.model.ValueSet;
import fx.presentment.ImageResources;
import fx.presentment.renderer.Renderer;
import fx.presentment.widget.CustomDialogBox;
import fx.presentment.design.EditableNode;
import fx.presentment.design.NodeTypeProperties;
import fx.presentment.design.repository.ContextualDirectory;
import fx.presentment.event.FormEvent;

/**
 * Base class of all pop-up editors implemented for specific node types.
 *
 * @author ke
 */
public abstract class NodeEditor {
  private static HashMap<Short, NodeEditor> SupportedNodeEditors=new HashMap<Short, NodeEditor>();
  static{
    SupportedNodeEditors.put(MetaForm.TC_HTML_BLOCK, new HTMLBlockEditor(false));
    SupportedNodeEditors.put(MetaForm.TC_TEXT_BLOCK, new HTMLBlockEditor(true));
    
    SupportedNodeEditors.put(MetaForm.TC_H_CHECK_BOX, new ChoiceDataSourceEditor());
    SupportedNodeEditors.put(MetaForm.TC_V_CHECK_BOX, new ChoiceDataSourceEditor());
    SupportedNodeEditors.put(MetaForm.TC_H_RADIO_BUTTON, new ChoiceDataSourceEditor());
    SupportedNodeEditors.put(MetaForm.TC_V_RADIO_BUTTON, new ChoiceDataSourceEditor());
    SupportedNodeEditors.put(MetaForm.TC_LIST_BOX, new ChoiceDataSourceEditor());

    SupportedNodeEditors.put(MetaForm.TC_COMPOSITE, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_ACTION_LINK, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_ACTION_BUTTON, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_PAGE_TURNER, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_PAGE_BROWSER, new NodeAppearanceEditor());
    
    SupportedNodeEditors.put(MetaForm.TC_OBSERVER, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_GRADER, new NodeAppearanceEditor());
    SupportedNodeEditors.put(MetaForm.TC_SHOPPING_CART, new NodeAppearanceEditor());
    
    SupportedNodeEditors.put(MetaForm.TC_CONTACT, new NodeAppearanceEditor());
  }
  
  public static String[] supportedStyleVariations(MetaForm node){
    String ss=NodeTypeProperties.Instance.stylesMap().get("styles."+node.typeCode);
    return (ss!=null)? ss.split(";\\s*") : null;
  }
  public static String defaultStyleLabel(){
    return fx.presentment.design.NodeTypeProperties.Instance.defaultStyleLabel();
  }
  public static void Show(EditableNode target, boolean ro, boolean newNode){
    NodeEditor ed=SupportedNodeEditors.get(target.getInnerNode().typeCode);
    if(ed!=null)
      ed.editOrReview(target, ro, newNode);
  }

  @UiField CustomDialogBox dialogBox;
  @UiField ButtonBase xButton;
  @UiField Button ok, cancel, apply;
  
  @UiHandler("ok")
  void okClicked(ClickEvent event){
    applyChangeIfAny();
    dialogBox.hide();
  }

  @UiHandler("apply")
  void applyClicked(ClickEvent event){
    applyChangeIfAny();
  }
  
  @UiHandler("cancel")
  void cancelClicked(ClickEvent event){
    cancelAndClose();
  }

  @UiHandler("dialogBox")
  void escaped(CloseEvent<PopupPanel> event){
    if(event.getTarget()==null)
      cancelAndClose();
  }
  
  @UiHandler("xButton")
  void xClicked(ClickEvent event){
    cancelAndClose();
  }
  
  private void cancelAndClose(){
    if (isNewNode)
      FormEvent.fire(FormEvent.NodeLevel.ADDING_CANCLED, target);
    else if (hasChanged(target.getInnerNode())){
      restoreToInitial(target.getInnerNode());
      target.refresh();
    }
    dialogBox.hide();
  }
  
  protected void applyChangeIfAny() {
    if (isNewNode) {
      FormEvent.fire(FormEvent.NodeLevel.ADDED, target);
      isNewNode = false;
    } else if (hasChanged(target.getInnerNode())){
      FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
    }
    apply.setEnabled(false);
    keepARecord(target.getInnerNode());
  }
  
  protected transient EditableNode target;
  protected transient boolean isNewNode, readOnly;

  protected transient String initialText;
  protected transient String initialRenderingHints;
  protected transient String initialSettings;
  protected transient ValueSet initialValueSet;

  public void editOrReview(EditableNode target, boolean ro, boolean newNode){
    dialogBox.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.designIcon(), target.description()));
    this.target=target;
    this.isNewNode = newNode;
    this.readOnly= ro;
    apply.setEnabled(newNode);
    
    bind(target.getInnerNode());
    keepARecord(target.getInnerNode());
    dialogBox.center();
  }
  
  protected abstract void bind(MetaForm mf);
  
  protected void keepARecord(MetaForm mf){
    initialText=mf.text;
    initialRenderingHints=mf.renderingHints;
    initialSettings=mf.settings;
    initialValueSet=mf.valueSet;
  }
  protected boolean hasChanged(MetaForm mf){
    return mf.renderingHints!=null && !mf.renderingHints.equals(initialRenderingHints) ||
      (mf.text!=null && !mf.text.equals(initialText) || initialText!=mf.text) ||
      (mf.settings!=null && !mf.settings.equals(initialSettings) || initialSettings!=mf.settings) ||
      mf.valueSet!=null && !mf.valueSet.equals(initialValueSet);
  }
  protected void restoreToInitial(MetaForm mf){
    mf.renderingHints=initialRenderingHints;
    mf.text=initialText;
    mf.settings=initialSettings;
    mf.valueSet=initialValueSet;
  }
  protected void present(ListBox lb, String renderingHints, String key, String defaultValue){
    String value=Renderer.getHint(renderingHints, key);
    if(value==null)
      value=defaultValue;
    for(int i=0; i<lb.getItemCount(); i++)
      if(lb.getValue(i).equals(value)) {
        lb.setSelectedIndex(i);
        break;
    }
  }
}
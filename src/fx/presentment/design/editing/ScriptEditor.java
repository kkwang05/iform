package fx.presentment.design.editing;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ButtonBase;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.*;

import fx.model.MetaForm;
import fx.presentment.ImageResources;
import fx.presentment.widget.*;
import fx.presentment.design.EditableNode;
import fx.presentment.design.repository.ContextualDirectory;
import fx.presentment.event.FormEvent;

/**
 */
public class ScriptEditor {
  interface MyBinder extends UiBinder<PopupPanel, ScriptEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  private static ScriptEditor Instance=new ScriptEditor();
  public static void Show(EditableNode target, boolean ro){
    Instance.editOrReview(target, ro);
  }

  @UiField CustomDialogBox dialogBox;
  @UiField ButtonBase xButton;
  @UiField Button ok, cancel, apply;
  
  @UiField TooltipPushButton viewer, editor;
  @UiField DeckPanel joggler;
  @UiField TextArea scriptEditingArea;
  @UiField ScriptOverviewer scriptOverviewer;
  
  ScriptEditor(){
    dialogBox=(CustomDialogBox)binder.createAndBindUi(this);
  }

  @UiHandler("ok")
  void okClicked(ClickEvent event){
    applyChangeIfAny();
    dialogBox.hide();
  }

  @UiHandler("apply")
  void applyClicked(ClickEvent event){
    applyChangeIfAny();
  }
  
  @UiHandler("cancel")
  void cancelClicked(ClickEvent event){
    cancelAndClose();
  }

  @UiHandler("dialogBox")
  void escaped(CloseEvent<PopupPanel> event){
    if(event.getTarget()==null)
      cancelAndClose();
  }
  
  @UiHandler("xButton")
  void xClicked(ClickEvent event){
    cancelAndClose();
  }
  
  private void cancelAndClose(){
    if (hasChanged(target.getInnerNode())){
      restoreToInitial(target.getInnerNode());
      //target.refresh();
    }
    dialogBox.hide();
  }
  
  protected void applyChangeIfAny() {
    if (hasChanged(target.getInnerNode())){
      FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
    }
    apply.setEnabled(false);
    keepARecord(target.getInnerNode());
  }
  
  protected transient EditableNode target;
  protected transient boolean readOnly;

  protected transient String initialScript;

  public void editOrReview(EditableNode target, boolean ro){
    dialogBox.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.designIcon(), target.description()+" [Actions]"));
    this.target=target;
    this.readOnly= ro;
    
    bind(target.getInnerNode());
    apply.setEnabled(false);
    keepARecord(target.getInnerNode());
    dialogBox.center();
    
    joggler.showWidget(0);
    viewer.setEnabled(true);
    editor.setEnabled(false);
  }
  
  protected void bind(MetaForm mf){
    scriptEditingArea.setValue(mf.script);
    scriptEditingArea.setEnabled(!readOnly);
  }
  
  protected void keepARecord(MetaForm mf){
    initialScript=mf.script;
  }
  protected boolean hasChanged(MetaForm mf){
    return mf.script!=null && !mf.script.equals(initialScript);
  }
  protected void restoreToInitial(MetaForm mf){
    mf.script=initialScript;
  }

  @UiHandler("scriptEditingArea")
  void scriptChanged(ChangeEvent evt){
    String t= scriptEditingArea.getValue();
    target.getInnerNode().script=t;
    //target.refresh();
    apply.setEnabled(hasChanged(target.getInnerNode()));
  }

  @UiHandler("viewer")
  void showAll(ClickEvent event){
    MetaForm node=target.getInnerNode();
    scriptOverviewer.load(node);
    
    joggler.showWidget(1);
    viewer.setEnabled(false);
    editor.setEnabled(true);
  }
  
  @UiHandler("editor")
  void showOne(ClickEvent event){
    joggler.showWidget(0);
    viewer.setEnabled(true);
    editor.setEnabled(false);
  }
}
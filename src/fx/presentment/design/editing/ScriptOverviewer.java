package fx.presentment.design.editing;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.renderer.NodeImages;
import fx.presentment.widget.CustomDPHeader;

/**
 * @author kw
 */
public class ScriptOverviewer extends Composite {
  private FlowPanel panel;
  private transient Form theForm;
  
  public ScriptOverviewer(){
    initWidget(panel=new FlowPanel());
    DOM.setStyleAttribute(panel.getElement(), "overflow", "auto");
  }

  public void load(Form form){
    theForm=form;
    load(theForm.getBody());
  }
  
  public void load(MetaForm node){
    panel.clear();
    MetaForm root=node;
    while(!root.isRoot())
      root=root.getParent();
    addScriptBlock(root, node);
  }
  
  private void addScriptBlock(MetaForm node, MetaForm makeOpen){
    String s=node.script;
    if(s!=null){
      String header=node.isRoot()? "[onLoad]" : "["+node.getNodeId()+".onEvent]";
      HTML body=new HTML();
      body.setText(s);
      CustomDPHeader hdr=new CustomDPHeader(NodeImages.Instance.open(), NodeImages.Instance.close(), header);
      DisclosurePanel dp=new DisclosurePanel();
      hdr.bind(dp);
      dp.setContent(body);
      panel.add(dp);
      dp.setOpen(true);
      if(node==makeOpen){
        DOM.setStyleAttribute(hdr.getElement(), "color", "black");
        DOM.setStyleAttribute(dp.getElement(), "color", "black");
      }
      else{
        DOM.setStyleAttribute(hdr.getElement(), "color", "gray");
        DOM.setStyleAttribute(dp.getElement(), "color", "gray");
        dp.setOpen(false);
      }
    }
    
    if(node.getChildren()==null)
      return;
    for(MetaForm child : node.getChildren())
      addScriptBlock(child, makeOpen);
  }
}
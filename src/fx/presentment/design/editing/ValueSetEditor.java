package fx.presentment.design.editing;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.HTMLTable.Cell;
import com.google.gwt.event.dom.client.*;

import fx.model.*;
import fx.presentment.widget.TooltipPushButton;
import fx.presentment.ClientAppContext;

/**
 * @author kw
 */
public class ValueSetEditor extends Composite implements HasValueChangeHandlers<ValueSet> {
  interface MyBinder extends UiBinder<Widget, ValueSetEditor> {}
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  interface SelectionStyle extends CssResource {
    String selectedRow();
    String editedRow();
    int inputSize();
    String newRowPlaceHolder();
  }

  @UiField Panel toolbar;
  @UiField TooltipPushButton deleteButton, addButton;

  @UiField FlowPanel header;
  @UiField TextBox name;
  @UiField TextArea desc;
  @UiField CheckBox share;
  
  @UiField SelectionStyle selectionStyle;
  @UiField FlexTable table;

  public ValueSetEditor(){
    initWidget(binder.createAndBindUi(this));
  }

  public void setShowHeader(boolean showHeader){
    header.setVisible(showHeader);
  }
  
  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<ValueSet> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }
  
  private transient ValueSet vs;
  private transient boolean contentReadOnly;
  private transient int selectedRow=-1;
  private transient boolean isEditing;
  private transient String cpBuffer;
  
  public void load(ValueSet initialValue, boolean contentReadOnly){
    this.contentReadOnly=contentReadOnly;
    toolbar.setVisible(!contentReadOnly);
    load(initialValue);
  }
  private void load(ValueSet initialValue){
    // Present values content. These content can be modified if this editor is being used in the 
    // context of node editing and the loaded valueSet is in-line.
    vs=new ValueSet(initialValue);
    table.removeAllRows();
    for(int i=0; i<vs.values.size(); i++){
      String v=vs.values.get(i);
      table.setWidget(i, 0, new HTML(v));
    }
    isEditing=false;
    selectedRow=-1;
    
    if(header.isVisible()){
      // Launched from ValueSetRepository...initialize the annotation fields
      // The share option (!isInline) once set, is irreversible.
      name.setValue(vs.name);
      desc.setValue(vs.desc);
      share.setValue(!vs.isInline);
      
      boolean canDecorate=vs.ownedByDomainId==null; // private
      name.setReadOnly(!canDecorate);
      desc.setReadOnly(!canDecorate);
      share.setEnabled(canDecorate && vs.isInline);
    }
  }
  
  public ValueSet getValueSet(){
    return vs;
  }
  
  @UiHandler("table")
  void onTableClick(ClickEvent event) {
    Cell cell = table.getCellForEvent(event);
    if (cell != null) {
      int row = cell.getRowIndex();
      if(row!=selectedRow)
        doSelectRow(row);
      else
        doEditRow(row);
    }
    else
      doSelectRow(-1);
  }
  private void doSelectRow(int row) {
    String style = selectionStyle.selectedRow();
    if(selectedRow>=0)
      table.getRowFormatter().removeStyleName(selectedRow, style);
    if(row>=0)
      table.getRowFormatter().addStyleName(row, style);
    selectedRow = row;
    isEditing=false;
  }
  private void doEditRow(int row) {
    if(contentReadOnly || isEditing)
      return;
    isEditing=true;
    HTML html=(HTML)table.getWidget(selectedRow, 0);
    String v=html.getHTML();
    final TextBox ti=new TextBox();
    table.setWidget(selectedRow, 0, ti);
    ti.setText(v);
    ti.setVisibleLength(selectionStyle.inputSize());
    ti.addStyleName(selectionStyle.editedRow());
    ti.setFocus(true);
    ti.selectAll();
    ti.addBlurHandler(new BlurHandler(){
      public void onBlur(BlurEvent event){
        completeEditing(ti);
      }
    });
  }

  //
  // Content change events (only occur if in node editing and vs is in-line)
  //
  private void completeEditing(TextBox ti){
    String newValue=ti.getValue();
    table.setWidget(selectedRow, 0, new HTML(newValue));
    if(!newValue.equals(vs.values.get(selectedRow))){
      vs.values.remove(selectedRow);
      vs.values.add(selectedRow, newValue);
      changeNotify();
    }
    isEditing=false;
  }
  
  @UiHandler("deleteButton")
  void deleteRow(ClickEvent event){
    if(selectedRow<0)
      return;
    table.removeRow(selectedRow);
    table.getRowFormatter().addStyleName(selectedRow, selectionStyle.selectedRow());
    cpBuffer=vs.values.remove(selectedRow);
    changeNotify();
  }
  
  @UiHandler("addButton")
  void addRow(ClickEvent event){
    String tmp=selectionStyle.newRowPlaceHolder();
    if(cpBuffer!=null){
      tmp=cpBuffer;
      cpBuffer=null;
    }
    if(vs.values.contains(tmp))
      return;
    int insertionPoint=selectedRow>=0? selectedRow : vs.values.size();
    table.insertRow(insertionPoint);
    table.setWidget(insertionPoint, 0, new HTML(tmp));
    selectedRow++;
    doSelectRow(insertionPoint);
    doEditRow(insertionPoint);
    vs.values.add(insertionPoint, tmp);
    changeNotify();
  }
  
  private void changeNotify(){
    if(!vs.isInline){
      vs.isInline=true;
      vs.id=null;
      vs.name=null;
      vs.desc=null;
      vs.ownedByDomainId=null;
      vs.createdByUserId=ClientAppContext.CurrentUser.id;
    }
    ValueChangeEvent.<ValueSet>fire(ValueSetEditor.this, vs);
  }

  //
  // Metadata change events (only occur if in ValueSetRepository)
  //
  @UiHandler("name")
  void nameChanged(ValueChangeEvent<String> e){
    String nn=e.getValue();
    if(!nn.equals(vs.name)){
      vs.name=nn;
      ValueChangeEvent.<ValueSet>fire(ValueSetEditor.this, vs);
    }
  }

  @UiHandler("desc")
  void descChanged(ValueChangeEvent<String> e){
    String ndesc=e.getValue();
    if(!ndesc.equals(vs.desc)){
      vs.desc=ndesc;
      ValueChangeEvent.<ValueSet>fire(ValueSetEditor.this, vs);
    }
  }

  @UiHandler("share")
  void inlineChanged(ValueChangeEvent<Boolean> e){
    boolean isShared=e.getValue();
    if(isShared==vs.isInline){
      vs.isInline=!isShared;
      ValueChangeEvent.<ValueSet>fire(ValueSetEditor.this, vs);
    }
  }
}
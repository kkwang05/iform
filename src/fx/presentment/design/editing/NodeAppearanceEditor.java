package fx.presentment.design.editing;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.MetaForm;
import fx.presentment.widget.*;

/**
 * @author kw
 */
class NodeAppearanceEditor extends NodeEditor {
  interface MyBinder extends UiBinder<CustomDialogBox, NodeAppearanceEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField TextBox text, settings;
  @UiField Label settingsLabel;
  
  NodeAppearanceEditor(){
    dialogBox=binder.createAndBindUi(this);
  }
  
  protected void bind(MetaForm mf){
    text.setValue(mf.text);
    text.setEnabled(!readOnly);
    
    if(mf.isDomainObject()){
      settings.setValue(mf.settings);
      settings.setEnabled(!readOnly);
      settingsLabel.setVisible(true);
      settings.setVisible(true);
    }
    else{
      settingsLabel.setVisible(false);
      settings.setVisible(false);
    }
  }

  @UiHandler("text")
  void textChanged(ChangeEvent evt){
    String t= text.getValue();
    if(t!=null && t.trim().isEmpty())
      t=null;
    target.getInnerNode().text=t;
    target.refresh();
    apply.setEnabled(hasChanged(target.getInnerNode()));
  }
  
  @UiHandler("settings")
  void settingsChanged(ChangeEvent evt){
    String t= settings.getValue();
    if(t!=null && t.trim().isEmpty())
      t=null;
    target.getInnerNode().settings=t;
    target.refresh();
    apply.setEnabled(hasChanged(target.getInnerNode()));
  }
}
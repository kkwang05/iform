package fx.presentment.design.editing;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.MetaForm;
import fx.model.ValueSet;
import fx.presentment.event.FormEvent;
import fx.presentment.widget.*;

/**
 * @author kw
 */
public class ChoiceDataSourceEditor extends NodeEditor {
  interface MyBinder extends UiBinder<PopupPanel, ChoiceDataSourceEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField ValueSetComposer vsComposer;
  
  ChoiceDataSourceEditor(){
    dialogBox=(CustomDialogBox)binder.createAndBindUi(this);
  }
  
  protected void bind(MetaForm mf){
    vsComposer.load(mf.valueSet, readOnly);
  }
  
  protected void applyChangeIfAny() {
    if (isNewNode) {
      FormEvent.fire(FormEvent.NodeLevel.ADDED, target);
      isNewNode = false;
    } else if (hasChanged(target.getInnerNode())){
      ValueSet vs=target.getInnerNode().valueSet;
      if(initialValueSet.isInline)
        if(!vs.isInline)
          // Original in-line valueSet is no longer needed, tag on for deletion
          target.getInnerNode().valueSetId=initialValueSet.id;
        else
          // If not already so
          vs.id=initialValueSet.id;
      FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
    } 
    apply.setEnabled(false);
    keepARecord(target.getInnerNode());
    vsComposer.load(target.getInnerNode().valueSet, readOnly);
  }
  
  @UiHandler("vsComposer")
  void onValueSetChange(ValueChangeEvent<ValueSet> event) {
    ValueSet nv= event.getValue();
    target.getInnerNode().valueSet=nv;
    if(hasChanged(target.getInnerNode())){
      target.getInnerNode().valueSetId=nv.id;
      target.refresh();
      apply.setEnabled(true);
    }
  }
}
package fx.presentment.design;

import java.util.*;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.model.operation.design.*;
import fx.presentment.*;
import fx.presentment.widget.*;
import fx.presentment.design.editing.*;
import fx.presentment.design.palette.Toolkit;
import fx.presentment.design.repository.ContextualFormDirectory;
import fx.presentment.event.FormEvent;
import fx.presentment.event.FxEventHandler;
import fx.presentment.event.GlobalEvent;
import fx.presentment.event.NodeSelectionEvent;
import fx.service.*;

/**
 *
 * @author kw
 */
public class FormEditor extends Composite implements DoubleClickHandler,SelectionHandler<EditableNode>, FxEventHandler<FormEvent> {
  interface MyBinder extends UiBinder<Widget, FormEditor> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  private static final int DESIGN_VIEW=0, BUILD_MODE=0;
  private static final int PREVIEW=1;

  @UiField DeckPanel modeJoggler, viewJoggler;

  @UiField Toolkit toolkit;
  @UiField RecycleBin recycleBin;
  @UiField public ContextualFormDirectory formDirectory;

  @UiField FormInfo formHeader;
  @UiField FormCanvas formCanvas;
  @UiField FormEntryEditor previewPanel;

  @UiField TooltipPushButton newFormB, copyFormB, deleteFormB;
  @UiField TooltipPushButton cutB, nodeCopyB, pasteB, mergeB, explodeB, zoomInB, zoomOutB, insertPageB, nodeInfoB, nodeEventsB;
  @UiField TooltipToggleButton lockFormB, dockUndockB, modeB,viewB,vButton;
  @UiField NodeFormatMenu formatter;
  @UiField Paginator paginator;
  @UiField TextBox nodeId;

  @UiField Anchor publishLink;

  @UiField ResizableSplitLayoutPanel topContainer, rhs;
  @UiField Widget lhs, hdr;

  PickupDragController dragController;
  private FormEditingDropController nodeEditingDropController;

  SelectionModel selectionModel;
  transient Form theForm;
  transient MetaForm zoomLevel;
  private transient int currentView;

  public FormEditor(){
    AbsolutePanel boundaryPanel = new AbsolutePanel();
    boundaryPanel.add(binder.createAndBindUi(this));
    initWidget(boundaryPanel);
    FormEvent.addHandler(this);
    
    dragController = new PickupDragController(boundaryPanel, false);
    dragController.setBehaviorDragStartSensitivity(2);
    dragController.setBehaviorMultipleSelection(false);
    dragController.setBehaviorDragProxy(false);
    dragController.setBehaviorConstrainedToBoundaryPanel(true);

    toolkit.initiate(dragController);
    nodeEditingDropController=new FormEditingDropController(this, formCanvas);
    selectionModel=new SelectionModel(formCanvas);

    modeJoggler.showWidget(BUILD_MODE+1);
    viewJoggler.showWidget(currentView=DESIGN_VIEW);
    formDirectory.selectPersonalWorkspace();
    updateMenuStates();
  }

  private void updateMenuStates(){
    boolean initiatedCreateForm=(theForm!=null && theForm.getBody()==null);
    boolean persistentFormLoaded=(theForm!=null && theForm.getBody()!=null);
    boolean canEdit=theForm!=null && !theForm.isReadOnly();

    //
    //	FORM EDITING OPTIONS
    //
    newFormB.setEnabled(formDirectory.selectedPersonalWorkspace() && !initiatedCreateForm);
    copyFormB.setEnabled(persistentFormLoaded);
    deleteFormB.setEnabled(persistentFormLoaded && canEdit);
    lockFormB.setEnabled(persistentFormLoaded && !theForm.isPublished());
    lockFormB.setDown(persistentFormLoaded && theForm.isReadOnly());
    if(persistentFormLoaded && theForm.isReadOnly()){
      publishLink.setVisible(true);
      publishLink.setHTML(theForm.isPublished()? "Last Publication>>" : "Publish>>");
    }
    else
      publishLink.setVisible(false);

    //
    //  Viewing/zooming/navigation
    //
    viewB.setEnabled(persistentFormLoaded);
    EditableNode sn=selectionModel.getTheOnlySelected();
    boolean canZoomIn=(sn!=null && (sn.getSubSelectionNode()!=null || sn.getInnerNode().isComposite()||sn.getInnerNode().isPage()));
    boolean canZoomOut=(zoomLevel!=null && !zoomLevel.isRoot());
    boolean inContinuousPaging=theForm!=null && theForm.pages()!=null && zoomLevel==theForm.getBody();
    zoomInB.setEnabled(canZoomIn || inContinuousPaging);
    zoomOutB.setEnabled(canZoomOut);
    paginator.setVisible(theForm!=null && theForm.pages()!=null && zoomLevel!=theForm.getBody());
    
    //
    //  NODE SPECS (content, scripting, and identity)
    //
    nodeInfoB.setEnabled(sn!=null && Arrays.asList(NodeTypeProperties.Instance.editableList()).contains(sn.getInnerNode().typeCode+""));
    nodeEventsB.setEnabled(sn!=null && Arrays.asList(NodeTypeProperties.Instance.scriptableList()).contains(sn.getInnerNode().typeCode+""));
    if(!nodeEventsB.isEnabled() && selectionModel.getAllSelected()==null)
      nodeEventsB.setEnabled(theForm!=null);
    if(sn!=null)
      nodeId.setValue(sn.getInnerNode().getNodeId());
    else
      nodeId.setValue("");
    nodeId.setEnabled(canEdit);
    
    //
    //	NODE COPY, CUT, AND PASTE
    //
    boolean hasSelection=(selectionModel.getAllSelected()!=null);
    boolean selectionIsDocked=sn!=null && sn.getInnerNode().isDockedPage();
    nodeCopyB.setEnabled(hasSelection && !selectionIsDocked);
    
    boolean canCut1=hasSelection && zoomLevel!=null;
    boolean canCut2=true;
    if(sn!=null && sn.getSubSelectionNode()!=null)
      canCut2=!sn.getSubSelectionNode().isOnlyChild();
    else
      canCut2=hasSelection && selectionModel.getAllSelected().size()<zoomLevel.getChildren().size();
    boolean canCut=canEdit && canCut1 && canCut2 && !selectionIsDocked;
    cutB.setEnabled(canCut);
    
    boolean condition1=recycleBin.pageOnTop() && !inContinuousPaging;  
    boolean condition2=!recycleBin.pageOnTop() && inContinuousPaging && (sn==null || sn.getSubSelectionNode()==null);
    pasteB.setEnabled(canEdit && recycleBin.canPaste() && !(condition1||condition2) && !selectionIsDocked);

    // Disable special docked cut and paste for multiple selection.
    if(hasSelection && sn==null){
      Form.DockView dv=theForm.dockView();
      if(dv!=null && dv.isCenterPage(selectionModel.getAllSelected().get(0).getInnerNode())
         && dv.needShiftWhenPaste()){
        cutB.setEnabled(false);
        pasteB.setEnabled(false);
      }
    }
    
    //
    //  COMPOSITE NODES MERGING AND UN-MERGING
    //
    // Pages can be zoomed in, but not exploded
    boolean selectedFormlet=sn!=null && sn.getInnerNode().isComposite();
    explodeB.setEnabled(canEdit && selectedFormlet && sn.getSubSelectionNode()==null && !formCanvas.isInRow(sn));
    mergeB.setEnabled(canEdit && selectionModel.isSelectionInBlock() &&
        (!inContinuousPaging || selectionModel.getAllSelected().size()<3));
    
    //
    //	PAGE INSERTION, DOCKING, AND UNDOCKING
    //
    boolean initiatedCreatePage=zoomLevel!=null && zoomLevel.isPage() && zoomLevel.id==null;
    insertPageB.setEnabled(canEdit && !initiatedCreateForm && !initiatedCreatePage);

    boolean selectedSinglePageForEditing=sn!=null && sn.getInnerNode().isPage() && canEdit;
    dockUndockB.setDown(sn!=null && sn.getInnerNode().isDockedPage());
    if(selectedSinglePageForEditing){
      if(!sn.getInnerNode().isDockedPage()){
        Form.DockView dv=theForm.dockView();
        boolean selectedCenterPage=dv.isCenterPage(sn.getInnerNode());
        dockUndockB.setEnabled(dv.isDockingEmpty() || !selectedCenterPage && dv.hasDockingRoom());
        
        // Maybe veto the earlier decision to avoid leaving only docked pages on canvas after cutting
        if(canCut && selectedCenterPage && dv.p2==null) 
          cutB.setEnabled(false);
      }
      else
        dockUndockB.setEnabled(true);
    }
    else
      // Disallow docking/undocking in multiple selection
      dockUndockB.setEnabled(false);

    //
    //  NODE FORMATTING
    //
    formatter.setVisible(theForm!=null && !theForm.isReadOnly());
    if(formatter.isVisible()){
      if(sn!=null){
        formatter.setEnabled(true);
        formatter.load(sn);
      }
      else
        formatter.setEnabled(false);
    }
  }

  @UiHandler("formDirectory")
  void formSelected(SelectionEvent<Form> item){
    if(item.getSelectedItem()!=null || theForm!=null)
      loadForm(item.getSelectedItem());
    else
      updateMenuStates();
  }

  /**
   * Programmatically search and select/focus on a form. Requery if necessary.
   */
  public void showForm(Form aForm){
    loadForm(null);
    formDirectory.searchAndSelect(aForm, false);
  }

  public void createForm(){
    Form nf=new Form(FxConstants.Instance.newFormName(), FxConstants.Instance.newFormDescription());
    nf.createdByUserId=ClientAppContext.CurrentUser.id;
    formDirectory.clearSelection();
    loadForm(nf);
    if(modeJoggler.getVisibleWidget()!=BUILD_MODE)
      joggleModes();
  }

  //
  // Serves three purposes (see above three methods):
  //	1. Empty out this editor (null);
  //	2. Initate creating (head only form);
  // 	3. Edit or view a fully fledged form.
  public void loadForm(Form form){
    theForm=form;
    zoomLevel=(theForm!=null)? theForm.getBody():null;

    formHeader.load(theForm);
    formCanvas.layout(this, zoomLevel, null);
    paginator.reloadForm(theForm);

    if(theForm==null || theForm.isReadOnly())
      dragController.unregisterDropController(nodeEditingDropController);
    else
      dragController.registerDropController(nodeEditingDropController);

    switchToView(DESIGN_VIEW);
    //updateMenuStates();
  }

  @UiHandler("zoomInB")
  void clickedZoomIn(ClickEvent event) {
    if(selectionModel.getTheOnlySelected()!=null){
      MetaForm mf=selectionModel.getTheOnlySelected().getSubSelectionNode();
      if(mf!=null && mf.isComposite())
        zoomLevel=mf;
      else if(mf!=null)
        zoomLevel=mf.getParent();
      else
        zoomLevel=selectionModel.getTheOnlySelected().getInnerNode();
    }
    else
      zoomLevel=theForm.pages().get(0);

    formCanvas.layout(this, zoomLevel, null);
    updateMenuStates();
    if(zoomLevel.residesInPage()){
      paginator.reloadForm(theForm);
      paginator.focusOnPage(zoomLevel);
    }
  }
  @UiHandler("zoomOutB")
  void clickedZoomOut(ClickEvent event) {
    MetaForm currentNode=zoomLevel;
    zoomLevel=zoomLevel.getParent();
    formCanvas.layout(this,zoomLevel, currentNode);
    updateMenuStates();
  }

  @UiHandler("paginator")
  void turnPage(SelectionEvent<MetaForm> event) {
    zoomLevel=event.getSelectedItem();
    formCanvas.layout(this,zoomLevel, null);
    updateMenuStates();
  }

  @UiHandler("insertPageB")
  void clickedInsertPage(ClickEvent event) {
    if(theForm.pages()==null){
      Paginate req=new Paginate(theForm.getBody());
      RPC.Operation<?> persistOperation = new RPC.Operation<PaginateResponse>(req) {
        public void onSuccess(PaginateResponse resp) {
          super.onSuccess(resp);
          if (resp.errorMessage != null){
            showError(resp.errorMessage);
            return;
          }
          // Server side change done, now sync up on client
          theForm.setBody(resp.persistedRoot);
          clickedInsertPage(null);
        }
      };
      RPC.Instance.exec(persistOperation);
    }
    else{
      MetaForm currentPage=MetaForm.createPage(theForm.id, theForm.getBody().id);
      // theForm.getBody().addChild(currentPage);
      // Provide a way back to the top level in case addPage is cancelled.
      currentPage.setParent(theForm.getBody());
      zoomLevel=currentPage;
      formCanvas.layout(this, zoomLevel, null);
      // Load the newly created page, and set the transient page as current
      paginator.reloadForm(theForm);
      paginator.focusOnPage(zoomLevel);
      updateMenuStates();
      if(modeJoggler.getVisibleWidget()!=BUILD_MODE)
        joggleModes();
    }
  }

  @UiHandler("mergeB")
  void clickedMerge(ClickEvent event) {
    if(theForm.pages()!=null && zoomLevel==theForm.getBody()){
      mergePages();
      return;
    }
    final List<EditableNode> selectedList=selectionModel.getAllSelected();
    final int idx=formCanvas.getWidgetIndex(selectedList.get(0));
    final Long[] ids=new Long[selectedList.size()];
    for(int i=0; i<selectedList.size(); i++)
      ids[i]=selectedList.get(i).getInnerNode().id;

    MetaForm subroot=MetaForm.createComposite(theForm.id, zoomLevel.id);
    subroot.cardinal=selectedList.get(0).getInnerNode().cardinal;
    // Request creating new Formlet node and moving selected nodes under it
    MergeNodes req = new MergeNodes(subroot, ids);
    RPC.Operation<?> persistOperation = new RPC.Operation<MergeNodesResponse>(req) {
      public void onSuccess(MergeNodesResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        // Server side change done, now sync up on client
        MetaForm subroot=resp.persistedNode;
        for(EditableNode n:selectedList){
          n.getInnerNode().removeFromParent();
          subroot.addChild(n.getInnerNode());
        }
        zoomLevel.insertChild(subroot, idx);
        // Re-render and pre-select new node
        formCanvas.layout(FormEditor.this, zoomLevel, subroot);
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }
  private void mergePages() {
    final EditableNode page1=selectionModel.getAllSelected().get(0);
    final EditableNode page2=selectionModel.getAllSelected().get(1);
    final Long[] ids=new Long[page2.getInnerNode().getChildren().size()];
    for(int i=0; i<ids.length; i++)
      ids[i]=page2.getInnerNode().getChildren().get(i).id;
    float cardinal1=page1.getInnerNode().getChildren().get(page1.getInnerNode().getChildren().size()-1).cardinal;
    float cardinal2=page2.getInnerNode().getChildren().get(0).cardinal;
    MergeNodes req = new MergeNodes(page1.getInnerNode().id, ids);
    req.cardinalAdjustment=cardinal2-cardinal1;
    RPC.Operation<?> persistOperation = new RPC.Operation<MergeNodesResponse>(req) {
      public void onSuccess(MergeNodesResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null) {
          showError(resp.errorMessage);
          return;
        }
        // Server side change done, now sync up on client
        int insertionPoint = page1.getInnerNode().getChildren().size();
        for (int i = page2.getInnerNode().getChildren().size() - 1; i >= 0; i--) {
          MetaForm mf = page2.getInnerNode().getChildren().get(i);
          mf.removeFromParent();
          page1.getInnerNode().insertChild(mf, insertionPoint);
        }
        page2.getInnerNode().removeFromParent();
        // Re-render and pre-select first page
        formCanvas.layout(FormEditor.this, zoomLevel, page1.getInnerNode());
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  @UiHandler("explodeB")
  void clickedExplode(ClickEvent event) {
    MetaForm subroot=selectionModel.getTheOnlySelected().getInnerNode();
    final int idx=formCanvas.getWidgetIndex(selectionModel.getTheOnlySelected());
    final int nodeCount=subroot.getChildren().size();
    // Client-side change first - one removal and multiple moving
    subroot.removeFromParent();
    CompositeRequest req=new CompositeRequest();
    for(int i=nodeCount-1; i>=0; i--){
      MetaForm mf=subroot.getChildren().get(i);
      mf.removeFromParent();
      zoomLevel.insertChild(mf, idx);
      req.addRequest(CUDMetaForm.update(mf));
    }
    req.addRequest(CUDMetaForm.delete(subroot));
    // Server-side changes
    RPC.Operation<?> persistOperation = new RPC.Operation<CompositeResponse>(req) {
      public void onSuccess(CompositeResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        // Re-render and select exploded nodes
        formCanvas.layout(FormEditor.this, zoomLevel, null);
        for(int i=0;i<nodeCount;i++)
          selectionModel.select((EditableNode)formCanvas.getWidget(idx+i));
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  @UiHandler("nodeCopyB")
  void clickedNodeCopy(ClickEvent event) {
    recycleBin.storeOnTop(selectionModel.adjustSelected());
    updateMenuStates();
  }

  @UiHandler("cutB")
  void clickedNodeCut(ClickEvent event) {
    CompositeRequest req=new CompositeRequest();
    Form.DockView dv=theForm.dockView();
    EditableNode sn=selectionModel.getTheOnlySelected();
    if(dv!=null && sn!=null && dv.isCenterPage(sn.getInnerNode()) && dv.needShiftWhenCut()){
      zoomLevel.replace(dv.p2, dv.p);
      req.addRequest(CUDMetaForm.update(dv.p2));
      req.addRequest(CUDMetaForm.delete(dv.p));
    }
    else{
      // Update before deletion - otherwise nextSibling()/previousSibling will not work!
      for(EditableNode n : selectionModel.adjustAffected()){
        MetaForm mf=n.getInnerNode();
        req.addRequest(CUDMetaForm.update(mf));
      }
      for(EditableNode n : selectionModel.getAllSelected()){
        MetaForm mf=n.getInnerNode();
        mf.removeFromParent();
        req.addRequest(CUDMetaForm.delete(mf));
      }
    }
    RPC.Operation<?> persistOperation = new RPC.Operation<CompositeResponse>(req) {
      public void onSuccess(CompositeResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        recycleBin.storeOnTop(selectionModel.adjustSelected());
        formCanvas.layout(FormEditor.this, zoomLevel,null);
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  @UiHandler("pasteB")
  void clickedNodePaste(ClickEvent event) {
    if(zoomLevel==null){
      pastedOnBlankForm();
      return;
    }
    if (zoomLevel.isPage() && zoomLevel.id==null){
      pastedOnBlankPage();
      return;
    }
    //
    // Paste to an existing form (in editing)
    //
    final MetaForm insertionParent;
    final int insertionPoint;
    final EditableNode sn=selectionModel.getTheOnlySelected();
    
    final Form.DockView dv=theForm.dockView();
    final boolean pasteOnDock=sn!=null && dv!=null && 
      dv.isCenterPage(sn.getInnerNode()) && dv.needShiftWhenPaste();
    if(pasteOnDock){
      // SPECIAL PAGE PASTING
      insertionParent=zoomLevel;
      insertionPoint=formCanvas.getWidgetIndex(sn);
    }
    else if(sn!=null && sn.getSubSelectionNode()!=null){
      // INTERIOR PASTING
      MetaForm subNode=sn.getSubSelectionNode();
      insertionParent=subNode.getParent();
      insertionPoint=subNode.preInsertionPoint();
    }
    else if(selectionModel.getAllSelected()==null){
      // PASTE TO END OF CANVAS 
      insertionParent=zoomLevel;
      insertionPoint=zoomLevel.getChildren().size();
    }
    else {
      // NORMAL Paste above current selection
      insertionParent=zoomLevel;
      insertionPoint=formCanvas.getFirstWidgetInRowIndex(selectionModel.getAllSelected().get(0));
    }

    CompositeRequest req=new CompositeRequest();
    
    // STEP 1. Insert nodes on client side
    int i=0;
    for(EditableNode n: recycleBin.toPaste()){
      MetaForm mf=n.getInnerNode().clone();
      if(i==0 && pasteOnDock){
        insertionParent.replace(mf, dv.p);
        i+=dv.s1.size();
      }
      else{
        insertionParent.insertChild(mf, insertionPoint+i);
      }
      req.addRequest(CUDMetaForm.create(mf));
      i++;
    }
    if(pasteOnDock){
      dv.p.rowMarker=0;
      insertionParent.insertChild(dv.p, insertionPoint+i);
      req.addRequest(CUDMetaForm.update(dv.p));
    }

    RPC.Operation<?> persistOperation = new RPC.Operation<CompositeResponse>(req) {
      public void onSuccess(CompositeResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        // STEP 2. SYNC UP with server
        // (Must sync up before layout in order to stamp the correct metaForm nodes in the new widgets.)
        int i=0;
        for(Response r: resp.responseList){
          MetaForm saved=((CUDMetaFormResponse)r).persistedNode;
          insertionParent.getChildren().get(insertionPoint+i).syncUp(saved);
          if(i==0 && pasteOnDock)
            i+=dv.s1.size();
          i++;
        }

        // STEP 3. RE-RENDER and SELECT
        if(insertionParent!=zoomLevel){
          sn.refresh();
        }else{
          formCanvas.layout(FormEditor.this, zoomLevel, null);
          i=0;
          for(int j=0; j<recycleBin.toPaste().size(); j++){
            selectionModel.select((EditableNode)formCanvas.getWidget(insertionPoint+i));
            if(i==0 && pasteOnDock)
              i+=dv.s1.size();
            i++;
          }
        }
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }
  private void pastedOnBlankForm(){
    MetaForm root=new MetaForm(MetaForm.TC_ROOT, null, null);
    for(int i=0; i<recycleBin.toPaste().size();i++){
      MetaForm mf = recycleBin.toPaste().get(i).getInnerNode().clone();
      root.addChild(mf);
    }
    theForm.setBody(root);
    CreateForm req = new CreateForm(theForm);
    RPC.Operation<?> persistOperation = new RPC.Operation<CreateFormResponse>(req) {
      public void onSuccess(CreateFormResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        formDirectory.addThenSelect(resp.createdForm, true);
        selectionModel.selectAll();
        updateMenuStates();
      }
    };
    RPC.Instance.exec(persistOperation);
  }
  private void pastedOnBlankPage(){
    theForm.getBody().addChild(zoomLevel);
    for(int i=0; i<recycleBin.toPaste().size();i++){
      MetaForm mf = recycleBin.toPaste().get(i).getInnerNode().clone();
      zoomLevel.addChild(mf);
    }
    CUDMetaForm req = CUDMetaForm.create(zoomLevel);
    RPC.Operation<?> persistOperation = new RPC.Operation<CUDMetaFormResponse>(req) {
      public void onSuccess(CUDMetaFormResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage == null) {
          zoomLevel.syncUp(resp.persistedNode);
          formCanvas.layout(FormEditor.this, zoomLevel,null);
          selectionModel.selectAll();
          updateMenuStates();
        } else
          showError(resp.errorMessage);
      }
    };
    RPC.Instance.exec(persistOperation);
  }
  
  @UiHandler("dockUndockB")
  void dockUndock(ClickEvent event){
    Form.DockView dv=theForm.dockView();
    boolean centerPageDraggable=!dv.needShiftWhenPaste();
    EditableNode sn=selectionModel.getTheOnlySelected();
    sn.removeStyleName(sn.styleName());
    if(sn.getInnerNode().isDockedPage())
      sn.getInnerNode().typeCode=MetaForm.TC_PAGE;
    else
      sn.getInnerNode().typeCode=MetaForm.TC_DOCKED_PAGE;
    sn.refresh();
    sn.addStyleName(sn.styleName());
  
    Widget dummy = new HTML("dummy");
    if (sn.getInnerNode().isDockedPage()) { // Docking
      int newPosition = dv.insertionPoint();
      formCanvas.remove(sn);
      if (newPosition == 0)
        formCanvas.insert(dummy, 0);
      else if (newPosition == 1)
        formCanvas.prepend(dummy, 1);
      else if (newPosition == 3)
        formCanvas.append(dummy, 2);
      else if (newPosition == 4)
        formCanvas.insert(dummy, 4);
      else
        System.out.println("ERROR!!! NO PLACE TO PUT DOCKED PAGE");
      formCanvas.replace(sn, dummy);
      FormEvent.fire(FormEvent.NodeLevel.MOVED, sn);
      dragController.makeNotDraggable(sn);
      // Maybe disable dragging of center page after docking
      if(centerPageDraggable && (dv=theForm.dockView()).needShiftWhenPaste())
        dragController.makeNotDraggable(formCanvas.getWidget(dv.s0.size()));
    } else { // Un-docking
      int bottomIdx=dv.s0.size() + dv.s1.size();
      boolean wasLastDock=formCanvas.getWidgetIndex(sn)==bottomIdx;
      boolean wasOnlyDock=formCanvas.getWidgetIndex(sn)==0 && bottomIdx==1;
      if((wasLastDock || wasOnlyDock) && sn.getInnerNode().rowMarker==0)
        FormEvent.fire(FormEvent.NodeLevel.CHANGED, sn);
      else{
        formCanvas.insertBelow(dummy, bottomIdx);
        formCanvas.remove(sn);
        formCanvas.replace(sn, dummy);
        FormEvent.fire(FormEvent.NodeLevel.MOVED, sn);
      }
      dragController.makeDraggable(sn);
      // Maybe re-allow dragging of center page after un-docking
      if(!centerPageDraggable && !(dv=theForm.dockView()).needShiftWhenPaste())
        dragController.makeDraggable(formCanvas.getWidget(dv.s0.size()));
    }
  }

  @UiHandler("formCanvas")
  void clickedCanvas(ClickEvent event){
    selectionModel.clearSelection();
    updateMenuStates();
  }

  @UiHandler("nodeId")
  void nodeIdChanged(ValueChangeEvent<String> e){
    String nodeName=e.getValue();
    if(nodeName.trim().isEmpty())
      nodeName=null;
    EditableNode sn=selectionModel.getTheOnlySelected();
    sn.getInnerNode().name=nodeName;
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, sn);
  }
  @UiHandler("nodeInfoB")
  void clickedNodeInfo(ClickEvent event){
    EditableNode sn=selectionModel.getTheOnlySelected();
    NodeEditor.Show(sn, theForm.isReadOnly(), false);
  }
  @UiHandler("nodeEventsB")
  void clickedNodeEvents(ClickEvent event){
    EditableNode sn=selectionModel.getTheOnlySelected();
    if(sn!=null)
      ScriptEditor.Show(sn, theForm.isReadOnly());
    else
      ScriptEditor.Show(new EditableNode(theForm.getBody()), theForm.isReadOnly());
  }

  public void onSelection(SelectionEvent<EditableNode> event){
    updateMenuStates();
  }

  /**
   *  Double click event handling
   */
  public void onDoubleClick(DoubleClickEvent event){
    selectionModel.clearSelection();
    NodeSelectionEvent.fire((EditableNode)event.getSource());
    NodeEditor.Show((EditableNode)event.getSource(), theForm.isReadOnly(), false);
  }

  @UiHandler("formHeader")
  void formInfoChanged(ValueChangeEvent<Form> event){
    updateForm(true, false);
  }

  private void updateForm(final boolean refreshNavigation, final boolean refreshContent){
    UpdateForm req = new UpdateForm(theForm);
    RPC.Operation<?> persistOperation = new RPC.Operation<UpdateFormResponse>(req) {
      public void onSuccess(UpdateFormResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        if(refreshNavigation)
          formDirectory.refreshSelectedNode();
        if(refreshContent) // Reload
          loadForm(theForm);
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  @UiHandler("newFormB")
  void clickedNew(ClickEvent event) {
    createForm();
  }

  @UiHandler("copyFormB")
  void clickedCopy(ClickEvent event) {
    CreateForm req = new CreateForm(copyForm(theForm));
    RPC.Operation<?> persistOperation = new RPC.Operation<CreateFormResponse>(req) {
      public void onSuccess(CreateFormResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        formDirectory.addThenSelect(resp.createdForm, true);
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  private Form copyForm(Form orig){
    Form copy=new Form("Copy of "+orig.name, orig.desc);
    copy.createdByUserId=ClientAppContext.CurrentUser.id;
    copy.setBody(orig.getBody().clone());
    return copy;
  }

  @UiHandler("deleteFormB")
  void clickedDelete(ClickEvent event) {
    DeleteForm req = new DeleteForm(theForm.id);
    RPC.Operation<?> persistOperation = new RPC.Operation<DeleteFormResponse>(req) {
      public void onSuccess(DeleteFormResponse resp) {
        super.onSuccess(resp);
        if (resp.errorMessage != null){
          showError(resp.errorMessage);
          return;
        }
        formDirectory.removeSelected();
        //loadForm(null);
      }
    };
    RPC.Instance.exec(persistOperation);
  }

  @UiHandler("lockFormB")
  void clickedLock(ClickEvent event) {
    if(lockFormB.isDown())
      theForm.status=Form.FORM_ST_LOCKED;
    else
      theForm.status=Form.FORM_ST_EDITING;
    updateForm(false, true);
  }

  @UiHandler("publishLink")
  void clickedPublish(ClickEvent event) {
    if(theForm.status==Form.FORM_ST_LOCKED)
      GlobalEvent.fire(GlobalEvent.Kind.CREATE_PUBLICATION, theForm);
    else if(theForm.status==Form.FORM_ST_PUBLISHED)
      GlobalEvent.fire(GlobalEvent.Kind.SHOW_PUBLICATION, theForm);
  }

  @UiHandler("vButton")
  void clickedV(ClickEvent event) {
    if(vButton.isDown()){
      rhs.setSplitPosition(hdr, 0);
      topContainer.setSplitPosition(lhs, 0);
    }
    else{
      topContainer.restoreToPreviousSplitPosition(lhs);
      rhs.restoreToPreviousSplitPosition(hdr);
    }
  }

  @UiHandler("modeB")
  void clickedMode(ClickEvent event) {
    joggleModes();
  }

  @UiHandler("viewB")
  void clickedView(ClickEvent event) {
    joggleViews();
  }

  private boolean formDirectoryOpened;
  public void joggleModes(){
    modeJoggler.showWidget((modeJoggler.getVisibleWidget()+1) % 2);
    if(formDirectory.isVisible() && !formDirectoryOpened){
      formDirectory.open();
      formDirectoryOpened=true;
    }
  }

  public void joggleViews(){
    switchToView((currentView+1)%2);
  }

  private void switchToView(int view){
    if(view!=currentView && view==PREVIEW && theForm!=null){
      previewPanel.startSimulatedEntry(theForm);
      if(!zoomLevel.isRoot())
        previewPanel.viewNode(zoomLevel);
    }
    viewJoggler.showWidget(currentView=view);

    viewB.setDown(currentView!=DESIGN_VIEW);
    if(currentView==PREVIEW){
      cutB.setEnabled(false);
      nodeCopyB.setEnabled(false);
      pasteB.setEnabled(false);
      nodeInfoB.setEnabled(false);
      nodeEventsB.setEnabled(false);
      nodeId.setEnabled(false);
      dockUndockB.setEnabled(false);
      zoomInB.setEnabled(false);
      zoomOutB.setEnabled(false);
      mergeB.setEnabled(false);
      explodeB.setEnabled(false);
      insertPageB.setEnabled(false);
      paginator.setVisible(false);
      formatter.setEnabled(false);
    }
    else
      updateMenuStates();
  }

  /**
   *  FormEvent handling
   */
  public boolean process(FormEvent evt) {
    if(evt.nature==FormEvent.FormLevel.UPDATED){
      formDirectory.update((Form)evt.data);
      return true;
    }

    final EditableNode node = (EditableNode) evt.data;
    final boolean needSync;
    if (evt.nature == FormEvent.NodeLevel.ADDING_CANCLED) {
      formCanvas.remove(node);
      selectionModel.go(node);
      updateMenuStates();
      if (formCanvas.getWidgetCount() == 0)
        formCanvas.ready();
        assert(formCanvas.collectPostureChangedNodes().isEmpty()):
          "Error!!! Lingering posture changes after cancelled insertion";
      return true;
    }

    RPC.Operation<?> persistOperation=null;
    CUDMetaForm cudReq = null;
    if (evt.nature == FormEvent.NodeLevel.ADDED) {
      if (zoomLevel==null && formCanvas.getWidgetCount() == 1) {
        // CREATION
        MetaForm root = new MetaForm(MetaForm.TC_ROOT, null, null);
        root.addChild(node.getInnerNode());
        theForm.setBody(root);
        CreateForm req = new CreateForm(theForm);
        persistOperation = new RPC.Operation<CreateFormResponse>(req) {
          public void onSuccess(CreateFormResponse resp) {
            super.onSuccess(resp);
            if (resp.errorMessage == null) {
              theForm=resp.createdForm;
              formDirectory.addThenSelect(theForm, true);
              selectionModel.selectAll();
              updateMenuStates();
            } else
              showError(resp.errorMessage);
          }
        };
      }
      else if (zoomLevel.isPage() && formCanvas.getWidgetCount() == 1) {
      // CREATION PAGE
        theForm.getBody().addChild(zoomLevel);
        zoomLevel.addChild(node.getInnerNode());
        CUDMetaForm req = CUDMetaForm.create(zoomLevel);
        persistOperation = new RPC.Operation<CUDMetaFormResponse>(req) {
          public void onSuccess(CUDMetaFormResponse resp) {
            super.onSuccess(resp);
            if (resp.errorMessage == null) {
              //zoomLevel.syncUp(resp.persistedNode);
              zoomLevel.id=resp.persistedNode.id;
              node.getInnerNode().syncUp(resp.persistedNode.getChildren().get(0));
              selectionModel.selectAll();
              updateMenuStates();
            } else
              showError(resp.errorMessage);
          }
        };
      }
      else {
	  // INSERTION
        int idx = formCanvas.getWidgetIndex(node);
        zoomLevel.insertChild(node.getInnerNode(), idx);
        cudReq = CUDMetaForm.create(node.getInnerNode());
      }
      needSync=true;
    }
    else if (evt.nature == FormEvent.NodeLevel.CHANGED) {
      cudReq = CUDMetaForm.update(node.getInnerNode());
      needSync=false;
    }
    else if (evt.nature == FormEvent.NodeLevel.REMOVED) {
      node.getInnerNode().removeFromParent();
      cudReq = CUDMetaForm.delete(node.getInnerNode());
      needSync=false;
    }
    else if (evt.nature == FormEvent.NodeLevel.MOVED) {
      int idx = formCanvas.getWidgetIndex(node);
      zoomLevel.moveChild(node.getInnerNode(), idx);
      cudReq = CUDMetaForm.update(node.getInnerNode());
      needSync=false;
    }
    else {
      System.out.println("FormPanel evtHandler: Ignoring "+evt.toString());
      needSync=false;
    }

    if(cudReq!=null)
      persistOperation = new RPC.Operation<CUDMetaFormResponse>(cudReq) {
        public void onSuccess(CUDMetaFormResponse resp) {
          super.onSuccess(resp);
          if (resp.errorMessage == null) {
            if(needSync)
              node.getInnerNode().syncUp(resp.persistedNode);
            updateMenuStates();
          } else
            showError(resp.errorMessage);
        }
      };
    if(persistOperation!=null)
      RPC.Instance.exec(persistOperation);

    List<EditableNode> changedNodes=formCanvas.collectPostureChangedNodes();
    changedNodes.remove(node);
    // Save posture changes in affected nodes
    for(EditableNode n:changedNodes){
      cudReq = CUDMetaForm.update(n.getInnerNode());
      persistOperation = new RPC.Operation<CUDMetaFormResponse>(cudReq) {
        public void onSuccess(CUDMetaFormResponse resp) {
          super.onSuccess(resp);
          if (resp.errorMessage != null)
            showError(resp.errorMessage);
        }
      };
      RPC.Instance.exec(persistOperation);
    }
    return true;
  }

  private void showError(String errMsg){
  }
}

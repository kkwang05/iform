package fx.presentment.design;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;

import fx.model.*;

/**
 * Template mapped UI class for showing and modifying form header (meta data) info.
 * Source of ValueChangeEvent<Form>.
 *
 * @author kw
 */
public class FormInfo extends Composite implements HasValueChangeHandlers<Form> {
  public static final DateTimeFormat Dtf=DateTimeFormat.getFormat("MM/dd/yyyy HH:mm:ss");
  interface MyBinder extends UiBinder<Widget, FormInfo> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField TextBox formName;
  @UiField HTML formStatus;
  @UiField HTML createDate;
  @UiField TextArea formDesc;
  
  private transient Form theForm;
  public FormInfo(){
    initWidget(binder.createAndBindUi(this));
    load(null);
  }

  public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Form> handler) {
    return addHandler(handler, ValueChangeEvent.getType());
  }
  
  @UiHandler("formName")
  void nameFocused(FocusEvent fe){
    if(!formName.isReadOnly()){
      formName.selectAll();
    }
  }

  @UiHandler("formDesc")
  void descFocused(FocusEvent fe){
    if(!formDesc.isReadOnly()){
      formDesc.selectAll();
    }
  }

  @UiHandler("formName")
  void NameChanged(ValueChangeEvent<String> e){
    theForm.name=e.getValue();
    if(theForm.getBody()!=null)
      ValueChangeEvent.<Form>fire(this, theForm);
    formName.setFocus(false);
  }

  @UiHandler("formDesc")
  void descChanged(ValueChangeEvent<String> e){
    theForm.desc=e.getValue();
    formDesc.setVisibleLines(theForm.desc.split("\r?\n").length);
    if(theForm.getBody()!=null)
      ValueChangeEvent.<Form>fire(this, theForm);
    formDesc.setFocus(false);
  }

  public void load(Form aForm){
    theForm=aForm;
    boolean vis=theForm!=null;
    formName.setVisible(vis);
    formStatus.setVisible(vis);
    createDate.setVisible(vis);
    formDesc.setVisible(vis);
    if(!vis)
      return;
    
    formName.setReadOnly(theForm.isReadOnly());
    formDesc.setReadOnly(theForm.isReadOnly());
    
    formName.setText(theForm.name);
    formStatus.setHTML(theForm.getStatusText());
    createDate.setHTML(Dtf.format(theForm.createDate));
    formDesc.setText(theForm.desc);
    
    formDesc.setVisibleLines(theForm.desc.split("\r?\n").length);
  }
}
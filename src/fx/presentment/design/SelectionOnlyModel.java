package fx.presentment.design;

import java.util.*;

import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;

import fx.presentment.event.NodeSelectionEvent;

/**
 * Manages the selection/unselection of <code>EditableNode</code> objects. Usually associated with 
 * a container type throughout its life cycle.
 *
 * @author kw
 */
public class SelectionOnlyModel implements SelectionHandler<EditableNode> {

  protected ComplexPanel container;
  
  protected transient List<EditableNode> selectedNodes=new ArrayList<EditableNode>(); 
  protected transient int selectionRangeStartIndex=-1;
  
  public SelectionOnlyModel(ComplexPanel container){
    this.container=container;
  }
  
  public void come(EditableNode node){
    node.addSelectionHandler(this);
  }
  public void go(EditableNode node){
    unselect(node);
    node.removeSelectionHandlers();
  }

  public void selectAll(){
    Iterator<?> it=container.iterator();
    while(it.hasNext())
      select((EditableNode)it.next());
  }

  public void clearSelection(){
    for(int i=selectedNodes.size()-1; i>=0; i--)
      unselect(selectedNodes.get(i));
  }
  
  public void select(EditableNode node){
    if(node.isSelected())
      return;
    node.setSelected(true);
    addToSelection(node);
  }
  
  public void unselect(EditableNode node){
    if(!node.isSelected())
      return;
    node.setSelected(false);
    removeFromSelection(node);
  }
  
  public void onSelection(SelectionEvent<EditableNode> event){
    NodeSelectionEvent ne=(NodeSelectionEvent)event;
    EditableNode node=ne.getSelectedItem();
    if(ne.isTogglingSelect()){
      if(node.isSelected()){
        unselect(node);
        selectionRangeStartIndex=-1;
      }
      else{
        select(node);
        selectionRangeStartIndex=container.getWidgetIndex(node);
      }
    }
    else if(ne.isExclusiveSelect()){
      for(int i=selectedNodes.size()-1; i>=0; i--)
        if(!selectedNodes.get(i).equals(node))
          unselect(selectedNodes.get(i));
      
      if(selectedNodes.isEmpty())
        select(node);
      selectionRangeStartIndex=container.getWidgetIndex(node);
    }
    // Sweeping range selection
    else{
      int selectedIndex=container.getWidgetIndex(node);
      clearSelection();
      if(selectionRangeStartIndex<0 || selectionRangeStartIndex==selectedIndex){
        select(node);
      }
      else{
        int s, e;
        if(selectionRangeStartIndex<selectedIndex){
          s=selectionRangeStartIndex;
          e=selectedIndex;
        }
        else{
          s=selectedIndex;
          e=selectionRangeStartIndex;
        } 
        while(s<=e)
          select((EditableNode)container.getWidget(s++));
      }
      selectionRangeStartIndex=selectedIndex;
    }
  }
  private void addToSelection(EditableNode node){
    assert(node.isSelected());
    if(!selectedNodes.contains(node)){
      int idx=container.getWidgetIndex(node);
      if(selectedNodes.isEmpty() || idx > container.getWidgetIndex(selectedNodes.get(selectedNodes.size()-1)))
        selectedNodes.add(node);
      else if(idx < container.getWidgetIndex(selectedNodes.get(0)))
        selectedNodes.add(0,node);
      else{
        int i=1;
        for(;i<selectedNodes.size()-1 && idx > container.getWidgetIndex(selectedNodes.get(i)); i++)
          ;
        selectedNodes.add(i,node);
      }
    }
  }
  private void removeFromSelection(EditableNode node){
    assert(!node.isSelected());
    selectedNodes.remove(node);
  }
  
  public EditableNode getTheOnlySelected(){
    if(selectedNodes.size()==1)
      return selectedNodes.get(0); 
    else
      return null;
  }
  public List<EditableNode> getAllSelected(){
    if(selectedNodes.size()>0)
      return selectedNodes; 
    else
      return null;
  }
}
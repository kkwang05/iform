package fx.presentment.design;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.ClickEvent;

import fx.model.MetaForm;
import fx.presentment.design.editing.NodeEditor;
import fx.presentment.widget.*;
import fx.presentment.event.FormEvent;
import static fx.presentment.renderer.Renderer.*;

/**
 * @author kw
 */
public class NodeFormatMenu extends Composite implements ValueChangeHandler<String> {
  interface MyBinder extends UiBinder<Widget, NodeFormatMenu> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField ListBox styleVariations;
  @UiField ButtonBase borderButton, marginButton, paddingButton, colorButton, widthButton, heightButton, 
  	colWidthButton, horzAlignButton, vertAlignButton, morphButton;
  
  private BorderSetter borderSetter;
  private RangedValueSetter marginSlider, paddingSlider;
  private ColorPicker colorPicker;
  private RangedValueSetter widthSlider, heightSlider;
  private RangedValueSetter colWidthSlider;
  private ItemizedSelector horzAlignSelector, vertAlignSelector, morphSelector;

  @UiField ListBox fonts;
  @UiField PushButton textSizeButton, textColorButton;
  @UiField ToggleButton textBoldButton, textItalicButton, textUnderlineButton, textStrikethroughButton, 
  textSuperscriptButton,textSubscriptButton, textAlignLeftButton,textAlignCenterButton,textAlignRightButton,textAlignJustifyButton;
  
  private RangedValueSetter textSizeSlider;
  private ColorPicker textColorPicker;

  NodeFormatMenu(){
    initWidget(binder.createAndBindUi(this));
    borderSetter=new BorderSetter(borderButton);
    colorPicker=new ColorPicker(colorButton);
    
    marginSlider=new RangedValueSetter(marginButton);
    marginSlider.setKeyAndUnit(NODE_ATTRIBUTE_MARGIN,"px");
    
    paddingSlider=new RangedValueSetter(paddingButton);
    paddingSlider.setKeyAndUnit(NODE_ATTRIBUTE_PADDING,"px");

    widthSlider=new RangedValueSetter(widthButton);
    widthSlider.setKeyAndUnit(NODE_ATTRIBUTE_WIDTH,"em");
    widthSlider.setMaxValue(100);
    
    heightSlider=new RangedValueSetter(heightButton);
    heightSlider.setKeyAndUnit(NODE_ATTRIBUTE_HEIGHT,"em");
    heightSlider.setMaxValue(100);
    
    colWidthSlider=new RangedValueSetter(colWidthButton);
    colWidthSlider.setKeyAndUnit(NODE_ATTRIBUTE_TD_WIDTH, "%");
    colWidthSlider.setMaxValue(100);

    horzAlignSelector=new ItemizedSelector(horzAlignButton);
    vertAlignSelector=new ItemizedSelector(vertAlignButton);
    horzAlignSelector.setSelectionItems("Unset:","Left","Center","Right");
    vertAlignSelector.setSelectionItems("Unset:","Top","Middle","Bottom");
    morphSelector=new ItemizedSelector(morphButton);

    textSizeSlider=new RangedValueSetter(textSizeButton);
    textSizeSlider.setKeyAndUnit("font-size","%");
    textSizeSlider.setMaxValue(200);
    
    textColorPicker=new ColorPicker(textColorButton);
    
    borderSetter.addValueChangeHandler(this);
    colorPicker.addValueChangeHandler(this);
    marginSlider.addValueChangeHandler(this);
    paddingSlider.addValueChangeHandler(this);
    widthSlider.addValueChangeHandler(this);
    heightSlider.addValueChangeHandler(this);
    colWidthSlider.addValueChangeHandler(this);
    horzAlignSelector.addValueChangeHandler(this);
    vertAlignSelector.addValueChangeHandler(this);
    morphSelector.addValueChangeHandler(this);
    textSizeSlider.addValueChangeHandler(this);
    textColorPicker.addValueChangeHandler(this);
    
    styleVariations.addItem(NodeEditor.defaultStyleLabel(), "");
  }
  
  public void setEnabled(boolean enable){
    styleVariations.setEnabled(enable);
    borderButton.setEnabled(enable);
    marginButton.setEnabled(enable);
    paddingButton.setEnabled(enable);
    colorButton.setEnabled(enable);
    widthButton.setEnabled(enable);
    heightButton.setEnabled(enable);
    colWidthButton.setEnabled(enable);
    horzAlignButton.setEnabled(enable);
    vertAlignButton.setEnabled(enable);
    morphButton.setEnabled(enable);
    setTextControlEnabled(enable);
  }
  private void setTextControlEnabled(boolean enable){
    fonts.setEnabled(enable);
    textSizeButton.setEnabled(enable);
    textColorButton.setEnabled(enable);
    textBoldButton.setEnabled(enable);
    textItalicButton.setEnabled(enable);
    textUnderlineButton.setEnabled(enable);
    textStrikethroughButton.setEnabled(enable);
    textSuperscriptButton.setEnabled(enable);
    textSubscriptButton.setEnabled(enable);
    textAlignLeftButton.setEnabled(enable);
    textAlignCenterButton.setEnabled(enable);
    textAlignRightButton.setEnabled(enable);
    textAlignJustifyButton.setEnabled(enable);
  }
  
  private transient EditableNode target;
  public void load(EditableNode node){
    target=node;
    MetaForm mf=target.getInnerNode();
    
    styleVariations.clear();
    styleVariations.addItem(NodeEditor.defaultStyleLabel(), "");
    String[] styles=NodeEditor.supportedStyleVariations(mf);
    if(styles!=null){
      styleVariations.setEnabled(true);
      int selectedItem=0;
      String savedStyle=getHint(mf.renderingHints, NODE_ATTRIBUTE_STYLE);
      for(int i=0;i<styles.length;i++){
        String[] labelAndValue=styles[i].split("=");
        styleVariations.addItem(labelAndValue[0], labelAndValue[1]);
        if(savedStyle!=null && savedStyle.equals(labelAndValue[1]))
          selectedItem=i+1;
      }
      styleVariations.setSelectedIndex(selectedItem);
    }
    else {
      styleVariations.setEnabled(false);
    }
    borderSetter.setBorderValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_BORDER));
    marginSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_MARGIN));
    paddingSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_PADDING));
    colorPicker.setColor(getHint(mf.renderingHints, NODE_ATTRIBUTE_COLOR));
    widthSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_WIDTH));
    heightSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_HEIGHT));
    horzAlignSelector.setSelectedValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_HORZ_ALIGN));
    vertAlignSelector.setSelectedValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_TD_ALIGN));
    colWidthSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_TD_WIDTH));
    // Either this or the fixed positionInRow() method would work
    boolean isInRow=mf.isInRow();
    //boolean isInRow=mf.positionInRow()>=0;
    vertAlignButton.setEnabled(isInRow);
    colWidthButton.setEnabled(isInRow);

    if(mf.isMorphable()){
      String tmp=NodeTypeProperties.Instance.morphsMap().get("morphs."+mf.typeCode);
      if(tmp!=null){
        morphButton.setEnabled(true);
        morphSelector.setSelectionItems(tmp.split(";\\s*"));
        morphSelector.setSelectedValue(mf.typeCode+"");
      }
      else
        morphButton.setEnabled(false);
    }
    else{
      morphButton.setEnabled(false);
    }
    
    if(mf.hasText()){
      loadTextAttributes();
      setTextControlEnabled(true);
    }
    else
      setTextControlEnabled(false);
  }
  
  private void loadTextAttributes(){
    MetaForm mf=target.getInnerNode();
    String savedFont=getHint(mf.renderingHints, NODE_ATTRIBUTE_FONT_NAME);
    int selectedIndex=0;
    for(int i=1;i<fonts.getItemCount();i++){
      String f=fonts.getValue(i);
      if(f.equalsIgnoreCase(savedFont)){
        selectedIndex=i;
        break;
      }
    }
    fonts.setSelectedIndex(selectedIndex);
    
    textSizeSlider.setValue(getHint(mf.renderingHints, NODE_ATTRIBUTE_FONT_SIZE));
    textColorPicker.setColor(getHint(mf.renderingHints, NODE_ATTRIBUTE_FONT_COLOR));
    
    String weight=getHint(mf.renderingHints, NODE_ATTRIBUTE_FONT_WEIGHT);
    textBoldButton.setDown(weight!=null && weight.contains("bold"));

    String style=getHint(mf.renderingHints, NODE_ATTRIBUTE_FONT_STYLE);
    textItalicButton.setDown(style!=null && style.contains("italic"));
    
    String deco=getHint(mf.renderingHints, NODE_ATTRIBUTE_TEXT_DECO);
    textUnderlineButton.setDown(deco!=null && deco.contains("underline"));
    textStrikethroughButton.setDown(deco!=null && deco.contains("line-through"));

    String vertAlign=getHint(mf.renderingHints, NODE_ATTRIBUTE_TEXT_VALIGN);
    textSuperscriptButton.setDown(vertAlign!=null && vertAlign.contains("super"));
    textSubscriptButton.setDown(vertAlign!=null && vertAlign.contains("sub"));

    String align=getHint(mf.renderingHints, NODE_ATTRIBUTE_TEXT_ALIGN);
    textAlignLeftButton.setDown(align!=null && align.contains("left"));
    textAlignCenterButton.setDown(align!=null && align.contains("center"));
    textAlignRightButton.setDown(align!=null && align.contains("right"));
    textAlignJustifyButton.setDown(align!=null && align.contains("justify"));
  }
  
  @UiHandler("styleVariations")
  void styleChanged(ChangeEvent evt){
    String v= styleVariations.getValue(styleVariations.getSelectedIndex());
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_STYLE, v);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  
  public void onValueChange(ValueChangeEvent<String> event){
    if(event.getSource()==borderSetter)
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_BORDER, event.getValue());
    else if(event.getSource()==colorPicker)
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_COLOR, event.getValue());
    else if(event.getSource()==marginSlider || event.getSource()==paddingSlider || event.getSource()==widthSlider || event.getSource()==heightSlider || event.getSource()==colWidthSlider){
      String v=event.getValue();
      String k=v.substring(0, v.indexOf("="));
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, k, event.getValue());
    }
    else if(event.getSource()==horzAlignSelector){
      String ha=event.getValue();
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_HORZ_ALIGN, ha);
    }
    else if(event.getSource()==vertAlignSelector){
      String va=event.getValue();
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_TD_ALIGN, va);
    }
    else if(event.getSource()==morphSelector){
      target.getInnerNode().typeCode=Short.parseShort(event.getValue());
    }
    else if(event.getSource()==textSizeSlider){
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_FONT_SIZE, event.getValue());
    }
    else if(event.getSource()==textColorPicker){
      target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_FONT_COLOR, event.getValue());
    }
    
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }

  @UiHandler("fonts")
  void fontChanged(ChangeEvent evt){
    String font=fonts.getValue(fonts.getSelectedIndex());
    if(fonts.getSelectedIndex()==0)
      font="";
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_FONT_NAME, font);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  @UiHandler("textBoldButton")
  void clickedBold(ClickEvent evt){
    String weight=textBoldButton.isDown()? "bold":"";
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_FONT_WEIGHT, weight);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  @UiHandler("textItalicButton")
  void clickedItalic(ClickEvent evt){
    String style=textItalicButton.isDown()? "italic":"";
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_FONT_STYLE, style);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  
  @UiHandler("textUnderlineButton")
  void clickedUnderline(ClickEvent evt){
    updateDeco();
  }
  @UiHandler("textStrikethroughButton")
  void clickedStrikethrough(ClickEvent evt){
    updateDeco();
  }
  private void updateDeco(){
    StringBuffer sb=new StringBuffer();
    if(textUnderlineButton.isDown())
      sb.append("underline ");
    if(textStrikethroughButton.isDown())
      sb.append("line-through");
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_TEXT_DECO, sb.toString().trim());
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  
  @UiHandler("textSuperscriptButton")
  void clickedSuperscript(ClickEvent evt){
    textSubscriptButton.setDown(false);
    updateVertAlign();
  }
  @UiHandler("textSubscriptButton")
  void clickedSubscript(ClickEvent evt){
    textSuperscriptButton.setDown(false);
    updateVertAlign();
  }
  private void updateVertAlign(){
    String valign;
    if(textSuperscriptButton.isDown())
      valign="super";
    else if(textSubscriptButton.isDown())
      valign="sub";
    else
      valign="";
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_TEXT_VALIGN, valign);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
  
  @UiHandler("textAlignLeftButton")
  void clickedAlignLeft(ClickEvent evt){
    textAlignCenterButton.setDown(false);
    textAlignRightButton.setDown(false);
    textAlignJustifyButton.setDown(false);
    updateAlign();
  }
  @UiHandler("textAlignCenterButton")
  void clickedAlignCenter(ClickEvent evt){
    textAlignLeftButton.setDown(false);
    textAlignRightButton.setDown(false);
    textAlignJustifyButton.setDown(false);
    updateAlign();
  }
  @UiHandler("textAlignRightButton")
  void clickedAlignRight(ClickEvent evt){
    textAlignLeftButton.setDown(false);
    textAlignCenterButton.setDown(false);
    textAlignJustifyButton.setDown(false);
    updateAlign();
  }
  @UiHandler("textAlignJustifyButton")
  void clickedAlignJustify(ClickEvent evt){
    textAlignLeftButton.setDown(false);
    textAlignCenterButton.setDown(false);
    textAlignRightButton.setDown(false);
    updateAlign();
  }
  private void updateAlign(){
    String align;
    if(textAlignLeftButton.isDown())
      align="left";
    else if(textAlignCenterButton.isDown())
      align="center";
    else if(textAlignRightButton.isDown())
      align="right";
    else if(textAlignJustifyButton.isDown())
      align="justify";
    else
      align="";
    target.getInnerNode().renderingHints=encode(target.getInnerNode().renderingHints, NODE_ATTRIBUTE_TEXT_ALIGN, align);
    target.refresh();
    FormEvent.fire(FormEvent.NodeLevel.CHANGED, target);
  }
}
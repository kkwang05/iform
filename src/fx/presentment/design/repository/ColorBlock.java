package fx.presentment.design.repository;

import com.google.gwt.user.client.ui.*;
import fx.presentment.design.EditableNode;
import fx.presentment.widget.Tooltip;

public class ColorBlock extends EditableNode {
  public static final String NO_COLOR="None";
  
  private String colorDefinition;
  private boolean small;
  
  public ColorBlock(String colorDefinition){
    this(colorDefinition, false);
  }
  public ColorBlock(String colorDefinition, boolean small){
    this.colorDefinition=colorDefinition;
    this.small=small;
    addStyleName(styleName());
    String lblTxt=colorDefinition;
    if(lblTxt.length()>4)
      lblTxt=lblTxt.substring(0,4);
    Label lbl=new Label(lblTxt);
    lbl.addStyleName("label");
    setWidget(lbl);
    if(!colorDefinition.equals(NO_COLOR)){
      lbl.getElement().getStyle().setColor(colorDefinition);
      getElement().getStyle().setBackgroundColor(colorDefinition);
      setWidget(lbl);
    }
    addClickHandler(selectionHandler);
    new Tooltip(this, description());
  }
  
  public String description(){
    return colorDefinition;
  }
  public String tooltipText(){
    return colorDefinition;
  }
  
  public String styleName(){
    return small? "ColorBlock ColorBlock-Small" : "ColorBlock";
  }
  
  protected String selectionStyleName(){
    return "EditableNode-Selected";
  }
  
  public String getColorDefinition(){
    return colorDefinition;
  }
}

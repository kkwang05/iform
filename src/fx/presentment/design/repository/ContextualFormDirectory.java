package fx.presentment.design.repository;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.user.client.ui.TreeItem;

import fx.model.*;
import fx.model.operation.GetForm;
import fx.model.operation.GetFormList;
import fx.model.operation.GetFormListResponse;
import fx.model.operation.GetFormResponse;
import fx.presentment.ClientAppContext;
import fx.presentment.ImageResources;
import fx.service.RPC;

/**
 * Three-level tree list of forms.
 * @author kw
 */
public class ContextualFormDirectory extends ContextualDirectory<Form>{

  public ContextualFormDirectory(){
    subjectClass=Form.class;
  }

  protected String[] getTopLevelNodeTexts(){
    return new String[]{"My Forms","Forms in my Domain","Public Forms"};
  }

  protected void bind(TreeItem node, Form form){
    if(form.equals(node.getUserObject()))
      form.setBody(((Form)node.getUserObject()).getBody());
    node.setUserObject(form);
    node.setHTML(imageItemHTML(ImageResources.Instance.formIcon(),form.name));
  }

  protected RPC.Operation<GetFormListResponse> getPersonalQueryOp(){
    GetFormList req=new GetFormList();
    req.createdByUserId=ClientAppContext.CurrentUser.id;
    return new RPC.Operation<GetFormListResponse>(req){
      public void processSuccess(GetFormListResponse result){
        add(personal, result.formList);
      }
    };
  }

  protected RPC.Operation<GetFormListResponse> getPublicQueryOp(){
    return new RPC.Operation<GetFormListResponse>(new GetFormList()){
      public void processSuccess(GetFormListResponse result){
        add(pub, result.formList);
      }
    };
  }
  
  @Override
  protected void doFireSelectionEvent(final Form sel){
    if (sel != currentlySelected || sel==null) {
      if(sel!=null && sel.getBody()==null){
        GetForm req=new GetForm(sel.id);
        RPC.Operation<GetFormResponse> op=new RPC.Operation<GetFormResponse>(req){
          public void onSuccess(GetFormResponse result){
            super.onSuccess(result);
            sel.setBody(result.retrievedForm.getBody());
            SelectionEvent.<Form>fire(ContextualFormDirectory.this, sel);
            currentlySelected = sel;
          }
        };
        RPC.Instance.exec(op);
        return;
      }
      SelectionEvent.<Form>fire(this, sel);
      currentlySelected = sel;
    }
  }
  
}
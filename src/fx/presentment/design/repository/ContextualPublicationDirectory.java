package fx.presentment.design.repository;

import java.util.*;

import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.event.logical.shared.SelectionHandler;

import fx.model.*;
import fx.model.operation.GetForm;
import fx.model.operation.GetFormResponse;
import fx.model.operation.design.GetPublicationList;
import fx.model.operation.design.GetPublicationListResponse;
import fx.presentment.ClientAppContext;
import fx.presentment.ImageResources;
import fx.service.RPC;

/**
 * @author kw
 */
public class ContextualPublicationDirectory extends ContextualDirectory<Publication> {

  public ContextualPublicationDirectory(){
    subjectClass=Publication.class;
    // Override super class and fire an null SelectionEvent<Publication> whenever a non-publication 
    // node is selected.
    treeList.addSelectionHandler(new SelectionHandler<TreeItem>(){
	public void onSelection(SelectionEvent<TreeItem> evt){
          handleFormSelection(evt);
        }
    });
  }

  private void handleFormSelection(SelectionEvent<TreeItem> event){
    if(getSelectedSubject()==null)
      SelectionEvent.<Publication>fire(this, null);
  }
  
  /**
   * Add a persited instance to directory and optionally select it.
   * @param publication
   * @param select
   */
  public void add(TreeItem topContainer, Publication publication, boolean select){
    if(topContainer.getUserObject()==REQUERY)
      return;
   
    Form form=publication.getForm();
    TreeItem folder=null, newNode=null;
    for(int i=0; i<topContainer.getChildCount();i++){
      TreeItem n=topContainer.getChild(i);
      if(form.equals(n.getUserObject())){
	folder=n;
	break;
      }
    }
    if(folder==null){
      List<Publication> pl=new ArrayList<Publication>();
      pl.add(publication);
      newNode=addFamily(topContainer, form, pl);
      if(select)
	// Just select - pub already loaded, no need to fire event.
	treeList.setSelectedItem(newNode, true);
    }
    else{
      addNode(folder, publication, true, select, true);
    }
  }
  
  /**
   * Remove a deleted record from directory and possibly de-select it.
   * @param publication
   */
  public void remove(Publication publication){
    TreeItem found=null;
    for(TreeItem c: topLevelContainers){
      if((found=search(c, publication))!=null)
	break;
    }
    if(found != null) {
      TreeItem parent=found.getParentItem();
      found.remove();
      if(parent.getChildCount()==0)
	parent.remove();
      if(publication.equals(currentlySelected)){
	currentlySelected = null;
	SelectionEvent.<Publication>fire(this, null);
      }
    }
  }

  protected String[] getTopLevelNodeTexts(){
    return new String[]{"My Publications","Publications in my Domain","Public Publications"};
  }
  
  protected void bind(TreeItem node, Publication pub){
    node.setUserObject(pub);
    String lbl=pub.name;
    if(pub.isActive())
      lbl="<b>"+lbl+"</b>";
    node.setHTML(imageItemHTML(ImageResources.Instance.publicationIcon(),lbl));
  }

  protected RPC.Operation<GetPublicationListResponse> getPersonalQueryOp(){
    GetPublicationList req=new GetPublicationList();
    req.createdByUserId=ClientAppContext.CurrentUser.id;
    return new RPC.Operation<GetPublicationListResponse>(req){
      public void processSuccess(GetPublicationListResponse result){
        addPublications(personal, result.pubList);
      }
    };
  }

  protected RPC.Operation<GetPublicationListResponse> getPublicQueryOp(){
    return new RPC.Operation<GetPublicationListResponse>(new GetPublicationList()){
      public void processSuccess(GetPublicationListResponse result){
        addPublications(pub, result.pubList);
      }
    };
  }
  
  private void addPublications(final TreeItem owner, List<Publication> pubList){
    Map<Long, List<Publication>> formIdToPubs=new HashMap<Long, List<Publication>>();
    for(Publication p : pubList){
      List<Publication> lst=formIdToPubs.get(p.formId);
      if(lst==null)
	formIdToPubs.put(p.formId, lst=new ArrayList<Publication>());
      lst.add(p);
    }
    
    List<Long> keyList=new ArrayList<Long>();
    Iterator<Long> it=formIdToPubs.keySet().iterator();
    while(it.hasNext())
      keyList.add(it.next());
    Collections.sort(keyList, Collections.reverseOrder());
    
    for(Long formId:keyList){
      final List<Publication> formPubs=formIdToPubs.get(formId);
      // Depending on the server side implementation, the associated form could have been loaded.
      Form form=formPubs.get(0).getForm();
      if(form!=null){
        addFamily(owner, form, formPubs);
        continue;
      }
      // If not, then issue second-stage query. If results had not been cached, the act of populating
      // the tree will become asynchronous, which causes the nagging form->publication navigation failure.
      GetForm req=new GetForm(formId);
      RPC.Operation<GetFormResponse> op=new RPC.Operation<GetFormResponse>(req){
	public void processSuccess(GetFormResponse result){
          addFamily(owner, result.retrievedForm, formPubs);
        }
      };
      RPC.Instance.sequentialExec(op);
    }
  }
  private TreeItem addFamily(TreeItem grandParent, Form form, List<Publication> formPubs){
	String parentName = imageItemHTML(ImageResources.Instance.formIcon(),"<i>"+form.name+"</i>");
    TreeItem parent=new TreeItem();
    parent.setText(parentName);
    parent.setUserObject(form);
    grandParent.addItem(parent);
    TreeItem firstNode=null;
    for(Publication p:formPubs){
      p.setForm(form);
      TreeItem n=add(parent, p);
      if(firstNode==null)
        firstNode=n;
    }
    parent.setState(true);
    return firstNode;
  } 
}
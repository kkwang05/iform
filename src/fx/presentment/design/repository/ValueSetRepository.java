package fx.presentment.design.repository;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.ui.HTMLTable.Cell;

import fx.model.ValueSet;
import fx.model.operation.design.*;
import fx.presentment.*;
import fx.presentment.widget.TooltipPushButton;
import fx.service.RPC;

public class ValueSetRepository extends Composite {
  interface MyBinder extends UiBinder<Widget, ValueSetRepository> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  interface SelectionStyle extends CssResource {
    String selectedRow();
    String editedRow();
    int inputSize();
    String newRowPlaceHolder();
  }
  
  @UiField FileUpload uploader;
  @UiField TooltipPushButton submitButton, infoButton;
  @UiField ListBox filterControl;
  @UiField FlexTable directory;
  @UiField SelectionStyle selectionStyle;
  
  private boolean editable=true;
  private transient int selectedRow=-1;
  private transient List<ValueSet> presentList=new ArrayList<ValueSet>();
  private ValueSetManager vsMan;
  
  public ValueSetRepository(){
    initWidget(binder.createAndBindUi(this));
    directory.getColumnFormatter().setWidth(0, "20%");
    
    // Allowing ValueSetManager call back after a change
    //ValueSetManager.Instance.setHostRepository(this);
    vsMan=new ValueSetManager(this);
  }

  public void setEditable(boolean editable){
    if(!editable){
      filterControl.removeItem(0); // zeroth element is the "Recent" option
      uploader.setVisible(false);
      submitButton.setVisible(false);
      infoButton.setVisible(false);
      this.editable=false;
      vsMan=null;
    }
  }
  
  @UiHandler("filterControl")
  void filtering(ChangeEvent event){
    refresh();
  }

  public void refresh(){
    int idx=filterControl.getSelectedIndex();
    if(idx<0)
      idx=0;
    String criteria=filterControl.getValue(idx);
    
    GetValueSetList req=new GetValueSetList();
    if(criteria.equalsIgnoreCase("recent")){
      req.createdByUserId=ClientAppContext.CurrentUser.id;
      req.inline=true;
    }
    else if(criteria.equalsIgnoreCase("private")){
      req.createdByUserId=ClientAppContext.CurrentUser.id;
      req.inline=false;
    }
    else {
      // TBD - Shared, Public, and other filtering criteria inputs
      System.out.println("selection criteria="+criteria);
    }
    
    presentList.clear();
    directory.removeAllRows();
    selectedRow=-1;
    infoButton.setEnabled(false);
    
    RPC.Operation<?> svcCall=new RPC.Operation<GetValueSetListResponse>(req){
      public void onSuccess(GetValueSetListResponse result){
        super.onSuccess(result);
        List<ValueSet> vslist=result.valueSetList;
        for(int i=0; i<vslist.size(); i++)
          addValueSet(i, vslist.get(i), true);
        // At this point nobody needs be synchronized with the ValueSetRepository, so no need to fire a change event.
        //DomEvent.fireNativeEvent(Document.get().createChangeEvent(),ValueSetRepository.this);
      }
    };
    RPC.Instance.exec(svcCall);
  }
  
  public void refreshSelectedRow(){
    if(selectedRow<0 || selected()==null)
      return;
    addValueSet(selectedRow, selected(), false);
  }
  
  protected void addValueSet(int row, ValueSet vs, boolean addToList){
    String name=vs.name==null? "Anonymous": vs.name;
    if(!vs.isInline)
      name="<i>"+name+"</i>";
    
    directory.setWidget(row, 0, new HTML(name));
    String desc=vs.desc;
    StringBuffer sbuf=new StringBuffer();
    if(desc!=null)
      sbuf.append("["+desc+"]");

    int i=0;
    for(; i<vs.values.size() && sbuf.length()<20; i++)
      sbuf.append(vs.values.get(i)+", ");
    if(i>0 && i<vs.values.size())
      sbuf.append("...");
    else
      sbuf.delete(sbuf.length()-2,sbuf.length());
    
    desc=sbuf.toString();
    directory.setWidget(row, 1, new HTML(desc));
    if(addToList)
      presentList.add(vs);
  }

  @UiHandler("directory")
  void onTableClick(ClickEvent event) {
    Cell cell = directory.getCellForEvent(event);
    if (cell != null) {
      int row = cell.getRowIndex();
      if(row!=selectedRow)
        doSelectRow(row);
    }
    else
      doSelectRow(-1);
  }
  private void doSelectRow(int row) {
    String style = selectionStyle.selectedRow();
    if(selectedRow>=0)
      directory.getRowFormatter().removeStyleName(selectedRow, style);
    if(row>=0)
      directory.getRowFormatter().addStyleName(row, style);
    selectedRow = row;
    
    infoButton.setEnabled(selected()!=null);
  }

  @UiHandler("directory")
  void onTableDoubleClick(DoubleClickEvent event){
    if(!editable || selected()==null)
      return;
    //ValueSetManager.Instance.load(selected());
    vsMan.load(selected());
  }
  
  @UiHandler("infoButton") 
  void annotateValueSet(ClickEvent event){
    //ValueSetManager.Instance.load(selected());
    vsMan.load(selected());
  }
  
  public ValueSet selected(){
    if(selectedRow==-1 || selectedRow>=presentList.size())
      return null;
    return presentList.get(selectedRow);
  }
}
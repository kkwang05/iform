package fx.presentment.design.repository;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;

import fx.model.ColorList;
import fx.model.operation.design.*;
import fx.presentment.design.EditableNode;
import fx.presentment.design.FormCanvas;
import fx.presentment.design.SelectionOnlyModel;
import fx.presentment.widget.*;
import fx.presentment.*;
import fx.service.RPC;

public class ColorRepository extends Composite implements DoubleClickHandler, SelectionHandler<EditableNode>, HasChangeHandlers  {
  public static ColorRepository Instance;
  
  interface MyBinder extends UiBinder<Widget, ColorRepository> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField TextBox clipboard;
  @UiField TooltipPushButton addButton, deleteButton;
  @UiField ListBox filterControl;

  @UiField FormCanvas colorPane;

  private SelectionOnlyModel selectionModel;
  
  public ColorRepository(){
    initWidget(binder.createAndBindUi(this));
    selectionModel=new SelectionOnlyModel(colorPane);
    if(Instance==null)
      Instance=this;
  }

  public HandlerRegistration addChangeHandler(ChangeHandler handler) {
    return addHandler(handler, ChangeEvent.getType());
  }
  
  public void onDoubleClick(DoubleClickEvent event){
  }
  
  protected void updateMenuStates(){
    boolean hasSelection=(selectionModel.getAllSelected()!=null);
    int idx=filterControl.getSelectedIndex();
    boolean inPrivate=idx<1;
    deleteButton.setEnabled(hasSelection && inPrivate);
    addButton.setEnabled(inPrivate);
  }
  
  public void onSelection(SelectionEvent<EditableNode> event){
    updateMenuStates();
  }
  
  @UiHandler("colorPane") 
  void clickedCanvas(ClickEvent event){
    selectionModel.clearSelection();
    updateMenuStates();
  }
  
  @UiHandler("filterControl")
  void filtering(ChangeEvent event){
    refresh();
  }

  @UiHandler("deleteButton") 
  void deleteColors(ClickEvent event){
    StringBuffer sbuf=new StringBuffer();
    String colorDefs=currentColorList.colorDefinitions;
    for(EditableNode cb: selectionModel.getAllSelected()){
      String colorDef=((ColorBlock)cb).getColorDefinition()+",";
      colorDefs=colorDefs.replaceFirst(colorDef, "");
      sbuf.append(colorDef);
    }
    currentColorList.colorDefinitions=colorDefs;
    saveChanges();
    clipboard.setValue(sbuf.toString());
  }

  @UiHandler("addButton") 
  void addColor(ClickEvent event){
    String aside=clipboard.getValue().trim();
    if(aside.equals(""))
      return;
    if(!aside.endsWith(","))
      aside+=",";

    StringBuffer colorDefs=new StringBuffer(currentColorList.colorDefinitions);
    if(selectionModel.getAllSelected()!=null){
      ColorBlock cb=(ColorBlock)selectionModel.getAllSelected().get(0);
      int idx=colorDefs.indexOf(cb.getColorDefinition());
      colorDefs.insert(idx, aside);
    }
    else
      colorDefs.append(aside);
    currentColorList.colorDefinitions=colorDefs.toString();
    saveChanges();
    clipboard.setValue("");
  }
  
  private transient ColorList currentColorList=null;
  private void saveChanges(){
    currentColorList.createdByUserId=ClientAppContext.CurrentUser.id;
    CUDColorList req=CUDColorList.update(currentColorList);
    RPC.Operation<?> svcCall=new RPC.Operation<CUDColorListResponse>(req){
      public void onSuccess(CUDColorListResponse result){
        super.onSuccess(result);
        currentColorList=result.persistedColorList;
        update();
      }
    };
    RPC.Instance.exec(svcCall);
  }
  
  public void refresh(){
    int idx=filterControl.getSelectedIndex();
    String criteria=null;
    if(idx>=0)
      criteria=filterControl.getValue(idx);
    
    GetColorList req=new GetColorList();
    if(idx<=0)
      req.createdByUserId=ClientAppContext.CurrentUser.id;
    else {
      // TBD - Shared, Public, and other filtering criteria inputs
      System.out.println("selection criteria="+criteria);
    }
    
    RPC.Operation<?> svcCall=new RPC.Operation<GetColorListResponse>(req){
      public void onSuccess(GetColorListResponse result){
        super.onSuccess(result);
        currentColorList=result.colorList;
        update();
      }
    };
    RPC.Instance.exec(svcCall);
  }
  
  private void update(){
    colorPane.clear();
    selectionModel.clearSelection();
    String[] colorDefs=currentColorList.colorDefinitions.split("\\s*,\\s*");
    for(String color:colorDefs){
      EditableNode node=new ColorBlock(color);
      //node.popupDisabled=true;
      colorPane.add(node);
      node.addDoubleClickHandler(this);
      selectionModel.come(node);
      node.addSelectionHandler(this);
    }
    updateMenuStates();
    DomEvent.fireNativeEvent(Document.get().createChangeEvent(),ColorRepository.this);
  }
  
  public ColorList currentlyShowing(){
    return currentColorList;
  }
 }
package fx.presentment.design.repository;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.DoubleClickEvent;
import com.google.gwt.event.dom.client.DoubleClickHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;

import fx.model.MetaForm;
import fx.model.operation.design.*;
import fx.presentment.widget.*;
import fx.presentment.*;
import fx.presentment.design.EditableNode;
import fx.presentment.design.FormCanvas;
import fx.presentment.design.SelectionOnlyModel;
import fx.presentment.renderer.Renderer;
import fx.service.RPC;
import fx.util.FxCipher;

public class ImageRepository extends Composite implements DoubleClickHandler, SelectionHandler<EditableNode>, HasChangeHandlers  {
  public static ImageRepository Instance;
  
  interface MyBinder extends UiBinder<Widget, ImageRepository> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField com.google.gwt.user.client.ui.FormPanel formPanel;
  @UiField FileUpload uploader;
  @UiField TooltipPushButton submitButton, deleteButton, shareButton;
  @UiField ListBox filterControl;

  @UiField FormCanvas imagePane;

  private SelectionOnlyModel selectionModel;
  private List<MetaForm> currentImages;
  
  public ImageRepository(){
    initWidget(binder.createAndBindUi(this));
    selectionModel=new SelectionOnlyModel(imagePane);
    if(Instance==null)
      Instance=this;
  }

  public HandlerRegistration addChangeHandler(ChangeHandler handler) {
    return addHandler(handler, ChangeEvent.getType());
  }
  
  protected void updateMenuStates(){
    boolean hasSelection=(selectionModel.getAllSelected()!=null);
    int idx=filterControl.getSelectedIndex();
    boolean hasPrivateSelection=hasSelection && idx<1;
    deleteButton.setEnabled(hasPrivateSelection);
    shareButton.setEnabled(hasPrivateSelection);
  }

  @UiHandler("deleteButton") 
  void deleteImages(ClickEvent event){
    changeSelected(new ChangeImageList("D"));
  }
  
  @UiHandler("shareButton") 
  void shareImages(ClickEvent event){
    ChangeImageList req=new ChangeImageList("S");
    req.domainId=ClientAppContext.CurrentDomain.id;
    changeSelected(req);
  }
  private void changeSelected(ChangeImageList req){
    java.util.List<String> lst=new java.util.ArrayList<String>();
    for(EditableNode n: selectionModel.getAllSelected()){
      String uri=n.metaForm.text;
      lst.add(uri.substring("/image/T".length()));
    }
    req.imageList=lst;
    
    RPC.Operation<?> svcCall=new RPC.Operation<ChangeImageListResponse>(req){
      public void onSuccess(ChangeImageListResponse result){
        super.onSuccess(result);
        refresh();
      }
    };
    RPC.Instance.exec(svcCall);
  }
  
  @UiHandler("imagePane") 
  void clickedCanvas(ClickEvent event){
    selectionModel.clearSelection();
    updateMenuStates();
  }
  
  @UiHandler("uploader")
  void userEnteredInput(ChangeEvent event){
    if(!uploader.getFilename().isEmpty())
      submitButton.setEnabled(true);
  }
  
  @UiHandler("submitButton")
  void upload(ClickEvent event){
    String tok=""+ClientAppContext.CurrentUser.id;
    formPanel.setAction("/image/"+FxCipher.encrypt(tok));
    formPanel.submit();
  }

  @UiHandler("formPanel")
  void onSubmit(com.google.gwt.user.client.ui.FormPanel.SubmitEvent event) {
    submitButton.setEnabled(false);
  }

  @UiHandler("formPanel")
  void onSubmitComplete(com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent event) {
    filterControl.setSelectedIndex(0);
    refresh();
  }
 
  @UiHandler("filterControl")
  void filtering(ChangeEvent event){
    refresh();
  }
  
  public void refresh(){
    int idx=filterControl.getSelectedIndex();
    String criteria=null;
    if(idx>=0)
      criteria=filterControl.getValue(idx);
    
    GetImageList req=new GetImageList();
    if(idx<=0)
      req.createdByUserId=ClientAppContext.CurrentUser.id;
    else {
      // TBD - Shared, Public, and other filtering criteria inputs
      System.out.println("selection criteria="+criteria);
    }
    
    RPC.Operation<?> svcCall=new RPC.Operation<GetImageListResponse>(req){
      public void onSuccess(GetImageListResponse result){
        super.onSuccess(result);
        List<String> imageIdlist=result.imageList;
        imagePane.clear();
        selectionModel.clearSelection();
        if(currentImages==null)
          currentImages=new ArrayList<MetaForm>();
        else
          currentImages.clear();
        for(String id:imageIdlist)
          addImage(id);
        updateMenuStates();
        DomEvent.fireNativeEvent(Document.get().createChangeEvent(),ImageRepository.this);
      }
    };
    RPC.Instance.exec(svcCall);
  }

  protected void addImage(String id){
    MetaForm imgNode=MetaForm.createImage(id, true);
    currentImages.add(imgNode);
    EditableNode node=new EditableNode(imgNode);
    node.getElement().getStyle().setDisplay(Display.INLINE_BLOCK);
    imagePane.add(node);
    node.addDoubleClickHandler(this);
    selectionModel.come(node);
    node.addSelectionHandler(this);
  }
  
  public void onDoubleClick(DoubleClickEvent event){
    EditableNode imgNode=(EditableNode)event.getSource();
    MetaForm bigImage=imgNode.metaForm.enlargedImage();
    
    PopupPanel pp=new PopupPanel(true);
    pp.add(Renderer.Render(bigImage, null));
    pp.showRelativeTo(imgNode);
  }
  
  public void onSelection(SelectionEvent<EditableNode> event){
    updateMenuStates();
  }
  
  public List<MetaForm> list(){
    return currentImages;
  }
}
package fx.presentment.design.repository;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.model.ValueSet;
import fx.model.operation.design.*;
import fx.presentment.ImageResources;
import fx.presentment.design.editing.ValueSetEditor;
import fx.presentment.widget.*;
import fx.service.RPC;

/**
 * @author kw
 */
public class ValueSetManager {
  interface MyBinder extends UiBinder<PopupPanel, ValueSetManager> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);
  
  @UiField CustomDialogBox dialogBox;
  @UiField ValueSetEditor vsEditor;
  @UiField ButtonBase xButton;
  @UiField Button ok, cancel, apply;

  private ValueSetRepository hostRepository;
  
  ValueSetManager(ValueSetRepository hostRepository){
    dialogBox=(CustomDialogBox)binder.createAndBindUi(this);
    this.hostRepository=hostRepository;
  }
  
  private transient ValueSet initialValueSet;
  public void load(ValueSet vs){
    String title="Value Set Manager";
    dialogBox.setHTML(ContextualDirectory.imageItemHTML(ImageResources.Instance.designIcon(), title));
    initialValueSet=vs;
    vsEditor.load(new ValueSet(vs), true);  // contentReadOnly=true
    dialogBox.center();
    apply.setEnabled(false);
  }

  @UiHandler("vsEditor")
  void onValueSetChange(ValueChangeEvent<ValueSet> event) {
    boolean changed=!initialValueSet.equals(vsEditor.getValueSet());
    apply.setEnabled(changed);
  }

  //
  // Two ways of effecting the pending change
  //

  @UiHandler("ok")
  void okClicked(ClickEvent event){
    applyChangeIfAny();
    dialogBox.hide();
  }

  @UiHandler("apply")
  void applyClicked(ClickEvent event){
    applyChangeIfAny();
  }
  
  protected void applyChangeIfAny() {
    apply.setEnabled(false);
    if(initialValueSet.equals(vsEditor.getValueSet()))
      return;
    
    CUDValueSet req= CUDValueSet.update(vsEditor.getValueSet());
    RPC.Operation<?> svcCall=new RPC.Operation<CUDValueSetResponse>(req){
      public void onSuccess(CUDValueSetResponse result){
        super.onSuccess(result);
        // Sync up with host repository
        initialValueSet.name=vsEditor.getValueSet().name;
        initialValueSet.desc=vsEditor.getValueSet().desc;
        initialValueSet.isInline=vsEditor.getValueSet().isInline;
        hostRepository.refreshSelectedRow();
      }
    };
    RPC.Instance.exec(svcCall);
  }

  //
  // Three ways of cancelling
  //
  @UiHandler("cancel")
  void cancelClicked(ClickEvent event){
    cancelAndClose();
  }

  @UiHandler("dialogBox")
  void escaped(CloseEvent<PopupPanel> event){
    cancelAndClose();
  }
  
  @UiHandler("xButton")
  void xClicked(ClickEvent event){
    cancelAndClose();
  }
  
  protected void cancelAndClose(){
    dialogBox.hide();
  }
}
package fx.presentment.design.repository;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.*;
import com.google.gwt.user.client.ui.*;

import fx.presentment.ClientAppContext;
import fx.presentment.FxConstants;
import fx.presentment.ImageResources;
import fx.service.RPC;
import fx.service.Response;

/**
 * @author kw
 * @param <T>
 */
public abstract class ContextualDirectory<T> extends Composite implements HasSelectionHandlers<T> {
  public static String imageItemHTML(ImageResource imageProto, String title) {
    return AbstractImagePrototype.create(imageProto).getHTML() + " " + title;
  }

  interface MyBinder extends UiBinder<Widget, ContextualDirectory<?>> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  protected static final String REQUERY="Requery.....";
  
  @UiField protected ScrollPanel scrollContainer;
  @UiField public Tree treeList;
  public TreeItem pub, domain, personal;
  protected TreeItem[] topLevelContainers;
  
  protected Class<?> subjectClass;
  ButtonBase open=new Button();
  
  protected transient T currentlySelected;

  public ContextualDirectory() {
    initWidget(binder.createAndBindUi(this));
    String[] nodeTexts=getTopLevelNodeTexts();
    personal = createTopLevelContainerNode(imageItemHTML(ImageResources.Instance.personalIcon(),nodeTexts[0]));
    domain = createTopLevelContainerNode(imageItemHTML(ImageResources.Instance.domainIcon(),nodeTexts[1]));
    pub = createTopLevelContainerNode(imageItemHTML(ImageResources.Instance.publicIcon(),nodeTexts[2]));
    treeList.addItem(personal);
    treeList.addItem(domain);
    treeList.addItem(pub);
    if (ClientAppContext.CurrentDomain == null || ClientAppContext.CurrentDomain.isPublic()){
      domain.setVisible(false);
      topLevelContainers=new TreeItem[] {personal, pub};
    }
    else
      topLevelContainers=new TreeItem[] {personal, domain, pub};
  }
  
  // Also supports images
  protected String[] getTopLevelNodeTexts(){
    return FxConstants.Instance.topLevelNodeTexts().split(",");
  }
  
  protected TreeItem createTopLevelContainerNode(String name){
    TreeItem ti=new TreeItem();
    ti.setText(name);
    ti.addTextItem(REQUERY);
    ti.setUserObject(REQUERY);
    return ti;
  }

  private boolean initialized;
  public void open() {
    if(initialized)
      return;
    pub.setState(false);
    domain.setState(false);
    personal.setState(true);
    
    open.setEnabled(false); 
    currentlySelected=null;    // Initially no relevant node has been selected
    initialized=true;
  }
  
  // Conditionally re-query and re-populate container node
  @UiHandler("treeList")
  void handleOpen(OpenEvent<TreeItem> event){
    TreeItem ti = event.getTarget();
    if(ti.getUserObject()==REQUERY)
      populateList(ti, null);
  }

  protected void populateList(TreeItem owner, RPC.PostOperation onSuccessfulCompletion){
    owner.removeItems();
    owner.setUserObject(null);
    RPC.Operation<? extends Response> queryOp=(owner==personal)? getPersonalQueryOp() : 
      (owner==domain)? getDomainQueryOp() : getPublicQueryOp();
    if(queryOp==null)
      return;
    RPC.Instance.exec(queryOp.postSuccess(onSuccessfulCompletion));
  }
  
  protected void add(TreeItem container, List<T> uos){
    for(T uo: uos)
      add(container, uo);
  }
  protected TreeItem add(TreeItem container, T uo){
    TreeItem ti= new TreeItem();
    container.addItem(ti);
    bind(ti, uo);
    return ti;
  }
  protected void bind(TreeItem node, T uo){
    node.setUserObject(uo);
    node.setHTML(uo.toString());
  }
  
  //
  // All about selection
  //

  public void setChangeNotifyButton(ButtonBase notify){
    this.open=notify;
  }
  
  public HandlerRegistration addSelectionHandler(SelectionHandler<T> handler){
    return addHandler(handler, SelectionEvent.getType());
  }

  @UiHandler("treeList")
  void handleSelection(SelectionEvent<TreeItem> event){
    scrollContainer.setHorizontalScrollPosition(0);
    T sel = getSelectedSubject();
    if (sel !=null)
      open.setEnabled(true);
    else
      open.setEnabled(false);
    doFireSelectionEvent(sel);
  }
  
  protected void doFireSelectionEvent(T sel){
    if (sel != currentlySelected || sel==null) {
      SelectionEvent.<T>fire(this, sel);
      currentlySelected = sel;
    }
  }
  
  @SuppressWarnings("unchecked")
  public T getSelectedSubject(){
    TreeItem ti=treeList.getSelectedItem();
    if(ti!=null && ti.getUserObject()!=null && ti.getUserObject().getClass()== subjectClass)
      return (T)ti.getUserObject();
    else
      return null;
  }
  
  public boolean selectedPersonalWorkspace(){
    return (treeList.getSelectedItem()==personal);
  }
  
  public void selectPersonalWorkspace(){
    treeList.setSelectedItem(personal);
  }
  
  public void clearSelection(){
    treeList.setSelectedItem(null);
    currentlySelected=null;
  }
  
  /**
   * Programmatically search and select/focus on a node that has a specified user object. 
   * Requery if necessary. If <code>selectFirstChild</code>, then select first child instead 
   * of the node itself.
   */
  public void searchAndSelect(final Object uo, final boolean selectFirstChild) {
    clearSelection();
    int i = 0;
    for (TreeItem c : topLevelContainers) {
      int result = searchAndSelect(c, uo, selectFirstChild);
      System.out.println(result + " Searchandselect tnidx " + i++);
      if (result == 0) {
        populateList(c, new RPC.PostOperation() {
          public void perform() {
            searchAndSelect(uo, selectFirstChild);
          }
        });
        return;
      }
      if (result > 0) {
        c.setState(true);
        return;
      }
    }
  }

  private int searchAndSelect(TreeItem node, Object uo, boolean selectFirstChild){
    if(uo.equals(node.getUserObject())){
      if(selectFirstChild)
	treeList.setSelectedItem(node.getChild(0));
      else
	treeList.setSelectedItem(node);
      return 1;
    }
    else if (REQUERY.equals(node.getUserObject()))
      return 0;
    for(int i=0; i<node.getChildCount();i++){
      TreeItem n=node.getChild(i);
      int result=searchAndSelect(n, uo,selectFirstChild);
      if(result>=0)
	return result;
    }
    return -1;
  }

  // Refresh after an update
  @SuppressWarnings("unchecked")
  public void refreshSelectedNode(){
    TreeItem ti=treeList.getSelectedItem();
    if(ti!=null && ti.getUserObject()!=null && ti.getUserObject().getClass()== subjectClass)
      bind(ti, (T)ti.getUserObject());
  }

  public void addThenSelect(T uo, boolean riseToTop){
   addNode(personal, uo, riseToTop, true, true); 
  }
  
  protected void addNode(TreeItem folder, T uo, boolean riseToTop, boolean select, boolean fireEvent){
    TreeItem ti=add(folder, uo);
    if(riseToTop)
    for(int i=0; i<folder.getChildCount()-1;i++){
      TreeItem n=folder.getChild(0);
      folder.addItem(n);
    }
    if(select)
      treeList.setSelectedItem(ti,fireEvent);
  }

  public void removeSelected() {
    TreeItem ti = treeList.getSelectedItem();
    if (ti != null) {
      ti.remove();
      currentlySelected = null;
      SelectionEvent.<T>fire(ContextualDirectory.this, null);
    }
  }

  /**
   * Re-render according to latest persistence record.
   * @param uo
   */
  public void update(T uo){
    TreeItem found=null;
    for(TreeItem c: topLevelContainers){
      if((found=search(c, uo))!=null)
        break;
    }
    if (found != null) {
      bind(found, uo);
      if(uo.equals(currentlySelected))
        SelectionEvent.<T>fire(this, uo);
    }
  }

  protected TreeItem search(TreeItem node, T uo){
    if(uo.equals(node.getUserObject()))
      return node;
    for(int i=0; i<node.getChildCount();i++){
      TreeItem found=search(node.getChild(i), uo);
      if(found!=null)
        return found;
    }
    return null;
  }

  protected RPC.Operation<? extends Response> getPersonalQueryOp(){
    return null;
  }
  protected RPC.Operation<? extends Response> getDomainQueryOp(){
    return null;
  }
  protected RPC.Operation<? extends Response> getPublicQueryOp(){
    return null;
  }
}
package fx.presentment.design;

import java.util.*;

import fx.model.MetaForm;

/**
 * Manages the selection/unselection of <code>EditableNode</code> objects. Usually associated with 
 * a container type throughout its life cycle.
 *
 * @author kw
 */
public class SelectionModel extends SelectionOnlyModel {

  private FormCanvas container;

  public SelectionModel(FormCanvas container){
    super(container);
    this.container=container;
  }

  public List<EditableNode> adjustSelected(){
    List<EditableNode> adjustedList=new ArrayList<EditableNode>();
    List<EditableNode> row=new ArrayList<EditableNode>(5);
    for(int i=0; i<selectedNodes.size(); ){
      EditableNode n=selectedNodes.get(i++);
      EditableNode clone = new EditableNode(n.getInnerNode().clone());
      // Sub-Selection mode - disregard EditableNode and immediately return
      if(n.getSubSelectionNode()!=null){
        clone.getInnerNode().rowMarker=0;
        adjustedList.add(clone);
        return adjustedList;
      }
      if(!container.isInRow(n)){
        adjustedList.add(clone);
        continue;
      }
      row.clear();
      row.add(clone);
      int j=i;
      for(; j<selectedNodes.size(); j++){
        EditableNode n2 = selectedNodes.get(j);
        if (!container.isInSameRow(n, n2))
          break;
        clone = new EditableNode(n2.getInnerNode().clone());
        row.add(clone);
      }
      System.out.println("@@@ Adjusting 0 and "+row.size());
      if(row.size()>1){
        row.get(0).getInnerNode().rowMarker = 1;
        row.get(row.size() - 1).getInnerNode().rowMarker = 2;
      }
      else
        row.get(0).getInnerNode().rowMarker=0;
      adjustedList.addAll(row);
      
      for(EditableNode nn:row)
        System.out.println("@@@ stored "+nn.getInnerNode().rowMarker);
      
      i=j;
    }
    return adjustedList;
  }
  
  public List<EditableNode> adjustAffected(){
    List<EditableNode> adjustedList=new ArrayList<EditableNode>();
    List<EditableNode> row=new ArrayList<EditableNode>(5);
    for(int i=0; i<selectedNodes.size(); ){
      EditableNode n=selectedNodes.get(i++);
      // Sub-Selection mode - make inner adjustments and immediately return
      MetaForm subNode=n.getSubSelectionNode();
      if(subNode!=null){
        MetaForm sibling;
        if(subNode.rowMarker==1){
          sibling=subNode.nextSibling();
          sibling.rowMarker=(sibling.rowMarker==0)? 1:0;
          adjustedList.add(new EditableNode(sibling));  
        }
        else if(subNode.rowMarker==2){
          sibling=subNode.previousSibling();
          sibling.rowMarker=(sibling.rowMarker==0)? 2:0;
          adjustedList.add(new EditableNode(sibling));  
        }
        return adjustedList;
      }
      if(!container.isInRow(n))
        continue;
      row.clear();
      int rstart=container.getFirstWidgetInRowIndex(n);
      int rwidth=container.getRowWidth(n);
      for(int j=0; j<rwidth; j++)
        row.add((EditableNode)container.getWidget(rstart+j));
      
      int j=i;
      for(; j<selectedNodes.size(); j++){
        EditableNode n2 = selectedNodes.get(j);
        if (!container.isInSameRow(n, n2))
          break;
        row.remove(n2);
      }
      row.remove(n);
      
      if(row.size()>1){
        if (row.get(0).getInnerNode().rowMarker != 1) {
          row.get(0).getInnerNode().rowMarker = 1;
          adjustedList.add(row.get(0));
        }
        if (row.get(row.size() - 1).getInnerNode().rowMarker != 2) {
          row.get(row.size() - 1).getInnerNode().rowMarker = 2;
          adjustedList.add(row.get(row.size() - 1));
        }
      }
      else if(row.size()==1){
        if (row.get(0).getInnerNode().rowMarker != 0) {
          row.get(0).getInnerNode().rowMarker = 0;
          adjustedList.add(row.get(0));
        }
      }
      i=j;
    }
    return adjustedList;
  }
  
  public boolean isSelectionInBlock(){
    if(selectedNodes.size()<2)
      return false;
    EditableNode n=selectedNodes.get(selectedNodes.size()-1);
    if(container.isInRow(n) && !container.isLastInRow(n))
      return false;
    n=selectedNodes.get(0);
    if(container.isInRow(n) && !container.isFirstInRow(n))
      return false;
    int idx=container.getWidgetIndex(n);
    for(int i=1;i<selectedNodes.size();i++){
      int nxtIdx=container.getWidgetIndex(selectedNodes.get(i));
      if(nxtIdx!=idx+1)
        return false;
      idx=nxtIdx;
    }
    return true;
  }
}
package fx.presentment.design;

import java.util.*;

import com.allen_sauer.gwt.dnd.client.util.CoordinateLocation;
import com.allen_sauer.gwt.dnd.client.util.WidgetArea;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;

import fx.model.*;
import fx.presentment.event.NodeSelectionEvent;
import fx.presentment.renderer.Renderer;
import fx.presentment.widget.Tooltip;

/**
 * The widget that wraps around MetaForm nodes for focusing, reviewing, and editing on the FormCanvas.
 *
 * @author kw
 */
public class EditableNode extends FocusPanel implements HasDoubleClickHandlers, HasSelectionHandlers<EditableNode>, Tooltip.Supplier {
  
  public MetaForm metaForm;
  private DoubleClickHandler dblClickHandler;
  private ArrayList<HandlerRegistration> selectionHandlers=new ArrayList<HandlerRegistration>();
  
  private transient boolean isCurrentlySelected;
  private transient Widget currentExclusivelySelectedWidget;
  protected ClickHandler selectionHandler =new ClickHandler() {
    public void onClick(ClickEvent event) {
      event.stopPropagation();
      event.preventDefault();
      if(isOffPalette())
        return;
      if(event.getNativeEvent().getCtrlKey() || event.getNativeEvent().getShiftKey()){
        exitSubselectState();
        NodeSelectionEvent.fire(EditableNode.this, event.getNativeEvent().getCtrlKey(), event.getNativeEvent().getShiftKey());
        return;
      }
      // In SubSelection mode
      Widget wrapped=EditableNode.this.getWidget();
      CoordinateLocation loc=new CoordinateLocation(event.getClientX()+Window.getScrollLeft(), event.getClientY()+Window.getScrollTop());
      List<Widget> hitList = new ArrayList<Widget>(5);
      hitList.add(0,EditableNode.this);
      buildHitList(hitList, wrapped, loc);
      Collections.reverse(hitList);
      subSelect(hitList);
      NodeSelectionEvent.fire(EditableNode.this, false,false);
    }
  };
  private void buildHitList(List<Widget> hl, Widget w, CoordinateLocation loc){
    if(!(w instanceof HasWidgets))
      return;
    Iterator<Widget> it=((HasWidgets)w).iterator();
    while(it.hasNext()){
      WidgetArea wa=new WidgetArea(it.next(),null);
      if(wa.intersects(loc)){
        MetaForm mf = Renderer.extractNodeFromWidget(wa.getWidget());
        if (mf != null)
          hl.add(wa.getWidget());
        buildHitList(hl, wa.getWidget(), loc);
        break;
      }
    }
  }
  private void subSelect(List<Widget> hitList){
    int currentLevel=0;
    if(hitList.contains(currentExclusivelySelectedWidget))
      currentLevel=hitList.indexOf(currentExclusivelySelectedWidget)+1;
    else
      currentLevel=0;
    
    if(isCurrentlySelected){
    	if(currentExclusivelySelectedWidget!=null)
    		currentExclusivelySelectedWidget.removeStyleName(selectionStyleName());
    	else
    		removeStyleName(selectionStyleName());
    }
    currentExclusivelySelectedWidget=hitList.get(currentLevel%hitList.size());
    currentExclusivelySelectedWidget.addStyleName(selectionStyleName());
  }
  private void exitSubselectState(){
    if(currentExclusivelySelectedWidget!=null){
      currentExclusivelySelectedWidget.removeStyleName(selectionStyleName());
      currentExclusivelySelectedWidget=null;
    }
  }
  
  protected EditableNode(){
  }
  
  public EditableNode(MetaForm metaForm){
    this.metaForm=metaForm;
    Widget wrapped=Renderer.Render(metaForm, null);
    if(wrapped!=null){
      setWidget(wrapped);
      addStyleName(styleName());
      
      addClickHandler(selectionHandler);
      new Tooltip(this, description());
    }
  }
  
  public void refresh(){
    MetaForm subNode=getSubSelectionNode();
    Widget wrapped=Renderer.Render(metaForm, null);
    setWidget(wrapped);

    Renderer.applyTDStyles(metaForm, this);

    // Re-select subNode
    if(subNode!=null){
      currentExclusivelySelectedWidget=locateSubNode(wrapped, subNode);
      if(currentExclusivelySelectedWidget!=null)
        currentExclusivelySelectedWidget.addStyleName(selectionStyleName());
      else
        System.err.println("ERROR: failed to retrieve subNode widget ("+metaForm.id+"/"+subNode.id+") from wrapped="+wrapped.getClass().getName());
    }
  }
  
  private Widget locateSubNode(Widget w, MetaForm subNode){
    MetaForm stamped=Renderer.extractNodeFromWidget(w);
    if(subNode.equals(stamped))
      return w;
    if(!(w instanceof HasWidgets) || stamped==null)
      return null;
    
    HasWidgets p=(HasWidgets)w;  
    Iterator<Widget> it=p.iterator();
    while(it.hasNext()){
      Widget c=locateSubNode(it.next(), subNode);
      if(c!=null)
        return c;
    }
    //System.err.println("Locating "+subNode+" from "+stamped+" found none!");
    return null;
  }
  
  public MetaForm getInnerNode(){
    MetaForm subNode=getSubSelectionNode();
    if(subNode!=null)
      return subNode;
    return metaForm;
  }
  
  public MetaForm getSubSelectionNode(){
    if(currentExclusivelySelectedWidget!=null && currentExclusivelySelectedWidget!=this)
      return Renderer.extractNodeFromWidget(currentExclusivelySelectedWidget);
    return null;
  }
  
  public boolean isSelected(){
    return isCurrentlySelected;
  }
  
  public void setSelected(boolean selected){
    if(selected==isCurrentlySelected)
      return;
    isCurrentlySelected=selected;
    
    if(isCurrentlySelected){
      if(currentExclusivelySelectedWidget==null)
        addStyleName(selectionStyleName());
    }
    else{
      if(currentExclusivelySelectedWidget!=null)
        exitSubselectState();
      else
        removeStyleName(selectionStyleName());
    }
  }
  
  public String styleName(){
    if(metaForm.isDockedPage())
      return "EditableNode EditableNode_Page EditableNode_DockedPage";
    else if(metaForm.isPage())
      return "EditableNode EditableNode_Page";
    else
      return "EditableNode";
  }
  
  protected String selectionStyleName(){
    MetaForm node=getSubSelectionNode();
    if(node==null)
      return "EditableNode-Selected";
    else
      return "EditableNode-Selected_Inner";
  }
  
  public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
    if(handler==dblClickHandler)
      return null;
    else{
      dblClickHandler=handler;
      return addDomHandler(handler, DoubleClickEvent.getType());
    }
  }

  public boolean isOffPalette(){
    return dblClickHandler==null;
  }
  
  public HandlerRegistration addSelectionHandler(SelectionHandler<EditableNode> handler){
    HandlerRegistration reg=addHandler(handler, SelectionEvent.getType());
    selectionHandlers.add(reg);
    return reg;
  }
  
  public void removeSelectionHandlers(){
    for(HandlerRegistration reg:selectionHandlers)
      reg.removeHandler();
  }
  
  public String tooltipText(){
    String tt=description();
    if(getInnerNode().isPage()){
      MetaForm parent=getInnerNode().getParent();
      int pageNo=parent.getChildren().indexOf(getInnerNode())+1;
      tt+=" "+pageNo;
    }
    else if(getInnerNode().isImage()){
      String[] nodeText=getInnerNode().text.split("=");
      if(nodeText.length>1)
        return nodeText[1];
      else
        return tt;
    }
    return tt;
  }
  
  public Widget tooltipSource() {
    if (currentExclusivelySelectedWidget != null)
      return currentExclusivelySelectedWidget;
    else
      return this;
  }
  
  public String description(){
    return NodeTypeProperties.Instance.descMap().get("desc."+getInnerNode().typeCode);
  }
  
  public String toString(){
    return "Node<"+metaForm.id+","+metaForm.typeCode+","+metaForm.cardinal+","+metaForm.renderingHints+">";
  }
}
package fx.presentment.design;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.model.Form;
import fx.model.Publication;
import fx.model.operation.GetFormList;
import fx.model.operation.GetFormListResponse;
import fx.model.operation.design.GetPublicationList;
import fx.model.operation.design.GetPublicationListResponse;
import fx.presentment.ClientAppContext;
import fx.presentment.FxMessages;
import fx.presentment.event.GlobalEvent;
import fx.service.RPC;

/**
 * A primary landing page and central hub.
 *
 */
public class Dashboard extends Composite {
  interface MyBinder extends UiBinder<Widget, Dashboard> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField HTML greeting;
  @UiField Anchor updateProfile;
  
  @UiField FlowPanel recentFormsSection;
  @UiField Grid recentForms;

  @UiField FlowPanel recentVisitsSection;
  @UiField Grid recentVisits;
  
  public Dashboard(){
    initWidget(binder.createAndBindUi(this));
    update();
  }
  
  public void update(){
    // Greetings and Profile reminder
    String name=ClientAppContext.CurrentUser.firstName;
    if(name!=null && !name.isEmpty()){
      greeting.setHTML(FxMessages.Instance.greeting1(name));
      updateProfile.setVisible(false);
    }
    else{
      greeting.setHTML(FxMessages.Instance.greeting0());
      updateProfile.setVisible(true);
    }
    
    // Recently worked on forms
    GetFormList req1=new GetFormList();
    req1.createdByUserId=ClientAppContext.CurrentUser.id;
    req1.changedSince=ClientAppContext.CurrentUser.lastLoggedinDate;
    RPC.Operation<?> op1=new RPC.Operation<GetFormListResponse>(req1){
      public void processSuccess(GetFormListResponse result){
        updateRecentForms(result.formList);
      }
    };
    RPC.Instance.exec(op1);

    // Recently accessed publications
    GetPublicationList req2=new GetPublicationList();
    req2.createdByUserId=ClientAppContext.CurrentUser.id;
    req2.accessedSince=ClientAppContext.CurrentUser.lastLoggedinDate;
    RPC.Operation<?> op2=new RPC.Operation<GetPublicationListResponse>(req2){
      public void processSuccess(GetPublicationListResponse result){
        updateRecentActivities(result.pubList);
      }
    };
    RPC.Instance.exec(op2);
  }
  
  @UiHandler("updateProfile")
  void updateProfile(ClickEvent event){
    GlobalEvent.fire(GlobalEvent.Kind.SHOW_PROFILE,null);
  }
  
  //
  // Show recently worked on Forms 
  //
  void updateRecentForms(List<Form> formList){
   if(formList==null || formList.isEmpty()){
     recentFormsSection.setVisible(false);
     return;
   }
   recentFormsSection.setVisible(true);
   recentForms.resize(formList.size(), 5);
   for(int i=0; i<formList.size(); i++){
     final Form f=formList.get(i);
     Anchor lnk=new Anchor(f.name);
     lnk.addClickHandler(new ClickHandler(){
       public void onClick(ClickEvent event){
         GlobalEvent.fire(GlobalEvent.Kind.SHOW_FORM,f);
       }
     });
     
     recentForms.setWidget(i, 0, new Label(f.getStatusText()));
     recentForms.setWidget(i, 1, lnk);
     recentForms.setWidget(i, 2, new HTML(f.desc));
     
     String cdt=FxMessages.Instance.createdLine(FormInfo.Dtf.format(f.createDate));
     recentForms.setWidget(i, 3, new HTML(cdt));
     String lcdt=FxMessages.Instance.lastChangedLine(FormInfo.Dtf.format(f.lastChangeDate));
     recentForms.setWidget(i, 4, new HTML(lcdt));
   }
  }
  
  //
  // Show recent user activities
  //
  void updateRecentActivities(List<Publication> pubList){
    if(pubList==null || pubList.isEmpty()){
      recentVisitsSection.setVisible(false);
      return;
    }
    recentVisitsSection.setVisible(true);
    recentVisits.resize(pubList.size(), 5);
    for(int i=0; i<pubList.size(); i++){
      final Publication p=pubList.get(i);
      Anchor lnk1=new Anchor(p.getForm().name);
      lnk1.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event){
          GlobalEvent.fire(GlobalEvent.Kind.SHOW_FORM,p.getForm());
        }
      });
      
      String pdt=FxMessages.Instance.publishedLine(FormInfo.Dtf.format(p.publishingDate));
      Anchor lnk2=new Anchor(pdt);
      lnk2.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event){
          GlobalEvent.fire(GlobalEvent.Kind.SHOW_PUBLICATION,p);
        }
      });

      String lad=FxMessages.Instance.lastAccessedLine(FormInfo.Dtf.format(p.lastAccessDate));
      Anchor lnk3=new Anchor(lad);
      lnk3.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event){
          GlobalEvent.fire(GlobalEvent.Kind.SHOW_ENTRIES, p);
        }
      });

      String totalVisits=FxMessages.Instance.totalVisitsLine(p.entryCount);
      Anchor lnk4=new Anchor(totalVisits);
      lnk4.addClickHandler(new ClickHandler(){
        public void onClick(ClickEvent event){
          GlobalEvent.fire(GlobalEvent.Kind.SHOW_ENTRIES, p);
        }
      });
      
      recentVisits.setWidget(i, 0, lnk1);
      recentVisits.setWidget(i, 1, lnk2);
      recentVisits.setWidget(i, 2, new HTML(p.desc));
      recentVisits.setWidget(i, 3, lnk3);
      recentVisits.setWidget(i, 4, lnk4);
    }
  }
  
}
package fx.presentment.design;

import java.util.*;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Widget;

import fx.model.Form;
import fx.model.MetaForm;
import fx.presentment.FxConstants;
import fx.presentment.widget.TabularFlowPanel;

/**
 * Container for tabular form editing support. User drag/drop actions change the physical layout 
 * of affected form nodes; these changed nodes are accumulated, and extracted by FormEditor for
 * persistence.
 * 
 * @author kw
 */
public class FormCanvas extends TabularFlowPanel  implements HasClickHandlers {
  public static final Widget PlaceHolder=new HTML(FxConstants.Instance.dragAndDropHint());

  void ready(){
    add(PlaceHolder);
  }
  void accept(){
    remove(PlaceHolder);
  }
  
  void layout(FormEditor caller, MetaForm subroot, MetaForm preSelect){
    clear();
    caller.selectionModel.clearSelection();
    if(subroot==null || subroot.getChildren()==null || subroot.getChildren().isEmpty()){
      if(caller.theForm!=null)
        ready();
      return;
    }
    MetaForm centerPage=null;
    if(subroot.isRoot()){
      Form.DockView dv=caller.theForm.dockView();
      if(dv!=null && dv.needShiftWhenPaste())
        centerPage=dv.p;
    }
    
    Map<MetaForm, Widget> row=new HashMap<MetaForm, Widget>();
    for(MetaForm c:subroot.getChildren()){
      EditableNode node=new EditableNode(c); 
      if(c.rowMarker==1 || inRow)
        row.put(c, node);
      
      add(node);
      if (c.rowMarker == 1)
        rowStarted();
      else if (c.rowMarker == 2){
        rowEnded();
        applyRowStyling(row);
        row.clear();
      }

      caller.selectionModel.come(node);
      if(c==preSelect){
        caller.selectionModel.select(node);
      }
      if(!caller.theForm.isReadOnly() && !c.isDockedPage() && c!=centerPage)
        caller.dragController.makeDraggable(node);
      node.addDoubleClickHandler(caller); // regardless
      node.addSelectionHandler(caller); // must be added AFTER SelectionModel
    }
    
    // Ensure the newly rendered page is visible. This cures the problem that when zooming in a page
    // from down below, the windows scroll bar remains down and the page goes to the top and becomes
    // hidden from the viewport.
    com.google.gwt.user.client.Window.scrollTo(0,0);
  }

  @Override
  public void replace(Widget w1, Widget w2){
    super.replace(w1, w2);
    
    if(!isInRow(w1) || isInMiddleOfRow(w1)){
      updateLayoutState(w1, 0);
      return;
    }
    if(isLastInRow(w1)){
      updateLayoutState(w1, 2);
      Widget leftSibling=getWidget(getWidgetIndex(w1)-1);
      updateLayoutState(leftSibling,isFirstInRow(leftSibling)? 1:0);
    }
    else {
      updateLayoutState(w1, 1);
      Widget rightSibling=getWidget(getWidgetIndex(w1)+1);
      updateLayoutState(rightSibling,isLastInRow(rightSibling)? 2:0);
    }
  }  

  @Override
  public boolean remove(Widget w1) {
    if(!(w1 instanceof EditableNode) || !isInRow(w1) || isInMiddleOfRow(w1))
      return super.remove(w1);

    if(isLastInRow(w1)){
      Widget leftSibling=getWidget(getWidgetIndex(w1)-1);
      updateLayoutState(leftSibling,isFirstInRow(leftSibling)? 0:2);
    }
    else{
      Widget rightSibling=getWidget(getWidgetIndex(w1)+1);
      updateLayoutState(rightSibling,isLastInRow(rightSibling)? 0:1);
    }
    updateLayoutState(w1, 0);
    return super.remove(w1);
  }
  
  // Nodes pending posture changes and their current persist posture values
  private HashMap<EditableNode, Integer> nodesPendingPostureChanges=new HashMap<EditableNode, Integer>(); 
  
  private boolean updateLayoutState(Widget w, int newPosture){
    if(!(w instanceof EditableNode))
      return false;
    EditableNode node=(EditableNode)w;
    if(node.metaForm.rowMarker==newPosture)
      return false;
    
    if(nodesPendingPostureChanges.get(node)==null)
      nodesPendingPostureChanges.put(node, node.metaForm.rowMarker);
    else if(nodesPendingPostureChanges.get(node)==newPosture)
      nodesPendingPostureChanges.remove(node);
    
    node.metaForm.rowMarker=newPosture;
    return true;
  }
  
  ArrayList<EditableNode> collectPostureChangedNodes(){
    ArrayList<EditableNode> result=new ArrayList<EditableNode>();
    Iterator<EditableNode> it=nodesPendingPostureChanges.keySet().iterator();
    while(it.hasNext()){
      EditableNode node=it.next();
      if(node.metaForm.rowMarker!=nodesPendingPostureChanges.get(node)){
        result.add(node);
      }
    }
    nodesPendingPostureChanges.clear();
    return result;
  }

  public HandlerRegistration addClickHandler(ClickHandler handler) {
    return addDomHandler(handler, ClickEvent.getType());
  }
}
package fx.presentment.design;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;



/**
 * The RecycleBin/Clipboard metaphor, with a UI representation (currently a half-opened bin below the tools),
 * and a list of stored node clippings - those currently on top can be repeatedly pasted to forms in editing.
 * Once a new cut/copy action is done, old ones get shoved down the bucket.
 * 
 * @author kw
 */
class RecycleBin extends Composite {
  private static final int DEPTH=100;
  
  interface MyBinder extends UiBinder<Widget, RecycleBin> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField HTML labelText;
  RecycleBin(){
    initWidget(binder.createAndBindUi(this));
    addStyleName("recycleBin");
  }
  
  void activate(){
    labelText.addStyleName("recycleBin-engage");
  }

  void deactivate(){
    labelText.removeStyleName("recycleBin-engage");
  }
  
  //
  // Copy/Cut/Paste Support
  //

  private List<EditableNode> bucket=new ArrayList<EditableNode>(DEPTH);
  
  private transient List<EditableNode> nodesOnTop;
  
  boolean canPaste(){
    return nodesOnTop!=null;
  }
  
  boolean pageOnTop(){
    return nodesOnTop!=null && nodesOnTop.get(0).metaForm.isPage();
  }
  
  void copied(List<EditableNode> nodes){
    if(nodes.equals(nodesOnTop))
      return;
    storeOnTop(nodes);
  }

  void cut(List<EditableNode> nodes){
    storeOnTop(nodes);
  }
  
  List<EditableNode> toPaste(){
    return nodesOnTop;
  }
  
  public void storeOnTop(List<EditableNode> nodes){
    if(nodesOnTop!=null){
      // Put away previous selection
      int count0=nodesOnTop.size();
      int count1=bucket.size();
      int purgeCount=count1+count0-DEPTH;
      for(int i=0;i<purgeCount; i++)
	bucket.remove(count1-1-i);
      for(int i=count0-1;i>=0; i--)
	bucket.add(0,nodesOnTop.get(i));
    }
    // Put new nodes on top
    nodesOnTop=new ArrayList<EditableNode>();
    nodesOnTop.addAll(nodes);
  }
}
package fx.presentment.design.dnd;

import com.allen_sauer.gwt.dnd.client.DragContext;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.allen_sauer.gwt.dnd.client.drop.AbstractPositioningDropController;
import com.allen_sauer.gwt.dnd.client.util.CoordinateLocation;
import com.allen_sauer.gwt.dnd.client.util.DOMUtil;
import com.allen_sauer.gwt.dnd.client.util.DragClientBundle;
import com.allen_sauer.gwt.dnd.client.util.WidgetArea;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

import fx.presentment.widget.TabularFlowPanel;

/**
 * A "precisely postured positioning" drop controller for TabularFlowPanel canvas.
 * The insertion point is defined by separate logical/physical indices. The "posture"
 * value represents the 4 possibilities created by the presence of linear multi-cell rows.
 *
 * @author kw
 */
public class TabularFlowPanelDropController extends AbstractPositioningDropController {
  private static final Label DUMMY_LABEL_IE_QUIRKS_MODE_OFFSET_HEIGHT = new Label("x");

  private final TabularFlowPanel dropTarget;
  transient protected Widget positioner;
  transient protected int dropIndex;
  transient private int dropPosture;
  
  /**
   * @param dropTarget
   */
  public TabularFlowPanelDropController(TabularFlowPanel dropTarget) {
    super(dropTarget);
    this.dropTarget = dropTarget;
  }
  
  @Override
  public void onEnter(DragContext context) {
    super.onEnter(context);
    positioner = newPositioner(context);
    // The following changes are critical for TabularFlowPanel.isInMiddleOfRow(Widget)
    // when the widget being dragged is already in the container. The picking-up removal
    // of the widget apparently happens after the position is placed on the canvas, thus
    // isInMiddleOfRow() will return true if the the user clicks on the right zone of the
    // target widget.
    //updateInsertionPoint(new CoordinateLocation(context.mouseX,context.mouseY));
    //place(positioner);
  }

  @Override
  public void onMove(DragContext context) {
    if(positioner==null)
      return;
    super.onMove(context);
    updateInsertionPoint(new CoordinateLocation(context.mouseX,context.mouseY));
    place(positioner);
  }

  @Override
  public void onPreviewDrop(DragContext context) throws VetoDragException {
    if(positioner==null)
      return;
    super.onPreviewDrop(context);
    dropIndex = dropTarget.getWidgetIndex(positioner);
    if (dropIndex == -1) {
      throw new VetoDragException();
    }
  }

  @Override
  public void onDrop(DragContext context) {
    if(positioner==null)
      return;
    super.onDrop(context);
    Widget widget=context.selectedWidgets.get(0);
    dropTarget.replace(widget, positioner);
  }

  @Override
  public void onLeave(DragContext context) {
    if(positioner==null)
      return;
    super.onLeave(context);
    positioner.removeFromParent();
    positioner = null;
  }

  protected void place(Widget w){
    if(dropIndex<0)
      return;
    
    if(dropPosture<0){
      dropTarget.prepend(w, dropIndex);
    }
    else if (dropPosture==0){
      dropTarget.insert(w, dropIndex);
    }
    else if (dropPosture==1){
      dropTarget.append(w, dropIndex);
    }
    else {
      dropTarget.insertBelow(w, dropIndex);
    }
  }
  
  protected void updateInsertionPoint(CoordinateLocation location){
    int widgetCount = dropTarget.getWidgetCount();
    if (widgetCount == 0) {
      dropIndex=0;
      dropPosture=0;
      return;
    }
    int hitIndex=-1;
    Widget hitWidget=null;
    WidgetArea hitArea=null;
    int low=0, high=widgetCount, r=0;
    while(low<high){
      int mid = (low + high) / 2;
      Widget widget = dropTarget.getWidget(mid);
      WidgetArea midArea = new WidgetArea(widget, null);
      r=compare(location, midArea);
      if(r==0){
        hitIndex=mid;
        hitWidget=widget;
        hitArea=midArea;
        break;
      }
      else if(r<0)
        high=mid;
      else
        low=mid+1;
    }
    
    if(offLimit(hitWidget)){
      dropIndex=-1;
      return;
    } 
    if(hitIndex<0){
      if(r>0){
        dropIndex=low<widgetCount? low : widgetCount-1;
        dropPosture=2;
      }
      else
        dropIndex=-1;
      return;
    } 
    dropPosture=findPosture(location, hitArea);
    int positionerIndex=dropTarget.getWidgetIndex(positioner);
    if(positionerIndex<0){
      dropIndex=hitIndex;
      return;
    }
    int hitPhysicalIndex=dropTarget.getWidgetPhysicalIndex(hitWidget);
    int positionerPhysicalIndex=dropTarget.getWidgetPhysicalIndex(positioner);
    boolean positionerInRow=dropTarget.isInRow(positioner);
    boolean inSameRow=(hitPhysicalIndex==positionerPhysicalIndex);
    if(positionerPhysicalIndex==hitPhysicalIndex-1 && !positionerInRow && dropPosture==0 ||
        positionerPhysicalIndex==hitPhysicalIndex+1 && !positionerInRow && dropPosture==2 ||
        positionerIndex==hitIndex-1 && inSameRow && dropPosture==-1 ||
        positionerIndex==hitIndex+1 && inSameRow && dropPosture==1)
      dropIndex=-1;
    else
      dropIndex=hitIndex;
  }
  
  protected boolean offLimit(Widget hitTarget){
    return hitTarget==positioner; 
  }
  
  protected int findPosture(CoordinateLocation location, WidgetArea wa){
    if (location.getLeft() - wa.getLeft() < wa.getWidth() / 4) {
      return -1;
    } else if (-location.getLeft() + wa.getRight() < wa.getWidth() / 4) {
      return 1;
    } else if (location.getTop() - wa.getTop() < wa.getHeight() / 2) {
      return 0;
    } else {
      return 2;
    }
  }

  protected int compare(CoordinateLocation l, WidgetArea wa){
    int top=wa.getTop();
    int bottom=wa.getBottom();
    boolean inRow=dropTarget.isInRow(wa.getWidget());
    if(inRow){
      Element td=DOM.getParent(wa.getWidget().getElement());
      top=DOM.getAbsoluteTop(td);
      bottom=top+DOM.getElementPropertyInt(td, "offsetHeight");
    }
    if(l.getTop()<top)
      return -1;
    else if(l.getTop()>bottom)
      return 1;
    if(!inRow)
      return 0;
    
    if(l.getLeft()<wa.getLeft()){
      if(dropTarget.isFirstInRow(wa.getWidget()))
        return 0;
      return -1;
    }
    else if(l.getLeft()>wa.getRight()){
      if(dropTarget.isLastInRow(wa.getWidget()))
        return 0;
      return 1;
    }
    return 0;
  }
  
  protected Widget newPositioner(DragContext context) {
    SimplePanel outer = new SimplePanel();
    outer.addStyleName(DragClientBundle.INSTANCE.css().positioner());

    RootPanel.get().add(outer, -500, -500);
    outer.setWidget(DUMMY_LABEL_IE_QUIRKS_MODE_OFFSET_HEIGHT);

    int width = 0;
    int height = 0;
    for (Widget widget : context.selectedWidgets) {
      width = Math.max(width, widget.getOffsetWidth());
      height += widget.getOffsetHeight();
    }

    SimplePanel inner = new SimplePanel();
    inner.setPixelSize(width/2 - DOMUtil.getHorizontalBorders(outer), height - DOMUtil.getVerticalBorders(outer));
    outer.setWidget(inner);
    return outer;
  }
}
package fx.presentment;

import fx.model.*;
import fx.presentment.event.*;

/**
 * @author kw
 */
public class FormEntryEditor extends FormEntryPresenter {
  
  public void startSimulatedEntry(Form form){
    loadEntry(new FormEntry(form));
  }

  public void loadEntry(FormEntry entry){
    loadEntry(entry, false);
    if(entry.status==FormEntry.ENTRY_ST_NEW){
      entry.start();
      FormEntryEvent evt=new FormEntryEvent.LifeCycle(entry, null, FormEntryEvent.LifeCycle.STARTED);
      FormEntryEvent.fire(evt);
    }
  }
}
package fx.presentment.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.model.operation.user.RetrievePassword;
import fx.service.Response;
import fx.service.RPC;
import fx.presentment.widget.Status;

class PasswordPanel extends Composite {
  interface MyBinder extends UiBinder<Widget, PasswordPanel> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Status status;
  
  @UiField Grid retrievePwdSection;
  @UiField TextBox firstName;
  @UiField TextBox lastName;
  @UiField TextBox nickName;
  @UiField TextBox passphrase;
  @UiField Button request;
  
  private Long userId;
  PasswordPanel() {
    initWidget(binder.createAndBindUi(this));
    retrievePwdSection.getColumnFormatter().setWidth(0, "200");
    ValueChangeHandler<String> vch=new ValueChangeHandler<String>(){
      public void onValueChange(ValueChangeEvent<String> event){
        request.setEnabled(true);
      }
    };
    firstName.addValueChangeHandler(vch);
    lastName.addValueChangeHandler(vch);
    nickName.addValueChangeHandler(vch);
    passphrase.addValueChangeHandler(vch);
  }

  void reset(Long userId){
    this.userId=userId;
    firstName.setValue("");
    lastName.setValue("");
    nickName.setValue("");
    passphrase.setValue("");
    firstName.setFocus(true);
    firstName.setCursorPos(0);
    request.setEnabled(false);
    status.info(UserConstants.Instance.retrievePasswordInstructions());
  }

  @UiHandler("firstName")
  void enteringFirstName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(firstName,null);
      lastName.setFocus(true);
    }
  }
  @UiHandler("lastName")
  void enteringLastName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(lastName,null);
      nickName.setFocus(true);
    }
  }
  @UiHandler("nickName")
  void enteringNickName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(nickName,null);
      passphrase.setFocus(true);
    }
  }
  @UiHandler("passphrase")
  void enteringSecurityAnswer(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(passphrase,null);
      request.setFocus(true);
    }
  }

  @UiHandler("request")
  void requestPassword(ClickEvent event) {
    RetrievePassword req = new RetrievePassword();
    req.userId=userId;
    req.firstName = firstName.getText().trim();
    req.lastName = lastName.getText().trim();
    req.nickName = nickName.getText().trim();
    req.passphrase = passphrase.getText().trim();
    
    RPC.Operation<Response> op = new RPC.Operation<Response>(req) {
      public void onSuccess(Response resp) {
        retrievePwdSection.setVisible(false);
        super.onSuccess(resp);
        if (resp.errorMessage != null) {
          status.error(resp.errorMessage);
        } else {
          status.info(resp.successMessage);
        }
      }
    };
    RPC.Instance.exec(op);
  }
}
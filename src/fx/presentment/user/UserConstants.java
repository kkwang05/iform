package fx.presentment.user;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.*;

/**
 * User UI Resources
 * @author kw
 */
public interface UserConstants extends Constants {
  public static final UserConstants Instance=GWT.create(UserConstants.class);

  @DefaultBooleanValue(false)
  boolean startDesignerInPopupWindow();
  @DefaultStringValue("/designer.html?gwt.codesvr=127.0.0.1:9997")
  String applicationUrl();
  @DefaultStringValue("/index.html?gwt.codesvr=127.0.0.1:9997")
  String portalUrl();
  @DefaultIntValue(3)
  int maximumAttemptsAllowed();
  
  @DefaultStringValue("Enter here")
  String enterButtonText();
  
  @DefaultStringValue("<b>User Sign in</b>")
  String loginTitle();
  @DefaultStringValue("<b>User Registration</b>")
  String registerTitle();
  @DefaultStringValue("<b>Request Lost Password</b>")
  String retrievePasswordTitle();

  @DefaultStringValue("That is not a valid email address, try again.")
  String invalidEmailMessage();
  @DefaultStringValue("Password must have at least 4 characters, try again.")
  String invalidPasswordMessage();
  @DefaultStringValue("Your passwords do not match, try again.")
  String passwordMismatchMessage();
  
  Map<String, String> loginResponseMap();
  Map<String, String> registerResponseMap();

  String retrievePasswordInstructions();

}
package fx.presentment.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.*;

import fx.model.operation.user.AuthenticateUser;
import fx.model.operation.user.AuthenticateUserResponse;
import fx.service.RPC;
import fx.util.FxCipher;
import fx.presentment.widget.Status;

class LoginPanel extends Composite {
  interface MyBinder extends UiBinder<Widget, LoginPanel> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Grid loginSection;
  
  @UiField TextBox emailField;
  @UiField PasswordTextBox passwordField;
  @UiField CheckBox remember;
  @UiField Button signIn;
  
  @UiField FlowPanel registerSection;
  @UiField Anchor signUp;

  @UiField Status status;
  @UiField FlowPanel retrievePwdSection;
  @UiField Anchor retrieve;
  
  private transient int state; // 1=started logging in; 2=email valid
  private transient int attempts;
  private transient Long userId;

  LoginPanel() {
    initWidget(binder.createAndBindUi(this));
    loginSection.getColumnFormatter().setWidth(0, "200");
  }

  void reset(){
    registerSection.setVisible(true);
    retrievePwdSection.setVisible(false);
    emailField.setEnabled(true);
    passwordField.setEnabled(true);
    
    emailField.setValue("");
    passwordField.setValue("");
    emailField.setFocus(true);
    emailField.setCursorPos(0);
    signIn.setEnabled(false);
    status.clear();
    state=0;
  }
  
  @UiHandler("signUp")
  void register(ClickEvent event) {
    UserAccessDialog.getInstance().register();
  }

  @UiHandler("retrieve")
  void retrievePassword(ClickEvent event) {
    UserAccessDialog.getInstance().retrievePassword(userId);
  }
  
  @UiHandler("emailField")
  void enteringEmail(KeyDownEvent event){
    if(state!=1){
      registerSection.setVisible(false);
      attempts=0;
      state=1;
    }
    status.clear();
    signIn.setEnabled(false);
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(emailField,null);
  }
  
  @UiHandler("emailField")
  void enteredEmail(ValueChangeEvent<String> e){
    final String enteredEmail=emailField.getValue().trim();
    if(RegisterPanel.ValidateEmailAddress(enteredEmail)){
      passwordField.setFocus(true);
      state=2;
    }
    else {
      status.error(UserConstants.Instance.invalidEmailMessage());
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          emailField.setFocus(true);
          emailField.setCursorPos(enteredEmail.length());
        }
      });
    }
  }

  @UiHandler("passwordField")
  void enteringPassword(KeyDownEvent event){
    if(state!=2)
      return;
    status.clear();
    signIn.setEnabled(false);
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(passwordField,null);
  }

  @UiHandler("passwordField")
  void enteredPassword(ValueChangeEvent<String> e){
    if(state!=2)
      return;
    final String enteredPassword=passwordField.getValue();
    if(RegisterPanel.ValidatePassword(enteredPassword)){
      if(e.getValue()==null){
        signin(null);
        passwordField.setFocus(false);
        signIn.setEnabled(false);
      }
      else
        signIn.setEnabled(true);
    }
    else {
      status.error(UserConstants.Instance.invalidPasswordMessage());
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          passwordField.setFocus(true);
          passwordField.setCursorPos(enteredPassword.length());
        }
      });
    }
  }
  
  @UiHandler("signIn")
  void signin(ClickEvent event){
    attempts++;
    //signIn.setEnabled(false);
    String email = emailField.getText();
    String password = passwordField.getText();
    doLogin(email, FxCipher.encrypt(password), remember.getValue());
  }
  
  void doLogin(String email, String encryptedPassword, final boolean rememberCredentials){
    final AuthenticateUser req = new AuthenticateUser();
    req.email = email;
    req.password = encryptedPassword;
    emailField.setEnabled(false);
    passwordField.setEnabled(false);
    signIn.setEnabled(false);
    RPC.Operation<AuthenticateUserResponse> op = new RPC.Operation<AuthenticateUserResponse>(req){
      public void onSuccess(AuthenticateUserResponse resp) {
        super.onSuccess(resp);
        userId=resp.userId;
        if (resp.errorMessage == null){
          if(rememberCredentials)
            Cookies.setCookie("loggedInState", req.email+"|"+req.password);
          else
            Cookies.removeCookie("loggedInState");
          Bootstrap.transfer(resp.voucher);
        }
        else{
          Cookies.removeCookie("loggedInState");
          processResponse(resp.errorMessage);
          emailField.setEnabled(true);
          passwordField.setEnabled(true);
          signIn.setEnabled(true);
        }
      }
    };
    RPC.Instance.exec(op);
  }

  private void processResponse(String errorCode){
    status.error(UserConstants.Instance.loginResponseMap().get("loginResponse."+errorCode));
    
    // No record was found
    if(errorCode.equals("0")){
      emailField.selectAll();
      emailField.setFocus(true);
    }
    // Account is not in active state
    else if(errorCode.equals("1")){
      emailField.selectAll();
      emailField.setFocus(true);
    }
    // Active account exists, but wrong password
    else if(errorCode.equals("2")){
      if(attempts == UserConstants.Instance.maximumAttemptsAllowed()) {
        disableLogin();
        retrievePwdSection.setVisible(true);
      }
      else{
        passwordField.selectAll();
        passwordField.setFocus(true);
      }
    }
    // Reserved for future extensions
    else{ 
      disableLogin();
    }
  }
  
  private void disableLogin(){
    emailField.setEnabled(false);
    passwordField.setEnabled(false);
    signIn.setEnabled(false);
    remember.setEnabled(false);
  }
}
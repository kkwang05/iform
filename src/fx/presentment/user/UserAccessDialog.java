package fx.presentment.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.*;

import fx.presentment.widget.CustomDialogBox;

/**
 * Combined dialog for user login/registration/password recover management. 
 * @author kw
 */
public class UserAccessDialog {
  interface MyBinder extends UiBinder<CustomDialogBox, UserAccessDialog> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  private static UserAccessDialog Instance;
  public static final UserAccessDialog getInstance(){
    if(Instance==null)
      Instance=new UserAccessDialog();
    return Instance;
  }
  
  @UiField CustomDialogBox dialogBox;
  @UiField DeckPanel joggler;
  @UiField LoginPanel loginCard;
  @UiField RegisterPanel registerCard;
  @UiField PasswordPanel passwordCard;
  
  private UserAccessDialog() {
    dialogBox=binder.createAndBindUi(this);
  }
  
  @UiHandler("xButton")
  void xClicked(ClickEvent event){
    dialogBox.hide();
  }
  @UiHandler("dialogBox")
  void escaped(CloseEvent<PopupPanel> event){
    dialogBox.hide();
  }
  
  public void show(){
    login();
  }

  public void hide(){
    dialogBox.hide();
  }

  void login(){
    String savedLogin=Cookies.getCookie("loggedInState");
    if(savedLogin!=null){
      String[] credentials=savedLogin.split("\\|");
      if(credentials.length==2){
        loginCard.doLogin(credentials[0], credentials[1], true);
        return;
      }
    }
    if(!dialogBox.isShowing())
      dialogBox.center();
    dialogBox.setHTML(UserConstants.Instance.loginTitle());
    joggler.showWidget(0);
    Scheduler.get().scheduleDeferred(new ScheduledCommand(){
      public void execute(){
        loginCard.reset();
      }
    });
  }
  
  void register(){
    dialogBox.setHTML(UserConstants.Instance.registerTitle());
    joggler.showWidget(1);
    registerCard.reset();
  }
  
  void retrievePassword(Long userId){
    dialogBox.setHTML(UserConstants.Instance.retrievePasswordTitle());
    joggler.showWidget(2);
    passwordCard.reset(userId);
  }
}
package fx.presentment.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.*;

import fx.model.operation.user.RegisterUser;
import fx.presentment.widget.Status;
import fx.service.RPC;
import fx.service.Response;
import fx.util.FxCipher;

public class RegisterPanel extends Composite {
  
  public static boolean ValidateEmailAddress(String email){
    return !email.isEmpty() && email.matches("[\\w\\.-]+@[\\w\\.-]+\\.[a-zA-Z]{2,4}");
  }
  public static boolean ValidatePassword(String password){
    return !password.isEmpty() && password.length()>3;
  }
    
  interface MyBinder extends UiBinder<Widget, RegisterPanel> { }
  private static final MyBinder binder = GWT.create(MyBinder.class);

  @UiField Grid registrationSection;
  
  @UiField TextBox firstNameField, lastNameField;
  @UiField TextBox emailField;
  @UiField PasswordTextBox passwordField, passwordField2;
  @UiField Button register;
  @UiField Status status;
  
  RegisterPanel() {
    initWidget(binder.createAndBindUi(this));
    registrationSection.getColumnFormatter().setWidth(0, "200");
  }
  
  void reset(){
    firstNameField.setValue("");
    lastNameField.setValue("");
    emailField.setValue("");
    passwordField.setValue("");
    passwordField2.setValue("");
    firstNameField.setFocus(true);
    register.setEnabled(false);
  }

  @UiHandler("firstNameField")
  void enteringFirstName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(firstNameField,null);
      lastNameField.setFocus(true);
    }
  }
  @UiHandler("lastNameField")
  void enteringLastName(KeyDownEvent event){
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER){
      ValueChangeEvent.<String>fire(lastNameField,null);
      emailField.setFocus(true);
    }
  }
  @UiHandler("emailField")
  void enteringEmail(KeyDownEvent event){
    status.clear();
    register.setEnabled(false);
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(emailField,null);
  }
  @UiHandler("passwordField")
  void enteringPassword(KeyDownEvent event){
    status.clear();
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(passwordField,null);
  }
  @UiHandler("passwordField2")
  void enteringPassword2(KeyDownEvent event){
    status.clear();
    if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
      ValueChangeEvent.<String>fire(passwordField2,null);
  }
  
  @UiHandler("emailField")
  void enteredEmail(ValueChangeEvent<String> e){
    final String enteredEmail=emailField.getValue().trim();
    if(ValidateEmailAddress(enteredEmail)){
      passwordField.setFocus(true);
    }
    else {
      status.error(UserConstants.Instance.invalidEmailMessage());
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          emailField.setFocus(true);
          emailField.setCursorPos(enteredEmail.length());
        }
      });
    }
  }
  @UiHandler("passwordField")
  void enteredPassword(ValueChangeEvent<String> e){
    final String enteredPassword=passwordField.getValue();
    if(ValidatePassword(enteredPassword)){
      passwordField2.setFocus(true);
    }
    else {
      status.error(UserConstants.Instance.invalidPasswordMessage());
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          passwordField.setFocus(true);
          passwordField.setCursorPos(enteredPassword.length());
        }
      });
    }
  }
  @UiHandler("passwordField2")
  void enteredPassword2(ValueChangeEvent<String> e){
    final String enteredPassword2=passwordField2.getValue();
    if(enteredPassword2.equals(passwordField.getValue())){
      register.setEnabled(true);
      register.setFocus(true);
    }
    else {
      status.error(UserConstants.Instance.passwordMismatchMessage());
      Scheduler.get().scheduleDeferred(new ScheduledCommand(){
        public void execute(){
          passwordField2.setFocus(true);
          passwordField2.setCursorPos(enteredPassword2.length());
        }
      });
    }
  }
  
  @UiHandler("register")
  void signup(ClickEvent event){
    RegisterUser req=new RegisterUser();
    req.firstName=firstNameField.getText().trim();
    req.lastName=lastNameField.getText().trim();
    req.email=emailField.getText().trim();
    req.encryptedPassword=FxCipher.encrypt(passwordField.getText());
    
    RPC.Operation<Response> op=new RPC.Operation<Response>(req){
      public void onSuccess(Response resp){
        super.onSuccess(resp);
        if(resp.errorMessage!=null){
          processResponse(resp.errorMessage);
        }
        else{
          registrationSection.setVisible(false);
          status.info(resp.successMessage);
        }
      }
    };
    RPC.Instance.exec(op);
  }

  private void processResponse(String errorCode){
    status.error(UserConstants.Instance.registerResponseMap().get("registerResponse."+errorCode));
    // User already exists
    if(errorCode.equals("registered") || errorCode.equals("activated")){
      emailField.selectAll();
      emailField.setFocus(true);
    }
    // Reserved for future extensions
    else{ 
      registrationSection.setVisible(false);
    }
  }
  
}
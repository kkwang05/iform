package fx.presentment.user;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.CssResource.NotStrict;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.Cookies;

/**
 * Main entry point for the entire application.
 * 
 * @author kw
 */
public class Bootstrap implements EntryPoint {

  interface GlobalResources extends ClientBundle {
    @NotStrict
    @Source("user.css")
    CssResource css();
  }
  
  private static final String PAGE_HOOK_USER="user";
  private Button userButton=new Button(UserConstants.Instance.enterButtonText());

  public void onModuleLoad() {
    GWT.<GlobalResources>create(GlobalResources.class).css().ensureInjected();
    RootPanel.get(PAGE_HOOK_USER).add(userButton);
    userButton.setStyleName("btn");
    userButton.addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        UserAccessDialog.getInstance().show();
      }
    });
  }
  
  public static void transfer(String oneTimeVoucher){
    Cookies.setCookie("voucher", oneTimeVoucher);
    String target=UserConstants.Instance.startDesignerInPopupWindow()? "_blank" : "_self";
    Window.open(UserConstants.Instance.applicationUrl(), target, "resizable=yes");
    UserAccessDialog.getInstance().hide();
  }
}
package fx.service;

import java.util.*;

public class CompositeResponse extends Response {
  public static final long serialVersionUID=1L;

  public ArrayList<Response> responseList=new ArrayList<Response>();
  public void addResponse(Response resp){
    responseList.add(resp);
  }
}

package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.MetaForm;
import fx.model.operation.design.MergeNodes;
import fx.model.operation.design.MergeNodesResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class MergeNodesHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(MergeNodesHandler.class.getName());

  public MergeNodesResponse handle(MergeNodes req) {
    String ids=Arrays.toString(req.nodeIds);
    if(req.parentNodeId!=null)
      MyLogger.fine("Received req to merge "+req.nodeIds.length+" nodes:"+ids+" into another pageId="+req.parentNodeId);
    else
      MyLogger.fine("Received req to merge "+req.nodeIds.length+" nodes:" + ids);

    MergeNodesResponse resp = new MergeNodesResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.parentNodeId!=null){  
        // Merge nodes under another parent page
        float cardInc=0f;
        if(req.cardinalAdjustment<100)
          cardInc=100-req.cardinalAdjustment;
        Long oldParentId=null;
        for(Long id:req.nodeIds){
          MetaForm mf=pm.getObjectById(MetaForm.class, id);
          if(oldParentId==null)
            oldParentId=mf.parentId;
          MyLogger.fine("Changing node["+mf.id+"] parent from "+mf.parentId+"->"+req.parentNodeId);
          mf.setParentId(req.parentNodeId);
          mf.setCardinal(mf.cardinal+cardInc);
        }
        MetaForm page2=pm.getObjectById(MetaForm.class, oldParentId);
        pm.deletePersistent(page2);
        MyLogger.info("Deleted merged page, id=" + oldParentId);
      }
      else{
    // Merge nodes under a new formlet
    pm.makePersistent(req.subroot);
    MyLogger.info("Created new formlet root node with id=" + req.subroot.id);
    resp.persistedNode=req.subroot;
    for(Long id:req.nodeIds){
      MetaForm mf=pm.getObjectById(MetaForm.class, id);
      MyLogger.fine("Changing node["+mf.id+"] parent from "+mf.parentId+"->"+resp.persistedNode.id);
      mf.setParentId(resp.persistedNode.id);
    }
      }
      MyLogger.info("Success");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to merge nodes: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
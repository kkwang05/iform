package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.*;
import fx.model.operation.design.DeleteForm;
import fx.model.operation.design.DeleteFormResponse;
import fx.server.PMF;
import fx.server.entity.FxClob;

/**
 * 
 * @author KW
 */
public class DeleteFormHandler {
  private static final Logger MyLogger = Logger.getLogger(DeleteFormHandler.class.getName());

  @SuppressWarnings("unchecked")
  public DeleteFormResponse handle(DeleteForm req) {
    MyLogger.fine("Received req to delete form " + req.formId);
    
    DeleteFormResponse resp=new DeleteFormResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q = pm.newQuery("select valueSetId from fx.model.MetaForm where formId==:formId && valueSetId!=null");
      List<Long> vsIds=(List<Long>)q.execute(req.formId);
      List<ValueSet> inlineVs=new ArrayList<ValueSet>();
      for(Long vsId: vsIds){
      	ValueSet vs=(ValueSet)pm.getObjectById(ValueSet.class, vsId);
      	if(vs.isInline)
      	  inlineVs.add(vs);
      }
      if(inlineVs.size()>0)
        pm.deletePersistentAll(inlineVs);
      MyLogger.fine("Deleted "+inlineVs.size()+" inline valuesets");

      q = pm.newQuery(FxClob.class, "ownerId==:formId");
      long deleted=q.deletePersistentAll(req.formId);
      MyLogger.fine("Deleted "+deleted+" text blocks");

      q = pm.newQuery(MetaForm.class, "formId==:formId");
      deleted=q.deletePersistentAll(req.formId);
      MyLogger.fine("Deleted "+deleted+" form nodes");
      
      Form f = (Form)pm.getObjectById(Form.class, req.formId);
      pm.deletePersistent(f);
      MyLogger.fine("Deleted form");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve form: "+ex.getMessage());
      resp.errorMessage=ex.getMessage();
    } finally {
      pm.close();
    }
    return resp;
  }
}

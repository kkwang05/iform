package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import fx.model.*;
import fx.model.operation.user.RegisterUser;
import fx.service.Response;
import fx.server.ActivateUser;
import fx.server.PMF;

/**
 * Create a new user on the server.
 * @author kw
 */
public class RegisterUserHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(RegisterUserHandler.class.getName());

  public Response handle(RegisterUser req) {
    User candidate = new User();
    candidate.firstName=req.firstName;
    candidate.lastName=req.lastName;
    candidate.email=req.email;
    candidate.password=req.encryptedPassword;
    
    MyLogger.fine("Received request for creating new user: "+candidate);
    
    Response resp = new Response();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    Transaction tx=pm.currentTransaction();
    try {
      tx.begin();
      Query q = pm.newQuery(User.class, "email==:email && (status==:registeredStatus || status==:activeStatus)");
      q.setUnique(true);
      User u= (User)q.execute(candidate.email, User.USER_ST_REGISTERED, User.USER_ST_ACTIVATED);
      if (u!=null) {
        String msg=(u.status==User.USER_ST_REGISTERED)? "registered":"activated";
        resp.errorMessage = msg;
        return resp;
      }
      candidate.status=User.USER_ST_REGISTERED;
      candidate.createDate=new Date();
      pm.makePersistent(candidate);
      ActivateUser.sendActivationEmail(candidate);
      String msg=Res.getString("registerUserSuccessMessage");
      resp.successMessage = msg;
      tx.commit();
      MyLogger.info("Successfully created new user["+candidate.id+"]");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to register new user: "+ex.getMessage());
    } finally {
      if (tx.isActive())
        tx.rollback();
      pm.close();
    }
    return resp;
  }
}
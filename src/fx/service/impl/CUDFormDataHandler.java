package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.operation.*;
import fx.server.PMF;

/**
 * @author kw
 */
public class CUDFormDataHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDFormDataHandler.class.getName());

  public CUDFormDataResponse handle(CUDFormData req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' formdata "+req.transientRecord);
    
    CUDFormDataResponse resp = new CUDFormDataResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.disposition.equals(CUDFormData.CREATE) || req.disposition.equals(CUDFormData.UPDATE)){
	pm.makePersistent(req.transientRecord);
	resp.persistedRecord=req.transientRecord;
      }
      else if(req.disposition.equals(CUDFormData.RETRIEVE)){
      }
      else if(req.disposition.equals(CUDFormData.DELETE)){
      }
      MyLogger.info("Success");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to perform above operation: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
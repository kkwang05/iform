package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.*;
import fx.model.operation.*;
import fx.server.PMF;

/**
 * @author KW
 */
public class GetEntryListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetEntryListHandler.class.getName());

  @SuppressWarnings("unchecked")
  public GetEntryListResponse handle(GetEntryList req) {
    GetEntryListResponse resp=new GetEntryListResponse();
    resp.entryList=new ArrayList<FormEntry>();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q = pm.newQuery(FormEntry.class, "publicationId==:pubId");
      q.declareImports("import java.util.Date");
      q.setOrdering("startDate descending");
      List<FormEntry> rs = (List<FormEntry>) q.execute(req.publicationId);
      MyLogger.fine("Retrieved "+rs.size()+" form entries for publication id="+req.publicationId);
      
      for(FormEntry fe:rs){
	retrieveData(fe, pm);
        resp.entryList.add(fe);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve form entry list: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
  
  @SuppressWarnings("unchecked")
  private void retrieveData(FormEntry fe, PersistenceManager pm){
    Query q = pm.newQuery(FormData.class, "formEntryId==:entryId");
    List<FormData> rs = (List<FormData>) q.execute(fe.id);
    MyLogger.fine("Retrieved "+rs.size()+" data records for formentry "+fe.id);
    fe.data=new ArrayList<FormData>();
    for(FormData d:rs)
      fe.data.add(d);
    if(fe.voucherId!=null){
      fe.voucher=pm.getObjectById(Voucher.class, fe.voucherId);
    }
  }
  
}
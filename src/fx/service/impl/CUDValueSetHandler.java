package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;

import fx.model.operation.design.*;
import fx.server.PMF;

/**
 * @author kw
 */
public class CUDValueSetHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDValueSetHandler.class.getName());

  public CUDValueSetResponse handle(CUDValueSet req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' vs "+req.vsInQuestion+"(id="+req.vsInQuestion.id+")");

    CUDValueSetResponse resp = new CUDValueSetResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.disposition.equals(CUDValueSet.DELETE)){
        // Not needed yet
      }
      else {
        pm.makePersistent(req.vsInQuestion);
        resp.persistedValueSet=req.vsInQuestion;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to CUD ValueSet: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
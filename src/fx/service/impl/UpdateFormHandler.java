package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.operation.design.UpdateForm;
import fx.model.operation.design.UpdateFormResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class UpdateFormHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(UpdateFormHandler.class.getName());

  public UpdateFormResponse handle(UpdateForm req) {
    MyLogger.fine("Received req to update form " + req.changedForm);

    UpdateFormResponse resp = new UpdateFormResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      req.changedForm.lastChangeDate=new Date();
      pm.makePersistent(req.changedForm);
      MyLogger.info("Successfully updated form ["+req.changedForm+"]");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to update form: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.*;
import fx.model.operation.*;
import fx.server.PMF;

/**
 * @author kw
 */
public class CUDFormEntryHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDFormEntryHandler.class.getName());

  public CUDFormEntryResponse handle(CUDFormEntry req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' FormEntry "+req.transientRecord);
    
    CUDFormEntryResponse resp = new CUDFormEntryResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.disposition.equals(CUDFormEntry.CREATE) || req.disposition.equals(CUDFormEntry.UPDATE)){
	req.transientRecord.tracker=req.remoteAddr;
	pm.makePersistent(req.transientRecord);
	if(req.disposition.equals(CUDFormEntry.CREATE)){
	  Publication pub=(Publication)pm.getObjectById(Publication.class, req.transientRecord.publicationId);
	  pub.incrementEntryCount();
	  pm.makePersistent(pub);
	  MyLogger.info("Incremented entry count for pubId="+req.transientRecord.publicationId+" to "+pub.entryCount);
	}
	resp.persistedRecord=req.transientRecord;
      }
      else if(req.disposition.equals(CUDFormEntry.RETRIEVE)){
      }
      else if(req.disposition.equals(CUDFormEntry.DELETE)){
      }
      MyLogger.info("Success");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to perform above operation: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
package fx.service.impl;

import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;

import fx.model.operation.UpdateUser;
import fx.service.Response;
import fx.server.PMF;

/**
 * Update an existing user on the server.
 * @author kw
 */
public class UpdateUserHandler {
  private static final Logger MyLogger = Logger.getLogger(UpdateUserHandler.class.getName());

  public Response handle(UpdateUser req) {
    
    MyLogger.fine("Received request for updating user: "+req.user);
    
    Response resp = new Response();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    Transaction tx=pm.currentTransaction();
    try {
      tx.begin();
      pm.makePersistent(req.user);
      tx.commit();
      MyLogger.info("Successfully updated user["+req.user.id+"]");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to update user: "+ex.getMessage());
    } finally {
      if (tx.isActive())
        tx.rollback();
      pm.close();
    }
    return resp;
  }
}
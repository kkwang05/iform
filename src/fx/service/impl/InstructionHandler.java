package fx.service.impl;

import java.lang.reflect.Method;
import java.util.logging.Logger;
import java.util.*;
import java.util.regex.*;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import org.dom4j.*;

import fx.model.*;
import fx.model.operation.admin.*;
import fx.server.PMF;
import fx.server.ServerAppContext;
import fx.server.entity.FxClob;

/**
 * Send password in email on user request.
 * @author kw
 */
public class InstructionHandler {
  private static final Logger MyLogger = Logger.getLogger(InstructionHandler.class.getName());

  public InstructionResponse handle(Instruction req) {
    InstructionResponse resp = new InstructionResponse();
    try{
      String[] commands=req.statement.split("\\s+",2);
      String inst=commands[0];
      String param=commands.length>1? commands[1]:null;
      MyLogger.info("inst="+inst+"; param="+param);

      Method m=getClass().getDeclaredMethod(inst, String.class, InstructionResponse.class);
      m.invoke(this, param, resp);
    }
    catch(Exception ex){
      ex.printStackTrace();
      MyLogger.severe(ex.toString());
      resp.errorMessage=ex.getMessage();
    }
    return resp;
  }
  
  public void delete(String param, InstructionResponse resp) throws Exception {
    MyLogger.fine("delete() starts");
    StringBuffer sbuf=new StringBuffer();
    PersistenceManager pm = PMF.get().getPersistenceManager();

    long deleted=deleteType(pm, FxClob.class);
    String out="Deleted "+deleted+" text blocks";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    deleted=deleteType(pm, ValueSet.class);
    out="Deleted "+deleted+" VSs";
    sbuf.append("\n"+out);
    MyLogger.fine(out);
    
    deleted=deleteType(pm, MetaForm.class);
    out="Deleted "+deleted+" Nodes";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    deleted=deleteType(pm, Form.class);
    out="Deleted "+deleted+" Forms";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    deleted=deleteType(pm, FormData.class);
    out="Deleted "+deleted+" data items";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    deleted=deleteType(pm, FormEntry.class);
    out="Deleted "+deleted+" form entries";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    deleted=deleteType(pm, Publication.class);
    out="Deleted "+deleted+" publication";
    sbuf.append("\n"+out);
    MyLogger.fine(out);

    resp.setOutcome(sbuf.toString());
  }
  private int deleteType(PersistenceManager pm, Class<?> clz){
    Query q = pm.newQuery(clz);
    long deleted=q.deletePersistentAll();
    return (int)deleted;
  }

  public void reseed(String param, InstructionResponse resp) throws Exception {
    MyLogger.fine("reseed() starts");
    ServerAppContext.init();
    resp.setOutcome("success");
  } 
  
  private static final Pattern GET_PTN=Pattern.compile("get (\\w+( \\w+)*)\\|?");
  private static final Pattern PUT_PTN=Pattern.compile("put (\\w+)");
  private transient HashMap<String, Object> localStore=new HashMap<String, Object>();
  @SuppressWarnings("unchecked")
  public void loadXml(String param, InstructionResponse resp) throws Exception {
    MyLogger.fine("loadXml() starts");
    localStore.clear();
    Document doc = DocumentHelper.parseText(param);
    Iterator<Element> it = doc.getRootElement().elementIterator();
    while (it.hasNext()) {
      Element el = it.next();
      if (el.getName().equals("RETRIEVE"))
        retrieveRecords(el.elementIterator());
      else if (el.getName().equals("CREATE"))
        createRecords(el.elementIterator());
      else if (el.getName().equals("UPDATE"))
        updateRecords(el.elementIterator());
      else if (el.getName().equals("DELETE"))
        deleteRecords(el.elementIterator());
    }
    resp.setOutcome("success");	
  } 
  
  private String[] storeGets(Element rec){
    String directive=rec.attributeValue("directive");
    if(directive==null)
      return null;
    Matcher m=GET_PTN.matcher(directive);
    if(m.lookingAt())
      return m.group(1).split("\\s+");
    else
      return null;
  }
  private String storePut(Element rec){
    String directive=rec.attributeValue("directive");
    if(directive==null)
      return null;
    Matcher m=PUT_PTN.matcher(directive);
    if(m.lookingAt())
      return m.group(1);
    else
      return null;
  }
  private Object storeGetRef(Element rec, Class<?> clz){
    for(String ref: storeGets(rec)){
      Object e=localStore.get(ref);
      if(clz.isInstance(e)){
        MyLogger.fine("Found "+e+" from store with '"+ref+"'");
        return e;
      }
    }
    return null;
  }
  private void storePutRef(Element rec, Object entity){
    String ref=storePut(rec);
    if(entity!=null && ref!=null){
      localStore.put(ref, entity);
      MyLogger.fine("Put "+entity + "in store as '"+ref+"'");
    }
  }
  @SuppressWarnings("unchecked")
  private void createRecords(java.util.Iterator<Element> records){
    PersistenceManager pm = PMF.get().getPersistenceManager();
    while(records.hasNext()){
      Element rec=records.next();
      MyLogger.fine("Creating "+rec.getName());
      Object entity=null;
      
      // CREATE a Domain
      if(rec.getName().equalsIgnoreCase("Domain")){
        String nm=rec.elementTextTrim("name");
        String desc=rec.elementTextTrim("desc");
        MyLogger.fine("nm="+nm+", desc="+desc);
        entity=new Domain(nm, desc);
      }
      // CREATE a ValueSet
      else if(rec.getName().equalsIgnoreCase("ValueSet")){
        List<String> values=new ArrayList<String>();
        for(Element e: (List<Element>)rec.element("values").elements("item"))
          values.add(((Element)e).getText());
        String[] va=new String[values.size()];
        values.toArray(va);
        
        String nm=rec.elementTextTrim("name");
        MyLogger.fine("nm="+nm+", values="+values);
        ValueSet vs = new ValueSet(nm, va);
        vs.isInline=Boolean.valueOf(rec.elementText("isInline"));
        
        Long domainId=null;
        String domainIdStr=rec.elementText("ownedByDomainId");
        if(domainIdStr!=null && !domainIdStr.trim().equals(""))
          domainId=Long.parseLong(domainIdStr);
        if(domainId==null){ // A local reference needed
          Domain pd=(Domain)storeGetRef(rec, Domain.class);
          if(pd!=null)
            domainId=pd.id;
        }
        vs.ownedByDomainId=domainId;  
        entity=vs;
      }
      // CREATE a Form
      else if(rec.getName().equalsIgnoreCase("Form")){
      
      }
      if(entity!=null){
        pm.makePersistent(entity);
        MyLogger.fine("Created "+entity);
        storePutRef(rec, entity);
      }
    }
  }
  private void retrieveRecords(java.util.Iterator<Element> records){
    PersistenceManager pm = PMF.get().getPersistenceManager();
    while(records.hasNext()){
      Element rec=records.next();
      Object entity=null;
      MyLogger.fine("Retrieving "+rec.getName());
      // RETRIEVE a Domain
      if(rec.getName().equalsIgnoreCase("Domain")){
        String nm=rec.elementTextTrim("name");
        Query q=pm.newQuery(Domain.class, "name==:nm");
        q.setUnique(true);
        entity=(Domain)q.execute(nm);
      }
      
      storePutRef(rec, entity);
    }
  }
  private void updateRecords(java.util.Iterator<Element> records){
    while(records.hasNext())
      MyLogger.fine("Updating "+records.next().getName());
    
  }
  private void deleteRecords(java.util.Iterator<Element> records){
    while(records.hasNext())
      MyLogger.fine("Deleting "+records.next().getName());
    
  }
}
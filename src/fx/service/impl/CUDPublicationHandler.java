package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.Form;
import fx.model.operation.design.CUDPublication;
import fx.model.operation.design.CUDPublicationResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class CUDPublicationHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDPublicationHandler.class.getName());

  public CUDPublicationResponse handle(CUDPublication req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' publication "+req.transientRecord);
    
    CUDPublicationResponse resp = new CUDPublicationResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      pm.makePersistent(req.transientRecord);
      Form form=req.transientRecord.getForm();
      if(form.status==Form.FORM_ST_LOCKED){
        form.status=Form.FORM_ST_PUBLISHED;
        pm.makePersistent(form);
      }
      if(req.disposition.equals(CUDPublication.CREATE) || 
         req.disposition.equals(CUDPublication.UPDATE)){
        resp.persistedRecord=req.transientRecord;
      }
      else if(req.disposition.equals(CUDPublication.DELETE)){
        pm.deletePersistent(req.transientRecord);
        javax.jdo.Query q=pm.newQuery("select count(this) from fx.model.Publication where formId==:fid");
        int c=(Integer)q.execute(form.id);
        if(c==0){
          form.status=Form.FORM_ST_LOCKED;
          pm.makePersistent(form);
          resp.associatedForm=form;
        }
      }
      MyLogger.info("Success");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to perform above operation: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
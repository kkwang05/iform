package fx.service.impl;

import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.operation.design.*;
import fx.server.entity.*;
import fx.server.PMF;

/**
 * Retrieve a list of fully-populated forms. Two convenience utility methods are provided.
 * 
 * @author KW
 */
public class ChangeImageListHandler {
  private static final Logger MyLogger = Logger.getLogger(ChangeImageListHandler.class.getName());

  public ChangeImageListResponse handle(ChangeImageList req) {
    ChangeImageListResponse resp=new ChangeImageListResponse();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      for(String uri: req.imageList){
        String idStr=uri;
        int idx=uri.indexOf("=");
        if(idx>0)
          idStr=uri.substring(0, idx);
        FxBlob bb=pm.getObjectById(FxBlob.class, Long.parseLong(idStr));
        if("D".equals(req.disposition)){
          pm.deletePersistent(bb);
          MyLogger.info("Deleted image block[ "+bb.id+"]");
        }
        else if("S".equals(req.disposition)){
          bb.setUserId(null);
          bb.setDomainId(req.domainId);
          MyLogger.info("Shared image block[ "+bb.id+"]");
        }
        else
          MyLogger.severe("Unrecognized request: '"+req.disposition+"'");
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to change image list: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
}
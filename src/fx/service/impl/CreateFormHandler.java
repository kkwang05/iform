package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.operation.design.CreateForm;
import fx.model.operation.design.CreateFormResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class CreateFormHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CreateFormHandler.class.getName());

  public CreateFormResponse handle(CreateForm req) {
    MyLogger.fine("Received req to create new form " + req.newForm);

    CreateFormResponse resp = new CreateFormResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Date now=new Date();
      req.newForm.createDate=now;
      req.newForm.lastChangeDate=now;
      pm.makePersistent(req.newForm);
      CUDMetaFormHandler.recursiveCreateNode(pm, req.newForm.getBody(), req.newForm.id);
      resp.createdForm=req.newForm;
      MyLogger.info("Successfully created new form ["+req.newForm+"]");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to create new form: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
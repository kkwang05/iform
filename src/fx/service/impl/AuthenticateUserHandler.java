package fx.service.impl;

import java.util.Date;
import java.util.logging.Logger;

import javax.jdo.*;

import fx.model.*;
import fx.model.operation.*;
import fx.model.operation.user.AuthenticateUser;
import fx.model.operation.user.AuthenticateUserResponse;
import fx.server.ServerAppContext;
import fx.server.PMF;
import fx.util.FxCipher;

/**
 * Process user logon request.
 * @author kw
 */
public class AuthenticateUserHandler {
  public static final int TTL=60; //seconds
  
  private static final Logger MyLogger = Logger.getLogger(AuthenticateUserHandler .class.getName());

  public AuthenticateUserResponse handle(AuthenticateUser req) {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.isAccessRequest()){
        MyLogger.info("Received access req: (" + req.voucher + ").");
        GainedAccess resp = new GainedAccess();
        
        String tokenStr = FxCipher.decrypt(req.voucher);
        String[] tokens = tokenStr.split("\\|");
        String uid = tokens[0];
        String expiry = tokens[1];
        long tms=Long.parseLong(expiry);
        if(tms<System.currentTimeMillis()){
          resp.errorMessage="Login voucher expired!";
          MyLogger.severe("LOGIN VOUCHER EXPIRED: " + tokenStr);
          return resp;
        }
        User theUser=(User)pm.getObjectById(User.class, Long.parseLong(uid));
        theUser.setLoggedinDate(new Date());
        pm.makePersistent(theUser);
        resp.user=theUser.dto();
        resp.user.defaultDomain=ServerAppContext.PublicDomain;
        resp.user.templates=ServerAppContext.PublicTemplates;
        MyLogger.info("Successfully gained access to autheticated user");
        return resp;
      }
        
      MyLogger.info("\nReceived original authentication req: (" + "/" + req.email + "/" + req.password+ ").");
      AuthenticateUserResponse resp = new AuthenticateUserResponse();
      Query q = pm.newQuery(User.class, "email==:email && (status==:registeredStatus || status==:activeStatus)");
      q.setUnique(true);
      User match= (User)q.execute(req.email, User.USER_ST_REGISTERED, User.USER_ST_ACTIVATED);
      if(match==null){
        MyLogger.info("No account record was found.");
        resp.errorMessage = "0";
        return resp;
      }
      if (!match.isActive()) {
        MyLogger.fine("PRE-MATURE LOGON.");
        resp.errorMessage = "1";
        return resp;
      }
      resp.userId=match.id;
      if (!match.password.equals(req.password)) {
        MyLogger.fine("INCORRECT PASSWORD.");
        resp.errorMessage = "2";
        return resp;
      }
      String v=resp.userId+"|"+System.currentTimeMillis()+TTL*1000;
      resp.voucher=FxCipher.encrypt(v);
      MyLogger.info("Successfully autheticated user:"+resp.voucher);
      return resp;
      
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to process authentication request: " + ex.getMessage());
    } finally {
      pm.close();
    }
    return null;
  }
  
}
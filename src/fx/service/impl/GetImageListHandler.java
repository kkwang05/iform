package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.operation.design.GetImageList;
import fx.model.operation.design.GetImageListResponse;
import fx.server.entity.*;
import fx.server.ServerAppContext;
import fx.server.PMF;

/**
 * Retrieve a list of fully-populated forms. Two convenience utility methods are provided.
 * 
 * @author KW
 */
public class GetImageListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetImageListHandler.class.getName());

  @SuppressWarnings("unchecked")
  public GetImageListResponse handle(GetImageList req) {
    GetImageListResponse resp=new GetImageListResponse();
    resp.imageList=new ArrayList<String>();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q=null;
      Long param=null;
      List<Object[]> rs=null;
      if(req.createdByUserId!=null){
        q = pm.newQuery(FxBlob.class, "createdByUserId==:userId");
        param=req.createdByUserId;
      }
      else {
        q = pm.newQuery(FxBlob.class, "ownedByDomainId==:domainId && createdByUserId==null");
        param= (req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId;
      }
      q.setResult("id, tags");
      rs = (List<Object[]>) q.execute(param);
      for(Object[] r : rs){
        String imageRec=r[0].toString()+"="+ (r[1]==null? "":(String)r[1]);
        resp.imageList.add(imageRec);
      }
      if(rs!=null)
        MyLogger.fine("Retrieved "+rs.size()+" image records for domain/user="+req.ownedByDomainId+"/"+req.createdByUserId);
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve image list: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
}
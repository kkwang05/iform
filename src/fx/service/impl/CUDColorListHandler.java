package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;
import javax.jdo.PersistenceManager;

import fx.model.operation.design.*;
import fx.server.PMF;

/**
 * @author kw
 */
public class CUDColorListHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDColorListHandler.class.getName());

  public CUDColorListResponse handle(CUDColorList req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' colorList "+req.transientRecord.colorDefinitions);

    CUDColorListResponse resp = new CUDColorListResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.disposition.equals(CUDColorList.DELETE)){
        // Not needed yet
      }
      else {
        pm.makePersistent(req.transientRecord);
        resp.persistedColorList=req.transientRecord;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to CUD ColorList: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
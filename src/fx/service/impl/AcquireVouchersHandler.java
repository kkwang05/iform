package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.*;
import fx.model.operation.design.AcquireVouchers;
import fx.model.operation.design.AcquireVouchersResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class AcquireVouchersHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(AuthorizeEntryHandler.class.getName());

  public AcquireVouchersResponse handle(AcquireVouchers req) {
    MyLogger.fine("Received req to acquire " + req.count+ " vouchers for pubId="+req.ownerId);
    
    AcquireVouchersResponse resp = new AcquireVouchersResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Date expirationDate=null;
      if(req.expirationDays>0){
	long expiryPeriod=req.expirationDays * 24 * 3600 * 1000L;
	expirationDate=new Date();
	expirationDate.setTime(expirationDate.getTime() + expiryPeriod);
      }
      Voucher[] vs=new Voucher[req.count];
      for(int i=0; i<vs.length; i++){
	vs[i]=new Voucher(req.ownerId, req.visitLimit, expirationDate);
        if(req.recipients!=null && i<req.recipients.length)
          vs[i].recipientId=req.recipients[i];
      }
      pm.makePersistentAll(vs);
      MyLogger.info("Successfully created " + req.count+ " vouchers for pubId="+req.ownerId);
      resp.vouchers=vs;
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to perform above operation: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
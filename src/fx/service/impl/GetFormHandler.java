package fx.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.*;
import fx.model.operation.*;
import fx.server.PMF;
import fx.server.entity.FxClob;

/**
 * 
 * @author KW
 */
public class GetFormHandler {
  private static final Logger MyLogger = Logger.getLogger(GetFormHandler.class.getName());

  public GetFormResponse handle(GetForm req) {
    GetFormResponse resp=new GetFormResponse();

    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Form f = (Form)pm.getObjectById(Form.class, req.formId);
      if(!req.headerOnly)
        populateForm(f,pm);
      resp.retrievedForm=f;
      MyLogger.fine("Retrieved form by id="+req.formId);
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve form: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
  
  @SuppressWarnings("unchecked")
  public static void populateForm(Form form, PersistenceManager pm){
    Query q = pm.newQuery(MetaForm.class, "formId==:formId");
    List<MetaForm> nodes= (List<MetaForm>)q.execute(form.id);
    MyLogger.fine("Retrieved "+nodes.size()+ " nodes for form id="+form.id+"["+form.name+"]");
    
    MetaForm root=null;
    HashMap<Long, MetaForm> nodeMap=new HashMap<Long, MetaForm>();
    HashMap<Long, List<MetaForm>> branchMap=new HashMap<Long, List<MetaForm>>();
    for(MetaForm c:nodes){
      nodeMap.put(c.id, c);
      if(c.parentId==null){
        if(c.isRoot())
          root = c;
        else
          MyLogger.severe("Orphaned form node [id="+c.id+ ", typeCode="+c.typeCode+"]");
        continue;
      }
      List<MetaForm> branch=branchMap.get(c.parentId);
      if(branch==null){
        branch = new ArrayList<MetaForm>();
        branchMap.put(c.parentId, branch);
      }
      branch.add(c);
      if(c.textId!=null){
        // Restore full long text from TextBlock
        FxClob tb=pm.getObjectById(FxClob.class, c.textId);
        c.text+=tb.getValue();
      }
      if(c.valueSetId!=null){
        // Retrieve the value set (need to adapt it for transmission)
        c.valueSet=pm.getObjectById(ValueSet.class, c.valueSetId).dto();
      }
    }
    
    for(Map.Entry<Long, List<MetaForm>> e : branchMap.entrySet()){
      MetaForm k=nodeMap.get(e.getKey());
      List<MetaForm> lst=e.getValue();
      Collections.sort(lst);
      k.setChildren(lst);
      for(MetaForm child:lst)
        child.setParent(k);
    }
    form.setBody(root);
  }
}
package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import fx.model.*;
import fx.model.operation.*;
import fx.server.ServerAppContext;
import fx.server.PMF;

/**
 * Retrieve a list of fully-populated forms. Two convenience utility methods are provided.
 * 
 * @author KW
 */
public class GetFormListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetFormListHandler.class.getName());

  @SuppressWarnings("unchecked")
  public GetFormListResponse handle(GetFormList req) {
    GetFormListResponse resp=new GetFormListResponse();
    resp.formList=new ArrayList<Form>();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    Transaction tx=pm.currentTransaction();
    try {
      List<Form> rs=null;
      tx.begin();
      if(req.createdByUserId!=null){
        String qs="createdByUserId==:userId && typeCode==" + req.typeCode;
        if(req.changedSince!=null){
          qs+=" && lastChangeDate>:lcd";
          Query q = pm.newQuery(Form.class, qs);
          q.declareImports("import java.util.Date");
          q.setOrdering("lastChangeDate descending");
          rs = (List<Form>) q.execute(req.createdByUserId, req.changedSince);
          if(rs==null || rs.isEmpty()){
            qs="createdByUserId==:userId && typeCode==" + req.typeCode;
            q = pm.newQuery(Form.class, qs);
            q.declareImports("import java.util.Date");
            q.setOrdering("lastChangeDate descending");
            q.setRange(0,2);
            rs = (List<Form>) q.execute(req.createdByUserId);
          }
        }
        else{
          Query q = pm.newQuery(Form.class, qs);
          q.declareImports("import java.util.Date");
          q.setOrdering("createDate descending");
          rs = (List<Form>) q.execute(req.createdByUserId);
        }
      }
      else {
        String qs="ownedByDomainId==:domainId && typeCode==" + req.typeCode;
        if(req.changedSince!=null){
          qs+=" && lastChangeDate>:lcd";
          Query q = pm.newQuery(Form.class, qs);
          rs = (List<Form>) q.execute((req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId, req.changedSince);
        }
        else{
          Query q = pm.newQuery(Form.class, qs);
          q.declareImports("import java.util.Date");
          q.setOrdering("createDate descending");
          rs = (List<Form>) q.execute((req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId);
        }
      }
      tx.commit();
      MyLogger.fine("Retrieved "+rs.size()+" form(s) of type="+req.typeCode+" for user/domain="+req.createdByUserId+"/"+req.ownedByDomainId);
      for(Form f:rs){
        //GetFormHandler.populateForm(f, pm);
        resp.formList.add(f);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve form list: "+ex.getMessage());
    } finally {
      if (tx.isActive())
        tx.rollback();
      pm.close();
    }
    return resp;
  }
}
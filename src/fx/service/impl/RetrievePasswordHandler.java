package fx.service.impl;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import org.antlr.stringtemplate.StringTemplate;

import fx.model.User;
import fx.model.operation.user.RetrievePassword;
import fx.service.Response;
import fx.server.ActivateUser;
import fx.server.FxUtil;
import fx.server.PMF;
import fx.util.FxCipher;

/**
 * Send password in email on user request.
 * @author kw
 */
public class RetrievePasswordHandler {
  private static final Logger MyLogger = Logger.getLogger(ActivateUser.class.getName());
  private static final ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");

  public Response handle(RetrievePassword req) {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      User usr = (User)pm.getObjectById(User.class, req.userId);

      String subj = Res.getString("retrievePasswordSubject");
      StringTemplate tmpl = FxUtil.getTemplate("email/RetrievePassword");
      tmpl.setAttribute("userName", usr.firstName);
      tmpl.setAttribute("password", FxCipher.decrypt(usr.password));
      
      String msg = tmpl.toString();
      String adminEmail=Res.getString("emailFrom");
      MyLogger.fine("From: "+adminEmail+"\nTo: "+usr.email+"\nSubj="+subj+"\nMsg=\n" + msg);
      FxUtil.sendAdminEmail(usr.email, subj, msg);
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve user or send email: "+ex.getMessage());
    } finally {
      pm.close();
    }
    Response resp = new Response();
    resp.successMessage = Res.getString("retrievePasswordSuccessMessage");
    return resp;
  }
}
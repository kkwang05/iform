package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.*;
import fx.model.operation.*;
import fx.server.PMF;
import fx.util.FxCipher;

/**
 * Processing a form entry request.
 * 
 * @author kw
 */
public class AuthorizeEntryHandler {
  private static ResourceBundle Res = ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(AuthorizeEntryHandler.class.getName());

  public AuthorizeEntryResponse handle(AuthorizeEntry req) {
    MyLogger.fine("Received entry req '" + req.secureAccessToken + "'");
    AuthorizeEntryResponse resp = new AuthorizeEntryResponse();
    Long publicationId = null;
    String voucherIdStr = null;
    try {
      String tok = FxCipher.decrypt(req.secureAccessToken);
      MyLogger.fine("token=" + tok);
      String[] fields = tok.split("\\|");
      String pubId = fields[0].substring("publicationid=".length());
      voucherIdStr = fields[1].substring("voucherid=".length());
      publicationId = Long.valueOf(pubId);
    } catch (Exception ex) {
      resp.errorMessage = Res.getString("SecurityTokenCorrupt");
      return resp;
    }

    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Publication p = pm.getObjectById(Publication.class, publicationId);
      p.lastAccessDate=new Date();
      String validityCheckMessage = checkAccess(p, voucherIdStr, pm);
      if (validityCheckMessage == null) {
        Form f = (Form) pm.getObjectById(Form.class, p.formId);
        GetFormHandler.populateForm(f, pm);
        p.setForm(f);
        FormEntry entry = new FormEntry(p);
        pm.makePersistent(entry);
        if (voucherIdStr != null && !voucherIdStr.trim().equals(""))
          entry.voucherId = Long.valueOf(voucherIdStr);
        resp.formEntry = entry;
        resp.authToken = p.authToken;
        MyLogger.info("Success");
      } else {
        resp.errorMessage = validityCheckMessage;
        MyLogger.info("Retrieved publication denied access: " + validityCheckMessage);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to perform above operation: " + ex.getMessage());
      resp.errorMessage = Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }

  private String checkAccess(Publication publication, String voucherIdStr,
      PersistenceManager pm) {
    if (publication.inactive)
      return Res.getString("publicationDisabled");

    Date today = new Date();
    if (publication.startDate != null && today.before(publication.startDate))
      return Res.getString("publicationHasNotStarted");
    if (publication.endDate != null && today.after(publication.endDate))
      return Res.getString("publicationHasEnded");

    if (publication.entryLimit > 0) {
      Query q = pm.newQuery("select count(this) from fx.model.FormEntry where publicationId==:pubId");
      Integer count = (Integer) q.execute(publication.id);
      MyLogger.info("EntryLimit=" + publication.entryLimit + ", CurrentCount=" + count);
      if (count.intValue() >= publication.entryLimit)
        return Res.getString("publicationReachedEntryLimit");
    }

    if (publication.requiresVoucher) {
      if (voucherIdStr == null || voucherIdStr.trim().equals(""))
        return Res.getString("publicationRequiresVoucher");
      Long voucherId = Long.valueOf(voucherIdStr);
      Voucher voucher = pm.getObjectById(Voucher.class, voucherId);
      if (voucher.expirationDate != null && today.after(voucher.expirationDate))
        return Res.getString("publicationVoucherExpired");
      if (voucher.visitLimit > 0) {
        voucher.incrementVisitCount();
        if (voucher.visitCounter > voucher.visitLimit)
          return Res.getString("publicationVoucherReachedLimit");
        pm.makePersistent(voucher);
      }
    }
    // Good to go; but user may still be challenged with an authtoken
    return null;
  }
}
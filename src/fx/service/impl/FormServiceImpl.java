package fx.service.impl;

import java.lang.reflect.*;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fx.service.*;

public class FormServiceImpl extends RemoteServiceServlet implements FormService {
  public static final long serialVersionUID=1L;
  private static final String ImplPackageName=FormServiceImpl.class.getPackage().getName();
  static {
    fx.server.ServerAppContext.init();
  }
  private static final Logger MyLogger = Logger.getLogger(FormServiceImpl.class.getName());
  public Response execute(Request req) throws Exception {
    if(req instanceof CompositeRequest){
      CompositeRequest cr=(CompositeRequest)req;
      CompositeResponse crsp=new CompositeResponse();
      MyLogger.fine("Received CompositeRequest with "+cr.requestList.size()+" substeps");
      StringBuffer sb=new StringBuffer();
      for(Request r:cr.requestList){
        Response rsp=execute(r);
        crsp.responseList.add(rsp);
        if(rsp.errorMessage!=null)
          sb.append(rsp.errorMessage).append('\n');
      }
      if(sb.length()>0){
        crsp.errorMessage=sb.toString();
        MyLogger.warning("There were errors during exec of substeps: "+crsp.errorMessage);
      }
      return crsp;
    }
    
    String base=req.getClass().getSimpleName();
    String handlerClassName=ImplPackageName+"."+base+"Handler";
    Class<?> handlerClass=Class.forName(handlerClassName);
    Method m=handlerClass.getDeclaredMethod("handle", req.getClass());
    String remoteAddr=getThreadLocalRequest().getRemoteAddr();
    req.remoteAddr=remoteAddr;
    Object r=m.invoke(handlerClass.newInstance(), req);
    return (Response)r;
  }
}
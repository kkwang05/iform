package fx.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

import fx.model.*;
import fx.model.operation.design.GetValueSetList;
import fx.model.operation.design.GetValueSetListResponse;
import fx.server.ServerAppContext;
import fx.server.PMF;

/**
 * Retrieve ValueSet list by either user or domain id.
 * 
 * @author KW
 */
public class GetValueSetListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetValueSetListHandler.class.getName());
  
  @SuppressWarnings("unchecked")
  public GetValueSetListResponse handle(GetValueSetList req) {
    GetValueSetListResponse resp = new GetValueSetListResponse();
    resp.valueSetList=new ArrayList<ValueSet>();

    PersistenceManager pm = PMF.get().getPersistenceManager();
    Transaction tx=pm.currentTransaction();
    try {
      tx.begin();
      List<ValueSet> rs=null;
      Query q = null;
      
      if(req.createdByUserId!=null){
        String qs="createdByUserId==:userId && isInline==" + (req.inline? "true":"false");
        q=pm.newQuery(ValueSet.class, qs);
        rs= (List<ValueSet>)q.execute(req.createdByUserId);
        MyLogger.info("Retrieved "+rs.size()+" ValueSets (qs="+qs+"), uid="+req.createdByUserId);
      } 
      else{
	String qs="ownedByDomainId==:domainId";
        q=pm.newQuery(ValueSet.class, qs);
        rs= (List<ValueSet>)q.execute(req.ownedByDomainId!=null? req.ownedByDomainId:ServerAppContext.PublicDomainId);
        MyLogger.info("Retrieved "+rs.size()+" ValueSets (qs="+qs+"), domainId="+req.ownedByDomainId);
      }
      
      for(ValueSet vs:rs)
        resp.valueSetList.add(vs.dto());
      tx.commit();
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve valueset: "+ex.getMessage());
    } finally {
      if (tx.isActive())
          tx.rollback();
      pm.close();
    }

    return resp;
  }
  
}
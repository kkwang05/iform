package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.*;
import fx.model.operation.design.CUDMetaForm;
import fx.model.operation.design.CUDMetaFormResponse;
import fx.server.PMF;
import fx.server.entity.FxClob;

/**
 * @author kw
 */
public class CUDMetaFormHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(CUDMetaFormHandler.class.getName());

  public CUDMetaFormResponse handle(CUDMetaForm req) {
    MyLogger.fine("Received req to '" + req.disposition+ "' node "+req.nodeInQuestion+"(id="+req.nodeInQuestion.id+")");

    CUDMetaFormResponse resp = new CUDMetaFormResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      if(req.disposition.equals(CUDMetaForm.CREATE)){
      	recursiveCreateNode(pm, req.nodeInQuestion, req.nodeInQuestion.formId);
      	resp.persistedNode=req.nodeInQuestion;
      	MyLogger.fine("Success on creating nodeId="+resp.persistedNode.id);
      }
      else if(req.disposition.equals(CUDMetaForm.UPDATE)){
        recursiveUpdateNode(pm, req.nodeInQuestion);
        resp.persistedNode=req.nodeInQuestion;
        MyLogger.fine("Success on updating nodeId="+resp.persistedNode.id);
      }
      else if(req.disposition.equals(CUDMetaForm.DELETE)){
        recursiveDeleteNode(pm, req.nodeInQuestion);
        MyLogger.fine("Success on deleting nodeId="+req.nodeInQuestion.id);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to create new form: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
  
  static void recursiveCreateNode(PersistenceManager pm, MetaForm node, Long formId) throws Exception {
    if(formId!=null)
      node.formId=formId;
    
    if(node.text!=null && node.text.length()>500){
      String truncatedBlock=node.text.substring(500);
      FxClob tb=new FxClob(node.formId, truncatedBlock);
      pm.makePersistent(tb);
      node.text=node.text.substring(0,500);
      node.textId=tb.id;
      MyLogger.info("Created TextBlock, id="+tb.id);
    }
    if(node.valueSet!=null && node.valueSet.isInline){
      pm.makePersistent(node.valueSet);
      node.valueSetId=node.valueSet.id;
    }
    pm.makePersistent(node);
    List<MetaForm> children=node.getChildren();
    if(children!=null)
      for(MetaForm c: children){
        c.parentId=node.id;
        recursiveCreateNode(pm, c, formId);
      }
  }

  static void recursiveUpdateNode(PersistenceManager pm, MetaForm node) throws Exception {
    if(node.valueSetId!=null && node.valueSet!=null && !node.valueSetId.equals(node.valueSet.id)){
      // Delete old in-line ValueSet
      ValueSet vs=pm.getObjectById(ValueSet.class, node.valueSetId);
      if(vs.isInline)
        pm.deletePersistent(vs);
      else
        MyLogger.severe("Inconsistent node update request: old vsid="+node.valueSetId+", new="+node.valueSet.id);
    }
    if(node.valueSet!=null && node.valueSet.isInline){
      // Create/Update in-line ValueSet
      pm.makePersistent(node.valueSet);
    }
    if(node.valueSet!=null)
      node.valueSetId=node.valueSet.id;
    
    if(node.textId!=null){ 
      //Update TextBlock
      FxClob tb=pm.getObjectById(FxClob.class, node.textId);
      if(node.text!=null && node.text.length()>500){
        String truncatedBlock=node.text.substring(500);
        tb.setValue(truncatedBlock);
        node.text=node.text.substring(0,500);
        MyLogger.info("Updated TextBlock, id="+tb.id);
      }
      else{
        pm.deletePersistent(tb);
        node.textId=null;
        MyLogger.info("Deleted TextBlock, id="+tb.id);
      }
    }
    if(node.text!=null && node.text.length()>500){
      //Create new TextBlock
      String truncatedBlock=node.text.substring(500);
      FxClob tb=new FxClob(node.formId, truncatedBlock);
      pm.makePersistent(tb);
      node.text=node.text.substring(0,500);
      node.textId=tb.id;
      MyLogger.info("Created TextBlock, id="+tb.id);
    }
    pm.makePersistent(node);
  }
  
  static void recursiveDeleteNode(PersistenceManager pm, MetaForm node) throws Exception {
    if(node.getChildren()!=null)
      for(MetaForm c: node.getChildren())
        recursiveDeleteNode(pm, c);
    // Delete assoicated TextBlocks, if any
    if (node.textId!= null){
      FxClob tb=pm.getObjectById(FxClob.class, node.textId);
      pm.deletePersistent(tb);
      node.text="";	// This is critical for the subsequent node delete
      MyLogger.info("Deleted TextBlock, id="+tb.id);
    }
    // Delete assoicated inline ValueSet, if any
    ValueSet vs = node.valueSet;
    if (vs != null && vs.isInline) {
      pm.makePersistent(vs);
      pm.deletePersistent(vs);
    }
    pm.makePersistent(node);
    pm.deletePersistent(node);
  }
}
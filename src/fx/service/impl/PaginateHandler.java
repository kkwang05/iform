package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;

import fx.model.MetaForm;
import fx.model.operation.design.Paginate;
import fx.model.operation.design.PaginateResponse;
import fx.server.PMF;

/**
 * @author kw
 */
public class PaginateHandler {
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(MergeNodesHandler.class.getName());

  public PaginateResponse handle(Paginate req) {
    MyLogger.fine("Received req to convert root into page on rootId="+req.root.id);
    
    PaginateResponse resp = new PaginateResponse();
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      resp.persistedRoot=MetaForm.createRoot(req.root.formId);
      pm.makePersistent(resp.persistedRoot);
      MyLogger.info("Created new rootId=" + resp.persistedRoot.id);
      
      req.root.typeCode=MetaForm.TC_PAGE;
      resp.persistedRoot.addChild(req.root);
      pm.makePersistent(req.root);
      MyLogger.info("Converted old root to page");
      
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to merge nodes under new formlet: "+ex.getMessage());
      resp.errorMessage=Res.getString("genericError");
    } finally {
      pm.close();
    }
    return resp;
  }
}
package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.ColorList;
import fx.model.operation.design.GetColorList;
import fx.model.operation.design.GetColorListResponse;
import fx.server.ServerAppContext;
import fx.server.PMF;

/**
 * Retrieve a list of fully-populated forms. Two convenience utility methods are provided.
 * 
 * @author KW
 */
public class GetColorListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetColorListHandler.class.getName());

  @SuppressWarnings("unchecked")
  public GetColorListResponse handle(GetColorList req) {
    GetColorListResponse resp=new GetColorListResponse();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q=null;
      Long param=null;
      if(req.createdByUserId!=null){
        q = pm.newQuery(ColorList.class, "createdByUserId==:userId");
        param=req.createdByUserId;
      }
      else {
        q = pm.newQuery(ColorList.class, "ownedByDomainId==:domainId && createdByUserId==null");
        param= (req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId;
      }
      
      List<ColorList> rs = (List<ColorList>) q.execute(param);
      if(rs!=null && rs.size()>0){
        MyLogger.fine("Retrieved "+rs.size()+" color records for domain/user="+req.ownedByDomainId+"/"+req.createdByUserId);
        resp.colorList=rs.get(0);
      }
      else{
        // PlaceHolder - system default color palette
        resp.colorList=defaultColorList();
      }
        
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve ColorList: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
  
  private ColorList defaultColorList(){
    ColorList cl=new ColorList();
    cl.colorDefinitions="None,red,green,blue,cyan,#0d0d0d,#505050,#a0a0a0,#f0f0f0,";
    return cl;
  }
}
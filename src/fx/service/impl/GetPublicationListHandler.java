package fx.service.impl;

import java.util.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import fx.model.*;
import fx.model.operation.design.GetPublicationList;
import fx.model.operation.design.GetPublicationListResponse;
import fx.server.ServerAppContext;
import fx.server.PMF;

/**
 * Retrieve a list of fully-populated forms. Two convenience utility methods are provided.
 * 
 * @author KW
 */
public class GetPublicationListHandler {
  private static final Logger MyLogger = Logger.getLogger(GetPublicationListHandler.class.getName());

  @SuppressWarnings("unchecked")
  public GetPublicationListResponse handle(GetPublicationList req) {
    GetPublicationListResponse resp=new GetPublicationListResponse();
    resp.pubList=new ArrayList<Publication>();
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q=null;
      List<Publication> rs=null;
      if(req.createdByUserId!=null){
        if(req.accessedSince!=null){
          q = pm.newQuery(Publication.class, "createdByUserId==:userId && lastAccessDate>:lad");
          rs = (List<Publication>) q.execute(req.createdByUserId, req.accessedSince);
        }
        else{
          q = pm.newQuery(Publication.class, "createdByUserId==:userId");
          q.declareImports("import java.util.Date");
          q.setOrdering("publishingDate descending");
          rs = (List<Publication>) q.execute(req.createdByUserId);
        }
      }
      else {
        if(req.accessedSince!=null){
          q = pm.newQuery(Publication.class, "ownedByDomainId==:domainId && lastAccessDate>:lad");
          rs = (List<Publication>) q.execute((req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId, req.accessedSince);
        }
        else{
          q = pm.newQuery(Publication.class, "ownedByDomainId==:domainId");
          q.declareImports("import java.util.Date");
          q.setOrdering("publishingDate descending");
          rs = (List<Publication>) q.execute((req.ownedByDomainId != null) ? req.ownedByDomainId : ServerAppContext.PublicDomainId);
        }
      }
      for(Publication p:rs){
        Form f = (Form)pm.getObjectById(Form.class, p.formId);
        p.setForm(f);
        resp.pubList.add(p);
      }
      if(rs!=null)
      MyLogger.fine("Retrieved "+rs.size()+" publication(s) for user/domain="+req.ownedByDomainId+"/"+req.createdByUserId);
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve publication list: "+ex.getMessage());
    } finally {
      pm.close();
    }
    return resp;
  }
}
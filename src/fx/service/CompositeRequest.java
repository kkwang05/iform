package fx.service;

import java.util.*;

public class CompositeRequest extends Request {
  public static final long serialVersionUID=1L;

  public ArrayList<Request> requestList=new ArrayList<Request>();
  public void addRequest(Request req){
    requestList.add(req);
  }
}

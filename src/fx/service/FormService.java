package fx.service;

import com.google.gwt.user.client.rpc.RemoteService;


public interface FormService extends RemoteService {
  public Response execute(Request req) throws Exception;
}
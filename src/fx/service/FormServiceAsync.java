package fx.service;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface FormServiceAsync {
  public <T extends Response> void execute(Request req, AsyncCallback<T> cb);
}
package fx.service;

import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

/**
 * Client side utility for conducting form service calls.
 * 
 * @author kw
 */
public class RPC {
  
  public static final RPC Instance=new RPC();
  private static HashMap<Request, Response> cache=new HashMap<Request, Response>();
  
  private final FormServiceAsync service;
  private RPC(){
    service = GWT.create(FormService.class);
    ServiceDefTarget endpoint=(ServiceDefTarget) service;
    endpoint.setServiceEntryPoint("/_fxrpc");
  }

  @SuppressWarnings("unchecked")
  public <T extends Response> void exec(Operation<T> op){
    if(op.req.isCacheable() && !op.invalidateCache){
      Response cachedResponse=cache.get(op.req);
      if(cachedResponse!=null){
        op.onSuccess((T)cachedResponse);
        return;
      }
    }
    service.execute(op.req, op);
  }

  //
  // A simple sequential execution queue based on the single-thread browser environment.
  //
  private List<Operation<? extends Response>> queue=new ArrayList<Operation<? extends Response>>();
  private transient boolean busy;
  private PostOperation chainAction=new PostOperation(){
    public void perform(){
      workOffQueue();
    }
  };
  public void sequentialExec(Operation<? extends Response> myOp){
    queue.add(myOp);
    if(!busy)
      workOffQueue();
  }
  private void workOffQueue(){
    if(queue.size()==0){
      busy=false;
      return;
    }
    busy=true;
    Operation<? extends Response> op=queue.remove(0);
    exec(op.postFailure(chainAction).postSuccess(chainAction));
  }

  /**
   * Base call-back class that supports caching and post operations.
   */
  public static class Operation<T extends Response> implements AsyncCallback<T> {
    public boolean invalidateCache;
    
    private Request req;
    protected T resp;
    protected Throwable error;
    
    public Operation(Request req){
      this.req=req;
    }
    public Operation(Request req, boolean invalidateCache){
      this.req=req;
      this.invalidateCache=invalidateCache;
    }
    
    private transient PostOperation postFailure, postSuccess;
    public Operation<T> postFailure(PostOperation postFailure){
      this.postFailure=postFailure;
      return this;
    }
    public Operation<T> postSuccess(PostOperation postSuccess){
      this.postSuccess=postSuccess;
      return this;
    }
    
    public void onFailure(Throwable oops) {
      resp=null;
      error=oops;
      oops.printStackTrace();
      processFailure(oops);
      if(postFailure!=null)
        postFailure.perform();
    }
    
    public void onSuccess(T response) {
      resp=response;
      if(req.isCacheable() && !resp.equals(cache.get(req)))
        cache.put(req,resp);
      processSuccess(response);
      if(postSuccess!=null)
        postSuccess.perform();
    }
    
    // Subclasses with PostOperations must override these methods to ensure correct execution order.
    // These include all Operations that need be executed via RPC.sequentialExec().
    protected void processFailure(Throwable oops){}
    protected void processSuccess(T response){}
  }

  /**
   * For injecting post-RPC actions to make sure of updated data.
   */
  public static class PostOperation {
    public void perform(){}
  }
}
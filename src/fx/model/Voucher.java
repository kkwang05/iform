package fx.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class Voucher implements java.io.Serializable {
  public static final long serialVersionUID=1L;
  
  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent  
  public Long ownerId;	// entity this voucher is issued for

  @Persistent  
  public String recipientId;	// Name, email, etc. this voucher is issued to

  @Persistent  
  public java.util.Date expirationDate;

  @Persistent  
  public int visitLimit=1;
  @Persistent  
  public int visitCounter;
  
  public Voucher(){}
  
  public Voucher(Long ownerId, int limit, java.util.Date expiration){
    this.ownerId=ownerId;
    this.visitLimit=limit;
    this.expirationDate=expiration;
  }
  public void incrementVisitCount(){
    visitCounter++;
  }
}
package fx.model;

import javax.jdo.annotations.*;

import fx.model.bo.BusinessObject;

@PersistenceCapable
public class FormData implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent  
  public Long formEntryId;

  @Persistent  
  public Long formNodeId;

  @Persistent  
  public String value;

  @Persistent  
  public boolean hidden;

  public transient MetaForm node;
  public transient String text;
  public transient String renderingHints;
  public transient String settings;
  public transient BusinessObject bo;
  public transient boolean bound;

  public FormData(){
  }
  
  public FormData(Long entryId, MetaForm node){
    this.formEntryId=entryId;
    this.formNodeId=node.id;
  }

  public FormData(Long entryId, MetaForm node, String value){
    this(entryId, node);
    this.value=value;
  }
}
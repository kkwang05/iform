package fx.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * @author kw
 */
@PersistenceCapable
public class ColorList implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;
  
  @Persistent  
  public Long createdByUserId;

  @Persistent  
  public Long ownedByDomainId;

  @Persistent  
  public String tags;

  @Persistent  
  public String colorDefinitions;

  public ColorList(){
  }
  
  public void setUserId(Long userId){
    this.createdByUserId=userId;
  }

  public void setDomainId(Long domainId){
    this.ownedByDomainId=domainId;
  }
  
  public boolean equas(Object rhs){
    return (rhs instanceof ColorList && 
        colorDefinitions.equals(((ColorList)rhs).colorDefinitions));
  }
}
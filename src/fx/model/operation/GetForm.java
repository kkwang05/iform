package fx.model.operation;

import fx.service.Request;

public class GetForm extends Request {
  public static final long serialVersionUID=1L;

  public Long formId;
  public boolean headerOnly=false;
  
  public GetForm(){}
  public GetForm(Long id){
    formId=id;
  }
  
  public boolean isCacheable(){
    return false; //true;
  }

  public int hashCode(){
    return formId.intValue();
  }
  
  public boolean equals(Object rhs){
    if(this==rhs)
      return true;
    if(!(rhs instanceof GetForm))
      return false;
    GetForm rhsReq=(GetForm)rhs;
    return (formId.equals(rhsReq.formId) && (headerOnly==rhsReq.headerOnly));
  }
  
  public String toString(){
    return "GetForm[+"+formId+"]";
  }
}
package fx.model.operation;

import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations on Publication. 
 *
 * @author kw
 */
public class AuthorizeEntry extends Request {
  public static final long serialVersionUID=1L;

  public String secureAccessToken;
  public AuthorizeEntry(){}
  public AuthorizeEntry(String ticket){
    this.secureAccessToken=ticket;
  }
}
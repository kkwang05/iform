package fx.model.operation;

import fx.model.Form;
import fx.service.Response;

public class GetFormListResponse extends Response {
  public static final long serialVersionUID=1L;
  public java.util.List<Form> formList;
}
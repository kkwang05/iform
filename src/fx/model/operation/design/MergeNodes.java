package fx.model.operation.design;

import fx.model.*;
import fx.service.Request;

/**
 * @author kw
 */
public class MergeNodes extends Request {
  public static final long serialVersionUID=1L;

  public MergeNodes(){}
  
  public Long parentNodeId;
  public float cardinalAdjustment;
  public MetaForm subroot;
  public Long[] nodeIds;
  public MergeNodes(MetaForm firstNode, Long[] nodeIds){
    this.subroot=firstNode;
    this.nodeIds=nodeIds;
  }
  public MergeNodes(Long parentNodeId, Long[] nodeIds){
    this.parentNodeId=parentNodeId;
    this.nodeIds=nodeIds;
  }
}
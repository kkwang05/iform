package fx.model.operation.design;

import fx.model.*;
import fx.model.operation.CRUDConstants;
import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations on Publication. 
 *
 * @author kw
 */
public class CUDPublication extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDPublication create(Publication publication){
    CUDPublication req=new CUDPublication(publication, CREATE);
    
    return req;
  }
  public static CUDPublication update(Publication publication){
    CUDPublication req=new CUDPublication(publication, UPDATE);
    return req;
  }
  public static CUDPublication delete(Publication publication){
    CUDPublication req=new CUDPublication(publication, DELETE);
    return req;
  }
  
  public String disposition;
  public Publication transientRecord;
  
  public CUDPublication(){}
  
  private CUDPublication(Publication publication, String disposition){
    this.disposition=disposition;
    this.transientRecord=publication;
  }
}
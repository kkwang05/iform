package fx.model.operation.design;

import fx.model.Voucher;
import fx.service.Response;

public class AcquireVouchersResponse extends Response {
  public static final long serialVersionUID=1L;

  public Voucher[] vouchers;
}
package fx.model.operation.design;

import fx.service.Request;

public class DeleteForm extends Request {
  public static final long serialVersionUID=1L;

  public Long formId;
  
  public DeleteForm(){}
  public DeleteForm(Long id){
    formId=id;
  }
}
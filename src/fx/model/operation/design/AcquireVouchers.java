package fx.model.operation.design;

import fx.service.Request;

public class AcquireVouchers extends Request {
  public static final long serialVersionUID=1L;

  public Long ownerId;
  public int visitLimit=1;
  public int expirationDays;

  public int count;
  public String[] recipients;
  
  public AcquireVouchers(){
  }
  
  public AcquireVouchers(Long ownerId, int visitLimit, int longevity){
    this.ownerId=ownerId;
    this.visitLimit=visitLimit;
    this.expirationDays=longevity;
    this.count=1;
  }
}
package fx.model.operation.design;

import fx.service.Response;
import fx.model.ColorList;

public class GetColorListResponse extends Response {
  public static final long serialVersionUID=1L;
  public ColorList colorList;
}
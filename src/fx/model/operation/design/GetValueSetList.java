package fx.model.operation.design;

import fx.service.Request;

public class GetValueSetList extends Request {
  public static final long serialVersionUID=1L;
  
  public Long createdByUserId;
  public boolean inline;
  public Long ownedByDomainId;
}
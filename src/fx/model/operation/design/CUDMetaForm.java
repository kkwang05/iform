package fx.model.operation.design;

import fx.model.*;
import fx.model.operation.CRUDConstants;
import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations around a MetaForm node. 
 *
 * @author kw
 */
public class CUDMetaForm extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDMetaForm create(MetaForm node){
    CUDMetaForm req=new CUDMetaForm(node, CREATE);
    
    return req;
  }
  public static CUDMetaForm update(MetaForm node){
    CUDMetaForm req=new CUDMetaForm(node, UPDATE);
    return req;
  }
  public static CUDMetaForm delete(MetaForm node){
    CUDMetaForm req=new CUDMetaForm(node, DELETE);
    return req;
  }
  
  public String disposition;
  public MetaForm nodeInQuestion;
  
  private CUDMetaForm(){}
  
  private CUDMetaForm(MetaForm node, String disposition){
    this.disposition=disposition;
    this.nodeInQuestion=node;
  }
}
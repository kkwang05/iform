package fx.model.operation.design;

import fx.service.Response;

public class GetImageListResponse extends Response {
  public static final long serialVersionUID=1L;
  public java.util.List<String> imageList;
}
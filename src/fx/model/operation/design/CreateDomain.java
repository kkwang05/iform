package fx.model.operation.design;

import fx.service.Request;
import fx.model.Domain;

public class CreateDomain extends Request {
  public static final long serialVersionUID=1L;

  public Long userId;
  public Domain newDomain;
}
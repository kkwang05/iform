package fx.model.operation.design;

import fx.model.*;
import fx.model.operation.CRUDConstants;
import fx.service.Request;

/**
 * @author kw
 */
public class CUDValueSet extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDValueSet create(ValueSet vs){
    CUDValueSet req=new CUDValueSet(vs, CREATE);
    
    return req;
  }
  public static CUDValueSet update(ValueSet vs){
    CUDValueSet req=new CUDValueSet(vs, UPDATE);
    return req;
  }
  public static CUDValueSet delete(ValueSet vs){
    CUDValueSet req=new CUDValueSet(vs, DELETE);
    return req;
  }
  
  public String disposition;
  public ValueSet vsInQuestion;
  
  private CUDValueSet(){}
  
  private CUDValueSet(ValueSet vs, String disposition){
    this.disposition=disposition;
    this.vsInQuestion=vs;
  }
}
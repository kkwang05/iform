package fx.model.operation.design;

import fx.model.*;
import fx.model.operation.CRUDConstants;
import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations on Publication. 
 *
 * @author kw
 */
public class CUDColorList extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDColorList create(ColorList cl){
    CUDColorList req=new CUDColorList(cl, CREATE);
    
    return req;
  }
  public static CUDColorList update(ColorList cl){
    CUDColorList req=new CUDColorList(cl, UPDATE);
    return req;
  }
  public static CUDColorList delete(ColorList cl){
    CUDColorList req=new CUDColorList(cl, DELETE);
    return req;
  }
  
  public String disposition;
  public ColorList transientRecord;
  
  public CUDColorList(){}
  
  private CUDColorList(ColorList cl, String disposition){
    this.disposition=disposition;
    this.transientRecord=cl;
  }
}
package fx.model.operation.design;

import fx.service.Response;
import fx.model.MetaForm;

public class MergeNodesResponse extends Response {
  public static final long serialVersionUID=1L;
  
  public MetaForm persistedNode;
}
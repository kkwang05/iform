package fx.model.operation.design;

import fx.model.*;
import fx.service.Response;

public class GetValueSetListResponse extends Response {
  public static final long serialVersionUID=1L;
  public java.util.List<ValueSet> valueSetList;
}
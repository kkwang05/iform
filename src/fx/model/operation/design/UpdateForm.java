package fx.model.operation.design;

import fx.model.*;
import fx.service.Request;

public class UpdateForm extends Request {
  public static final long serialVersionUID=1L;

  public Form changedForm;
  
  public UpdateForm(){}
  
  public UpdateForm(Form changedForm){
    this.changedForm=changedForm;
  }
}
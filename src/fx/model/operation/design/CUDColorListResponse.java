package fx.model.operation.design;

import fx.service.Response;
import fx.model.*;

public class CUDColorListResponse extends Response {
  public static final long serialVersionUID=1L;
  public ColorList persistedColorList;
}
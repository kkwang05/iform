package fx.model.operation.design;

import fx.service.Response;
import fx.model.*;

public class CUDPublicationResponse extends Response {
  public static final long serialVersionUID=1L;
  
  // This field is set only when requesting deletion AND there has been a state change 
  // in the associated Form.
  public Form associatedForm;
  public Publication persistedRecord;
}
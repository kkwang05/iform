package fx.model.operation.design;

import fx.model.Form;
import fx.service.Response;

public class CreateFormResponse extends Response {
  public static final long serialVersionUID=1L;

  public Form createdForm;
}
package fx.model.operation.design;

import fx.model.*;
import fx.service.Request;

/**
 * @author kw
 */
public class Paginate extends Request {
  public static final long serialVersionUID=1L;

  public MetaForm root;
  
  public Paginate(){}
  public Paginate(MetaForm root){
    this.root=root;
  }
}
package fx.model.operation.design;

import fx.model.*;
import fx.service.Request;

public class CreateForm extends Request {
  public static final long serialVersionUID=1L;

  public Form newForm;
  
  public CreateForm(){}
  
  public CreateForm(Form newForm){
    this.newForm=newForm;
  }
}
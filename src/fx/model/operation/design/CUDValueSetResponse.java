package fx.model.operation.design;

import fx.service.Response;
import fx.model.ValueSet;

public class CUDValueSetResponse extends Response {
  public static final long serialVersionUID=1L;
  
  public ValueSet persistedValueSet;
}
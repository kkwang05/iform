package fx.model.operation.design;

import fx.service.Request;

public class ChangeImageList extends Request {
  public static final long serialVersionUID=1L;
  
  public java.util.List<String> imageList;
  public String disposition;
  public Long domainId;
  
  public ChangeImageList(){}
  public ChangeImageList(String disposition){
    this.disposition=disposition;
  }
}
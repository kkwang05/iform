package fx.model.operation.design;

import fx.service.Request;

public class GetImageList extends Request {
  public static final long serialVersionUID=1L;
  
  public Long createdByUserId;
  public Long ownedByDomainId;
  public String searchString;
  
  public boolean isCacheable(){
    return false; //true;
  }

  public int hashCode(){
    int hc=0;
    if(createdByUserId!=null)
      hc=createdByUserId.intValue();
    if(ownedByDomainId!=null)
      hc ^=ownedByDomainId.intValue();
    return hc;
  }
  
  public boolean equals(Object rhs){
    if(this==rhs)
      return true;
    if(!(rhs instanceof GetImageList))
      return false;
    GetImageList rhsReq=(GetImageList)rhs;
    boolean t1=createdByUserId==rhsReq.createdByUserId || createdByUserId.equals(rhsReq.createdByUserId);
    boolean t2=ownedByDomainId==rhsReq.ownedByDomainId || ownedByDomainId.equals(rhsReq.ownedByDomainId);
    return t1 && t2;
  }
  
  
  public GetImageList(){}
  
  public GetImageList(Long domainId, Long userId){
    ownedByDomainId=domainId;
    createdByUserId=userId;
  }
}
package fx.model.operation.design;

import fx.model.Publication;
import fx.service.Response;

public class GetPublicationListResponse extends Response {
  public static final long serialVersionUID=1L;
  public java.util.List<Publication> pubList;
}
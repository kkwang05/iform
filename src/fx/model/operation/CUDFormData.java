package fx.model.operation;

import fx.model.*;
import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations on FormData. 
 *
 * @author kw
 */
public class CUDFormData extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDFormData createOrUpdate(FormData fd){
    String disposition=(fd.id==null)? CREATE:UPDATE;
    CUDFormData req=new CUDFormData(fd, disposition);
    return req;
  }
  
  public String disposition;
  public FormData transientRecord;
  
  public CUDFormData(){}
  
  private CUDFormData(FormData fd, String disposition){
    this.disposition=disposition;
    this.transientRecord=fd;
  }
}
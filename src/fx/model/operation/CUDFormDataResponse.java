package fx.model.operation;

import fx.service.Response;
import fx.model.*;

public class CUDFormDataResponse extends Response {
  public static final long serialVersionUID=1L;
  
  public FormData persistedRecord;
}
package fx.model.operation.user;

import fx.service.Response;

public class AuthenticateUserResponse extends Response {
  public static final long serialVersionUID=1L;

  public Long userId;
  public String voucher;
}
package fx.model.operation.user;

import fx.service.Request;

public class RegisterUser extends Request {
  public static final long serialVersionUID=1L;

  public String firstName;
  public String lastName;
  public String email;
  public String encryptedPassword;
}
package fx.model.operation.user;

import fx.service.Request;

public class RetrievePassword extends Request {
  public static final long serialVersionUID=1L;

  public Long userId;
  public String firstName;
  public String lastName;
  public String nickName;
  public String passphrase;
}
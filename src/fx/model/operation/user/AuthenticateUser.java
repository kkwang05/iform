package fx.model.operation.user;

import fx.service.Request;

public class AuthenticateUser extends Request {
  public static final long serialVersionUID=1L;

  public String voucher;
  public String email;
  public String password;
  
  public AuthenticateUser(){}
  public AuthenticateUser(String voucher, String email, String password){
    this.voucher=voucher;
    this.email=email;
    this.password=password;
  }
  
  public boolean isAccessRequest(){
    return (email==null && password==null && voucher!=null);
  }
}
package fx.model.operation;

import fx.service.Response;
import fx.model.*;

public class AuthorizeEntryResponse extends Response {
  public static final long serialVersionUID=1L;
  
  public FormEntry formEntry;
  public String authToken;
}
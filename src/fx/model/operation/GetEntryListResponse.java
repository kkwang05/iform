package fx.model.operation;

import fx.model.FormEntry;
import fx.service.Response;

public class GetEntryListResponse extends Response {
  public static final long serialVersionUID=1L;
  public java.util.List<FormEntry> entryList;
}
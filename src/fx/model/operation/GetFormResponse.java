package fx.model.operation;

import fx.model.Form;
import fx.service.Response;

public class GetFormResponse extends Response {
  public static final long serialVersionUID=1L;

  public Form retrievedForm;
}
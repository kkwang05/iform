package fx.model.operation;

import java.util.Date;

import fx.service.Request;

public class GetFormList extends Request {
  public static final long serialVersionUID=1L;
  
  public Long createdByUserId;
  public Long ownedByDomainId;
  public short typeCode;
  public Date changedSince;

}
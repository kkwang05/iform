package fx.model.operation;

import fx.model.*;
import fx.service.Request;

/**
 * Aggregate Create/Update/Delete operations on FormEntry. 
 *
 * @author kw
 */
public class CUDFormEntry extends Request implements CRUDConstants {
  public static final long serialVersionUID=1L;

  public static CUDFormEntry createOrUpdate(FormEntry entry){
    String disposition=(entry.id==null)? CREATE:UPDATE;
    CUDFormEntry req=new CUDFormEntry(entry, disposition);
    return req;
  }
  public static CUDFormEntry delete(FormEntry entry){
    CUDFormEntry req=new CUDFormEntry(entry, DELETE);
    return req;
  }
  
  public String disposition;
  public FormEntry transientRecord;
  
  public CUDFormEntry(){}
  
  private CUDFormEntry(FormEntry entry, String disposition){
    this.disposition=disposition;
    this.transientRecord=entry.dto();
  }
}
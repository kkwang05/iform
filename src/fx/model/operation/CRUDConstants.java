package fx.model.operation;

public interface CRUDConstants {
  public static final String CREATE="C";
  public static final String RETRIEVE="R";
  public static final String UPDATE="U";
  public static final String DELETE="D";
}

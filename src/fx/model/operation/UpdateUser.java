package fx.model.operation;

import fx.service.Request;
import fx.model.User;

public class UpdateUser extends Request {
  public static final long serialVersionUID=1L;
  
  public User user;
  public UpdateUser(){}
  
  public UpdateUser(User user){
    this.user=user;
  }
}
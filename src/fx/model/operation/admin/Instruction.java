package fx.model.operation.admin;

import fx.service.Request;

public class Instruction extends Request {
  public static final long serialVersionUID=1L;

  public String statement;
  public Instruction(){}

  public Instruction(String s){this.statement=s;}
  
}
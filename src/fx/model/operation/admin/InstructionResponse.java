package fx.model.operation.admin;

import fx.service.Response;

public class InstructionResponse extends Response {
  public static final long serialVersionUID=1L;

  public String outcome;
  public void setOutcome(String out){
    outcome=out;
  }
}
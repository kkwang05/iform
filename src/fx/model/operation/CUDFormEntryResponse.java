package fx.model.operation;

import fx.service.Response;
import fx.model.*;

public class CUDFormEntryResponse extends Response {
  public static final long serialVersionUID=1L;
  
  public FormEntry persistedRecord;
}
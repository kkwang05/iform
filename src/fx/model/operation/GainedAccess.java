package fx.model.operation;

import fx.model.User;
import fx.model.operation.user.AuthenticateUserResponse;

public class GainedAccess extends AuthenticateUserResponse {
  public static final long serialVersionUID=1L;

  public User user;
}
package fx.model;

import java.util.*;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class User implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  /* user status codes */
  public static final short USER_ST_TRANSIENT=0;
  public static final short USER_ST_REGISTERED=1;
  public static final short USER_ST_ACTIVATED=2;
  public static final short USER_ST_DEACTIVATED=9;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent
  public short status;

  @Persistent  
  public Date createDate;

  @Persistent  
  public Date loggedinDate;
  
  @Persistent  
  public String firstName;
  @Persistent
  public String lastName;
  
  @Persistent
  public String passPhrase;

  @Persistent
  public String email;
  @Persistent
  public String password;
  
  @Persistent
  public List<Long> domainIds=new ArrayList<Long>();

  @NotPersistent  
  public Date lastLoggedinDate;
  
  @NotPersistent  
  public Domain defaultDomain;

  @NotPersistent  
  public List<Form> templates;
  
  public void setStatus(short st){
    status=st;
  }
  
  public void setLoggedinDate(Date loggedinDate){
    this.lastLoggedinDate=this.loggedinDate;
    this.loggedinDate=loggedinDate;
  }
  
  public void setFirstName(String firstName){
    this.firstName=firstName;
  }
  public void setLastName(String lastName){
    this.lastName=lastName;
  }

  public void setPassphrase(String passPhrase){
    this.passPhrase=passPhrase;
  }

  public void setEmail(String email){
    this.email=email;
  }
  public void setPassword(String password){
    this.password=password;
  }
  
  public void addDomainId(Long domainId){
    domainIds.add(domainId);
  }

  public boolean isActive(){
    return status==USER_ST_ACTIVATED;
  }

  public User dto(){
    User dto=new User();
    dto.id=id;
    dto.status=status;
    dto.createDate=createDate;
    dto.loggedinDate=loggedinDate;
    dto.lastLoggedinDate=lastLoggedinDate;
    dto.email=email;
    dto.password=password;
    dto.firstName=firstName;
    dto.lastName=lastName;
    dto.passPhrase=passPhrase;
    dto.domainIds=new ArrayList<Long>();
    for(Long id:domainIds)
      dto.domainIds.add(id);
    dto.defaultDomain=defaultDomain;
    return dto;
  }
  
  public String toString(){
    return "[ id="+id+"|status="+status+"|firstName="+firstName+"|email="+email+"|password="+password+"]";
  }
}
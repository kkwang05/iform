package fx.model;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class Domain implements java.io.Serializable {
  public static final long serialVersionUID=1L;
  
  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent  
  public Long createdByUserId;
  
  @Persistent  
  public String name;
  
  @Persistent  
  public String desc;

  public Domain(){
  }
  
  public Domain(String name, String desc){
    this.name=name;
    this.desc=desc;
  }
  
  public boolean isPublic(){
    return name.equalsIgnoreCase("PUBLIC");
  }
}
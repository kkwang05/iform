package fx.model;

import java.util.*;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * Represents a form, annotated container of form nodes.
 *
 * @author kw
 */
@PersistenceCapable
public class Form implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  /* form status codes */
  public static final short FORM_ST_NEW=0;
  public static final short FORM_ST_EDITING=1;
  public static final short FORM_ST_LOCKED=2;
  public static final short FORM_ST_PUBLISHED=3;

  /* template types */
  public static final short TC_REGULAR=0;
  public static final short TC_TOOL_TEMPLATE=1;
  public static final short TC_FORMLET=2;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent  
  public short status;
  @Persistent  
  public short typeCode;

  @Persistent  
  public Long createdByUserId;
  @Persistent  
  public Long ownedByDomainId;
  
  @Persistent  
  public String name;
  @Persistent  
  public String desc;
  @Persistent  
  public Date createDate;
  @Persistent  
  public Date lastChangeDate;

  @NotPersistent  
  private MetaForm root;

  public Form(){
    status=FORM_ST_EDITING;
    createDate=new Date();
  }

  public Form(String name, String desc){
    this();
    this.name=name;
    this.desc=desc;
  }
  
  public MetaForm getBody(){
    return root;
  }
  public void setBody(MetaForm root){
    this.root=root;
  }
  
  public boolean isReadOnly(){
    return status>=Form.FORM_ST_LOCKED;
  }
  public boolean isPublished(){
    return status>=Form.FORM_ST_PUBLISHED;
  }
  
  public String getStatusText(){
    if(status==FORM_ST_EDITING)
      return "EDITING";
    else if(status==FORM_ST_LOCKED)
      return "LOCKED";
    else if(status==FORM_ST_PUBLISHED)
      return "PUBLISHED";
    else
      return "Unknown";
  }
  
  public boolean equals(Object rhs){
    if(this==rhs)
      return true;
    if(!(rhs instanceof Form))
      return false;
    Form rhsForm=(Form)rhs;
    return (id!=null && id.equals(rhsForm.id));
  }
  
  public String toString(){
    return name;
  } 
  
  /**
   * Assemble and return a list of all the pages contained in this form. Return null if not paginated yet.
   */
  public List<MetaForm> pages(){
    if(root==null || root.getChildren().isEmpty() || !root.getChildren().get(0).isPage())
      return null;
    List<MetaForm> pages=new ArrayList<MetaForm>();
    pages.addAll(root.getChildren());
    return pages;
  }
  
  /**
   * Assemble and return a docked view of this form. Return null if not paginated yet.
   */
  public DockView dockView(){
    int n=root.getChildren().size();
    if(!(n>0 && root.getChildren().get(0).isPage()))
      return null;
    
    DockView dv=new DockView();
    MetaForm x=null;
    int i=0;
    for(; i<3 && i<n; i++){
      x=root.getChildren().get(i);
      if(x.isDockedPage())
        dv.s0.add(x);
      else {
        dv.p=x;
        break;
      }
    }
    if(++i<n){
      if((x=root.getChildren().get(i)).isDockedPage()){
        dv.s1.add(x);
        if(++i<n)
          if((x=root.getChildren().get(i)).isDockedPage()){
            dv.s1.add(x);
            if(++i<n)
              dv.p2=root.getChildren().get(i);
          }
          else
            dv.p2=x;
      }
      else
        dv.p2=x;
    }
    return dv;
  }
  
  /**
   * Represents the "framed" layout of a form that consists of up to 4 DockedPages, 1 center page, and the next page waiting in line.
   * This logical view is composed from the natural order of pages, of which the first 5 spots have special significance: DockedPages
   * can only be placed in the 0th, 1th, 3th, and 4th position.
   */
  public static class DockView {
    public List<MetaForm> s0=new ArrayList<MetaForm>(2);
    public List<MetaForm> s1=new ArrayList<MetaForm>(2);
    public MetaForm p, p2;
    
    public int insertionPoint(){
      int n0=s0.size();
      if(n0==0)
        return 0;
      else if(n0==1){
        if(s0.get(0).rowMarker==1)
          return 0;
        else
          return 1;
      }
      int n1=s1.size();
      if(n1==0)
        return 3;
      else if (n1==1){
        if(s1.get(0).rowMarker==2)
          return 4;
        else
          return 3;
      }
      return -1;
    }
    
    public boolean needShiftWhenCut(){
      if(p2==null)
        return false;
      return !s1.isEmpty() || p.rowMarker!=0;
    }
    public boolean needShiftWhenPaste(){
      return !s1.isEmpty() || p.rowMarker!=0;
    }
    
    public boolean isCenterPage(MetaForm aPage){
      return aPage.equals(p);
    }
    
    public boolean hasDockingRoom(){
      return s0.size()<2 || s1.size()<2;
    }
    
    public boolean isDockingEmpty(){
      return s0.isEmpty() && s1.isEmpty();
    }
  }
}
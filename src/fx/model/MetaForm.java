package fx.model;

import java.util.*;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * Represents any form node with the following components: [*typeCode, *renderingHints, text, textId, valueSetId].
 *
 * @author kw
 */
@PersistenceCapable
public class MetaForm implements java.io.Serializable, Comparable<MetaForm> {
  public static final long serialVersionUID=1L;
  
  public static final short TC_ROOT=0;
  public static final short TC_PAGE=3;
  public static final short TC_DOCKED_PAGE=4;
  public static final short TC_COMPOSITE=5;
  
  public static final short TC_HTML_BLOCK=10;
  public static final short TC_TEXT_BLOCK=11;
  public static final short TC_IMG_BLOCK=20;
  
  public static final short TC_TEXT_FIELD=30;
  public static final short TC_TEXT_BOX=31;
  
  public static final short TC_H_RADIO_BUTTON=40;
  public static final short TC_V_RADIO_BUTTON=41;
  public static final short TC_LIST_BOX=42;
  public static final short TC_H_CHECK_BOX=50;
  public static final short TC_V_CHECK_BOX=51;
  
  public static final short TC_ACTION_LINK=60;
  public static final short TC_ACTION_BUTTON=70;
  public static final short TC_PAGE_TURNER=80;
  public static final short TC_PAGE_BROWSER=90;

  public static final short TC_HTML5_CANVAS=100;
  public static final short TC_AUDIO=110;
  public static final short TC_VIDEO=115;

  public static final short TC_OBSERVER=1000;
  public static final short TC_GRADER=1001;
  public static final short TC_SHOPPING_CART=1002;

  public static final short TC_CONTACT=1010;
  
  //
  // Convenience static creation methods
  //
  public static MetaForm createRoot(Long formId){
    return createSubroot(formId, null, TC_ROOT);
  }
  public static MetaForm createPage(Long formId, Long parentId){
    return createSubroot(formId, parentId, TC_PAGE);
  }
  public static MetaForm createComposite(Long formId, Long parentId){
    return createSubroot(formId, parentId, TC_COMPOSITE);
  }
  private static MetaForm createSubroot(Long formId, Long parentId, short type){
    MetaForm subroot=new MetaForm(type, null, null);
    subroot.formId=formId;
    subroot.parentId=parentId;
    return subroot;
  }
  public static MetaForm createImage(String id, boolean preferThumbnail){
    String path=preferThumbnail? "/image/T":"/image/";
    MetaForm mf=new MetaForm(MetaForm.TC_IMG_BLOCK, null, path+id);
    return mf;
  }

  //
  // Persistent instance fields
  //

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent  
  public String name;

  @Persistent  
  public String script;
  
  @Persistent  
  public Long formId;

  @Persistent  
  public Long parentId;

  @Persistent  
  public float cardinal;
  @Persistent  
  public int rowMarker;
  
  @Persistent  
  public short typeCode;
  
  @Persistent  
  public String renderingHints;

  // Non-presentation oriented business state/settings; maybe encoded.
  @Persistent  
  public String settings;

  // This field is "half-persistent". Its content is synthesized on server side when text block is present
  @Persistent  
  public String text;

  // Client side should never touch this field.
  @Persistent  
  public Long textId;
  
  @Persistent  
  public Long valueSetId;
  
  //
  // Serialized, but not persisted fields
  //
  @NotPersistent  
  public ValueSet valueSet;
  
  @NotPersistent  
  private MetaForm parent;
  
  @NotPersistent  
  private List<MetaForm> children;
  
  public MetaForm(){
  }

  public MetaForm(short typeCode, String hints, String text){
    this.text=text;
    this.typeCode=typeCode;
    this.renderingHints=hints;
    if(isParent())
      children=new ArrayList<MetaForm>();
  }
  
  public void setSettings(String settings){
    this.settings=settings;
  }
  
  public String getNodeId(){
    if(name!=null)
      return name;
    return "n"+id;
  }
  
  //
  // Convenience interrogative methods 
  //
  public boolean isParent(){
    return (typeCode<=TC_COMPOSITE);
  }  
  public boolean isRoot(){
    return (typeCode==TC_ROOT);
  }
  public boolean isDockedPage(){
    return (typeCode==TC_DOCKED_PAGE);
  }
  public boolean isPage(){
    return (typeCode==TC_PAGE || typeCode==TC_DOCKED_PAGE);
  }
  public boolean residesInPage(){
    return (myPage()!=null);
  }
  public MetaForm myPage(){
    MetaForm node=this;
    while(node!=null && !node.isPage())
      node=node.getParent();
    return node;
  }
  public int positionInRow(){
    int i=0, idx=parent.getChildren().indexOf(this);
    while(idx>=0 && parent.getChildren().get(idx).rowMarker!=1){
      if(rowMarker!=2 && parent.getChildren().get(idx).rowMarker==2)
        return -1;
      idx--;
      i++;
    }
    if(idx>=0)
      return i;
    else
      return -1;
  }
  public boolean isInRow(){
    if(rowMarker!=0)
      return true;
    int idx=parent.getChildren().indexOf(this);
    while(--idx>=0){
      int rm=parent.getChildren().get(idx).rowMarker;
      if(rm==1)
        return true;
      else if(rm==2)
        return false;
    }
    return false;
  }
  
  public boolean isComposite(){
    return (typeCode==TC_COMPOSITE);
  }
  public boolean isImage(){
    return (typeCode==TC_IMG_BLOCK);
  }
  public boolean isMorphable(){
    return (typeCode==TC_HTML_BLOCK || typeCode==TC_TEXT_BLOCK || 
        typeCode>=TC_TEXT_FIELD && typeCode<=TC_V_CHECK_BOX);
  }
  public boolean hasText(){
    return (typeCode==TC_HTML_BLOCK || typeCode==TC_TEXT_BLOCK || typeCode==TC_COMPOSITE || typeCode>=TC_TEXT_FIELD);
  }
  public boolean isDomainObject(){
    return (typeCode>=TC_OBSERVER);
  }
  
  //
  // Dangerous operation
  //
  public void syncUp(MetaForm persistent){
    id=persistent.id;
    parentId=persistent.parentId;
    if(valueSet!=null && valueSet.id==null){
      valueSet.id=persistent.valueSetId;
      valueSetId=persistent.valueSetId;
    }
    textId=persistent.textId;
    
    // Adopt his children properly - bug fix
    if(persistent.children!=null && children!=null){
      children.clear();
      for(MetaForm c: persistent.children)
        addChild(c);
    }
  }
  public MetaForm clone(){
    MetaForm clone=new MetaForm(typeCode, renderingHints, text);
    clone.script=script;
    clone.settings=settings;
    clone.rowMarker=rowMarker;
    if(valueSet!=null && valueSet.isInline){
      ValueSet vsCopy=new ValueSet(valueSet);
      vsCopy.id=null;
      clone.valueSet=vsCopy;
    }
    else {
      clone.valueSet=valueSet;
      clone.valueSetId=valueSetId;
    }
    if(getChildren()!=null)
      for(MetaForm c: getChildren())
        clone.addChild(c.clone());
    return clone;
  }
  
  public void enlargeImage(){
    assert isImage():"Wrong call";
    text=text.replaceFirst("/image/T", "/image/");
  }
  public MetaForm enlargedImage(){
    assert isImage():"Wrong call";
    return new MetaForm(MetaForm.TC_IMG_BLOCK, null, text.replaceFirst("/image/T", "/image/"));
  }
  
  public int compareTo(MetaForm to){
    return (cardinal<to.cardinal)? -1:
      (cardinal>to.cardinal)? +1 : 0;
  }

  public boolean equals(Object rhs){
    if(this==rhs)
      return true;
    if(!(rhs instanceof MetaForm))
      return false;
    MetaForm r=(MetaForm)rhs;
    if(id!=null && id.equals(r.id))
      return true;
    return false;
  }
  public String toString(){
    return "InternalNode<"+id+","+typeCode+","+cardinal+","+renderingHints+">";
  }
  
  public void setParentId(Long parentId){
    this.parentId=parentId;
  }
  
  public void setCardinal(float cardinal){
    this.cardinal=cardinal;
  }
  
  public MetaForm getParent(){
    return parent;
  }
  public void setParent(MetaForm par){
    this.parent=par;
  }

  public List<MetaForm> getChildren(){
    return children;
  }

  public void setChildren(List<MetaForm> children){
    this.children=children;
  }
  
  //
  // Form Editing Support
  //
  public void addChild(MetaForm c){
    c.formId=formId;
    c.parentId=id;
    c.parent=this;
    
    if(children==null)
      children=new ArrayList<MetaForm>();
    children.add(c);
    adjustCardinal(c, children.size()-1);
  }
  public void insertChild(MetaForm c, int idx){
    c.formId=formId;
    c.parentId=id;
    c.parent=this;
    
    if(children==null)
      children=new ArrayList<MetaForm>();
    children.add(idx, c);
    adjustCardinal(c, idx);
  }
  public void moveChild(MetaForm c, int idx){
    if(children.indexOf(c)==idx)
      return;
    children.remove(c);
    children.add(idx, c);
    adjustCardinal(c, idx);
  }
  private void adjustCardinal(MetaForm c, int idx){
    float lower= (idx==0)? 0 : children.get(idx-1).cardinal;
    float upper= (idx==children.size()-1)? lower+200 : children.get(idx+1).cardinal;
    c.cardinal=(lower+upper)/2.0f;
  }

  public void removeFromParent(){
    parent.children.remove(this);
    this.parent=null;
  }
  public void replace(MetaForm c1, MetaForm c2){
    children.remove(c1);
    c1.formId=c2.formId;
    c1.parentId=c2.parentId;
    c1.rowMarker=c2.rowMarker;
    c1.cardinal=c2.cardinal;
    c1.parent=c2.parent;
    
    int idx=children.indexOf(c2);
    children.add(idx, c1);
    children.remove(c2);
  }

  public boolean isOnlyChild(){
    return parent.children.size()==1;
  }
  public MetaForm nextSibling(){
    int idx=parent.children.indexOf(this);
    if(idx<parent.children.size()-1)
      return parent.children.get(idx+1);
    else
      return null;
  }
  public MetaForm previousSibling(){
    int idx=parent.children.indexOf(this);
    if(idx>0)
      return parent.children.get(idx-1);
    else
      return null;
  }
  public List<MetaForm> undockedPages(){
    if(getChildren().isEmpty() || !getChildren().get(0).isPage())
      return null;
    List<MetaForm> pages=new ArrayList<MetaForm>();
    for(MetaForm c: getChildren())
      if(c.isPage() && !c.isDockedPage())
        pages.add(c);
    return pages;
  }
  public int preInsertionPoint(){
    int idx=parent.getChildren().indexOf(this);
    int i=(rowMarker==2)? idx-1:idx;
    while(i>=0 && parent.getChildren().get(i).rowMarker!=2 && parent.getChildren().get(i).rowMarker!=1)
      --i;
    return (i<0 || parent.getChildren().get(i).rowMarker==2)? idx:i;
  }
}
package fx.model;

import java.util.Date;
import javax.jdo.annotations.*;

@PersistenceCapable
public class Publication implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;
  
  @Persistent  
  public Long formId;
  
  @Persistent  
  public int entryCount;
  
  @Persistent  
  public Long createdByUserId;
  @Persistent  
  public Long ownedByDomainId;

  @Persistent  
  public String name;
  
  @Persistent  
  public String desc;

  @Persistent  
  public boolean inactive;
  @Persistent 
  public boolean isListed;
  @Persistent  
  public Date publishingDate;
  @Persistent  
  public Date lastAccessDate;
  @Persistent  
  public Date startDate;
  @Persistent  
  public Date endDate;
  @Persistent  
  public int entryLimit;
  @Persistent  
  public boolean requiresVoucher;
  @Persistent  
  public String voucherType;
  @Persistent  
  public int voucherExpiration;

  @Persistent
  public String authToken;
  
  @NotPersistent  
  private Form form;
  
  public Publication(){
    inactive=true;
    publishingDate=new Date();
  }

  public Form getForm(){
    return form;
  }
  public void setForm(Form form){
    this.form=form;
    if(desc==null)
      desc="Publication of form \""+form.name+"\"";
  }
  public void incrementEntryCount(){
    entryCount++;
  }
  
  public boolean isActive(){
    if(inactive)
      return false;
    Date today=new Date();
    if(startDate!=null && today.before(startDate))
      return false;
    if(endDate!=null && today.after(endDate))
      return false;
    return true;
  }

  public boolean equals(Object rhs){
    if(this==rhs)
      return true;
    if(!(rhs instanceof Publication))
      return false;
    Publication rhsPub=(Publication)rhs;
    return (id!=null && id.equals(rhsPub.id));
  }
  
  public String toString(){
    return name;
  }
}
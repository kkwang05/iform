package fx.model;

import java.util.Collection;
import java.util.Date;
import javax.jdo.annotations.*;

@PersistenceCapable
public class FormEntry implements java.io.Serializable {
  public static final long serialVersionUID=1L;
  
  /* form entry status codes */
  public static final short ENTRY_ST_NEW=0;
  public static final short ENTRY_ST_EDITING=1;
  public static final short ENTRY_ST_SUSPENDED=2;
  public static final short ENTRY_ST_COMPLETED=3;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;
  
  @Persistent  
  public short status;

  @Persistent  
  public Long publicationId;
  @Persistent  
  public Long formId;
  @Persistent  
  public Long voucherId;
  
  @Persistent  
  public String tracker;	// remote IP
  
  @Persistent  
  public Date startDate;
  @Persistent  
  public Date lastChangeDate;

  @NotPersistent  
  public Form theForm;

  @NotPersistent  
  public Voucher voucher;

  @NotPersistent  
  public Collection<FormData> data;
  
  // For form preview. Not to be persisted on server.
  public FormEntry(Form form){
    theForm=form;
    formId=form.id;
  }

  // Publication must be fully populated (as in the FormConnApp).
  public FormEntry(Publication publication){
    publicationId=publication.id;
    formId=publication.formId;
    theForm=publication.getForm();
  }
  
  private FormEntry(){}

  public FormEntry dto(){
    FormEntry clone=new FormEntry();
    clone.id=id;
    clone.status=status;
    clone.publicationId=publicationId;
    clone.formId=formId;
    clone.voucherId=voucherId;
    clone.startDate=startDate;
    clone.lastChangeDate=lastChangeDate;
    return clone;
  }
  
  public void start(){
    startDate=new Date();
    status=ENTRY_ST_EDITING;
  }
  public void submit(){
    lastChangeDate=new Date();
    status=ENTRY_ST_COMPLETED;
  }
  
  public boolean isReadOnly(){
    return (status==ENTRY_ST_COMPLETED);
  }

  public boolean isSimulated(){
    return (publicationId==null);
  }
}
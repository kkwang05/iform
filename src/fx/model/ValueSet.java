package fx.model;

import java.util.*;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable
public class ValueSet implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;

  @Persistent
  public Long createdByUserId;
  @Persistent
  public Long ownedByDomainId;

  @Persistent
  public String name;
  @Persistent
  public String desc;
  @Persistent
  public ArrayList<String> values;
  @Persistent
  public boolean isInline=true;

  public ValueSet(){
    values=new ArrayList<String>();
  }
  public ValueSet(String name, String... el){
    this.name=name;
    values=new ArrayList<String>(el.length);
    for(String s:el)
      values.add(s);
  }
  
  public ValueSet(ValueSet from){
    this.id=from.id;
    this.createdByUserId=from.createdByUserId;
    this.ownedByDomainId=from.ownedByDomainId;
    this.name=from.name;
    this.desc=from.desc;
    this.isInline=from.isInline;
    this.values=new ArrayList<String>(from.values.size());
    for(String v:from.values)
      this.values.add(v);
  }
  
  public ValueSet dto(){
    ValueSet dto=new ValueSet(this);
    return dto;
  }
  
  public boolean equals(Object rhs){
    if(rhs==null)
      return false;
    
    assert (rhs instanceof ValueSet);
    ValueSet rhsvs=(ValueSet)rhs;
    if(!values.equals(rhsvs.values))
      return false;
    if(isInline!=rhsvs.isInline)
      return false;
    if(name!=null && !name.equals(rhsvs.name))
      return false;
    if(name==null && name!=rhsvs.name)
      return false;
    if(desc!=null && !desc.equals(rhsvs.desc))
      return false;
    if(desc==null && desc!=rhsvs.desc)
      return false;
    
    return true;
  }
  
  public String toString(){
    boolean singleLine=true;
    String terminator=singleLine? "":"\n";
    StringBuffer sb=new StringBuffer();
    
    if(name!=null)
      sb.append(name).append(":").append(terminator);
    if(desc!=null) 
      sb.append(desc).append(terminator);
    sb.append("{").append(terminator);
    for(String s:values)
      sb.append(s+", ");
    sb.delete(sb.length()-2,sb.length());
    sb.append(terminator);
    sb.append("}");
    return sb.toString();
  }
  
}
package fx.model.bo;

import java.util.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 */
public class InfoSheet implements BusinessObject {
  private static final String[] Ops={".set"};
  private static final List<String> OpsList=Arrays.asList(Ops);
  
  protected FormEntrySessionContext context;
  protected FormData dynaNode;

  public String[] propertyList; 
  
  protected Map<String, Object> info=new HashMap<String, Object>(); 

  public InfoSheet(FormEntrySessionContext context, FormData dynaNode){
    this.context=context;
    this.dynaNode=dynaNode;
    propertyList=dynaNode.settings.split(",");
  }
  
  public FormData attachedNode(){
    return dynaNode;
  }
  
  public boolean supports(String act){
    return OpsList.contains(act);
  }
  
  public void actuate(String act, List<Object> params){
    if(act.equals(Ops[0]))
      set(params);
  }
  
  protected void set(List<Object> params){
    String name=(String)params.get(0);
    Object val=(Object)params.get(1);
    info.put(name,val);
  }

  public Object getProperty(String propertyName){
    return info.get(propertyName);
  }
  
  public String infoXml(){
    return null;
  }
  
  public void hibernate(){
  }
}
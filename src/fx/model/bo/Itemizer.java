package fx.model.bo;

import java.util.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 * Rudimentary shopping cart. There will be two modes of this BO - ShoppingCart and ShoppingCartDisclosure.
 */
public class Itemizer implements BusinessObject {
  private static final String[] Ops={".add", ".remove"};
  private static final List<String> OpsList=Arrays.asList(Ops);
  
  protected FormEntrySessionContext context;
  protected FormData dynaNode;
  
  protected List<MetaForm> itemList=new ArrayList<MetaForm>();
  protected Map<MetaForm, Number> weights=new HashMap<MetaForm, Number>();
  
  public Itemizer(FormEntrySessionContext context, FormData dynaNode){
    this.context=context;
    this.dynaNode=dynaNode;
  }
  
  public FormData attachedNode(){
    return dynaNode;
  }
  
  public boolean supports(String act){
    return OpsList.contains(act);
  }
  
  public void actuate(String act, List<Object> params){
    MetaForm node=(MetaForm)params.get(0);
    if(act.equals(Ops[0]))
      add(node, params);
    else if(act.equals(Ops[1]))
      remove(node);
  }

  protected void add(MetaForm node, List<Object> params){
    if(!itemList.contains(node))
      itemList.add(node);
    Number weight=params.size()>1? (Number)params.get(1):null;
    weights.put(node,weight);
  }
  protected void remove(MetaForm node){
    itemList.remove(node);
    weights.remove(node);
  }
  
  public List<MetaForm> items(){
    return itemList;
  }

  public Map<MetaForm, Number> weights(){
    return weights;
  }

  public String infoXml(){
    return null;
  }
  
  public void hibernate(){
  }
}
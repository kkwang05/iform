package fx.model.bo;

import java.util.*;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 * Score keeper/Grader for test forms.
 */
public class Grader extends Itemizer {
  protected Map<MetaForm, String> answers=new HashMap<MetaForm, String>();
  
  public Grader(FormEntrySessionContext context, FormData dynaNode){
    super(context, dynaNode);
  }
  
  protected void add(MetaForm node, List<Object> params){
    super.add(node, params);
    String answer=params.size()>2? (String)params.get(2):null;
    answers.put(node,answer);
  }
  
  public Map<MetaForm, String> answers(){
    return answers;
  }
}
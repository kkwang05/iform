package fx.model.bo;

import fx.model.*;
import fx.presentment.FormEntrySessionContext;

/**
 * Rudimentary shopping cart. There will be two modes of this BO - ShoppingCart and ShoppingCartDisclosure.
 */
public class ShoppingCart extends Itemizer {
  public ShoppingCart(FormEntrySessionContext context, FormData dynaNode){
    super(context, dynaNode);
  }
}
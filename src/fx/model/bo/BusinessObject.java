package fx.model.bo;

import java.util.*;

import fx.model.FormData;

/**
 * Session transient business logic object for corresponding node types that supports scripted operations.
 * It maintains a separate in-memory state but may also change the dynamic node state and even the persistent
 * record (through node.save()).
 */
public interface BusinessObject {
  boolean supports(String act);
  void actuate(String act, List<Object> params);
  String infoXml();
  void hibernate();
  FormData attachedNode();
}
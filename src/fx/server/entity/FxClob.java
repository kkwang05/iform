package fx.server.entity;

import com.google.appengine.api.datastore.Text;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * This is a server-side component with GAE dependency used to store large textual contents(>500 characters).
 *
 * @author kw
 */
@PersistenceCapable
public class FxClob implements java.io.Serializable {
  public static final long serialVersionUID=1L;
  
  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;
  
  @Persistent  
  public Long ownerId;

  @Persistent  
  public Text text;

  public FxClob(){
  }

  public FxClob(Long ownerId, String value){
    this.ownerId=ownerId;
    setValue(value);
  }

  public void setText(Text text){
    this.text=text;
  }
  
  public Text getText(){
    return text;
  }
  
  public void setValue(String txt){
    text=new Text(txt);
  }
  
  public String getValue(){
    return text.getValue();
  }
}
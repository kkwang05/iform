package fx.server.entity;

import com.google.appengine.api.datastore.Blob;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * @author kw
 */
@PersistenceCapable
public class FxBlob implements java.io.Serializable {
  public static final long serialVersionUID=1L;

  public static final short TC_IMAGE=0;

  @PrimaryKey
  @Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
  public Long id;
  
  @Persistent  
  public Long createdByUserId;

  @Persistent  
  public Long ownedByDomainId;

  @Persistent  
  public String tags;

  @Persistent  
  public short typeCode;
  
  @Persistent  
  public String mimeType;

  @Persistent  
  public long size;
  
  @Persistent  
  public Blob blob;

  @Persistent  
  public Blob thumbnailBlob;
  
  public FxBlob(){
  }
  
  public void setUserId(Long userId){
    this.createdByUserId=userId;
  }
  public void setDomainId(Long domainId){
    this.ownedByDomainId=domainId;
  }
  
  public FxBlob(Long domainId, Long userId, byte[] image, byte[] thumbnail){
    this.ownedByDomainId=domainId;
    this.createdByUserId=userId;
    blob=new Blob(image);
    if(thumbnail!=null)
      thumbnailBlob=new Blob(thumbnail);
  }
  
  public byte[] getData(){
    return blob.getBytes();
  }

  public byte[] getThumbnailData(){
    if(thumbnailBlob!=null)
      return thumbnailBlob.getBytes();
    else
      return null;
  }

}
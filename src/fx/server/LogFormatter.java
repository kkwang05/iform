package fx.server;

import java.util.Date;
import java.text.*;
import java.util.logging.*;

public class LogFormatter extends Formatter {
  private static DateFormat df=new SimpleDateFormat("MM-dd HH:mm:ss");
  public String format(LogRecord rec) {
    Date d=new Date(rec.getMillis());
    int tid=rec.getThreadID();
    Level lvl=rec.getLevel();
    String src=rec.getSourceClassName()+"."+rec.getSourceMethodName();
    //String logger=rec.getLoggerName();
    String msg=rec.getMessage(), realMsg=msg.trim();
    int idx=msg.indexOf(realMsg);
    
    StringBuffer buf = new StringBuffer(1000);
    if(idx>0){
      buf.append(msg.substring(0,idx));
      rec.setMessage(realMsg);
    }
    buf.append(df.format(d)).append(" T").append(tid).append(' ').append(lvl).append(" [");
    buf.append(src).append("] ").append(formatMessage(rec)).append('\n');
    return buf.toString();
  }
}
package fx.server;

import java.util.*;
import java.util.logging.*;
import javax.jdo.*;

import fx.model.*;
/**
 * Seed data objects.
 * 
 * @author kw
 */
public class ServerAppContext {
  private static final Logger MyLogger = Logger.getLogger(ServerAppContext.class.getName());
  
  public static Domain PublicDomain;
  public static Long PublicDomainId;
  public static ArrayList<Form> PublicTemplates=new ArrayList<Form>();
  
  private static ValueSet mc;
  
  public static void init(){
    initiatePublicDomain();
    
    // Seed data for testing
    initiatePublicValueSets();
    initiatePublicToolkits();
  }
  
  private static void initiatePublicDomain() {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    Transaction tx = pm.currentTransaction();
    try {
      tx.begin();
      Query q = pm.newQuery(Domain.class, "name==:name && createdByUserId==:creatorId");
      q.setUnique(true);
      PublicDomain= (Domain)q.execute("PUBLIC", null);
      
      if(PublicDomain==null){
        PublicDomain=new Domain("PUBLIC", "Default domain for everybody");
        pm.makePersistent(PublicDomain);
      }
      tx.commit();
      MyLogger.fine("PUBLIC domain id: "+PublicDomain.id);
      PublicDomainId=PublicDomain.id;
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to retrieve PUBLIC domain: "+ex.getMessage());
    } finally {
      if (tx.isActive())
        tx.rollback();
      pm.close();
    }
  }

  private static void initiatePublicValueSets() {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q = pm.newQuery("select count(this) from fx.model.ValueSet where ownedByDomainId==:domainId");
      Integer count=(Integer)q.execute(PublicDomainId);
      if(count!=null && count.intValue()>0){
        MyLogger.fine("Retrieved "+count.intValue()+" public valueset(s)");
        return;
      }
      
      ValueSet vs = new ValueSet("binary1", "Yes", "No");
      vs.isInline = false;
      vs.ownedByDomainId=PublicDomainId;
      pm.makePersistent(vs);
      
      vs = new ValueSet("binary2", "True", "False");
      vs.isInline = false;
      vs.ownedByDomainId=PublicDomainId;
      pm.makePersistent(vs);

      vs=new ValueSet("mc", "A","B","C");
      vs.isInline=false;
      vs.ownedByDomainId=PublicDomainId;
      pm.makePersistent(vs);
      mc=vs;
      
      vs = new ValueSet("months", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Ful", "Aug", "Sep", "Oct", "Nov", "Dec");
      vs.isInline = false;
      vs.ownedByDomainId=PublicDomainId;
      pm.makePersistent(vs);

      vs = new ValueSet("weekDays", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
      vs.isInline = false;
      vs.ownedByDomainId=PublicDomainId;
      pm.makePersistent(vs);
      
      MyLogger.fine("Seeded public valuesets");
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to init public valuesets: "+ex.getMessage());
    } finally {
      pm.close();
    }
  }

  @SuppressWarnings("unchecked")
  private static void initiatePublicToolkits() {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    try {
      Query q = pm.newQuery(Form.class, "ownedByDomainId==:domainId && typeCode==1");
      List<Form> templates=(List<Form>)q.execute(PublicDomainId);
      if(templates.size()>0){
        MyLogger.fine("Retrieved "+templates.size()+" public toolkit(s)");
        PublicTemplates.clear();
        for(Form f:templates){
          fx.service.impl.GetFormHandler.populateForm(f, pm);
          PublicTemplates.add(f);
        }
        return;
      }

      MyLogger.info("Started seeding public toolkit 'Basic'");
      Form toolkit1=new Form("UI Constructs","Standard primitive node types");
      toolkit1.ownedByDomainId=PublicDomainId;
      toolkit1.typeCode=Form.TC_TOOL_TEMPLATE;
      pm.makePersistent(toolkit1);
      MyLogger.fine("Created container form");
      
      MetaForm mf, r2=new MetaForm(MetaForm.TC_ROOT, null, null);
      r2.formId=toolkit1.id;
      pm.makePersistent(r2);
      
      r2.addChild(new MetaForm(MetaForm.TC_PAGE_BROWSER,null,null));
      String li="<font size=\"4\"><span style=\"font-weight: bold;\">&nbsp;L</span></font>orem <span style=\"font-style: italic;\">ipsum</span>";
      r2.addChild(mf=new MetaForm(MetaForm.TC_HTML_BLOCK, null, li));
      mf.rowMarker=1;
      r2.addChild(mf=new MetaForm(MetaForm.TC_TEXT_BLOCK, null, "Lorem ipsum"));
      mf.rowMarker=2;
      r2.addChild(mf=new MetaForm(MetaForm.TC_TEXT_FIELD, null,null));
      r2.addChild(mf=create(MetaForm.TC_V_RADIO_BUTTON, mc));
      mf.rowMarker=1;
      r2.addChild(create(MetaForm.TC_LIST_BOX, mc));
      r2.addChild(mf=create(MetaForm.TC_V_CHECK_BOX, mc));
      mf.rowMarker=2;
      r2.addChild(mf=create(MetaForm.TC_H_RADIO_BUTTON, mc));
      mf.rowMarker=1;
      r2.addChild(mf=create(MetaForm.TC_H_CHECK_BOX, mc));
      mf.rowMarker=2;
      r2.addChild(mf=new MetaForm(MetaForm.TC_ACTION_BUTTON, null, "Submit"));
      mf.rowMarker=1;
      r2.addChild(mf=new MetaForm(MetaForm.TC_ACTION_LINK, null, "ClickMe!"));
      mf.rowMarker=2;
      r2.addChild(new MetaForm(MetaForm.TC_PAGE_TURNER, null, null));
      
      for(MetaForm child:r2.getChildren()){
        pm.makePersistent(child);
        MyLogger.fine("Created child node "+child);
      }

      MyLogger.info("Seeding public toolkit 'Media Types'");
      Form toolkit2=new Form("Media Types", "Drawing, Audio, and Video");
      toolkit2.ownedByDomainId=PublicDomainId;
      toolkit2.typeCode=Form.TC_TOOL_TEMPLATE;
      pm.makePersistent(toolkit2);
      
      MetaForm r=new MetaForm(MetaForm.TC_ROOT, null, null);
      r.formId=toolkit2.id;
      pm.makePersistent(r);

      r.addChild(new MetaForm(MetaForm.TC_HTML5_CANVAS, "width=40px;height=30px;horzAlign=Center;", "Drawing Canvas"));
      for(MetaForm child:r.getChildren()){
          pm.makePersistent(child);
          MyLogger.fine("Created child node "+child);
      }

      MyLogger.info("Seeding public toolkit 'Domain Objects'");
      Form toolkit3=new Form("Domain Objects", "Business domain specific node types");
      toolkit3.ownedByDomainId=PublicDomainId;
      toolkit3.typeCode=Form.TC_TOOL_TEMPLATE;
      pm.makePersistent(toolkit3);
      
      r=new MetaForm(MetaForm.TC_ROOT, null, null);
      r.formId=toolkit3.id;
      pm.makePersistent(r);
      
      r.addChild(new MetaForm(MetaForm.TC_GRADER, "icon=grader;padding=5.0px;", "Your Score"));
      r.addChild(new MetaForm(MetaForm.TC_SHOPPING_CART, "icon=shoppingCart;padding=5.0px;", "Shopping Cart"));
      r.addChild(new MetaForm(MetaForm.TC_OBSERVER, "icon=observer;padding=5.0px;", "View Details"));
      r.addChild(new MetaForm(MetaForm.TC_CONTACT, "icon=infoSheet;padding=5.0px;", "User Info"));
      
      for(MetaForm child:r.getChildren()){
        pm.makePersistent(child);
        MyLogger.fine("Created child node "+child);
      }
      
      MyLogger.fine("Seeded all public toolkits");
      initiatePublicToolkits();
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.log(Level.SEVERE, "Failed to init public toolkits: ", ex);
    } finally {
      pm.close();
    }
  }
  private static MetaForm create(short typeCode, ValueSet vs){
    MetaForm node=new MetaForm();
    node.typeCode=typeCode;
    if (vs != null) {
      node.valueSetId = vs.id;
      node.valueSet = vs;
    }
    return node;
  }
}
package fx.server;

import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.*;
import javax.mail.internet.*;

import org.antlr.stringtemplate.*;

public class FxUtil {
  private static final String DFLT_GRP_ID = "Default";
  private static final String TEMPLATE_ROOT = "template/";
  private static ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");

  public static StringTemplate getTemplate(String templateId) {
    StringTemplateGroup group = new StringTemplateGroup(DFLT_GRP_ID);
    return group.getInstanceOf(TEMPLATE_ROOT + templateId);
  }

  public static void sendAdminEmail(String emailAddr, String subj, String body) {
    Properties props = new Properties();
    Session session = Session.getDefaultInstance(props, null);
    String from=Res.getString("emailFrom");
    sendEmail(session, from, emailAddr, subj, body, true);
  }

  protected static void sendEmail(Session session, String fromAddr,
      String toAddr, String subj, String body, boolean asHtml) {
    try {
      Message msg = new MimeMessage(session);
      msg.setFrom(new InternetAddress(fromAddr));
      msg.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddr));
      msg.setSubject(subj);

      if (asHtml) {
	Multipart mp = new MimeMultipart();
	MimeBodyPart htmlPart = new MimeBodyPart();
	htmlPart.setContent(body, "text/html");
	mp.addBodyPart(htmlPart);
	msg.setContent(mp);
      } else
	msg.setText(body);
      Transport.send(msg);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
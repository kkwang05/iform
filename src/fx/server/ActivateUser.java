package fx.server;

import java.util.*;
import java.util.logging.*;
import java.text.*;
import javax.jdo.*;

import org.antlr.stringtemplate.StringTemplate;

import fx.model.*;
import fx.util.FxCipher;

/**
 * Server side processing for user registration and activation.
 * 
 * @author kw
 */
public class ActivateUser {
  public static final String AppUrlRoot=System.getProperty("urlRoot");
  public static final String URL_PATTN_ACT_USR = "/_fxproc/activate?"; // Referenced in web.xml
  private static final ResourceBundle Res= ResourceBundle.getBundle("fx.server.Resources");
  private static final Logger MyLogger = Logger.getLogger(ActivateUser.class.getName());
  
  private static final long activationExpiryPeriod;
  static {
    // No need to do it here - the service servlet should have done it.
    //ServerAppContext.init();
    int days=Integer.parseInt(Res.getString("expiryDays"));
    activationExpiryPeriod=days * 24 * 3600 * 1000L;
    MyLogger.info("User Activation URL: "+AppUrlRoot+URL_PATTN_ACT_USR+", expiry(days)="+days);
  }
  
  public static void sendActivationEmail(User u) {
    String subj = Res.getString("activationEmailSubject");

    StringTemplate tmpl = FxUtil.getTemplate("email/ActivateAccount");
    tmpl.setAttribute("userName", u.firstName);
    tmpl.setAttribute("activationUrl", composeActivationUrl(u));
    tmpl.setAttribute("period", Res.getString("expiryDays"));
    String msg = tmpl.toString();
    
    String adminEmail=Res.getString("emailFrom");
    MyLogger.info("From: "+adminEmail+"\nTo: "+u.email+"\nSubj="+subj+"\nMsg=\n" + msg);
    FxUtil.sendAdminEmail(u.email, subj, msg);
  }

  private static String composeActivationUrl(User u) {
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    Date edate = new Date();
    edate.setTime(edate.getTime() + activationExpiryPeriod);
    String queryString = u.email + "|" + df.format(edate);
    return AppUrlRoot+URL_PATTN_ACT_USR + FxCipher.encrypt(queryString);
  }

  public static String activate(String ticket) {
    PersistenceManager pm = PMF.get().getPersistenceManager();
    User user = null;
    Transaction tx = pm.currentTransaction();
    try {
      String tokenStr = FxCipher.decrypt(ticket);
      String[] tokens = tokenStr.split("\\|");
      String email = tokens[0];
      String expiry = tokens[1];

      DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
      Date ed = df.parse(expiry);
      boolean expired = ed.before(new Date());
      MyLogger.fine("Received activation request on [email=" + email + ", " + ed+"] expired="+expired);
      
      if (expired)
        return Res.getString("voucherExpiredMessage");

      tx.begin();
      Query q = pm.newQuery(User.class, "email==:email && (status==:registeredStatus || status==:activeStatus)");
      q.setUnique(true);
      user= (User)q.execute(email, User.USER_ST_REGISTERED, User.USER_ST_ACTIVATED);
      MyLogger.fine("Found activation record: " + user);

      if(user==null)
        return Res.getString("userNotFoundMessage");
      if (user.status == User.USER_ST_ACTIVATED) 
        return Res.getString("userAlreadyActivatedMessage");

      // TBD ......mock
      user.setStatus(User.USER_ST_ACTIVATED);
      user.addDomainId(ServerAppContext.PublicDomainId);
      tx.commit();
      MyLogger.info("Successfully activated user: " + user);
      return null;
      
    } catch (Exception ex) {
      ex.printStackTrace();
      MyLogger.severe("Failed to register new user: "+ex.getMessage());
    } finally {
      if (tx.isActive())
        tx.rollback();
      pm.close();
    }
    return Res.getString("activateUserErrorMessage");
  }
}
package fx.server;

import java.io.*;
import java.util.logging.Logger;

import javax.jdo.PersistenceManager;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;

import fx.server.entity.*;
import fx.util.FxCipher;

import com.google.appengine.api.images.*;

public class ImageServlet extends HttpServlet {
  public static final long serialVersionUID=1L;

  public static final float MAX_IMAGE_SIZE=1000000; // 1M 
  public static final int MAX_IMAGE_WIDTH=800; 
  public static final int THUMB_NAIL_WIDTH=100; 

  private static final Logger MyLogger = Logger.getLogger(ImageServlet.class.getName());

  private static final String PUT="post";
  private static final String GET="get";
  private static class UriData {
    Long userId, domainId;
    String tags;
    
    Long blobBlockId;
    boolean preferThumbnail;
  }
  
  protected UriData extractParams(String uri, String method){
    MyLogger.info("GOT "+method+" REQ, uri="+uri);
    UriData params=new UriData();
    if(method==PUT){
      String tok=FxCipher.decrypt(uri.substring(uri.indexOf("image/")+"image/".length()));
      String[] fields = tok.split("\\|");
      params.userId=Long.parseLong(fields[0]);
      if(fields.length>1 && fields[1].length()>0)
        params.tags=fields[1];
    }
    else if(method==GET){
      String tok=uri.substring(uri.indexOf("image/")+"image/".length());
      if(tok.startsWith("T")){
        params.preferThumbnail=true;
        tok=tok.substring(1);
      }
      params.blobBlockId=Long.parseLong(tok);
    }
    return params;
  }
  
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    try {
      response.setContentType("text/plain");
      UriData params=extractParams(request.getRequestURI(), PUT);
      
      ServletFileUpload ul = new ServletFileUpload();
      FileItemIterator fiit = ul.getItemIterator(request);
      Long blobId=null;
      while (fiit.hasNext()) {
        FileItemStream item = fiit.next();
        String name = item.getFieldName();
        InputStream stream = item.openStream();
        if (item.isFormField()) {
          MyLogger.info("Form FIELD NAME=" + name + " VALUE="+ Streams.asString(stream));
        } else {
          MyLogger.info("Form FIELD NAME=" + name + " FILE=" + item.getName() + " CONTENTTYPE="+item.getContentType());
          if(params.tags==null){
            String fn=item.getName();
            int idx=fn.lastIndexOf('/');
            if(idx<0)
              idx=fn.lastIndexOf('\\');
            if(idx>0)
              fn=fn.substring(idx+1);
            params.tags=fn;
          }
          blobId=processFileStream(stream, item.getContentType(), params);
        }
      }
      response.getWriter().print("/image/T"+blobId);
    } catch (Exception ex){
      ex.printStackTrace();
      MyLogger.severe(ex.getMessage());
      response.getWriter().println("FAILED");
    }
  }
  
  protected Long processFileStream(InputStream stream, String mimeType, UriData params) throws IOException {
    long size=0;
    int len;
    byte[] buffer = new byte[8192];
    ByteArrayOutputStream baos=new ByteArrayOutputStream();
    while ((len = stream.read(buffer, 0, buffer.length)) != -1){
      baos.write(buffer, 0, len);
      size+=len;
    }
    
    ImagesService imagesService = ImagesServiceFactory.getImagesService();
    Image origImage = ImagesServiceFactory.makeImage(baos.toByteArray());
    int w=origImage.getWidth();
    int h=origImage.getHeight();
    
    if(size>MAX_IMAGE_SIZE){
      /*
      // Square root is more technically correct.
      float fw=w * MAX_IMAGE_SIZE / size;
      float fh=h * MAX_IMAGE_SIZE / size;
      int sw=(int)fw;
      int sh=(int)fh;
      */
      int sw=(w>h)? MAX_IMAGE_WIDTH : MAX_IMAGE_WIDTH*w/h;
      int sh=(w>h)? MAX_IMAGE_WIDTH*h/w : MAX_IMAGE_WIDTH;
      
      Transform scalingResize = ImagesServiceFactory.makeResize(sw, sh);
      origImage = imagesService.applyTransform(scalingResize, origImage, ImagesService.OutputEncoding.JPEG);
      MyLogger.fine("Original image dim="+w+"X"+h+", scaled down to "+sw+"X"+sh+", ("+size+"->"+origImage.getImageData().length+")");
    }
    byte[] imageData=origImage.getImageData();
    
    byte[] thumbnailData=null;
    if(w>THUMB_NAIL_WIDTH*2 || h>THUMB_NAIL_WIDTH*2){
      Transform thumbNailResize = ImagesServiceFactory.makeResize(THUMB_NAIL_WIDTH, (int)(THUMB_NAIL_WIDTH*h/w));
      Image thumbnail = imagesService.applyTransform(thumbNailResize, origImage, ImagesService.OutputEncoding.PNG);
      MyLogger.fine("Generated thumbnail ("+thumbnail.getImageData().length+" bytes)");
      thumbnailData=thumbnail.getImageData();
    }
    
    PersistenceManager pm = PMF.get().getPersistenceManager();
    FxBlob bb=new FxBlob(params.domainId, params.userId, imageData, thumbnailData);
    bb.size=imageData.length; 
    bb.mimeType=mimeType;
    bb.tags=params.tags;
    pm.makePersistent(bb);
    MyLogger.info("Created BLOB of size="+bb.size);
    return bb.id;
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    try {
      UriData params=extractParams(request.getRequestURI(), GET);
      
      PersistenceManager pm = PMF.get().getPersistenceManager();
      FxBlob bb=pm.getObjectById(FxBlob.class, params.blobBlockId);
      MyLogger.fine("Retrieved Blob = " + bb);
      
      byte[] ba=null;
      if(params.preferThumbnail)
        ba=bb.getThumbnailData(); 
      if(ba==null)
        ba=bb.getData();
      
      response.setContentType(bb.mimeType);
      response.setContentLength(ba.length);
      response.getOutputStream().write(ba, 0, ba.length);
      response.getOutputStream().close();
    }
    catch(Exception ex){
      ex.printStackTrace();
      MyLogger.severe(ex.getMessage());
    }
  }
}

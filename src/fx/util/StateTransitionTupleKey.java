package fx.util;

import java.util.*;

/**
 * A composite key to aid multi-event state transitions.
 * @author KW
 */
public class StateTransitionTupleKey {
  private Object state;
  private HashSet<Object> events;
  public StateTransitionTupleKey(Object state, Object... evts){
    this.state=state;
    events=new HashSet<Object>();
    for(Object e:evts)
      events.add(e);
  }
  
  public int hashCode(){
    return state.hashCode();
  }
  
  public boolean equals(Object rhs){
   if (!(rhs instanceof StateTransitionTupleKey))
     return false;
   
   StateTransitionTupleKey rhs2=(StateTransitionTupleKey)rhs;
   if(!state.equals(rhs2.state))
     return false;

   for(Object o:rhs2.events)
     if(events.contains(o))
       return true;
   
   return false;
  }
}

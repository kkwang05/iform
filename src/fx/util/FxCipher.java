package fx.util;

import com.googlecode.gwt.crypto.bouncycastle.engines.DESedeEngine;
import com.googlecode.gwt.crypto.bouncycastle.modes.CBCBlockCipher;
import com.googlecode.gwt.crypto.bouncycastle.paddings.PaddedBufferedBlockCipher;
import com.googlecode.gwt.crypto.bouncycastle.params.KeyParameter;
import com.googlecode.gwt.crypto.bouncycastle.util.encoders.Hex;
import com.googlecode.gwt.crypto.client.*;
import com.googlecode.gwt.crypto.util.Str;

/**
 * 3Des-based string encryption/decryption. 
 *
 * @author kw
 */
public class FxCipher {
  public static final String BINARY_ENCODING_HEX="HEX";
  public static final String BINARY_ENCODING_BASE64="BASE64";
  public static final String BINARY_ENCODING_DEFAULT=BINARY_ENCODING_BASE64;
  
  private static byte[] TDES_KEY= new TripleDesKeyGenerator().decodeKey("074a4597fb915da48923492fecba34f2eab35d1a757913f4");
  private static PaddedBufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CBCBlockCipher(new DESedeEngine()));

  public static String encrypt(String in) {
    return encrypt(in, BINARY_ENCODING_DEFAULT);
  }
  public static String decrypt(String in) {
    return decrypt(in, BINARY_ENCODING_DEFAULT);
  }

  public static String encrypt(String in, String opt) {
    cipher.reset();
    cipher.init(true, new KeyParameter(TDES_KEY));

    byte[] input = Str.toBytes(in.toCharArray());
    byte[] output = new byte[cipher.getOutputSize(input.length)];
    int length = cipher.processBytes(input, 0, input.length, output, 0);
    try {
      cipher.doFinal(output, length);
    } catch (Exception e1) {
      System.err.println("Encrypt/Decrypt failed! " + e1.getMessage());
    }
    if(opt.equalsIgnoreCase(BINARY_ENCODING_HEX)){
      byte[] result = Hex.encode(output, 0, output.length);
      return new String(Str.toChars(result));
    }
    else
      return Base64Coder.encode(output);
  }
  
  public static String decrypt(String in, String opt) {
    cipher.reset();
    cipher.init(false, new KeyParameter(TDES_KEY));

    byte[] input=null;
    if(opt.equalsIgnoreCase(BINARY_ENCODING_HEX))
      input = Hex.decode(Str.toBytes(in.toCharArray()));
    else
      input = Base64Coder.decode(in);
    byte[] output = new byte[cipher.getOutputSize(input.length)];
    int length = cipher.processBytes(input, 0, input.length, output, 0);
    int remaining=0;
    try {
      remaining = cipher.doFinal(output, length);
    } catch (Exception e1) {
      System.err.println("Encrypt/Decrypt failed! " + e1.getMessage());
    }
    return new String(Str.toChars(output), 0, length + remaining);
  }

  // Tester
  public static void main(String[] args) {
    if (args.length == 0) {
      byte[] k = new TripleDesKeyGenerator().generateNewKey();
      String ek = new TripleDesKeyGenerator().encodeKey(k);
      System.out.println("Generated: " + ek);
      return;
    }
    String in = args[0];
    String en = encrypt(in);
    String de = decrypt(en);
    System.out.println(in + "\n" + en + "\n" + de);
  }
}
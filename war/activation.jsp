<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="fx.server.ActivateUser" %>
<%@ page import="fx.model.User" %>

<html>
  <body>

<p>
	Process activation request......
</p>

<%
	String u=request.getQueryString();
	String errorMsg=ActivateUser.activate(u);
	if(errorMsg==null) {
%>

<p>
	Congratulations! Your iForm account has been activated.
</p>
<p>
	<a href="/">Proceed to the iForm front page</a>
</p>

<%
 } else { 
%>

<p>
	Sorry, we were unable to process your request. <%=errorMsg%>
</p>
<% } %>

  </body>
</html>
